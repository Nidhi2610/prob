<?php
/**
 * Created by Mobio Solutions.
 * User: Nikhil Jain
 * Date: 19-01-2017
 * Time: 13:59
 */

return [
    'FROM_EMAIL' => 'contact@mobiosolutions.com',
    'FROM_NAME' => 'Probtp',
    'user_types' => [
        1 => "Super Administrator",
        2 => "Administrator",
        3 => "Responsable RH",
        4 => "Collaborateur RH",
        5 => "Formateur"
    ],
    'category_type' => [

        1 => "Privé",
        2 => "Public"

    ],

    'resource_type' => [

    1 => "Privé",
    2 => "Public"

]

];

