<?php

namespace App\Helpers;


use App\Models\Comments;
use App\Models\ModuleCategory;
use Illuminate\Support\Facades\Auth;
use Config;
use File;
use Storage;
use CloudConvert;
use DB;
use App\Models\User;
use App\Models\Entreprise;
use App\Models\Category;
use Image;
use FFMpeg;
use App\Models\LikeCategory;

class LaraHelpers
{

    /*
     *  get user type
     * check the authenctication and generate list
     * */
    public static function get_user_types()
    {

        $userTypes = [];
        $types = [];
        if (Auth::user()->user_type == 1) {
//            $start = Auth::user()->user_type+1;
//            $end = 3;
            $types = [
                2 => "Administrator",
                5 => "Formateur",
                3 => "Responsable RH",
                4 => "Collaborateur",
            ];
        } elseif (Auth::user()->user_type == 2) {
//            $start = Auth::user()->user_type + 1;
//            $end = 5;
            $types = [
                5 => "Formateur",
                3 => "Responsable RH",
            ];
        } elseif (Auth::user()->user_type == 3) {
            $types = [
                4 => "Collaborateur RH",
            ];

        } elseif (Auth::user()->user_type == 4) {
            $types = [];

        }


        $j = 0;
        if(sizeof($types)>0){
            foreach ($types as $type_key => $type_val) {
                $userTypes[$j]['user_type_id'] = $type_key;
                $userTypes[$j]['user_type'] = $type_val;
                $j++;
            }
        }


        return $userTypes;
    }

    /*
     * get category type
     * Public or Private
     * */

    public static function get_category_type()
    {
        $categoryType = [];
        $j = 0;
        for ($i = 1; $i <= 2; $i++) {
            $categoryType[$j]['category_type_id'] = $i;
            $categoryType[$j]['category_type'] = Config::get('constant.category_type')[$i];
            $j++;

        }
        return $categoryType;
    }

    /*
     * get category type
     * Public or Private
     * */

    public static function get_resource_type()
    {
        $resourceType = [];
        $j = 0;
        for ($i = 1; $i <= 2; $i++) {
            $resourceType[$j]['resource_type_id'] = $i;
            $resourceType[$j]['resource_type'] = Config::get('constant.resource_type')[$i];
            $j++;

        }
        return $resourceType;
    }

    public static function upload_image($filepath, $image_name, $unlink_image)
    {

        if (!is_dir($filepath)) {
            if (env('FILE_STORAGE') == 'Storage') {
                Storage::makeDirectory($filepath, 777);
            } else {
                File::makeDirectory($filepath, 0777, true);

            }
        }

        if ($image_name != "") {

            $file = $image_name;
            $filename = time() . '_' . $file->getClientOriginalName();
            //$filename = str_replace(' ', '_', $filename);
            // For removing the specical characters
//dd($filename);
            $filename = substr($filename, 0, strpos($filename, '.'));
            $filename = str_slug($filename, '_');
            $filename = $filename . '.' . $file->getClientOriginalExtension();


            $size = $file->getClientSize();

            $extension = "";
            $extension = '.' . $file->getClientOriginalExtension();
            $publicPath = $filepath;
            //dd( $publicPath);
            if ($file->getClientOriginalExtension() == "wmv") { // echo "<br/>";    print_r($filename);
                $videotmp = time();
                $video = $file;
                $input['video'] = $videotmp . '.' . $video->getClientOriginalExtension();
                $destinationPath = $filepath;
                $video->move($destinationPath, $input['video']);

                if ($video->getClientOriginalExtension() != "mp4") {
                    CloudConvert::file($destinationPath . '/' . $input['video'])->to('mp4');
                    File::delete($destinationPath . '/' . $input['video']);

                    $input['video'] = $videotmp . '.mp4';
                    // For generating the Thumbnail for the video

                    FFMpeg::fromDisk($destinationPath)
                        ->open($input['video'])
                        ->getFrameFromSeconds(10)
                        ->export()
                        ->toDisk($destinationPath)
                        ->save($input['video'] . '_thumbnail.jpeg');

                    return $input['video'] = $videotmp . '.mp4';
                }

            }


            $file->move($publicPath, $filename);

            if ($file->getClientOriginalExtension() == "jpg" || $file->getClientOriginalExtension() == "jpeg" || $file->getClientOriginalExtension() == "png") {  // dd($filepath);

                if (strpos($filepath,'module') !== false) {
                    $img = Image::make(url('storage/app/public/files/module') . "/" . $filename);
                } else {
                    $img = Image::make(url('storage/app/public/files/enterprise') . "/" . $filename);
                }
                //$img = Image::make(url('storage/app/public/files/module')."/".$filename);
                //$img = Image::make(url($publicPath)."/".$filename);


                $width = $img->width();
                if ($img->width() > 600) {
                    $img->resize(null, Config::get('app.ratio'), function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($filepath . $filename);
                }

            }

            if (isset($unlink_image) && $unlink_image != "") {
                if (file_exists($filepath . $unlink_image)) {
                    unlink($filepath . $unlink_image);
                }

            }
            return $filename;
        }
        return $unlink_image;

    }

    public static function buildTree(Array $data, $parent = 0)
    {
        $tree = array();
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $children = self::buildTree($data, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['_children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }

    // For genrating the select option of category

    public static function printTree($tree, $r = 0, $p = null, $parent_id)
    {
        foreach ($tree as $i => $t) {

            $dash = ($t['parent_id'] == 0) ? '' : str_repeat('-', $r) . ' ';
            $select = '';
            if ($parent_id == $t['id']) {
                $select = 'selected';
            }
            printf("\t<option value='%d' " . $select . " >%s%s</option>\n", $t['id'], $dash, $t['name']);
            if ($t['parent_id'] == $p) {
                // reset $r
                $r = 0;
            }
            if (isset($t['_children'])) {
                self::printTree($t['_children'], ++$r, $t['parent_id'], $parent_id);
            }
        }
    }

    /* For getting user assigned to Entreprise
     *
     *  @param int $enterprise_id
     *  @param int $user_type
     * */
    public static function getUserEnterpriseData($entreprise_id, $user_type)
    {

        $Sekey = 'entreprise_id';

        $userData = User::select('name', 'id')
            ->whereRaw('FIND_IN_SET(' . $entreprise_id . ',' . $Sekey . ')')
            ->where('user_type', $user_type)
            ->get();
        $users = [];
        foreach ($userData as $key => $user) {
            $users[] = $user->name;
        }
       // (implode(",", $users));
        return implode("</li><li>", $users);
    }

    /* For getting colloborterur user in responsable assigned to Entreprise
     *
     *  @param int $enterprise_id
     *  @param int $user_type
     * */
    public static function getCollobaroteurEnterpriseData($entreprise_id, $user_type)
    {

        $Sekey = 'entreprise_id';

        $userData = User::select('name', 'id')
            ->whereRaw('FIND_IN_SET(' . $entreprise_id . ',' . $Sekey . ')')
            ->where('user_type', $user_type)
            ->where('deleted_at',null)
            ->get();
        $users_collection = [];
        foreach ($userData as $key => $user) {
            $users_collection[$key]['name'] = $user->name;
            $child_user = User::select('name', 'id')->where('parent_id', $user->id)->where('deleted_at',null)->get();
            //dd($child_user);
            $sub_user_data = [];
            foreach ($child_user as $sub_key => $sub_user) {
                $sub_user_data[$sub_key] = $sub_user;

            }
            $users_collection[$key]['child_array'] = $sub_user_data;
        }

        return $users_collection;
    }

    /* For getting colloborterur user in responsable assigned to Entreprise
     *
     *  @param int $enterprise_id
     *  @param int $user_type
     * */
    public static function getchildColloborateru($user_id)
    {
        return User::select('name', 'id', 'email')->where('parent_id', $user_id)->get();
    }

    // For uploading User Images

    public static function upload_user_image($filepath, $image_name, $unlink_image)
    {

        if (!is_dir($filepath)) {
            if (env('FILE_STORAGE') == 'Storage') {
                Storage::makeDirectory($filepath, 777);
            } else {

                File::makeDirectory($filepath, 0777, true);

            }
        }

        if ($image_name != "") {
            $file = $image_name;
            $filename = time() . '_' . $file->getClientOriginalName();
            //$filename = str_replace(' ', '_', $filename);
            // For removing the specical characters

            $filename = substr($filename, 0, strpos($filename, '.'));
            $filename = str_slug($filename, '_');
            $filename = $filename . '.' . $file->getClientOriginalExtension();


            $size = $file->getClientSize();

            $extension = "";
            $extension = '.' . $file->getClientOriginalExtension();
            $publicPath = $filepath;


            $file->move($publicPath, $filename);
            if ($file->getClientOriginalExtension() == "jpg" || $file->getClientOriginalExtension() == "jpeg" || $file->getClientOriginalExtension() == "png") {

                $img = Image::make(url('storage/app/public/files/module') . "/" . $filename);
                $width = $img->width();
                if ($img->width() > 600) {
                    $img->resize(null, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($filepath . $filename);
                }

            }

            if (isset($unlink_image) && $unlink_image != "") {
                if (file_exists($filepath . $unlink_image)) {
                    unlink($filepath . $unlink_image);
                }

            }
            return $filename;
        }
        return $unlink_image;

    }

    // Get Parent from id
    public static function getParentData($field, $id)
    {
        $category = new Category();
        return $category->getParent($field, $id)->toArray();
    }

    /**
     *
     * @param Array $list
     * @param int $p
     * @return multitype:multitype:
     */
    public static function partition($list, $p)
    {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }

    /*
     *
     * @param int $id
     *
     * @return html responce
     * */
    public static function headingTitle($category_id)
    {
        $module = new ModuleCategory();
        $mdata = $module->getModuleCategoryId($category_id);
        $mdata = $mdata->toArray();
        $category = new Category();
        $parent_data = $category->getCategoryByID($category_id);
        $str = "";
        if ($parent_data->parent_id == 0) {
            echo '<div class="panel-heading" id="community">
                        <div class="title-heading">';
            echo '<h1  id="category_' . $category_id . '">' . $parent_data->name . '</h1>';
            echo '</div></div>';
        } else {
            $get_child_data = $category->getCategoryByID($parent_data->parent_id);
            if (sizeof($get_child_data) > 0) {
                if ($get_child_data->parent_id == 0) {
                    $str .= '<h2 class="part-title category-title" style="cursor:auto;background-color:' . $parent_data->color . ';" id="category_' . $category_id . '">' . $parent_data->name;
                    if (isset($mdata[0])) {
                        $str .= '<i class="fa fa-chevron-down down-arrow" style="cursor:pointer;" aria-hidden="true"></i>';
                    }
                    $str .= '</h2>';
                    echo $str;
                } else {
                    $str .= '<h3 style="margin-left:15px;border-left: 6px solid #00a3b4;background-color:' . $get_child_data->color . ';"  class="part-title category-title category-title-color" id="category_' . $category_id . '">' . $parent_data->name;
                    if (isset($mdata[0])) {
                        $str .= '<i class="fa fa-chevron-down down-arrow" aria-hidden="true"></i>';
                    }
                    $str .= '</h3>';
                    echo $str;
                }
            }

        }
    }


    /*
     * For uploading Entrepise Images
     * */
    public static function upload_entreprise_image($filepath, $image_name, $unlink_image)
    {

        if (!is_dir($filepath)) {
            if (env('FILE_STORAGE') == 'Storage') {
                Storage::makeDirectory($filepath, 777);
            } else {

                File::makeDirectory($filepath, 0777, true);

            }
        }

        if ($image_name != "") {
            $file = $image_name;
            $filename = time() . '_' . $file->getClientOriginalName();
            //$filename = str_replace(' ', '_', $filename);
            // For removing the specical characters

            $filename = substr($filename, 0, strpos($filename, '.'));
            $filename = str_slug($filename, '_');
            $filename = $filename . '.' . $file->getClientOriginalExtension();


            $size = $file->getClientSize();

            $extension = "";
            $extension = '.' . $file->getClientOriginalExtension();
            $publicPath = $filepath;


            $file->move($publicPath, $filename);
            if ($file->getClientOriginalExtension() == "jpg" || $file->getClientOriginalExtension() == "jpeg" || $file->getClientOriginalExtension() == "png") {

                $img = Image::make(url('storage/app/public/files/enterprise') . "/" . $filename);
                $width = $img->width();
                if ($img->width() > 600) {
                    $img->resize(null, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($filepath . $filename);
                }

            }

            if (isset($unlink_image) && $unlink_image != "") {
                if (file_exists($filepath . $unlink_image)) {
                    unlink($filepath . $unlink_image);
                }

            }
            return $filename;
        }
        return $unlink_image;

    }

    /**
     * Get Comment Cateogry by category Id
     *
     * @return $category_name
     * */

    public static function getCommentCategory($category_id)
    {
        $category_id = explode(":", $category_id);
        if ($category_id) {
            $id = $category_id[0];
        } else {
            $id = "";
        }

        $category = new Category();
        return $category->getCategoryByID($id);
    }

    /**
     * Get Category Like Status
     *
     * @return $status
     * */
    public static function likeStatus($category_id)
    {
        $likeCategory = new LikeCategory();
        return $likeCategory->getLikeStatus($category_id);
    }

    /**
     * Get Category Like Status
     *
     * @return $status
     * */
    public static function likeCount($category_id)
    {
        $likeCategory = new LikeCategory();
        return $likeCategory->getCount($category_id);
    }


    /* For getting all child  user in responsable assigned to Entreprise
     *
     *  @param int $enterprise_id
     *  @param int $user_type
     * */
    public static function getchildEnterprise($enterprise_id)
    {
        $enterprise = new Entreprise();
        $data = $enterprise->select('name', 'id')->where('parent_id', $enterprise_id)->get();
        return $data;
        //return Entreprise::select('name', 'id')->where('parent_id', $enterprise_id)->get();
    }

    /* For generating the link from sidebar most accessed module to detail page
     *
     *  @param object $module
     *  @param string variable $href
     * */
    public static function setHrefforLink($module)
    {
        $category = new Category();
        $category_data = $category->getCategoryByID($module['module_id']);
        $parcat_id = Category::select('parent_id')->where('id', $category_data['id'])->get();
        $url1 = '';
        $sub_url1 = '';
        //rint_r($parcat_id);
        foreach ($parcat_id as $p_id) {
            if ($p_id['parent_id']) {
                $main_category_id = Category::select('parent_id')->where('id', $p_id['parent_id'])->where('deleted_at', NULL)->get();
                if(sizeof($main_category_id) > 0){
                    $main_id = $main_category_id->toArray();

                    $url1 = $main_id[0]['parent_id'];
                    if($url1){
                        $parent_id = Category::select('id')->where('parent_id', $p_id['parent_id'])->where('deleted_at', NULL)->get();
                        if (sizeof($parent_id) > 0) {
                            $par_arr = $parent_id->toArray();
                            $sub_url1 = '';

                            foreach ($par_arr as $mid) {
                                $sub_url1 .= ':' . $mid['id'];
                            }
                        }
                        $href = 'details/' . $url1 . '/' .$url1 .':'.$p_id['parent_id']. $sub_url1 . '#category_' .$url1;

                    }else{
                        $parent_id = Category::select('id')->where('parent_id', $p_id['parent_id'])->where('deleted_at', NULL)->get();
                        if (sizeof($parent_id) > 0) {
                            $par_arr = $parent_id->toArray();
                            $sub_url1 = '';

                            foreach ($par_arr as $mid) {
                                $sub_url1 .= ':' . $mid['id'];
                            }
                        }
                        $href = 'details/' . $p_id['parent_id'] . '/' .$p_id['parent_id'] . $sub_url1 . '#category_' .$p_id['parent_id'];
                    }

                }else{
                    $parent_id = Category::select('id')->where('parent_id', $p_id['parent_id'])->where('deleted_at', NULL)->get();
                    if (sizeof($parent_id) > 0) {
                        $par_arr = $parent_id->toArray();
                        $sub_url1 = '';

                        foreach ($par_arr as $mid) {
                            $sub_url1 .= ':' . $mid['id'];
                        }
                    }
                    $href = 'details/' . $p_id['parent_id'] . '/' . $p_id['parent_id'] . $url1 . $sub_url1 . '#category_' . $p_id['parent_id'];

                }

                if($category_data['name']){

                    echo ' <li>
                        <div class="orange-cir">
                                    <span> <i class="fa fa-circle" aria-hidden="true"></i>
                                     <a class="clickCount" id="'.$module['module_id'].'" href="'. url($href).'">';
                    echo $category_data['name'];
                    echo '                   </a> 
                        </div>
                  </li>';
                }

            } else {

                $mod_id = Category::select('id')->where('parent_id', $module['module_id'])->get();
                if (sizeof($mod_id)) {
                    foreach ($mod_id as $mid) {
                        $url1 .= ':' . $mid['id'];
                        $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();

                        foreach ($sub_id as $s) {
                            $sub_url1 .= ':' . $s['id'];
                        }

                    }
                }
                $href = 'details/' . $module['module_id'] . '/' . $module['module_id'] . $url1 . $sub_url1 . '#category_' . $module['module_id'];
                if($category_data['name']){

                    echo ' <li>
                        <div class="orange-cir">
                                    <span> <i class="fa fa-circle" aria-hidden="true"></i>
                                     <a class="clickCount" id="'.$module['module_id'].'" href="'. url($href).'">';
                    echo $category_data['name'];
                    echo '                   </a> 
                        </div>
                  </li>';
                }
            }

        }
    }

    /* For generating the link from sidebar most accessed module to detail page
    *
    *  @param object $module
    *  @param string variable $href
    * */
    public static function setHrefforLinkRightBar($module)
    {
        $module_category = new ModuleCategory();
        $category = new Category();

        $category_name = $module_category->getCategoryByModuleId($module->module_id);
        if(sizeof($category_name) > 0) {
            $cat_id = $category_name[0]['category_id'];
            $cat_data = $category->getActiveCategoryByID($cat_id);
            $url = '';
            $sub_url = '';

            if ($cat_data['parent_id']) {
                $par_cat_id = Category::select('parent_id')->where('id', $cat_id)->get()->toArray();

                if (sizeof($par_cat_id)) {
                    $main_category = $par_cat_id[0]['parent_id'];
                    if ($par_cat_id[0]['parent_id'] != 0) {
                        $parent_id = \App\Models\Category::select('parent_id')->where('id', $par_cat_id[0]['parent_id'])->get()->toArray();
                        if (count($parent_id) > 0 && $parent_id[0]['parent_id'] != 0) {
                            $main_category = $parent_id[0]['parent_id'];
                            $mod_id = \App\Models\Category::select('id')->where('parent_id', $parent_id)->get()->toArray();

                            foreach ($mod_id as $mid) {
                                $url .= ':' . $mid['id'];
                                $sub_id = \App\Models\Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                                foreach ($sub_id as $s) {
                                    $sub_url .= ':' . $s['id'];
                                }
                            }

                        } else {
                            $mod_id = \App\Models\Category::select('id')->where('parent_id', $main_category)->where('category_type_id', 2)->get()->toArray();
                            foreach ($mod_id as $mid) {
                                $url .= ':' . $mid['id'];
                                $sub_id = \App\Models\Category::select('id')->where('parent_id', $mid['id'])->where('category_type_id', 2)->get()->toArray();
                                foreach ($sub_id as $s) {
                                    $sub_url .= ':' . $s['id'];
                                }
                            }
                        }
                    }
                } else {
                    $main_category = $cat_id;
                    $module_id = Category::select('id')->where('parent_id', $main_category)->get()->toArray();

                    if (count($module_id)) {
                        foreach ($module_id as $mid) {
                            $url .= ':' . $mid['id'];
                            $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                            foreach ($sub_id as $s) {
                                $sub_url .= ':' . $s['id'];
                            }
                        }
                    }
                }


            }else{
                $main_category = $cat_id;
                $module_id = Category::select('id')->where('parent_id', $cat_id)->get()->toArray();
                if (count($module_id)) {
                    foreach ($module_id as $mid) {
                        $url .= ':' . $mid['id'];
                        $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                        foreach ($sub_id as $s) {
                            $sub_url .= ':' . $s['id'];
                        }
                    }
                }
            }
            if (sizeof($category_name)) {
                echo '<li>
                            <div class="orange-cir">
                            <span> <i class="fa fa-newspaper-o iconsize" aria-hidden="true" ></i>
                                <a class="clickCount scroll-module" data-id="' . $cat_id . '" id="' . $module->module_id . '" href="javascript:void(0)" data-link="' . url('details/' . $main_category . '/' . $main_category . $url . $sub_url . '#subcategory_' . $module->module_id) . '">
                                        ' . $module->title . '
                                </a>
                            </span>
                            </div>
                        </li>';
            }
        }

    }
    /* For generating the link from sidebar most accessed module to detail page for admin preview
    *
    *  @param object $module
    *  @param string variable $href
    * */
    public static function setHrefforLinkAdmin($module )
    {
        $module_category = new ModuleCategory();
        $category = new Category();

        $category_name = $module_category->getCategoryByModuleId($module->module_id);

        if(sizeof($category_name) > 0) {
            $cat_id = $category_name[0]['category_id'];
            $cat_data = $category->getActiveCategoryByID($cat_id);
            $url = '';
            $sub_url = '';

            if ($cat_data['parent_id']) {
                $par_cat_id = Category::select('parent_id')->where('id', $cat_id)->get()->toArray();

                if (sizeof($par_cat_id)) {
                    $main_category = $par_cat_id[0]['parent_id'];
                    if ($par_cat_id[0]['parent_id'] != 0) {
                        $parent_id = Category::select('parent_id')->where('id', $par_cat_id[0]['parent_id'])->get()->toArray();
                        if (count($parent_id) > 0 && $parent_id[0]['parent_id'] != 0) {
                            $main_category = $parent_id[0]['parent_id'];
                            $mod_id = Category::select('id')->where('parent_id', $parent_id)->get()->toArray();

                            foreach ($mod_id as $mid) {
                                $url .= ':' . $mid['id'];
                                $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                                foreach ($sub_id as $s) {
                                    $sub_url .= ':' . $s['id'];
                                }
                            }

                        } else {
                            $mod_id = Category::select('id')->where('parent_id', $main_category)->where('category_type_id', 2)->get()->toArray();
                            foreach ($mod_id as $mid) {
                                $url .= ':' . $mid['id'];
                                $sub_id = Category::select('id')->where('parent_id', $mid['id'])->where('category_type_id', 2)->get()->toArray();
                                foreach ($sub_id as $s) {
                                    $sub_url .= ':' . $s['id'];
                                }
                            }
                        }
                    }
                } else {
                    $main_category = $cat_id;
                    $module_id = Category::select('id')->where('parent_id', $main_category)->get()->toArray();

                    if (count($module_id)) {
                        foreach ($module_id as $mid) {
                            $url .= ':' . $mid['id'];
                            $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                            foreach ($sub_id as $s) {
                                $sub_url .= ':' . $s['id'];
                            }
                        }
                    }
                }


            }else{
                $main_category = $cat_id;
                $module_id = Category::select('id')->where('parent_id', $main_category)->get()->toArray();

                if (count($module_id)) {
                    foreach ($module_id as $mid) {
                        $url .= ':' . $mid['id'];
                        $sub_id = Category::select('id')->where('parent_id', $mid['id'])->get()->toArray();
                        foreach ($sub_id as $s) {
                            $sub_url .= ':' . $s['id'];
                        }
                    }
                }
            }

            if (sizeof($category_name)) {
                echo '<li>
                            <div class="orange-cir">
                            <span> <i class="fa fa-newspaper-o iconsize" aria-hidden="true" ></i>
                                <a class="clickCount scroll-module" data-id="' . $cat_id . '" id="' . $module->module_id . '" href="javascript:void(0)" data-link="' . url('category/detail_page/' . $main_category . '/' . $main_category . $url . $sub_url . '#subcategory_' . $module->module_id) . '">
                                        ' . $module->title . '
                                </a>
                            </span>
                            </div>
                        </li>';
            }
        }

    }



}
