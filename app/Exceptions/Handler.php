<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use  \Symfony\Component\HttpKernel\Exception\ErrorException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

            return redirect()->back()->with('fail', __('probtp.token_mismatch_error'));

        }elseif($exception instanceof NotFoundHttpException)
        {
            return redirect()->route('404');
        }elseif($exception instanceof MethodNotAllowedHttpException)
        {
            return redirect()->route('404');
        }else{
            return redirect()->route('welcome');
        }
//        return redirect()->route('Index');
//        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
//            return redirect()->back()->with('fail', __('probtp.token_mismatch_error'));
//        }elseif($exception instanceof NotFoundHttpException)
//        {
//            return response()->view('welcome');
//        }
//        return parent::render($request, $exception);


    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->guest(route('welcome'));
            //return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('welcome'));
    }
}
