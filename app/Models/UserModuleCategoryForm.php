<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class UserModuleCategoryForm extends Model
{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'tbl_user_module_category_form';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','module_category_id','form_id','form_name','form_value','user_id'
    ];
	
	/*
    * get record of user form data
    *
	* @param object|mixed $module
    * @return  Object|mixed
    */
	public function getUserForm($module){
		
		return UserModuleCategoryForm::where('module_category_id',$module->id)
                ->where('form_id',$module->Module->form_id)
                ->where('user_id',Auth::id())
                ->get();
	}

	/*
	 *  add user form
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function addUserForm(array $data = [])
    {
        foreach ($data['form_data'] as $row){

            $userFormData = new UserModuleCategoryForm;
            $userFormData->module_category_id = $data['module_category_id'];
            $userFormData->form_id = $data['form_id'];
            $userFormData->form_name = $row['name'];
            if(is_array($row['value'])){
                $userFormData->form_value = implode(",",$row['value']);
            }else{
                $userFormData->form_value = $row['value'];
            }

            $userFormData->user_id = Auth::id();
            $userFormData->save();
        }

        return true;
    }
}
