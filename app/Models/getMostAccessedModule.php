<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Log;
use DB;
class getMostAccessedModule extends Model
{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'get_most_accessed_module';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','module_id','counter'
    ];

    public function Module()
    {
        return $this->belongsTo('App\Models\Module','module_id','id');
    }

	/*
    * get boolean value form
    *
	* @param array $data
    * @return  status= "string" if data is not already exist
	 *          else return 0.
    */
    public function insert($data)
    {
        $counter = getMostAccessedModule::where('module_id',$data['id'])->first();
        //echo $counter['counter'];die;
        if(!empty($counter)){
            Log::info($counter->module_id);
            $mid = $counter->module_id;
            $newCounter = $counter['counter']+1;
            $moduledata = ModuleCategory::select('module_id')->where('category_id',$data['id'])->get();
            Log::info($moduledata);
            $data = getMostAccessedModule::where('module_id',$data['id'])->update(['counter' => $newCounter ]);
            return $newCounter;
        }else{
            $data = getMostAccessedModule::create([
                'module_id' => $data['id'],
                'counter'   => 1
            ]);
            return $data;
        }


    }

    /*
     *  add user form
     *
     * @param array $data
     * @return mixed
     */

    public  function checkStatus($Id)
    {
        $return_data =  UserModuleComplete::where('module_id',$Id)->where('user_id',Auth::user()->id)->first();
        return $return_data;

    }

    /**
     * Get most viewed Module
     *
     * @param array $categoryArray
     *
     * @return  Object
     */
    public static function getMostViewedModule($categoryArray = array()){
        if(Auth::user()->user_type == 1){
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title,category.id as category_id FROM get_most_accessed_module module_access JOIN tbl_module ON tbl_module.id = module_access.module_id JOIN tbl_module_category module_category ON module_category.module_id = tbl_module.id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL GROUP BY module_id ORDER BY module_access.counter DESC limit 3');
        }
        if(count($categoryArray) > 0 ) {
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title,category.id as category_id FROM get_most_accessed_module module_access JOIN tbl_module ON tbl_module.id = module_access.module_id JOIN tbl_module_category module_category ON module_category.module_id = tbl_module.id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL and ( category.category_type_id = 2 or category.id IN (' . implode(',', $categoryArray) . '))  GROUP BY module_id ORDER BY module_access.counter DESC limit 3');
        }else{
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title,category.id as category_id FROM get_most_accessed_module module_access JOIN tbl_module ON tbl_module.id = module_access.module_id JOIN tbl_module_category module_category ON module_category.module_id = tbl_module.id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL and ( category.category_type_id = 2) GROUP BY module_id ORDER BY module_access.counter DESC limit 3');
        }
    }

}
