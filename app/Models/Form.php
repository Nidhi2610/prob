<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Form extends Model
{

    /*
  *  For Soft Delete
  */

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_form_manager';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'form_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'identifier', 'content','status','created_by','updated_by'
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Load all form data
     *
     * @return  Object
     */
    public function init(){
        return Form::get();
    }

    /**
     * Get Form name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getFormName($id){
        $get = Form::where('form_id',$id)->first(['title']);
        if($get){
            return $get->title;
        }
        return '-';
    }

    /**
     * Get Form data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getFormByID($id){
        return Form::where('form_id',$id)->first();
    }

    /**
     * Get Form data base on ID for admin Detail
     *
     * @param integer $id
     * @return  Object
     */
    public function getAdminFormByID($id){
        return Form::where('form_id',$id)->first();
    }


    /**
     * Insert Form data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data){

        return Form::create([
            'title' => $data['title'],
            'identifier' => $data['identifier'],
            'content' => $data['content'],
            'status' => $data['status'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id
        ]);
    }

    /**
     * Update Form data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data){
        return Form::where('form_id',$data['id'])
            ->update([
                'title' => $data['title'],
                'identifier' => $data['identifier'],
                'content' => $data['content'],
                'status' => $data['status'],
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            ]);
    }

    /**
     * Delete Form data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){

        $formdata = Module::where('form_id',$id)->count();
        if($formdata > 0){

            return false;
        }
        else{

            return Form::where('form_id',$id)->delete();
        }

    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata(){

        return $data = Form::onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restoreform($id){

        return $data=Form::withTrashed()
            ->where('form_id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @return  Object
     */

    public function removetrash($id){

        $data=Form::withTrashed()->where('form_id', $id);
        return $data->forceDelete();
    }

    /**
     * Duplcate data
     *
     *
     * @param int $id
     * @return  Object
     */

    public function duplicate($id){

        $data = Form::find($id);
        $duplicatedata['title'] = $data['title']."-Copy";
        $newData = $data->replicate();
        $newData->title =  $duplicatedata['title'];
        $newData->save();
        return $insertedId = $newData->form_id;


    }

}
