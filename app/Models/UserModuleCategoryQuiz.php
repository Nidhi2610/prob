<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class UserModuleCategoryQuiz extends Model
{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'tbl_user_module_category_quiz';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','module_category_id','quiz_id','quiz_que','quiz_ans','true_ans','user_ans','user_point','user_id'
    ];
	
	/*
    * get record of user quiz data
    *
	* @param object|mixed $module
    * @return  Object|mixed
    */
	public function getUserQuiz($module){
		
		return UserModuleCategoryQuiz::where('module_category_id',$module->id)
                ->where('quiz_id',$module->Module->quiz_id)
                ->where('user_id',Auth::id())
                ->get();
	}

	/*
	 *  add user quiz
	 *
	 * @param array $data
	 * @return mixed
	 */
	public function addUserQuiz(array $data = [])
    {
        foreach ($data['quiz_data'] as $row){

            $userQuizData = new UserModuleCategoryQuiz;
            $userQuizData->module_category_id = $data['module_category_id'];
            $userQuizData->quiz_id = $data['quiz_id'];
            $userQuizData->quiz_que = $row['quiz_que'];
            $userQuizData->quiz_ans = $row['quiz_ans'];
            $userQuizData->true_ans = $row['true_ans'];
            $userQuizData->user_ans = implode(",",$row['user_ans']);
            if(implode(",",$row['user_ans']) == $row['true_ans']){
                $userQuizData->user_point = 1;
            }else{
                $userQuizData->user_point = 0;
            }
            $userQuizData->user_id = Auth::id();
            $userQuizData->save();
        }

        return true;
    }
}
