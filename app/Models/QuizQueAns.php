<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

class QuizQueAns extends Model
{
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_quiz_que_ans';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = false;
    /**
     * Delete QuizQueAns data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){
        QuizQueAns::where('quiz_id',$id)->delete();
    }

    public function getAnswer($id){
        return QuizQueAns::where('quiz_id',$id)->get();
    }


}
