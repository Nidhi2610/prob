<?php

namespace App\Models;
use Log;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Comments extends Model

{
    // For soft Delete
    use SoftDeletes;
	/**
     * Define table name.
     *
     * @var string
     */
	protected $likeComments;
    public $table = 'tbl_module_comment';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'user_id','category_id','comment','entreprise_id'
    ];
    protected $dates = ['deleted_at'];

	public function User()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }
	public function LikeComments()
    {
        return $this->hasMany('App\Models\LikeComments','comment_id','id');
    }
    /**
     * Load Comments
     *
     * @return  Object
     */

    public function getComments($category_id)
    {
        $cat = [];

        if (strpos($category_id, ":")) {
            $cate_id = stristr($category_id, ":", true);
        } else {
            $cate_id = $category_id;
        }
        $data=  (in_array($cate_id, $cat)) ?: array_push($cat, $cate_id);
        if(Auth::user()->user_type == 3) {
            $user_ids = User::where('parent_id', Auth::user()->id)
                ->get()
                ->toArray();
            $id = [];
            if (!empty($user_ids)) {
                foreach ($user_ids as $userid) {
                    $id[] = $userid['id'];
                }
            }

            Log::Info($cat);
            array_push($id, Auth::user()->id);
            $comments = Comments::with('User', 'LikeComments')->where('category_id','Like', $cat[0].'%')->whereIn('user_id', $id)->where('entreprise_id', Auth::user()->entreprise_id)->orderBy('created_at','desc')->get();
            Log::Info($comments);

            return $comments;
        }
        elseif (Auth::user()->user_type == 4){
            return Comments::with('User')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
        }elseif(Auth::user()->user_type == 1){
            $comments = Comments::with('User', 'LikeComments')->where('category_id','Like', $cat[0].'%')->orderBy('created_at','desc')->get();
            return $comments;
        }
    }

    /**
     * Insert Comments
     *
     * @return  Object
     */

    public function insert($data)
    {  Log::Info($data);
        return Comments::create([
            'user_id' => Auth::user()->id,
            'category_id' => $data['category_id'],
            'entreprise_id' =>  Auth::user()->entreprise_id,
            'comment' => $data['comment']
        ]);
    }

    /**
     * Load rececnt comments for notification pannel
     *
     * @return  Object
     */

    public function getRecentComments(){
        if(Auth::user()->user_type == 3){

            $user_ids = User::where('parent_id',Auth::user()->id)
                        ->get()
                        ->toArray();
            $id = [];
            if(!empty($user_ids)){
                foreach ($user_ids as $userid){
                  $id[] = $userid['id'];
                }
            }
            array_push($id,Auth::user()->id);
//            array_push($id,);
            $data =  Comments::orderBy('created_at','desc')->take(3)->get();
//            $data =  Comments::whereIn('user_id',$id)->orderBy('created_at','desc')->where('entreprise_id', Auth::user()->entreprise_id)->take(3)->get();
            return $data;
        }
        elseif (Auth::user()->user_type == 4){
            $data =  Comments::where('user_id',Auth::user()->id)->where('entreprise_id', Auth::user()->entreprise_id)->orderBy('created_at','desc')->take(3)->get();
            return $data;
        }elseif (Auth::user()->user_type == 1){
            $data =  Comments::orderBy('created_at','desc')->take(3)->get();
            return $data;
        }

    }
    /**
     * Get latest updated module
     *
     * @param array $categoryArray
     *
     * @return  array
     */
    public function getLoggedInUsersCommentsList($categoryArray = array()){
        $catData = Category::select('id','category_type_id')->where('category_type_id',2)->get();
        $category = array();
        $privateCat = array();
        foreach ($catData as $k => $v){
            $category[$v->id] =$v->category_type_id ; // public category -2
        }
        foreach ($categoryArray as $cat){
            $privateCat[$cat] = 1;  // private loggged in user-1 $category[$cat]=
        }
        $data =  Comments::orderBy('created_at','desc')->get();
        $newComCollection = array();
        $newCommentsArray = array();

        foreach ($data as $d) {
            $id = explode(":", $d->category_id);
            $newCom = array();
            $cateData = Category::where('id', $id[0])->where('status', 0)->first();
            if (((array_key_exists($id[0], $privateCat) == 1) && $cateData['category_type_id'] == 1)) {  // manage private category
                $categoryData = DB::select('SELECT name  FROM tbl_category where id = ' . $id[0]);
                $userData = User::select('name')->where('id', $d->user_id)->first();
                if (count($categoryData) > 0) {
                        $newCom['categryName'] = $categoryData[0]->name;
                        $newCom['category_id'] = $d->category_id;
                        $newCom['userName'] = $userData->name;
                        $newCom['id'] = $d->id;
                        $newCom['entreprise_id'] = $d->user_id;
                        $newCom['comment'] = $d->comment;
                        $newCommentsArray[] = $d->comment;
                        $newComCollection[] = $newCom;

                        if (sizeof($newComCollection) == 3) {
                            return $newComCollection;

                        }
                }
            } elseif ($cateData['category_type_id'] == 2) {                  //manage if public category
                foreach ($id as $v) {
                    if ((array_key_exists($v, $category) == 1)) {             // check the child category_id is in private or not
                        $categoryData = DB::select('SELECT name  FROM tbl_category where id = ' . $id[0]);
                        $userData = User::select('name')->where('id', $d->user_id)->first();
                        if (count($categoryData) > 0) {
                            if (!in_array($d->comment, $newCommentsArray)) {
                                $newCom['categryName'] = $categoryData[0]->name;
                                $newCom['category'] = $v;
                                $newCom['category_id'] = $d->category_id;
                                $newCom['userName'] = $userData->name;
                                $newCom['id'] = $d->id;
                                $newCom['entreprise_id'] = $d->user_id;
                                $newCom['comment'] = $d->comment;
                                $newCommentsArray[] = $d->comment;
                                $newComCollection[] = $newCom;

                                if (sizeof($newComCollection) == 3) {
                                    return $newComCollection;

                                }
                            }
                        }
                    }

                }
            }


        }
    }


    /**
     * Get all the comments of all users
     *
     * @return Object
     * */
    public function getAll()
    {
        return Comments::with('User')->get();
    }

    /**
     * Get the softdelted Data
     * @return  Object
     */

    public function trasheddata(){

        return $data = Comments::with('User')->onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restorecomment($id){

        return $data=Comments::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @param array $id
     * @return  Object
     */

    public function removetrash($id){

        $data=Comments::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Delete Form data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id)
    {
        return Comments::where('id',$id)->delete();
    }

    /**
     * Get Comments by Category
     * */
    public function getCommentByCategory($categoryId)
    {
        return Comments::whereRaw("FIND_IN_SET(".$categoryId.", REPLACE(category_id, ':', ','))")->get();
    }
    /**
     * Get Id of Comments by Category Id
     * */
    public function getCommentId($category_id)
    {
        $data = Comments::where('user_id',Auth::user()->id)->where('category_id',$category_id)->get();
        return $data;
    }

    /**
     * Load Comments with pagination
     *
     * @return  Object
     */

    public function getCommentswithPaginate($category_id)
    {
        $cat = [];

        if (strpos($category_id, ":")) {
            $cate_id = stristr($category_id, ":", true);
        } else {
            $cate_id = $category_id;
        }
        $data=  (in_array($cate_id, $cat)) ?: array_push($cat, $cate_id);
        if(Auth::user()->user_type == 3) {
            $user_ids = User::where('parent_id', Auth::user()->id)
                ->get()
                ->toArray();
            $id = [];
            if (!empty($user_ids)) {
                foreach ($user_ids as $userid) {
                    $id[] = $userid['id'];
                }
            }

            Log::Info($cat);
            array_push($id, Auth::user()->id);
            $comments = Comments::with('User', 'LikeComments')->where('category_id','Like', $cat[0].'%')->whereIn('user_id', $id)->where('entreprise_id', Auth::user()->entreprise_id)->orderBy('created_at','desc')->paginate(9);
            Log::Info($comments);

            return $comments;
        }
        elseif (Auth::user()->user_type == 4){
            return Comments::with('User')->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->paginate(9);
        }elseif(Auth::user()->user_type == 1){
            $comments = Comments::with('User', 'LikeComments')->where('category_id','Like', $cat[0].'%')->orderBy('created_at','desc')->paginate(9);
            return $comments;
        }
    }

    /**
     * Get Comments by user id
     * @param $id
     * @return object
     * */
    public function getCommentuserID($id)
    {
        return Comments::where('user_id',$id)->get();
    }
}
