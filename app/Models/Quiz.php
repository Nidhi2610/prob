<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use App\Models\QuizQueAns;

class Quiz extends Model
{

    /*
*  For Soft Delete
*/

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_quiz';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quiz_name', 'status','created_by','updated_by'
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function QuizQueAns(){
        return $this->hasMany('App\Models\QuizQueAns','quiz_id','id');
    }

    /**
     * Load all quiz data
     *
     * @return  Object
     */
    public function init(){
        return Quiz::get();
    }

    /**
     * Get Quiz name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getQuizName($id){
        $get = Quiz::where('quiz_id',$id)->first(['title']);
        if($get){
            return $get->title;
        }
        return '-';
    }

    /**
     * Get Quiz data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getQuizByID($id){
        return Quiz::with('QuizQueAns')->where('id',$id)->first();
    }

    /**
     * Get Quiz data base on ID for Admin preview
     *
     * @param integer $id
     * @return  Object
     */
    public function getAdminQuizByID($id){
        return Quiz::with('QuizQueAns')->where('id',$id)->first();
    }


    /**
     * Insert Quiz data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data){

        $quiz = new Quiz;
        $quiz->quiz_name = $data['quiz_name'];
        $quiz->status = $data['status'];
        $quiz->created_by = Auth::user()->id;
        $quiz->updated_by = Auth::user()->id;
        $quiz->save();

        foreach ($data['quiz'] as $quiz_key => $quiz_value){
            $quiz_que_ans = new QuizQueAns;
            $quiz_que_ans->quiz_id = $quiz->id;
            $quiz_que_ans->question = $quiz_value['question'];
            $quiz_que_ans->answers = json_encode($quiz_value['answers']);
            if(isset($quiz_value['true_ans'])){
                $quiz_que_ans->true_answers = implode(",",$quiz_value['true_ans']);
            }
            $quiz_que_ans->save();
        }
        return true;
    }

    /**
     * Update Quiz data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data){

        $quiz = Quiz::find($data['id']);
        $quiz->quiz_name = $data['quiz_name'];
        $quiz->status = $data['status'];
        $quiz->created_by = Auth::user()->id;
        $quiz->updated_by = Auth::user()->id;
        $quiz->save();

        $quiz_que_ans_delete = new QuizQueAns();
        $quiz_que_ans_delete->remove($quiz->id);

        foreach ($data['quiz'] as $quiz_key => $quiz_value){
            $quiz_que_ans = new QuizQueAns;
            $quiz_que_ans->quiz_id = $quiz->id;
            $quiz_que_ans->question = $quiz_value['question'];
            $quiz_que_ans->answers = json_encode($quiz_value['answers']);
            if(isset($quiz_value['true_ans'])){
                $quiz_que_ans->true_answers = implode(",",$quiz_value['true_ans']);
            }
            $quiz_que_ans->save();
        }
        return true;
    }

    /**
     * Delete Quiz data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){

        $quizdata = Module::where('quiz_id',$id)->count();
        if($quizdata > 0){

            return false;
        }
        else{

            return Quiz::where('id',$id)->delete();
        }

    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata(){

        return $data = Quiz::onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restorequiz($id){

        return $data=Quiz::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @param array $id
     * @return  Object
     */

    public function removetrash($id){

        $data=Quiz::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Duplcate data
     *
     *
     * @param int $id
     * @return  Object
     */

    public function duplicate($id){

        $data = Quiz::find($id);
        $duplicatedata['quiz_name'] = $data['quiz_name']."-Copy";
        $newData = $data->replicate();
        $newData->quiz_name =  $duplicatedata['quiz_name'];
        $newData->save();
        //return $insertedId = $newData->id;
        $quiz_que_ans = new QuizQueAns;
        $quizAnswer = $quiz_que_ans->getAnswer($id);
        if($quizAnswer){
            foreach ($quizAnswer as $answer){
                $newAnswer = $answer->replicate();
                $newAnswer->quiz_id = $newData->id;
                $newAnswer->save();
            }
        }
        return $insertedId = $newData->id;
    }


}
