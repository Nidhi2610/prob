<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Bookmark extends Model
{

    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_bookmark';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id','formation_id',
    ];

    /**
     * Insert Comments
     *
     * @return  Object
     */

    public function insert($data)
    {
        return Bookmark::create([
            'user_id' => Auth::user()->id,
            'formation_id' => $data['category_bookmark_id'],
        ]);
    }
    /**
     * Insert Comments
     *
     * @return  Object
     */

    public function remove($data)
    {
        return Bookmark::where('formation_id',$data['category_bookmark_id'])->where('user_id',Auth::user()->id)->delete();
    }

    /**
     * Get Counts
     *
     * @return  Object
     */

    public function getCount($user_id){


        return Bookmark::where('user_id',$user_id)->count();
        //return Comments::with('User')->where('category_id',$category_id)->where('entreprise_id',Auth::user()->entreprise_id)->get();
    }

    /**
     * Get the user Bookmark
     *
     *
     *
     *
     * */
    public function getUserBookmark($id)
    {
        return Bookmark::where('user_id',$id)->get();
    }
}
