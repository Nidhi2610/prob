<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entreprise;
use Auth;
use App\Helpers\LaraHelpers;
use DB;
use Carbon\Carbon;
use Log;
class User extends Authenticatable
{
    use Notifiable;

    /*
	*  For Soft Delete
	*/

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','first_name','last_name','user_type','parent_id','entreprise_id',
    ];
//
//    protected $casts = [
//        'entreprise_id' => array()
//    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Entreprise()
    {
        return $this->hasOne('App\Models\Entreprise','id','entreprise_id');
    }

    public function Comments()
    {
        return $this->hasMany('App\Models\Comments','user_id','id');
    }

    public function ModuleQuiz()
    {
        return $this->hasMany('App\Models\UserModuleCategoryQuiz','user_id','id');
    }

    /**
     * Load all user data
     *
     * @return  Object
     */
    public function init(){
        if(Auth::user()->user_type == "3" || Auth::user()->user_type == "2"){
           return User::with('Comments')->where('parent_id',Auth::user()->id)->where('deleted_at',NULL)->get();

           //return User::get();
        }

        return User::with('Entreprise')->where('id','!=' ,Auth::user()->id)->where('deleted_at',NULL)->get();
    }

    /**
     * Get User name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getUserName($id){
        $get = User::where('id',$id)->first(['name']);
        if($get){
            return $get->name;
        }
        return '-';
    }

    /**
     * Get User data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getUserByID($id){
        return User::where('id',$id)->first();
    }

    /**
     * Get parent_id for entreprise using entreprise_id
     *
     * @param integer $id
     * @return  Object
     */
    public function getparentIDbyEntrepriseID($id){
        return Entreprise::select('parent_id')->where('id',$id)->first();
    }



    /**
     * Insert User data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data){

        // For Formature user
        if(isset($data['entreprise_id'])  && $data['user_type'] == 5 ) {
            $data['entreprise_id'] = implode(",", $data['entreprise_id']);
             if(!isset($data['parent_id'])){
                $data['parent_id'] = 0;
              }

            }

        // For Admin User

        if(isset($data['entreprise_id'])  && $data['user_type'] == 2){
            if(is_array($data['entreprise_id'])){
                $data['entreprise_id'] = implode(",", $data['entreprise_id']);//new array willbe passed here.
            }
            if(!isset($data['parent_id'])){
              $data['parent_id'] = 0;
            }
        }


        if(!isset($data['entreprise_id'])){
            $entreprise = new Entreprise();
            $entrepriseData = $entreprise->getallentreprise();
            $entrepriseIdArray = array();
            foreach ($entrepriseData as $entId){
                $entrepriseIdArray[] = $entId->id;
            }
            $data['entreprise_id'] = implode(",",$entrepriseIdArray);
        }

        // For Responsable user

       if(isset($data['entreprise_id']) && $data['user_type'] == 3){
            $data['entreprise_id']  = implode( ",",$data['entreprise_id']);
            if(!isset($data['parent_id'])){
                $data['parent_id'] = 0;
            }
       }

       // For collaborateur user

        if($data['user_type'] == 4){
            $data['entreprise_id']  = 0;
//            $dataEntreprise = User::select('entreprise_id')->where('id',$data['parent_id'])->first();
//            $data['entreprise_id'] = $dataEntreprise->entreprise_id;
        }
// echo "<pre>";print_r($data);exit;
        return User::create([
            'name' => $data['first_name']." ".$data['last_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'user_type' => $data['user_type'],
            'parent_id' => $data['parent_id'],
            'entreprise_id' => $data['entreprise_id'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Update User data to database
     *
     * @param array $data
     * @return  Object
     */
    public function edit($data){
        if(!isset($data['entreprise_id'])){
            $data['entreprise_id']=0;
        }
        if(is_array($data['entreprise_id'])){
            $data['entreprise_id'] = implode(",",$data['entreprise_id']);
            $id =json_encode ( $data['entreprise_id']);
            //$entre_id = json_decode($data['entreprise_id'],true);
            $entre_id = json_decode($id,true);
        }else{
            $entre_id = $data['entreprise_id'];
        }
        if($data['user_type'] == 3 || ($data['user_type'] == 4)){
            $entre_id = $data['single_entreprise_id'];

        }
//print_r($entre_id);exit;
        return User::where('id',$data['id'])
            ->update([
                'name' => $data['first_name']." ".$data['last_name'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'user_type' => $data['user_type'],
                'parent_id' => $data['parent_id'],
                'entreprise_id' => $entre_id,
            ]);
    }

    /**
     * Delete User data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){

        if(Auth::user()->user_type == "1" ||Auth::user()->user_type == "2" ){

            $userdata = Responsable::where('responsable_id',$id)->count();
            if($userdata > 0){
                return false;
            }
            else{

                return User::where('id',$id)->delete();
            }
        }

        if(Auth::user()->user_type == "3"){

           $userdata = Collaborateur::where('collaborateur_id',$id)->count();
            if($userdata > 0){
                return false;
            }
            else{

                return User::where('id',$id)->delete();
            }
       }
    }
	
	/**
     * Update UserAccount data to database for own userprofile
     * Updates only first_name and last_name
     *
     * @param array $data
     *@return  Object
     */
    public function edituseraccount($data){
        $updateData = [
            'name' => $data['first_name']." ".$data['last_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
        ];

      if (isset($data['profile_image']) && $data['profile_image'] != "") {
          $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;

          $updateData['profile_image'] = LaraHelpers::upload_user_image($filepath, $data['profile_image'], $data['original_old_filename']);
          $updateData['original_profile_image'] = $data['profile_image']->getClientOriginalName();

        }

        return User::where('id',$data['id'])
            ->update($updateData);
    }
	
	/**
     * Update User Password
     * 
     * @param varchar $password and old_password
     * @return  Object
     */
    public function editpassword($data){
		
		
        return User::where('id',$data['id'])
            ->update([
                'password' =>bcrypt( $data['password']),
              ]);
    }
	
	/**
     * Load all user data for assigning modules
     *
     * @return  Object
     */
    public function getusers(){
       
	   if(Auth::user()->user_type == 3){
		   
		   return User::where('id','!=',Auth::user()->id)
			->where('user_type', '=', 4)
			->where('parent_id', '=', Auth::user()->id)
			->get(); 
		   
	   }
	   else if(Auth::user()->user_type == 2 || Auth::user()->user_type == 1 ){
	   
            return User::where('id','!=',Auth::user()->id)
			->where('user_type', '=', 3)
			->get();
       }
	   
	   elseif(Auth::user()->user_type == 4){
	   
            return [];
       }
	}

    /**
     * Get the softdelted Data
     *
     * @param array $id
     * @return  Object
     */
    public function trasheddata(){

        return $data = User::onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     * @param array $id
     * @return  Object
     */
    public function restoreuser($id){

        return $data=User::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     * @param array $id
     * @return  Object
     */
    public function removetrash($id){

        $data=User::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Get Responasable User
     *
     * @return  Object
     */
    public function getResponsable(){
        return User::where('user_type',3)->get();
    }

    /**
     * get User By fieldname getUserByField
     *
     * @param string $field_name
     * @param mixed $id
     * @return mixed
     */
    public function getUserByField($field_name,$id)
    {
        return User::where($field_name, $id)->get();
    }

    /**
     * Update User data to database
     *
     * @param array $data
     * @return  Object
     */
    public function addEnterprise(array $data = [])
    {
        foreach ($data['users'] as $data_key => $data_val){
            $user = User::find($data_key);
            $user->entreprise_id = ($data_val != "")?$data_val:0;
            $user->save();

            if($user->user_type == 3){
                User::where('parent_id',$user->id)
                    ->update([
                        'entreprise_id' =>($data_val != "")?$data_val:0,
                    ]);
            }
        }
     return true;
    }

    /**
     * Load all community Member
     *
     * @return  Object
     */
    public function getCommunityMember(){
        if(Auth::user()->user_type == "3"){
            return User::where('id','!=',Auth::user()->id)
                    ->where(function($query){
                        $query->where('parent_id',Auth::user()->id);
                        $query->orWhere(function ($q){
                           $q->where('entreprise_id',Auth::user()->entreprise_id)->where('user_type','3');
                        });
                    })->get();

            //return User::get();
        }
        return User::where('parent_id',Auth::user()->parent_id)
            ->where('id','!=',Auth::user()->id)
            ->get();
    }

    /**
     * Add Last Login Time
     *
     * @param  int $id
     * @return object
     * */
    public function addLoginTime($id)
    {
        $currentTime = Carbon::now();
        return User::where('id',$id)->update([
            'last_login' =>$currentTime,
        ]);
    }

    /**
     *
     * Get the list of data who logged in recently
     *
     * @param int Auth::user_id :$id
     * @return object
     * */
    public function getRecentLogin($id)
    {
        $date = new Carbon;
        $date->subWeek();
        return User::where('last_login', '>', $date->toDateTimeString())->where('parent_id',$id)->get();
    }

    /**
     *
     * Get the list of Entreprise assigned by user
     *
     * @param int Auth::user_id :$id
     * @return $ids
     * */
    public function getAllEntreprisebyUser()
    {    $id = Auth::user()->id;
        $data = User::select('entreprise_id')->where('id',$id)->first();
        $Entreprises =  $data['entreprise_id'];
        $ids =  explode("," , $Entreprises);
        return $ids;
    }
//------------------------------------------------------------------
    /**
     * get Active User By fieldname getUserByField
     *
     * @param string $field_name
     * @param mixed $id
     * @return mixed
     */
    public function getUserByFieldName($field_name,$id)
    {
        return User::where($field_name, $id)->where('deleted_at',NULL)->get();
    }

    /**
     * Get User First name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getUserFirtName($id){
        $get = User::where('id',$id)->first(['first_name']);
        if($get){
            return $get->first_name;
        }
        return '-';
    }

    /**
     *
     * Get the list of Entreprise assigned by user
     *
     * @param int Auth::user_id :$id
     * @return ids
     * */
    public function getAllEntreprisebyUserid($id)
    {
        $data = User::select('entreprise_id')->where('id',$id)->first();
        $Entreprises =  $data['entreprise_id'];
        $ids =  explode("," , $Entreprises);
        return $ids;
    }

    public function getAllparentChildEntreprise(){
        $list = Entreprise::select('id')->where('parent_id',Auth::user()->entreprise_id)->get();
        $arrEnt = array();
        $arrEnt[] = Auth::user()->entreprise_id;
        $LstArr = array();
        foreach ($list as $value){
            $arrEnt[] = $value->id;
        }
        $LstArr['ent'] = $arrEnt;
    return $LstArr;
    }
}
