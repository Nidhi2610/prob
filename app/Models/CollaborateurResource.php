<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;


class CollaborateurResource extends Model
{

    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_collaborateur_resource';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'collaborateur_id','user_id','resource_id'
    ];



    /**
     * Insert Resource_id data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function insert($data){

        return CollaborateurResource::create([
            'collaborateur_id' => $data['collaborateur_id'],
            'user_id' => $data['user_id'],
            'resource_id' => $data['resource_id'],

        ]);
    }

    /**
     * Remove Assigned Collaborateur
     *
     * @param array $data
     *
     * @return  Object
     */

    public function remove($data){

        return CollaborateurResource::where('resource_id', '=', $data['resource_id'] )
            ->delete();
    }

    /**
     * Get Assigned Collaborateur
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getuser($fieldname, $id){
        return CollaborateurResource::where($fieldname, $id)->get();
    }
}
