<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntrepriseResource extends Model
{
    //
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_entreprise_resource';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entreprise_id', 'resource_id'
    ];

    public function EnterpriseModule()
    {
        return $this->hasMany('App\Models\ModuleResource', 'resource_id', 'resource_id');
    }

    /**
     * Load all Entreprise data
     *
     * @return  Object
     */

    public function init(){

        return EntrepriseResource::get();

    }

    /**
     * Load all Entreprise data from resourceid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getEntrepriseById($field_name,$id){
        return EntrepriseResource::where($field_name, $id)->get();
    }

    /**
     * Load all Entreprise data from resourceid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getMultipleEntrepriseById($field_name,$id){
        $id = explode(',',$id);
        return EntrepriseResource::whereIn($field_name,$id)->get();
    }


    /**
     * Remove Assigned Enterprise
     *
     * @param array $data
     *
     * @return  Object
     */

    public function remove($data){

        return EntrepriseResource::where('resource_id', '=', $data['resource_id'] )
            ->delete();
    }

    /**
     * Insert Enterprise data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function insert($data){

        return EntrepriseResource::create([

            'resource_id' => $data['resource_id'],
            'entreprise_id' => $data['entreprise_id'],
        ]);
    }

    /*Get Module by  enterprise_id
     *
     *  @param int $entreprise_id
     * */
    public function getModuleThroughEnterprise($entreprise_id)
    {
        return EntrepriseResource::with('EnterpriseModule')->where('entreprise_id',$entreprise_id)->get();
    }

}
