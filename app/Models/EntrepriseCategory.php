<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntrepriseCategory extends Model
{
    //
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_entreprise_category';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'entreprise_id', 'category_id'
    ];

    public function EnterpriseModule()
    {
        return $this->hasMany('App\Models\ModuleCategory', 'category_id', 'category_id');
    }

    /**
     * Load all Entreprise data
     *
     * @return  Object
     */

    public function init(){

        return EntrepriseCategory::get();

    }

    /**
     * Load all Entreprise data from categoryid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getEntrepriseById($field_name,$id){
        return EntrepriseCategory::where($field_name, $id)->get();
    }

    /**
     * Load all Entreprise data from categoryid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getMultipleEntrepriseById($field_name,$id){
        $id = explode(',',$id);
        return EntrepriseCategory::whereIn($field_name,$id)->get();
    }


    /**
     * Remove Assigned Enterprise
     *
     * @param array $data
     *
     * @return  Object
     */

    public function remove($data){

        return EntrepriseCategory::where('category_id', '=', $data['category_id'] )
            ->delete();
    }

    /**
     * Insert Enterprise data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function insert($data){

        return EntrepriseCategory::create([

            'category_id' => $data['category_id'],
            'entreprise_id' => $data['entreprise_id'],
        ]);
    }

    /*Get Module by  enterprise_id
     *
     *  @param int $entreprise_id
     * */
    public function getModuleThroughEnterprise($entreprise_id)
    {
        return EntrepriseCategory::with('EnterpriseModule')->where('entreprise_id',$entreprise_id)->get();
    }

    /**
     * Get all private Formation resource
     *
     * @return  Object
     */
    public function getAllPrivateFormationResource($id){

        return EntrepriseCategory::join('tbl_module_category','tbl_module_category.category_id','=','tbl_entreprise_category.category_id')->where('entreprise_id',$id)->count();

    }

}
