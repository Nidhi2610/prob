<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollaborateurAssignedResource extends Model
{
    //
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_collaborateur_resource';

    /**
     * Load all Entreprise data from resourceid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getResourceByCollaborateurId($field_name,$id){
        return CollaborateurAssignedResource::where($field_name, $id)->get();
    }
}
