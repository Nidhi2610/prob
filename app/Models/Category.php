<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ModuleCategory;
use App\Models\EntrepriseCategory;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Object_;

class Category extends Model
{
    /*
	*  For Soft Delete
	*/

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_category';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id', 'color', 'status', 'category_type_id', 'sort_order','description','font_awesome','category_classification'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Load all category data
     *
     * @return  Object
     */
    public function init($type)
    {
        $cat_data = Category::orderBy('sort_order', 'asc');
        if(!empty($type)){
            $cat_data->where('category_classification',$type);
        }
        $cat_data = $cat_data->get();
       return $cat_data;
    }

    /**
     * Get Category name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getCategoryName($id)
    {
        $get = Category::where('id', $id)->first(['name']);
        if ($get) {
            return $get->name;
        }
        return '-';
    }

    /**
     * Get Category data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getCategoryByID($id)
    {
        return Category::where('id', $id)->first();
    }

    /**
     * Insert Category data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data)
    {
        if(!isset($data['category_type_id'])){
        $data['category_type_id'] = 2;  // set as public
        }
        return Category::create([
            'name' => $data['name'],
            'parent_id' => $data['parent_category'],
            'category_type_id' => $data['category_type_id'],
            'color' => "#".$data['color'],
            'category_classification' => $data['category_classification'],
            'description' => $data['description'],
            'font_awesome' => $data['font_awesome'],
            'status' => $data['status'],
        ]);
    }

    /**
     * Update Category data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data)
    {
        if(!isset($data['status'])){
            $data['status'] = 1;  // set as Inactive
        }
        return Category::where('id', $data['id'])
            ->update([
                'name' => $data['name'],
                'parent_id' => $data['parent_category'],
                'category_type_id' => $data['category_type_id'],
                'category_classification' => $data['category_classification'],
                'color' => "#".$data['color'],
                'description' => $data['description'],
                'font_awesome' => $data['font_awesome'],
                'status' => $data['status'],
            ]);
    }

    /**
     * Delete Category data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id)
    {

        $result = Category::where('parent_id', $id)->count();
        if ($result > 0) {
            return false;
        } else {
            $resultmodule = ModuleCategory::where('category_id', $id)->count();
            if ($resultmodule > 0) {
                return false;
            }
            return Category::where('id', $id)->delete();
        }

    }

    public function updateAllCategory(array $data = [])
    {

        $j = 0;
        foreach ($data['list'] as $id => $parent_id) {
            if ($parent_id == 'null') {
                $parent_id = 0;
            }
            Category::where('id', $id)->update(['parent_id' => $parent_id, "sort_order" => $j]);
            $j++;
        }

        return true;

    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata($type)
    {
        $data = Category::onlyTrashed();
        if(!empty($type)){
            $data->where('category_classification',$type);
        }
        $data = $data->get();
        return $data;
    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restorecategory($id)
    {

        return $data = Category::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @param array $id
     * @return  Object
     */

    public function removetrash($id)
    {

        $data = Category::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Duplcate Category data
     *
     *
     * @param int $id
     * @return  Object
     */

    public function duplicate($id)
    {

        $data = Category::find($id);
        $newData = $data->replicate();
        $newData->name = $data['name'] . "-Copy";
        $insertedId = $newData->save();

        // For replicating the module attached with the category (Stored in tbl_module_category)

        $modulecategoryData = ModuleCategory::where('category_id',$id)->get();
        if($modulecategoryData->count() > 0){
            foreach ($modulecategoryData as $modulerow){
                $modulecategory = new ModuleCategory();
               $moduleresult = $modulecategory->duplicateModuleFromCategory($modulerow->module_id,$newData->id);
            }
        }
        // Ends here for first Condition

        $subCategory = Category::where('parent_id', $id);
        if ($subCategory->count() > 0) {
            foreach ($subCategory->get() as $row) {
                $subData = Category::find($row->id);
                $newSubData = $subData->replicate();
                $newSubData->parent_id = $newData->id;
                $newSubData->name = $subData['name'] . "-Copy";
                $newSubData->save();

                // For replicating the module attached with the subcategory (Stored in tbl_module_category)

                $submodulecategoryData = ModuleCategory::where('category_id',$row->id)->get();
                if($submodulecategoryData->count() > 0){
                    foreach ($submodulecategoryData as $submodulerow){
                        $modulecategory = new ModuleCategory();
                        $submoduleresult = $modulecategory->duplicateModuleFromCategory($submodulerow->module_id,$newSubData->id);
                    }
                }
                // Ends here for second Condition

                $subChildCategory = Category::where('parent_id', $row->id);

                if ($subChildCategory->count() > 0) {
                    foreach ($subChildCategory->get() as $row_child) {
                        $subChildData = Category::find($row_child->id);
                        $newSubChildData = $subChildData->replicate();
                        $newSubChildData->parent_id = $newSubData->id;
                        $newSubChildData->name = $subChildData['name'] . "-Copy";
                        $newSubChildData->save();

                        // For replicating the module attached with the subcategory (Stored in tbl_module_category)

                        $submodulechildcategoryData = ModuleCategory::where('category_id',$row_child->id)->get();
                        if($submodulechildcategoryData->count() > 0){
                            foreach ($submodulechildcategoryData as $submodulechildrow){
                                $modulecategory = new ModuleCategory();
                                $submodulechildresult = $modulecategory->duplicateModuleFromCategory($submodulechildrow->module_id,$newSubChildData->id);
                            }
                        }
                        // Ends here for third Condition

                    }

                }

            }

        }

        if($insertedId){
            return $newData;
        }
        return false;

    }

    /**
     * Load category data for Responsable
     *
     * @return  Object
     */
    public function getCategoryByEnterpriseId($entreprise_id)
    {
        $EntrepriseCategory = new EntrepriseCategory();

       // $category_data = $EntrepriseCategory->getEntrepriseById('entreprise_id',$entreprise_id);
        $category_data = $EntrepriseCategory->getMultipleEntrepriseById('entreprise_id',$entreprise_id);

        $category_id = [];
        foreach ($category_data as $row){
            $category_id[] = $row->category_id;
        }
       return Category::with('getCategory')->whereIn('id',$category_id)->where('parent_id',0)->where('category_type_id',1)->get();

    }

    /**
     * Load category data for Responsable
     *
     * @return  Object
     */
    public function getResourceByEnterpriseId($entreprise_id)
    {
        $EntrepriseCategory = new EntrepriseCategory();

       // $category_data = $EntrepriseCategory->getEntrepriseById('entreprise_id',$entreprise_id);
        $category_data = $EntrepriseCategory->getMultipleEntrepriseById('entreprise_id',$entreprise_id);

        $category_id = [];
        foreach ($category_data as $row){
            $category_id[] = $row->category_id;
        }
       return Category::with('getCategory')->whereIn('id',$category_id)->where('parent_id',0)->where('category_type_id',1)->where('category_classification','=','R')->orderBy('sort_order', 'asc')->get();

    }

    /**
     * Load category data for Responsable
     *
     * @return  Object
     */
    public function getFormationByEnterpriseId($entreprise_id)
    {
        $EntrepriseCategory = new EntrepriseCategory();

       // $category_data = $EntrepriseCategory->getEntrepriseById('entreprise_id',$entreprise_id);
        $category_data = $EntrepriseCategory->getMultipleEntrepriseById('entreprise_id',$entreprise_id);

        $category_id = [];
        foreach ($category_data as $row){
            $category_id[] = $row->category_id;
        }
       return Category::with('getCategory')->whereIn('id',$category_id)->where('parent_id',0)->where('category_type_id',1)->where('category_classification','=','F')->get();

    }

    /**
     * Get Resource By Id
     * @param int $id
     * @return  Object
     */
    public function getResourceById($id)
    {
      return Category::where('parent_id','=',$id)
          ->where('status',0)
          ->where('category_type_id','=',2)
          ->orderBy('sort_order', 'asc')
          ->get();
    }

    /**
     * Get Resource By Id
     * @param int $id
     * @return  Object
     */
    public function getallResourceById($id)
    {
        return Category::where('parent_id','=',$id)
            ->where('status',0)
            ->orderBy('sort_order', 'asc')
            ->get();
    }

    /**
     * Get Resource By Id
     * @param int $id
     * @return  Object
     */
    public function getPrivateResourceById($id)
    {
        return Category::where('parent_id','=',$id)
            ->where('status',0)
            ->where('category_type_id','=',1)
            ->orderBy('sort_order', 'asc')
            ->get();
    }

    /**
     * Get Formation By Id
     * @param int $id
     * @return  Object
     */
    public function getFormationById($id)
    {
        return Category::where('parent_id','=',$id)
            ->where('status',0)
            ->orderBy('sort_order', 'asc')
            ->get();
    }

    /**
     * Load category data for Responsable
     *
     * @return  Object
     */
    public function getCategory()
    {

        return $this->hasOne('App\Models\EntrepriseCategory');

    }

    /**
     * Load category data for Collaborateur
     *
     * @return  Object
     */
    public function getCollaborateurCategory($collaborateur_id)
    {
        $CollaborateurAssignedCategory = new CollaborateurAssignedCategory();

        $category_data = $CollaborateurAssignedCategory->getCategoryByCollaborateurId('collaborateur_id',$collaborateur_id);

        $category_id = [];
        foreach ($category_data as $row){
            $category_id[] = $row->category_id;
        }
        return Category::with('getCategoryForCollaborateur')->whereIn('id',$category_id)->where('parent_id',0)->where('status',0)->get();

    }

    /**
     * Load category data for Collaborateur for binding
     *
     * @return  Object
     */
    public function getCategoryForCollaborateur()
    {

        return $this->hasOne('App\Models\CollaborateurAssignedCategory');

    }

    /**
     * Load Ressource data where type is Public and Self parent
     *
     * @return  Object
     */
    public function getPublicResource()
    {
        return Category::where('category_type_id',2)->where('parent_id','=','0')->where('status','=','0')->where('category_classification','=','R')->orderBy('sort_order', 'asc')->get();
    }

    /**
     * Load Ressource data where type is Public and Self parent
     *
     * @return  Object
    */
    public function getPublicChildeResource()
    {
        return Category::where('category_type_id',2)->where('status','=','1')->where('category_classification','=','R')->get();
    }
    /**
     * Load Formation data where type is Public
     *
     * @return  Object
     */
    public function getPublicFormation()
    {
        return Category::where('category_type_id',2)->where('parent_id','=','0')->where('status','=','0')->where('category_classification','=','F')->orderBy('sort_order', 'asc')->get();
    }

    /**
     * Load child category data From parent_id
     *
     * @return  Object
     */
    public function getChildCategoryByParentId($parent_id)
    {

        return Category::where('parent_id',$parent_id)->where('category_type_id',2)->where('status',0)->get();

    }

    /*
     * Get the Parent_id from id
     *
     * @param $id
     * @return Object
     * */
    public function getParentId($id)
    {

        return Category::where('parent_id',$id)->where('status',0)->get();

    }

    /*
     * Get the Parent_id from arrya Id
     *
     * @param array $id
     * @return Object
     * */
    public function getParent($field,$categoryId)
    {

        return Category::whereIn($field,$categoryId)->where('status',0)->get();

    }

    /**
     *Get all Parent formation as well as resource category
     *
     *@return Object_
     * */
    public function getAllParentFormationCategory()
    {
        return Category::where('parent_id',0)->where('status',0)->where('category_classification','F')->get();
    }

    /**
     *Get all Parent formation as well as resource category
     *
     *@return Object_
     * */
    public function getAllParentResourceCategory()
    {
        return Category::where('parent_id',0)->where('status',0)->where('category_classification','R')->get();
    }

    /**
     * Get Category data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getCategoryNameByID($id)
    {

        return Category::where('id', $id)->where('status',0)->where('deleted_at',NULL)->first();

        $cat_id = ModuleCategory::where('module_id',$id)->first();
        $data= Category::where('id', $cat_id['category_id'])->where('status',0)->where('deleted_at',NULL)->first();
        return $data;
    }

    /**
     * Get Category data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getCategorybyIDandDeleted($id)
    {
        return Category::where('id', $id)->where('deleted_at','NULL')->get();
    }

    /**
     * Get Category data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getActiveCategoryByID($id)
    {
        return Category::where('id', $id)->where('status',0)->first();
    }

}
