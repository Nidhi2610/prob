<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollaborateurAssignedCategory extends Model
{
    //
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_collaborateur_category';

    /**
     * Load all Category data from collaborateurid
     *
     * @param string $field_name
     * @param mixed $id
     * @return  Object
     */

    public function getCategoryByCollaborateurId($field_name,$id){
        return CollaborateurAssignedCategory::where($field_name, $id)->get();
    }

    /**
     * Load all Resource and Formation data  from collaborateurid
     *
     * @param string $field_name
     * @param mixed $id
     * @param string $type
     * @return  Object
     */

    public function getCollaborateurResFor($field_name,$id,$type){
        return CollaborateurAssignedCategory::where($field_name, $id)
            ->where('type',$type)
            ->get();
    }

    /**
     * Load category data for Responsable
     *
     * @return  Object
     */
    public function Category()
    {

        return $this->hasMany('App\Models\Category','id','category_id');

    }
}
