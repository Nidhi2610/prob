<?php

namespace App\Models;
use Auth;
use DB;
use Carbon\Carbon;
use Log;
use Request;
use Session;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{

    /**
     * Define table name.
     *
     * @var string
     */
     
    public $table = 'tbl_users_login';
    
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    
    protected $primaryKey = 'id';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_type', 'last_loggedin','total_time','ip_address','user_agent','last_loggedout'
    ];


    /**
     * Tracking the logout and logout time
     * check point for existing user
     * if exist update
     *
     *
     */
    public function checkUserisExistorNot(){
        $datas = UserLogin::where('user_id',Auth::user()->id)->first(); 
        if(empty($datas)){
            $this->insert();
        }else{
            $this->updateData($datas);
        }
    	
    }

    /**
     *  for Tracking the logout and logout time
     *  insert new user
     *
     * @return user object
     */
    public function insert(){
        return UserLogin::create([
            'user_id' => Auth::user()->id,
            'user_type' => Auth::user()->user_type,
            'session_id' => null,
            'last_loggedin' => Carbon::now(),
            'last_loggedout' => null,
            'total_time' => 0,
            'ip_address' => Request::ip(),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);       
    }

    /**
     * update the login user data
     *
     * @return $data object
     */
     public function updateData($datas){
        $data = UserLogin::where('user_id',$datas->user_id)->first();
        $data->last_loggedin = Carbon::now();            
        $data->ip_address = Request::ip();
        $data->user_agent = $_SERVER['HTTP_USER_AGENT'];
        $data->save();            
        return $data;    
    }

    /**
     * update the logout and total time
     *
     * @return $data object
     */
    public function updateTime(){    
      $data = UserLogin::where('user_id', Auth::user()->id)->first();    
        $now = Carbon::now();
        $login =Carbon::parse($data->last_loggedin);
        $diff = $login->diffInSeconds($now);
        $total =$data->total_time;
        $time = $diff+$total;     
    //          Log::info($time);
    //          Log::info(Carbon::now());

      $data->last_loggedout = Carbon::now(); 
      $data->ip_address = Request::ip();
      $data->user_agent = $_SERVER['HTTP_USER_AGENT'];
      $data->total_time = $time;      
      $data->save();      
      return $data;
    }

    /**
     * @param $id user id
     * get the last login time for user $id
     *
     * @return  Date object $time
     */
    public function getLastLoginTime($id){
       $data = UserLogin::select('last_loggedin')->where('user_id',$id)->first();
       $date=  date($data->last_loggedin);

      $time = date('h:i:s',strtotime($data->last_loggedin));

      return $time;
    }

    /**
     * @param $id user id
     * get the total time for user $id
     *
     * @return object value total_time
     */
    public function getTotalTime($id){
        $time = UserLogin::select('total_time')->where('user_id',$id)->first();
        return $this->ChangeTime($time->total_time);
    }

   /*
    *  for converting it in
    *  Day.Hours.Minuts.Seconds format from seconds
    *
    *  @param $time
    *  @return $result string
    * */
    public function ChangeTime($time){

            $frTime = array("jours" => 86400,
                                "h" => 3600,
                                "min" => 60,
                                "sec" => 1
                              );
            $result = "";
            foreach($frTime as $uniteTemps => $nombreSecondesDansUnite) {

                $unitetime = floor($time/$nombreSecondesDansUnite);

                $time = $time%$nombreSecondesDansUnite;
                if($unitetime > 0 || !empty($result))
                    $result .= $unitetime." $uniteTemps ";
            }
            return $result;
    }

}
