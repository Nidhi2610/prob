<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class UserModuleComplete extends Model
{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'user_module_complete';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id','module_id'
    ];
	
	/*
    * get boolean value form
    *
	* @param array $data
    * @return  status= "string" if data is not already exist
	 *          else return 0.
    */
    public function insert($data)
    {
       $checkstatus =  $this->checkStatus($data['module_id']);
       if(empty($checkstatus))
       {

               $status =  UserModuleComplete::create([
               'user_id' => Auth::user()->id,
               'module_id' => $data['module_id'],
           ]);
           return $status;
       }else{
           return ["checkstatus" => $checkstatus,"status"=>1];
       }
 }

    /*
     *  add user form
     *
     * @param array $data
     * @return mixed
     */

    public  function checkStatus($Id)
    {
        $return_data =  UserModuleComplete::where('module_id',$Id)->where('user_id',Auth::user()->id)->first();
        return $return_data;

    }

}
