<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Responsable;
use App\Helpers\LaraHelpers;
use Auth;
class Entreprise extends Model
{

    /*
	*  For Soft Delete
	*/

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_entreprise';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','status','logo','parent_id'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Load all category data
     *
     * @return  Object
     */
    public function init(){
         return   Entreprise::with('User','CUser')->where('deleted_at',Null)->get();
        // dd($data);
       // return $data;
    }

    /**
     * Load all user assosicated with entrprise_id having user_type = 3 (Responsalbe User)
     *
     * @return  Object
     */

    public function User()
    {
        return $this->hasMany('App\Models\User','entreprise_id','id')
            ->where('user_type',3);
    }

    /**
     * Load all user assosicated with entrprise_id having user_type = 4 (Collaboreatur User)
     *
     * @return  Object
     */

    public function CUser()
    {
        return $this->hasMany('App\Models\User','entreprise_id','id')
            ->where('user_type',4);
    }

    /**
     * Get Entreprise data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getEntrepriseByID($id){
        return Entreprise::where('id',$id)->first();
    }

    /**
     * Get Entreprise data From Array of Id
     *
     * @param array $id
     * @return  Object
     */
    public function getMultipleEntrepriseByID($id){
        $id_array = explode(",",$id);
        return Entreprise::whereIn('id',$id_array)->get();
    }

    /**
     * Insert Entreprise data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data)
    {
        //dd($data);
        $logo = "";
        if (isset($data['logo']) && $data['logo'] != "") {
            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'enterprise' . DIRECTORY_SEPARATOR;
            $logo = LaraHelpers::upload_image($filepath, $data['logo'], "");
            //dd($logo);
        }

        $entreprise = Entreprise::create([
            'name' => $data['name'],
            'logo' => $logo,
            'parent_id' => $data['parent_id']
        ]);


        return $entreprise;
    }

    /**
     * Update Entreprise data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data){


        $logo="";
        if (isset($data['logo']) && $data['logo'] != "") {
            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'enterprise' . DIRECTORY_SEPARATOR;
            $logo = LaraHelpers::upload_entreprise_image($filepath, $data['logo'], $data['file_old']);

        }
        else{
            $logo = $data['file_old'];
        }
        $user = array($data['id']);
        $entre_id = 0;
        $entre_id = User::where('id',Auth::user()->id )->select('entreprise_id')->get();
        foreach ($entre_id as $key => $val){
            $entre_id = ($val['entreprise_id']);
            $entre_id = array($entre_id);
            $arra_id = (array_merge($user,$entre_id));
        }
        $ids = implode(",",$arra_id);

        User::where('id',Auth::user()->id )
            ->update([
                 'entreprise_id' => $ids,
            ]);

        return Entreprise::where('id',$data['id'])
            ->update([
                'name' => $data['name'],
                'logo' => $logo,
                'parent_id' => $data['parent_id']
            ]);
    }

    /**
     * Delete Entreprise data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){

            $entreprisedata = User::where('entreprise_id',$id)->count();
            if($entreprisedata > 0){
                return false;
                }
            else{
                return Entreprise::where('id',$id)->delete();
                }
    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata(){

        return $data = Entreprise::onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restoreuser($id){

        return $data=Entreprise::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @param array $id
     * @return  Object
     */

    public function removetrash($id){

        $data=Entreprise::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Get All Entreprise Data
     *
     *
     * @return  Object
     */

    public function getallentreprise(){

        return $data = Entreprise::get();

    }

    /**
     * Get Responasable User
     *
     *
     * @return  Object
     */
    public function getResponsable(){
        return User::where('user_type',3)->get();
    }

    /**
     * Get Array of Entreprise name form EntrepriseIds
     *
     * @param array of ids
     * @return  array

     */

    public function getEntrepriseCount(){
        $id = Auth::user()->id;
        $data = User::select('entreprise_id')->where('id',$id)->first();
        $Entreprises =  $data['entreprise_id'];
        $ids = Entreprise::select('id')->where('parent_id',$Entreprises)->count();
       // dd($ids);
        //$ids =  sizeof(explode("," , $Entreprises));
        return $ids;
    }

    /**
     * Get Array of Entreprise id which contain parent_id
     *
     * @param
     * @return  array of Entreprises which have parent_id = $data['entreprise_id']

     */

    public function getParentEntrepise(){

        $id = Auth::user()->id;
        $data = User::where('id',$id)->first();
        $status = Entreprise::where('parent_id',$data['entreprise_id'])->get();
        return  count($status);
    }


    /**
     * Get Array of Entreprise id which contain parent_id
     *
     * @param
     * @return  array of Entreprises which have parent_id = $data['entreprise_id']

     */

    public function getSubEntrepise($entreprises){
        $childs = array();
        foreach($entreprises as $e){
             $id = $e->id;
            $child=Entreprise::select('name','id','logo','status','parent_id')->where('parent_id',$id)->get();
            array_push($childs,$child);
            }
        return  $childs;
    }

}
