<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\LaraHelpers;
use Auth;
use App\Models\Responsable;
use App\Models\ModuleCategory;
use App\Models\Collaborateur;
use App\Models\EntrepriseCategory;

class Module extends Model

{
    /*
    *  For Soft Delete
    */

    use SoftDeletes;

    //
    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_module';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'title','category_id','file_name','keyword','summary','form_id','assigned_user_module','status','quiz_id','original_name','module_type','reference'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Load all module data
     *
     * @return  Object
     */

    public function init($type){

        if(Auth::user()->user_type == 3)
        {
            //return Module::select('tbl_module.*')->leftJoin('tbl_responsable','tbl_responsable.module_id','=','tbl_module.id')->where('responsable_id',Auth::user()->id)->get();
            $EnterpriseCategory = new EntrepriseCategory();
            $EnterpriseData = $EnterpriseCategory->getModuleThroughEnterprise(Auth::user()->entreprise_id);
            $category_id = [];
            foreach ($EnterpriseData as $entdata)
            {
                if(!empty($entdata->EnterpriseModule) && count($entdata->EnterpriseModule) > 0){
                    $category_id[] = $entdata->EnterpriseModule[0]->category_id;
                }
            }

           return  $ModuelCategoryData = Module::whereIn('id',$category_id)->get();

        }
        elseif(Auth::user()->user_type == 4)
        {
            return Module::select('tbl_module.*')->leftJoin('tbl_collaborateur','tbl_collaborateur.module_id','=','tbl_module.id')->where('collaborateur_id',Auth::user()->id)->get();
        }
        else
        {
            $module_data = Module::select('*');
            if(!empty($type)){
                //$module_data->where('module_type',$type);
                $module_data->where('module_type',$type)->orderBy('updated_at','dsc');
            }
            $module_data = $module_data->get();
            return $module_data;
        }

    }

    /**
     * Get Module which are not assigne to category
     *
     * @param integer $id
     * @return  Object
     */
    public function getmodulebycategoryid($id){

        $module_Data = ModuleCategory::select('module_id')->where('category_id','=',$id)->get();
        $model_ids = [];
        foreach ($module_Data as $row){
            $model_ids[] = $row->module_id;
        }
        return Module::select('title','keyword','id')->whereNotIn('id',$model_ids)->where('module_type','=','F')->get();
    }

    /**
     * Get Module which are not assigne to Resource
     *
     * @param integer $id
     * @return  Object
     */
    public function getmodulebyresourceid($id){

        $module_Data = ModuleResource::select('module_id')->where('resource_id','=',$id)->get();
        $model_ids = [];
        foreach ($module_Data as $row){
            $model_ids[] = $row->module_id;
        }
        return Module::select('title','keyword','id')->whereNotIn('id',$model_ids)->where('module_type','=','R')->get();
    }


    /**
     * Get Module name base on ID
     *
     * @param integer $id
     * @return  Object
     */

    public function getModuleName($id){
        $get = Module::where('id',$id)->first(['title']);
        if($get){
            return $get->title;
        }
        return '-';
    }

    /**
     * Get Module data base on ID
     *
     * @param integer $id
     * @return  Object
     */

    public function getModuleByID($id){
        return Module::where('id',$id)->first();
    }

    /**
     * Get all Module for Collaborateur and Responsable
     *
     * @param integer $id
     * @return  Object
     */

    public function getAllModule($id){
        return Module::where('id',$id)->get();
    }


    /**
     * Insert Module data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function insert($data){

        if (isset($data['file_name']) && $data['file_name'] != "") {
            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;

            $filename = LaraHelpers::upload_image($filepath, $data['file_name'], "");
            $original = $data['file_name']->getClientOriginalName();

        }else{
            $filename="";
            $original  = "";

        }


        $insertedModule =  Module::create([
            'title' => $data['title'],
            'keyword' => $data['keyword'],
            'summary' => $data['summary'],
            'reference' => $data['reference'],
            'file_name' => $filename,
            'original_name' => $original,
            'form_id' => $data['form_id'],
            'status' => $data['status'],
            'quiz_id' => $data['quiz_id'],
            'module_type' => $data['module_type'],
        ]);
        // For inserting Category id  in ModuleCategory Model
        /*
         * @param  array [] category_id
         *
        */

        if($insertedModule){

           if(!isset($data['category_id'])){
           $module_data['module_id']= $insertedModule->id;
           $module_data['category_id']= ['0'];
           $modulecategory = new ModuleCategory();
           return $modulecategory->store($module_data);
           }else{
               $module_data['module_id']= $insertedModule->id;
               $module_data['category_id']= $data['category_id'];
               $modulecategory = new ModuleCategory();
               return $modulecategory->store($module_data);
           }
        }
    }

    /**
     * Update Module data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function edit($data){

        if (isset($data['file_name']) && $data['file_name'] != "") {
            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;

            $filename = LaraHelpers::upload_image($filepath, $data['file_name'], $data['file_old']);
            $original_filename = $data['file_name']->getClientOriginalName();

        }else{
            $filename = "";
            $original_filename = "";
        }
        if(!isset($data['file_name'])){
            $filename = $data['file_old'];
            $original_filename = $data['original_old_filename'];
        }
        if(!isset($data['status'])){
            $data['status']=1;  //set as inactive
        }

        $updateModule =  Module::where('id',$data['id'])
            ->update([
                'title' => $data['title'],
                'keyword' => $data['keyword'],
                'summary' => $data['summary'],
                'reference' => $data['reference'],
                'file_name' => $filename,
                'original_name' => $original_filename ,
                'form_id' => $data['form_id'],
                'status' => $data['status'],
                'quiz_id' => $data['quiz_id'],
                'module_type' => $data['module_type'],
            ]);

        if($updateModule){
            if(!empty($data['category_id'])){

                $module_data['module_id']= $data['id'];
                //$module_data['category_id']= $data['category_id'];
                $modulecategory = new ModuleCategory();

                $oldModuleCateogryData = $modulecategory->getCategoryByModuleId($data['id']);

                // For setting the sort_order
                $categoryData = array();
                foreach ($oldModuleCateogryData as $key =>  $ModuleData)
                {
                    if(in_array($ModuleData->category_id,$data['category_id'])){
                        $categoryData[$key][$ModuleData->sort_order] = $ModuleData->category_id;
                    }
                }
                    if(!empty($categoryData)){
                        $module_data['category_id']= $categoryData;
                        $modulecategory->remove($data['id']);
                        return $modulecategory->updateCategory($module_data);
                    }else{
                        $modulecategory->remove($data['id']);
                        $module_data['category_id']= $data['category_id'];
                        return $modulecategory->store($module_data);
                    }
                }
            else{
                $modulecategory = new ModuleCategory();
                $modulecategory->remove($data['id']);
            }
            return $updateModule;
        }

    }

    /**
     *Soft Delete Category data base on ID
     *
     * Image will get delete when module is deleted
     * @param array $id
     * @return  Object
     */

    public function remove($id){

        return Module::where('id',$id)->delete();
    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata($type){

        $data = Module::onlyTrashed();
        if(!empty($type)){
            $data->where('module_type',$type);
        }
        $data = $data->get();
        return $data;

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restoremodule($id){

        return $data=Module::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     * Image will get delete when module is deleted
     * @param array $id
     * @return  Object
     */

    public function removetrash($id){
        $data1=Module::withTrashed()->where('id', $id);

        $data=Module::where('id','=',$id)->withTrashed()->first();

        if (isset($data['file_name']) && $data['file_name'] != "") {

            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;

            unlink($filepath . DIRECTORY_SEPARATOR . $data['file_name']);


        }
        // For removing module entried from ModuleCategory table

       ModuleCategory::where('module_id',$id)->delete();

        return $data1->forceDelete();
    }

    /**
     * Duplcate data
     *
     *
     * @param int $id
     * @return  Object
     */

    public function duplicate($id){

        $data = Module::find($id);
        $newName = "";
        if(!empty($data['file_name'])){

            $srcpath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR.$data['file_name'];
            $fileExtension = \File::extension($srcpath);
            $newName = time().'.'.$fileExtension;
            $newPathWithName = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR.$newName;
            \File::copy($srcpath , $newPathWithName);
        }


        $duplicatedata['title'] = $data['title']."-Copy";
        $duplicatedata['reference'] = $data['reference']."-Copy";
        $newData = $data->replicate();
        $newData->title =  $duplicatedata['title'];
        $newData->reference =  $duplicatedata['reference'];
        $newData->file_name = $newName;
        $newData->save();
        $modulecategory = ModuleCategory::where ('module_id',$id)->get();

        foreach($modulecategory as $categoryid)
        {
                ModuleCategory::create([
                'category_id' => $categoryid->category_id,
                'module_id' => $newData->id

            ]);
        }
        return $insertedId = $newData->id;


    }

    /**
     * Get Module data base on Id
     * For Front showing
     *
     * @param array[]$id
     * @return  Object
     */

    public function getAllModuleFromId($id){
        return Module::with('getFrom')->where('id',$id)->get();
    }

    /**
     * Load Form data for front cateogry
     *
     * @return  Object
     */
    public function getFrom()
    {
        return $this->hasOne('App\Models\Form','form_id','form_id');
    }

    /**
     * Delete File from the database
     *
     * @param $data
     * @return  Object
     */

    public function unlinkFile($data){

        if (isset($data['file_name']) && $data['file_name'] != "") {

            $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;

            unlink($filepath . DIRECTORY_SEPARATOR . $data['file_name']);


        }
        return Module::where('id',$data['id'])
            ->update([
                'file_name' => "",
                'original_name' => "",
            ]);
    }


    /**
     * Get Module data base on ID for
     * displaying particuiar data on page
     *
     * @param integer $id
     * @return  Object
     */

    public function getModuleDataByID($id){
        return Module::where('id',$id)->get();
    }

}