<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;
use App\Models\Module;
class Responsable extends Model

{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'tbl_responsable';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'user_id','module_id','entreprise_id','responsable_id'
    ];
	
	/**
     * Get Module from assigned module id
     *
     * @param int $id
     *
     * @return  Object
     */
    
	public function index(){
		$id = Auth::user()->id;
		$data = Responsable::select("module_id")->where("responsable_id","=",$id)->get();
		return $data;
    }
	
	/**
     * Insert Module data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    
	public function insert($data){
		
		return Responsable::create([
            'responsable_id' => $data['responsable_id'],
			'user_id' => $data['user_id'],
			'module_id' => $data['module_id'],
            'entreprise_id' => $data['entreprise_id'],
		]);
    }
	
	/**
     * Remove Assigned module
     *
     * @param array $data
     *
     * @return  Object
     */
	
	public function remove($data){
		
		return Responsable::where('user_id', '=', $data['user_id'] )
		->where('module_id', '=', $data['module_id'])
		->delete();
	}
	
	/**
     * Get Assigned responsable module
     *
     * @param array $data
     *
     * @return  Object
     */
	
	public function getmodule($data){
		
		$module_id = $data["module_id"];
		$user_id = $data["user_id"];
		$data = Responsable::select('responsable_id')->where("module_id", '=' ,$module_id)
						 ->where("user_id", '=' ,$user_id)
						 ->get()
						 ->toArray();
		return $data;
	}
	
}
