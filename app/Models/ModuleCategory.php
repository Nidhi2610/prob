<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;
use DB;
use function PHPSTORM_META\type;

class ModuleCategory extends Model

{
    //
    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_module_category';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'category_id','module_id'
    ];

    public function Module()
    {
        return $this->belongsTo('App\Models\Module','module_id','id');
    }

    /**
     * Insert CategoryId data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function store($data){


        foreach($data['category_id'] as $categoryid)
        {
            $moduledata = ModuleCategory::create([
                'category_id' => $categoryid,
                'module_id' => $data['module_id'],

            ]);
        }
        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * For getting List of all category id
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getCategoryByModuleId($id)
    {
        return ModuleCategory::where("module_id",'=', $id)->get();
    }

    /**
     * For deleteing the category Id
     *
     * @return  Object
     */

    public  function  remove($data)
    {

        $module_id = $data;
        ModuleCategory::where('module_id','=', $module_id)
            ->delete();

    }

    /**
     * Insert CategoryId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
     public function storeFromCategory($data){

         foreach($data['module_id'] as $module_id)
         {
             $moduledata = ModuleCategory::create([
                 'category_id' => $data['category_id'],
                 'module_id' => $module_id
             ]);
         }


        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * Insert CategoryId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function deleteFromCategory($data){

        foreach($data['module_id'] as $module_id)
        {
            $moduledata =  ModuleCategory::where('module_id','=', $module_id)
                ->delete();
        }




        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
 * Get Module which are assigned to category
 *
 * @param integer $id
 * @return  Object
 */
    public function getassignedmodulebycategoryid($id){

        return  ModuleCategory::with('Module')->select('tbl_module_category.id','tbl_module_category.module_id','tbl_module_category.category_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->whereIn('category_id',$id)->where('tbl_module.status',0)->where('deleted_at',NULL)->orderBy('sort_order')->get();

    }

    /**
     * Get Module which are assigned to all array of category
     *
     * @param array $id
     * @return  Object
     */
    public function getallModulebyCategory($ids){
        return  ModuleCategory::with('Module'   )->select('tbl_module_category.id','tbl_module_category.module_id','tbl_module_category.category_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->whereIn('tbl_module_category.category_id',$ids)->where('tbl_module.status',0)->where('tbl_module.deleted_at',NULL)->orderBy('tbl_module_category.sort_order')->get();
    }

    /**
     * Get Module which are assigned to category for showing in popup
     * @param integer $id
     * @return  Object
     */
    public function getModalassignedmodulebycategoryid($id){

        return  ModuleCategory::with('Module')->select('tbl_module_category.id','tbl_module_category.module_id','tbl_module_category.category_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->where('category_id','=',$id)->where('deleted_at',NULL)->orderBy('sort_order')->get();

    }

    /**
     * Get Module which are assigned to category for preview detail page
     *
     * @param integer $id
     * @return  Object
     */
    public function getAdminassignedmodulebycategoryid($id){
        return  ModuleCategory::with('Module')->select('tbl_module_category.id','tbl_module_category.module_id','tbl_module_category.category_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->where('category_id','=',$id)->where('deleted_at',NULL)->orderBy('sort_order')->get();
    }

    /**
     * Get Module from Module id for single module prview
     *
     * @param integer $id
     * @return  Object
     */
    public function getModuleByModuleId($id){
        return  ModuleCategory::with('Module')->select('tbl_module_category.id','tbl_module_category.module_id','tbl_module_category.category_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->where('module_id','=',$id)->where('deleted_at',NULL)->distinct()->get();
    }

    /**
     * Insert CategoryId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function duplicateModuleFromCategory($module_id,$category_id){

      $moduledata = ModuleCategory::create([
            'category_id' => $category_id,
            'module_id' => $module_id
        ]);
        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * Get Module which are assigned to category
     *
     * @param integer $id
     * @return  Object
     */
    public function getModuleCategory($id){

        return $module_Data = ModuleCategory::whereIn('category_id',$id)->get();

    }

    /**
     * For getting List of all category
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getModuleCategoryList($id)
    {
        return ModuleCategory::where("module_id",'=', $id)->with('Category')->get();
    }

    public function Category (){
        return $this->hasOne('App\Models\Category','id','category_id');
    }

    /**
     * For sorting order
     *
     * @param array $data
     *
     * @return  Object
     */

    public function updateAllModule(array $data = [])
    {

        $j = 0;
        foreach (explode(',',$data['list']) as $sort_id) {

            ModuleCategory::where('id', $sort_id)->update(["sort_order" => $j]);
            $j++;
        }

        return true;

    }

    /**
     * Update CategoryId data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function updateCategory($data){

        foreach($data['category_id'] as $key => $categoryid)
        {
            foreach ($categoryid as $key1 => $categories){

                $moduledata = ModuleCategory::create([
                    'category_id' => $categories,
                    'sort_order' => $key1,
                    'module_id' => $data['module_id'],

                ]);
            }

        }
        return true;
    }

    /**
     * Get all Module by Category Id
     *
     * @param $id
     *
     * @return  Object
     */
     public function getModuleCategoryId($id){

        return $module_Data = ModuleCategory::where('category_id',$id)->get();

    }

    /**
     * Get all Module by Category Id
     *
     * @return  Object
     */
    public function getAllPublicFormationResource(){

        return $module_Data = ModuleCategory::leftJoin('tbl_category','tbl_category.id','=','tbl_module_category.category_id')->where('tbl_category.category_type_id',2)->where('tbl_category.status','=','0')->count();

    }


    /**
     * For getting List of all category id
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getCategoryByModulesId($id)
    {
        return ModuleCategory::select('category_id','module_id','category_type_id')->where("module_id",'=', $id)->first();
    }

    /**
     * Get latest updated module
     *
     * @param array $categoryArray
     *
     * @return  array
     */

    public function getLoggedInUsersModuleList($categoryArray = array()){
        // for admin user
        if(Auth::user()->user_type == 1){
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title FROM tbl_module_category module_category JOIN tbl_module ON tbl_module.id = module_category.module_id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL GROUP BY module_id ORDER BY tbl_module.updated_at DESC limit 3');
        }
        if(count($categoryArray) > 0 ){
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title FROM tbl_module_category module_category JOIN tbl_module ON tbl_module.id = module_category.module_id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL and ( category.category_type_id = 2  or category.id IN ('.implode(',',$categoryArray).')) GROUP BY module_id ORDER BY tbl_module.updated_at DESC limit 3');
        }else {
            return DB::select('SELECT tbl_module.id as module_id,tbl_module.title FROM tbl_module_category module_category JOIN tbl_module ON tbl_module.id = module_category.module_id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL and category.category_type_id = 2 GROUP BY module_id ORDER BY tbl_module.updated_at DESC limit 3');
        }
    }

    /**
     * Get Recently updated Module which are assigned to category
     *
     * @param integer $id
     * @return  ObjectSELECT tbl_module.id as module_id,tbl_module.title FROM tbl_module_category module_category JOIN tbl_module ON tbl_module.id = module_category.module_id JOIN tbl_category category ON category.id = module_category.category_id where tbl_module.status = 0 and tbl_module.deleted_at IS NULL and category.category_type_id = 2 ORDER BY tbl_module.updated_at DESC
     */
    public function getRecentUpdatedModule($id){

        return  ModuleCategory::with('Module')->select('tbl_module.*')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_category.module_id')->whereIn('category_id',$id)->where('tbl_module.status',0)->where('tbl_module.deleted_at',NULL)->orderBy('tbl_module.updated_at','desc')->take(3)->get();
    }

}
