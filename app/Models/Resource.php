<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ModuleCategory;
use App\Models\EntrepriseCategory;
use Illuminate\Support\Facades\Auth;

class Resource extends Model
{
    /*
	*  For Soft Delete
	*/

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_resource';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id', 'color', 'status', 'resource_type_id', 'sort_order','description','font_awesome'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Load all resource data
     *
     * @return  Object
     */
    public function init()
    {

       return Resource::orderBy('sort_order', 'asc')->get();

    }

    /**
     * Get Resource name base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getResourceName($id)
    {
        $get = Resource::where('id', $id)->first(['name']);
        if ($get) {
            return $get->name;
        }
        return '-';
    }

    /**
     * Get Resource data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getResourceByID($id)
    {
        return Resource::where('id', $id)->first();
    }

    /**
     * Insert Resource data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data)
    {
        return Resource::create([
            'name' => $data['name'],
            'parent_id' => $data['parent_id'],
            'resource_type_id' => $data['resource_type_id'],
            'color' => "#".$data['color'],
            'description' => $data['description'],
            'font_awesome' => $data['font_awesome'],
            'status' => $data['status'],
        ]);
    }

    /**
     * Update Resource data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data)
    {
        return Resource::where('id', $data['id'])
            ->update([
                'name' => $data['name'],
                'parent_id' => $data['parent_resource'],
                'resource_type_id' => $data['resource_type_id'],
                'color' => "#".$data['color'],
                'description' => $data['description'],
                'font_awesome' => $data['font_awesome'],
                'status' => $data['status'],
            ]);
    }

    /**
     * Delete Resource data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id)
    {

        $result = Resource::where('parent_id', $id)->count();
        if ($result > 0) {
            return false;
        } else {
            $resultmodule = ModuleResource::where('resource_id', $id)->count();
            if ($resultmodule > 0) {
                return false;
            }
            return Resource::where('id', $id)->delete();
        }

    }

    public function updateAllResource(array $data = [])
    {

        $j = 0;
        foreach ($data['list'] as $id => $parent_id) {
            if ($parent_id == 'null') {
                $parent_id = 0;
            }
            Resource::where('id', $id)->update(['parent_id' => $parent_id, "sort_order" => $j]);
            $j++;
        }

        return true;

    }

    /**
     * Get the softdelted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function trasheddata()
    {

        return $data = Resource::onlyTrashed()->get();

    }

    /**
     * Restore SoftDeleted Data
     *
     *
     * @param array $id
     * @return  Object
     */

    public function restoreresource($id)
    {

        return $data = Resource::withTrashed()
            ->where('id', $id)
            ->restore();

    }

    /**
     * Delete SoftDeleted Data perpenantly
     *
     *
     * @param array $id
     * @return  Object
     */

    public function removetrash($id)
    {

        $data = Resource::withTrashed()->where('id', $id);
        return $data->forceDelete();
    }

    /**
     * Duplcate Resource data
     *
     *
     * @param int $id
     * @return  Object
     */

    public function duplicate($id)
    {

        $data = Resource::find($id);
        $newData = $data->replicate();
        $newData->name = $data['name'] . "-Copy";
        $insertedId = $newData->save();

        // For replicating the module attached with the resource (Stored in tbl_module_resource)

        $moduleresourceData = ModuleResource::where('resource_id',$id)->get();
        if($moduleresourceData->count() > 0){
            foreach ($moduleresourceData as $modulerow){
                $moduleresource = new ModuleResource();
               $moduleresult = $moduleresource->duplicateModuleFromResource($modulerow->module_id,$newData->id);
            }
        }
        // Ends here for first Condition

        $subResource = Resource::where('parent_id', $id);
        if ($subResource->count() > 0) {
            foreach ($subResource->get() as $row) {
                $subData = Resource::find($row->id);
                $newSubData = $subData->replicate();
                $newSubData->parent_id = $newData->id;
                $newSubData->name = $subData['name'] . "-Copy";
                $newSubData->save();

                // For replicating the module attached with the subresource (Stored in tbl_module_resource)

                $submoduleresourceData = ModuleResource::where('resource_id',$row->id)->get();
                if($submoduleresourceData->count() > 0){
                    foreach ($submoduleresourceData as $submodulerow){
                        $moduleresource = new ModuleResource();
                        $submoduleresult = $moduleresource->duplicateModuleFromResource($submodulerow->module_id,$newSubData->id);
                    }
                }
                // Ends here for second Condition

                $subChildResource = Resource::where('parent_id', $row->id);

                if ($subChildResource->count() > 0) {
                    foreach ($subChildResource->get() as $row_child) {
                        $subChildData = Resource::find($row_child->id);
                        $newSubChildData = $subChildData->replicate();
                        $newSubChildData->parent_id = $newSubData->id;
                        $newSubChildData->name = $subChildData['name'] . "-Copy";
                        $newSubChildData->save();

                        // For replicating the module attached with the subresource (Stored in tbl_module_resource)

                        $submodulechildresourceData = ModuleResource::where('resource_id',$row_child->id)->get();
                        if($submodulechildresourceData->count() > 0){
                            foreach ($submodulechildresourceData as $submodulechildrow){
                                $moduleresource = new ModuleResource();
                                $submodulechildresult = $moduleresource->duplicateModuleFromResource($submodulechildrow->module_id,$newSubChildData->id);
                            }
                        }
                        // Ends here for third Condition

                    }

                }

            }

        }

        if($insertedId){
            return $insertedId;
        }
        return false;

    }

    /**
     * Load resource data for Responsable
     *
     * @return  Object
     */
    public function getResourceByEnterpriseId($entreprise_id)
    {
        $EntrepriseResource = new EntrepriseResource();

       // $resource_data = $EntrepriseResource->getEntrepriseById('entreprise_id',$entreprise_id);
        $resource_data = $EntrepriseResource->getMultipleEntrepriseById('entreprise_id',$entreprise_id);

        $resource_id = [];
        foreach ($resource_data as $row){
            $resource_id[] = $row->resource_id;
        }
       return Resource::with('getResource')->whereIn('id',$resource_id)->where('parent_id',0)->where('resource_type_id',1)->get();

    }

    /**
     * Load resource data for Responsable
     *
     * @return  Object
     */
    public function getResource()
    {

        return $this->hasOne('App\Models\EntrepriseResource');

    }

    /**
     * Load resource data for Collaborateur
     *
     * @return  Object
     */
    public function getCollaborateurResource($collaborateur_id)
    {
        $CollaborateurAssignedResource = new CollaborateurAssignedResource();

        $resource_data = $CollaborateurAssignedResource->getResourceByCollaborateurId('collaborateur_id',$collaborateur_id);

        $resource_id = [];
        foreach ($resource_data as $row){
            $resource_id[] = $row->resource_id;
        }
        return Resource::with('getResourceForCollaborateur')->whereIn('id',$resource_id)->where('parent_id',0)->where('status',0)->get();

    }

    /**
     * Load resource data for Collaborateur for binding
     *
     * @return  Object
     */
    public function getResourceForCollaborateur()
    {

        return $this->hasOne('App\Models\CollaborateurAssignedResource');

    }

    /**
     * Load resource data where type is Public
     *
     * @return  Object
     */
    public function getPublicResource()
    {

        return Resource::where('resource_type_id',2)->where('parent_id',0)->where('status',0)->get();


    }

    /**
     * Load child resource data From parent_id
     *
     * @return  Object
     */
    public function getChildResourceByParentId($parent_id)
    {

        return Resource::where('parent_id',$parent_id)->where('status',0)->get();

    }

    /*
     * Get the Parent_id from id
     *
     * @param $id
     * @return Object
     * */
    public function getParentId($id)
    {

        return Resource::where('parent_id',$id)->where('status',0)->get();

    }

}
