<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
class LikeCategory extends Model
{

    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_category_like';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id','category_id','is_liked'
    ];

    /**
     * Insert Comments
     *
     * @return  Object
     */

    public function insert($data)
    {
        $likedData = $this->getUserLikeCategory($data['category_id']);

        if(count($likedData) > 0){
            if($likedData->is_liked == 0){
              $isliked = 1 ;
            }else{
                $isliked = 0 ;
            }
            return LikeCategory::where('id','=',$likedData->id)->update([
                'is_liked'=>$isliked
            ]);

        }else{
            return LikeCategory::create([
                'user_id' => Auth::user()->id,
                'category_id' => $data['category_id'],
                'is_liked'=>1
            ]);
        }

    }

    /**
     * Get Counts
     *
     * @return  Object
     */

    public function getCount($category_id){


        return LikeCategory::where('category_id',$category_id)->where('is_liked',1)->count();
        //return Comments::with('User')->where('category_id',$category_id)->where('entreprise_id',Auth::user()->entreprise_id)->get();
    }
    /**
     * Get Counts
     *
     * @return  integer
     */

    public function getLikeStatus($category_id){


         $like_count = LikeCategory::select('is_liked')->where('category_id',$category_id)->where('user_id','=',Auth::user()->id)->first();
        return empty($like_count)?'0':$like_count->is_liked;
        //return Comments::with('User')->where('category_id',$category_id)->where('entreprise_id',Auth::user()->entreprise_id)->get();
    }

    /**
     * Get the user LikeCategory
     *
     * */
    public function getUserLikeCategory($category_id)
    {
        return LikeCategory::where('user_id','=',Auth::user()->id)->where('category_id','=',$category_id)->first();

    }
}
