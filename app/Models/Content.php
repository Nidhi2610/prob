<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Content extends Model
{
    //
    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_setting';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content'
    ];

    /**
     * get Master Brochure
     *
     *
     * @return mixed
     */
    public function init()
    {
        $contentData = Content::first();
        if($contentData){
            return $contentData;
        }
        else{
            $content = new Content;
            $content->content = "Edit Content Here";
            $content->save();
            return $content;
        }
    }

    /**
     * Update Category data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data)
    {
        return Content::where('id', $data['id'])
            ->update([
                'content' => $data['content'],
            ]);
    }

    /**
     * Get Content Data
     *
     * @return  Object
     */
    public function getcontentData()
    {  //print_r( Content::first());exit;
        return Content::first();
    }
}
