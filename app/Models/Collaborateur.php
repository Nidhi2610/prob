<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;
class Collaborateur extends Model

{
    //
	/**
     * Define table name.
     *
     * @var string
     */
	 
    public $table = 'tbl_collaborateur';
    
	/**
     * Define Primary key field name.
     *
     * @var string
     */
    
	protected $primaryKey = 'id';
    
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'user_id','module_id','entreprise_id','collaborateur_id'
    ];
	
	/**
     * Get Module from assigned module id
     *
     * @param int $id
     *
     * @return  Object
     */
    
	public function index($id){
		$Module = new Module;
		return $Module->getAllModule($id);
    }
	
	/**
     * Insert Assignedmodule to database
     *
     * @param array $data
     * Also Delete if same data is found
     * @return  Object
     */
    
	public function insert($data){
		
		return Collaborateur::create([
            'collaborateur_id' => $data['collaborateur_id'],
			'user_id' => $data['user_id'],
			'module_id' => $data['module_id'],
            'entreprise_id' => $data['entreprise_id'],
		]);
    }
	
	/**
     * Remove Assignedmodule to database
     *
     * @param array $data
     * 
     * @return  Object
     */
	
	public function remove($data){
		
		return Collaborateur::where('user_id', '=', $data['user_id'] )
		->where('module_id', '=', $data['module_id'])
		->delete();
	}
	
	/**
     * Get Assigned Module from collaborateur table
     *
     * @param array $data
     * 
     * @return  Object
     */
	 
	public function getmodule($data){
		
		$module_id = $data["module_id"];
		$user_id = $data["user_id"];
		$data = Collaborateur::select('collaborateur_id AS responsable_id')->where("module_id", '=' ,$module_id)
						 ->where("user_id", '=' ,$user_id)
						 ->get()
						 ->toArray();
		return $data;
	}	
	
}
