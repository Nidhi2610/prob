<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmailTemplate extends Model
{
    /*
	*  For Soft Delete
	*/

    use SoftDeletes;

    /**
     * Define table name.
     *
     * @var string
     */
    public $table = 'tbl_emailtemplate';
    /**
     * Define Primary key field name.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_content','name'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Load all Email Template data
     *
     * @return  Object
     */
    public function init(){
        return EmailTemplate::get();
    }

    /**
     * Insert Email Template data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function insert($data){

        return EmailTemplate::create([
            'name' => $data['name'],
            'email_content' => $data['email_content'],

        ]);
    }

    /**
     * Get Template data base on ID
     *
     * @param integer $id
     * @return  Object
     */
    public function getTemplateByID($id){
        return EmailTemplate::where('id',$id)->first();
    }

    /**
     * Update Email Template data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function edit($data){
        return EmailTemplate::where('id',$data['id'])
            ->update([
                'name' => $data['name'],
                'email_content' => $data['email_content'],
            ]);
    }

    /**
     * Get Email template by slug
     *
     * @param array $data
     *
     * @return  Object
     */
    public function GetTemplateBySlug($data){
        return EmailTemplate::where('slug','=',$data)->get(['email_content']);

    }
}
