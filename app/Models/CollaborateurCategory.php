<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;


class CollaborateurCategory extends Model
{

    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_collaborateur_category';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'collaborateur_id','user_id','category_id','type','parent_id',
    ];



    /**
     * Insert Category_id data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function insert($data){

        return CollaborateurCategory::create([
            'collaborateur_id' => $data['collaborateur_id'],
            'user_id' => $data['user_id'],
            'category_id' => $data['category_id'],
            'type' => $data['type'],
            'parent_id' => $data['parent_id']

        ]);
    }

    /**
     * Remove Assigned Collaborateur
     *
     * @param array $data
     *
     * @return  Object
     */

    public function remove($data){

        return CollaborateurCategory::where('category_id', '=', $data['category_id'] )
            ->delete();
    }

    /**
     * Get Assigned Collaborateur
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getuser($fieldname, $id){
        return CollaborateurCategory::where($fieldname, $id)->get();
    }

    /**
     * Get all collaborateur  Formation resource
     *
     * @return  Object
     */
    public function getAllColloborateurFormationResource($id){

        return CollaborateurCategory::join('tbl_module_category','tbl_module_category.category_id','=','tbl_collaborateur_category.category_id')->where('collaborateur_id',$id)->count();

    }

    /**
     * Get all collaborateur  Formation resource
     *
     * @return  Object
     */
    public function getAllColloborateurFormationResourceData($id){

       $data = CollaborateurCategory::join('tbl_module_category','tbl_module_category.category_id','=','tbl_collaborateur_category.category_id')->where('user_id',$id)->count();


       return $data;
    }
}
