<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\LaraHelpers;
use Auth;
class ModuleResource extends Model

{
    //
    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'tbl_module_resource';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'resource_id','module_id'
    ];

    public function Module()
    {
        return $this->belongsTo('App\Models\Module','module_id','id');
    }
    /**
     * Insert ResourceId data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function store($data){


        foreach($data['resource_id'] as $resourceid)
        {
            $moduledata = ModuleResource::create([
                'resource_id' => $resourceid,
                'module_id' => $data['module_id'],

            ]);
        }
        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * For getting List of all resource id
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getResourceByModuleId($id)
    {
        return ModuleResource::where("module_id",'=', $id)->get();
    }

    /**
     * For deleteing the resource Id
     *
     * @return  Object
     */

    public  function  remove($data)
    {

        $module_id = $data;
        ModuleResource::where('module_id','=', $module_id)
            ->delete();

    }

    /**
     * Insert ResourceId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
     public function storeFromResource($data){

         foreach($data['module_id'] as $module_id)
         {
             $moduledata = ModuleResource::create([
                 'resource_id' => $data['resource_id'],
                 'module_id' => $module_id
             ]);
         }


        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * Insert ResourceId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function deleteFromResource($data){

        foreach($data['module_id'] as $module_id)
        {
            $moduledata =  ModuleResource::where('module_id','=', $module_id)
                ->delete();
        }




        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * Get Module which are assigned to resource
     *
     * @param integer $id
     * @return  Object
     */
    public function getassignedmodulebyresourceid($id){

        return  ModuleResource::with('Module')->select('tbl_module_resource.id','tbl_module_resource.module_id','tbl_module_resource.resource_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_resource.module_id')->where('resource_id','=',$id)->where('tbl_module.status',0)->where('deleted_at',NULL)->orderBy('sort_order')->get();

    }

    /**
     * Get Module which are assigned to resource for showing in popup
     * @param integer $id
     * @return  Object
     */
    public function getModalassignedmodulebyresourceid($id){

        return  ModuleResource::with('Module')->select('tbl_module_resource.id','tbl_module_resource.module_id','tbl_module_resource.resource_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_resource.module_id')->where('resource_id','=',$id)->where('deleted_at',NULL)->orderBy('sort_order')->get();

    }

    /**
     * Get Module which are assigned to resource for preview detail page
     *
     * @param integer $id
     * @return  Object
     */
    public function getAdminassignedmodulebyresourceid($id){
        return  ModuleResource::with('Module')->select('tbl_module_resource.id','tbl_module_resource.module_id','tbl_module_resource.resource_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_resource.module_id')->where('resource_id','=',$id)->where('deleted_at',NULL)->orderBy('sort_order')->get();
    }

    /**
     * Get Module from Module id for single module prview
     *
     * @param integer $id
     * @return  Object
     */
    public function getModuleByModuleId($id){
        return  ModuleResource::with('Module')->select('tbl_module_resource.id','tbl_module_resource.module_id','tbl_module_resource.resource_id','tbl_module.status')->leftJoin('tbl_module','tbl_module.id','=','tbl_module_resource.module_id')->where('module_id','=',$id)->where('deleted_at',NULL)->distinct()->get();
    }

    /**
     * Insert ResourceId data to database
     *
     * @param array $data
     *
     * @return  Object
     */
    public function duplicateModuleFromResource($module_id,$resource_id){

      $moduledata = ModuleResource::create([
            'resource_id' => $resource_id,
            'module_id' => $module_id
        ]);
        if($moduledata)
        {
            return $moduledata;
        }
    }

    /**
     * Get Module which are assigned to resource
     *
     * @param integer $id
     * @return  Object
     */
    public function getModuleResource($id){

        return $module_Data = ModuleResource::whereIn('resource_id',$id)->get();

    }

    /**
     * For getting List of all resource
     *
     * @param array $data
     *
     * @return  Object
     */

    public function getModuleResourceList($id)
    {
        return ModuleResource::where("module_id",'=', $id)->with('Resource')->get();
    }

    public function Resource (){
        return $this->hasOne('App\Models\Resource','id','resource_id');
    }

    /**
     * For sorting order
     *
     * @param array $data
     *
     * @return  Object
     */

    public function updateAllModule(array $data = [])
    {

        $j = 0;
        foreach (explode(',',$data['list']) as $sort_id) {

            ModuleResource::where('id', $sort_id)->update(["sort_order" => $j]);
            $j++;
        }

        return true;

    }

    /**
     * Update ResourceId data to database
     *
     * @param array $data
     *
     * @return  Object
     */

    public function updateResource($data){

        foreach($data['resource_id'] as $key => $resourceid)
        {
            foreach ($resourceid as $key1 => $categories){

                $moduledata = ModuleResource::create([
                    'resource_id' => $categories,
                    'sort_order' => $key1,
                    'module_id' => $data['module_id'],

                ]);
            }

        }
        return true;
    }


}
