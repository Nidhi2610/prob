<?php

namespace App\Models;

use App\Helpers\LaraHelpers;
use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\User;
use App\Models\Comments;


class LikeComments extends Model

{
    // For soft Delete
   // use SoftDeletes;
    /**
     * Define table name.
     *
     * @var string
     */

    public $table = 'like_comments';

    /**
     * Define Primary key field name.
     *
     * @var string
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id', 'comment_id'
    ];



    /**
     * check if the comment is Likes
     *  Insert Likes
     * @return  boolean
     *   return 1 if already liked else 0
     */
    public function like($data){
            $isliked = LikeComments::where('user_id', Auth::user()->id)
            ->where('comment_id', $data)
            ->where('counter',1)
            ->first();

        if(!empty($isliked)){
            LikeComments::where('user_id', Auth::user()->id)
                ->where('comment_id', $data)
                ->where('counter', 1)
                ->delete();
            //DB::Table('tbl_module_comment')->where('id',$data)->decreament('counter');
        }else{
            LikeComments::create([
                'user_id' => Auth::user()->id,
                'comment_id' => $data,
                'counter' => 1
            ]);

          //  DB::Table('tbl_module_comment')->where('id',$data)->increament('counter');
        }
    }
    /**
     * Counte  the  like of given comment_id
     *
     * @param  $comment_id
     * @return  $counter total count
     */
    public function getCounter($data){
     // $data = $data['id'];
      $counter = LikeComments::where('comment_id',$data)->where('counter',1)->groupBy('counter')->count();
     //print_r($counter);die;
        return $counter;
    }



}