<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Comments;
use Carbon\Carbon;
use Log;


class CommentDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:CommentDelete';
    protected $comments;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all Comment older then 30 Days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Comments $comments)
    {
        parent::__construct();
        $this->comments = $comments;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // For deleteing the comment greater then 30 Days

        $current_data = Carbon::now();
        $back_date = date('Y-m-d H:i:s', strtotime($current_data.'-30 day'));
        $comment_data = Comments::where('created_at','<',$back_date)->delete();
        if($comment_data){
            Log::info($back_date."CommentDelete : for this date is deleted");
        }
    }
}
