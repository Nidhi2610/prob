<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Bookmark;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CollaborateurAssignedCategory;
use App\Models\CollaborateurCategory;
use App\Models\Content;
use App\Models\Entreprise;
use App\Models\EntrepriseCategory;
use App\Models\ModuleCategory;
use App\Models\Comments;
use App\Models\Module;
use App\Models\User;
use App\Models\UserModuleComplete;
use Illuminate\Http\Request;
use Auth;
use App\Models\Form;
use App\Models\Quiz;
use Validator;
use App\Helpers\LaraHelpers;
use App\Models\UserModuleCategoryForm;
use App\Models\UserModuleCategoryQuiz;
use App\Models\LikeComments;
use App\Models\LikeCategory;
use App\Models\getMostAccessedModule;
use DB;

class IndexController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $category;
    protected $modulecategory;
    protected $module;
    protected $form;
    protected $quiz;
    protected $comments;
    protected $entreprise;
    protected $user;
    protected $bookmark;
    protected $user_module_category_form;
    protected $user_module_category_quiz;
    protected $collaborateur_assigned_category;
    protected $user_module_complete;
    protected $like_comments;
    protected $aftercomments;
    protected $getMost_accessedModule;

    public function __construct(Category $category, ModuleCategory $modulecategory, Module $module, Form $form, Quiz $quiz, Comments $comments, UserModuleCategoryForm $user_module_category_form, UserModuleCategoryQuiz $user_module_category_quiz, Entreprise $entreprise, User $user, Content $content, CollaborateurAssignedCategory $collaborateur_assigned_category, Bookmark $bookmark, UserModuleComplete $user_module_complete, likeComments $like_comments, likeCategory $likeCategory, getMostAccessedModule $getMost_accessedModule)
    {
        $this->middleware('auth');
        $this->category = $category;
        $this->module = $module;
        $this->form = $form;
        $this->quiz = $quiz;
        $this->user = $user;
        $this->bookmark = $bookmark;
        $this->modulecategory = $modulecategory;
        $this->comments = $comments;
        $this->entreprise = $entreprise;
        $this->collaborateur_assigned_category = $collaborateur_assigned_category;
        $this->user_module_category_form = $user_module_category_form;
        $this->user_module_category_quiz = $user_module_category_quiz;
        $this->user_module_complete = $user_module_complete;
        $this->like_comments = $like_comments;
        $this->likeCategory = $likeCategory;
        $this->getmost_accessed_module = $getMost_accessedModule;
    }


    /**
     * Get List of Category assigned to Collaborateur.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        /*$privateCategoryData =  $this->category->getCollaborateurCategory(Auth::user()->id)->toArray();
        $publicCategoryData = $this->category->getPublicCategory()->toArray();
        $data = array_merge($privateCategoryData,$publicCategoryData);
        $enterpriseId = $this->user->getUserByID(Auth::user()->id);
        $enterpriseLogo = $this->entreprise->getEntrepriseByID($enterpriseId->entreprise_id);
        return view('front.home',["data"=>$data,"entreprise_log" => $enterpriseLogo]);*/

        // For Responsable user Category List

        $bookmark_count = $this->bookmark->getCount(Auth::user()->id);

        if (Auth::user()->user_type == 3) {
            $publicResourceData = $this->category->getPublicResource()->toArray();
            if (count($publicResourceData) > 0) {
                foreach ($publicResourceData as $key => $data) {
                    $childDataResourceData = $this->category->getallResourceById($data['id'])->toArray();
                    $sub_child_arrs = [];
                    if (count($childDataResourceData) > 0) {
                        foreach ($childDataResourceData as $key_child => $key_child_data) {
                            if ($key_child_data['category_type_id'] == 1) {
                                $en = EntrepriseCategory::select('entreprise_id')->where('category_id', $key_child_data['id'])->get()->toArray();
                                if (count($en) > 0) {
                                    foreach ($en as $e) {
                                        if (Auth::user()->entreprise_id == $e['entreprise_id']) {
                                            $sub_child_arrs[] = $key_child_data;
                                            $subChildDataResourceData = $this->category->getallResourceById($key_child_data['id'])->toArray();
                                            if (count($subChildDataResourceData) > 0) {
                                                foreach ($subChildDataResourceData as $key_sub_child => $key_sub_child_data) {
                                                    if( $key_sub_child_data['category_type_id'] == 1) {
                                                        $ents = EntrepriseCategory::select('entreprise_id')->where('category_id',$key_sub_child_data['id'])->get()->toArray();
                                                        if(count($ents)>0) {
                                                            foreach ($ents as $ent) {
                                                                if (Auth::user()->entreprise_id == $ent['entreprise_id']) {
                                                                    $sub_child_arrs[] = $key_sub_child_data;
                                                                }
                                                            }
                                                        }
                                                        $sub_child_arrs[] = $key_sub_child_data;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }else {
//                                $subChildDataResourceData = $this->category->getResourceById($key_child_data['id'])->toArray();
//                                if (count($subChildDataResourceData) > 0) {
//                                    foreach ($subChildDataResourceData as $key_sub_child => $key_sub_child_data) {
                                        $sub_child_arrs[] = $key_child_data;
//                                    }
//                                }
                            }

                        }

                    }
                    $publicResourceData[$key]['ChildArray'] = $sub_child_arrs;
                }
            }

            $publicFormationData = $this->category->getPublicFormation()->toArray();
            if (count($publicFormationData) > 0) {
                foreach ($publicFormationData as $public_for_key => $publ_for_data) {
                    $childDataFormationData = $this->category->getFormationById($publ_for_data['id'])->toArray();
                    $sub_child_formation_arrs = [];
                    if (count($childDataFormationData) > 0) {
                        foreach ($childDataFormationData as $key_child => $key_formation_child_data) {
                                if( $key_formation_child_data['category_type_id'] == 1){
                                $en = EntrepriseCategory::select('entreprise_id')->where('category_id',$key_formation_child_data['id'])->get()->toArray();
                                if(count($en)>0){
                                    foreach($en as $e){
                                        if(Auth::user()->entreprise_id == $e['entreprise_id']){
                                            $sub_child_formation_arrs[] = $key_formation_child_data;
                                            $subChildDataFormationData = $this->category->getResourceById($key_formation_child_data['id'])->toArray();
                                            if (count($subChildDataFormationData) > 0) {
                                                foreach ($subChildDataFormationData as $key_sub_for_child => $key_sub_for_child_data) {
                                                    if( $key_sub_for_child_data['category_type_id'] == 1) {
                                                        $ents = EntrepriseCategory::select('entreprise_id')->where('category_id',$key_sub_for_child_data['id'])->get()->toArray();
                                                        if(count($ents)>0) {
                                                            foreach ($ents as $ent) {
                                                                if (Auth::user()->entreprise_id == $ent['entreprise_id']) {
                                                                    $sub_child_formation_arrs[] = $key_sub_for_child_data;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            } else{
                                $sub_child_formation_arrs[] = $key_formation_child_data;
                                $subChildDataFormationData = $this->category->getResourceById($key_formation_child_data['id'])->toArray();
                                if (count($subChildDataFormationData) > 0) {
                                    foreach ($subChildDataFormationData as $key_sub_for_child => $key_sub_for_child_data) {
                                        $sub_child_formation_arrs[] = $key_sub_for_child_data;
                                    }
                                }
                            }

                        }
                    }
                    $publicFormationData[$public_for_key]['ChildArray'] = $sub_child_formation_arrs;
                }
            }

            $privateResourceData = $this->category->getResourceByEnterpriseId(Auth::user()->entreprise_id)->toArray();
            if (count($privateResourceData) > 0) {
                foreach ($privateResourceData as $private_res_key => $data) {
                    $childPrivateDataResourceData = $this->category->getResourceById($data['id'])->toArray();
                    $sub_private_child_arrs = [];
                    if (count($childPrivateDataResourceData) > 0) {
                        foreach ($childPrivateDataResourceData as $key_private_child => $key_private_child_data) {
                            $sub_private_child_arrs[] = $key_private_child_data;
                            $subChildPrivateDataResourceData = $this->category->getResourceById($key_private_child_data['id'])->toArray();
                            if (count($subChildPrivateDataResourceData) > 0) {
                                foreach ($subChildPrivateDataResourceData as $key_sub_private_child => $key_sub_private_child_data) {
                                    $sub_private_child_arrs[] = $key_sub_private_child_data;
                                }
                            }
                        }
                    }

                }
            }

            $privateFormationData = $this->category->getFormationByEnterpriseId(Auth::user()->entreprise_id)->toArray();
            if (count($privateFormationData) > 0) {
                foreach ($privateFormationData as $private_for_key => $private_for_data) {
                    $childPrivateDataFormationData = $this->category->getFormationById($private_for_data['id'])->toArray();

                    $sub_private_for_child_arrs = [];
                    if (count($childPrivateDataFormationData) > 0) {
                        foreach ($childPrivateDataFormationData as $key_private_for_child => $key_private_for_child_data) {
                            $sub_private_for_child_arrs[] = $key_private_for_child_data;
                            $subChildPrivateDataFormationData = $this->category->getFormationById($key_private_for_child_data['id'])->toArray();
                            if (count($subChildPrivateDataFormationData) > 0) {
                                foreach ($subChildPrivateDataFormationData as $key_sub_private_child => $key_sub_private_for_child_data) {
                                    $sub_private_for_child_arrs[] = $key_sub_private_for_child_data;
                                }
                            }
                        }
                    }

                    $privateFormationData[$private_for_key]['ChildArray'] = $sub_private_for_child_arrs;

                }

            }

            $Resourcedata = array_merge( $publicResourceData,$privateResourceData);
            $Formationdata = array_merge($publicFormationData, $privateFormationData);

            // For getting the list of bookmark category

            $bookmark_data = $this->bookmark->getUserBookmark(Auth::user()->id)->toArray();
            $bookmark_id = [];
            if (count($bookmark_data) > 0) {
                foreach ($bookmark_data as $bookmark) {
                    $bookmark_id[] = $bookmark['formation_id'];
                }
            }
        }

        // For Collobaorateur User List of Category

        if (Auth::user()->user_type == 4) {

            $publicResourceData = $this->category->getPublicResource()->toArray();
            if (count($publicResourceData) > 0) {
                foreach ($publicResourceData as $key => $data) {
                    $childDataResourceData = $this->category->getResourceById($data['id'])->toArray();
                    $sub_child_arrs = [];
                    if (count($childDataResourceData) > 0) {
                        foreach ($childDataResourceData as $key_child => $key_child_data) {
                            $sub_child_arrs[] = $key_child_data;
                            $subChildDataResourceData = $this->category->getResourceById($key_child_data['id'])->toArray();
                            if (count($subChildDataResourceData) > 0) {
                                foreach ($subChildDataResourceData as $key_sub_child => $key_sub_child_data) {
                                    $sub_child_arrs[] = $key_sub_child_data;
                                }
                            }
                        }
                    }

                    $publicResourceData[$key]['ChildArray'] = $sub_child_arrs;
                }
            }

            $publicFormationData = $this->category->getPublicFormation()->toArray();
            if (count($publicFormationData) > 0) {
                foreach ($publicFormationData as $public_for_key => $publ_for_data) {
                    $childDataFormationData = $this->category->getFormationById($publ_for_data['id'])->toArray();
                    $sub_child_formation_arrs = [];
                    if (count($childDataFormationData) > 0) {
                        foreach ($childDataFormationData as $key_child => $key_formation_child_data) {
                            $sub_child_formation_arrs[] = $key_formation_child_data;
                            $subChildDataFormationData = $this->category->getResourceById($key_formation_child_data['id'])->toArray();
                            if (count($subChildDataFormationData) > 0) {
                                foreach ($subChildDataFormationData as $key_sub_for_child => $key_sub_for_child_data) {
                                    $sub_child_formation_arrs[] = $key_sub_for_child_data;
                                }
                            }
                        }
                    }

                    $publicFormationData[$public_for_key]['ChildArray'] = $sub_child_formation_arrs;
                }
            }
            // For private Resource Data

            $privateResourceData = $this->collaborateur_assigned_category->getCollaborateurResFor('collaborateur_id', Auth::user()->id, 'R')->toArray();
            $assignedCategoryIds = [];
            foreach ($privateResourceData as $resourceData) {
                $assignedCategoryIds[] = $resourceData['category_id'];
            }
            //dd($assignedCategoryIds);
            $categoryData = $this->category->getParent('parent_id', $assignedCategoryIds)->toArray();
            //dd($categoryData);
            $assignedSubParentIds = [];
            $assignedSubCatIds = [];
            if (count($categoryData) > 0) {
                foreach ($categoryData as $cateogry) {
                    $assignedSubParentIds[] = $cateogry['parent_id'];
                    $assignedSubCatIds[] = $cateogry['id'];
                }

                $parentChildId = array_merge($assignedSubParentIds, $assignedSubCatIds);
                $parentId = array_unique($assignedSubParentIds);
                $childIds = array_diff($parentId, $assignedSubCatIds);
                $getParentId = array_diff($assignedCategoryIds, $parentChildId);
                $finalParentId = array_merge($childIds, $getParentId);
                $finalParentIdData = $this->category->getParent('id', $finalParentId)->toArray();

            } else {
                $finalParentIdData = [];
            }
            // For Private Formation Data

            $privateFormationData = $this->collaborateur_assigned_category->getCollaborateurResFor('collaborateur_id', Auth::user()->id, 'F')->toArray();
            $assignedFormationCategoryIds = [];
            foreach ($privateFormationData as $FormationData) {
                $assignedFormationCategoryIds[] = $FormationData['category_id'];
            }
            $categoryFormationData = $this->category->getParent('parent_id', $assignedFormationCategoryIds)->toArray();
            $assignedFormationSubParentIds = [];
            $assignedFormationSubCatIds = [];
            if (count($categoryFormationData) > 0) {
                foreach ($categoryFormationData as $cateogryFormation) {
                    $assignedFormationSubParentIds[] = $cateogryFormation['parent_id'];
                    $assignedFormationSubCatIds[] = $cateogryFormation['id'];
                }

                $parentFormationChildId = array_merge($assignedFormationSubParentIds, $assignedFormationSubCatIds);
                $parentFormationId = array_unique($assignedFormationSubParentIds);
                $childFormationIds = array_diff($parentFormationId, $assignedFormationSubCatIds);
                $getFormationParentId = array_diff($assignedFormationCategoryIds, $parentFormationChildId);
                $finalFormationParentId = array_merge($childFormationIds, $getFormationParentId);

                $finalFormationParentIdData = $this->category->getParent('id', $finalFormationParentId)->toArray();

            } else {
                $finalFormationParentIdData = [];
            }
            // For getting the list of bookmark category

            $bookmark_data = $this->bookmark->getUserBookmark(Auth::user()->id)->toArray();
            $bookmark_id = [];
            if (count($bookmark_data) > 0) {
                foreach ($bookmark_data as $bookmark) {
                    $bookmark_id[] = $bookmark['formation_id'];
                }
            }

            $index = "active";
            return view('front1.collaborateur-home', ["finalParentIdData" => $finalParentIdData, "finalFormationParentIdData" => $finalFormationParentIdData, "assignedCategoryIds" => $assignedCategoryIds, "assignedFormationCategoryIds" => $assignedFormationCategoryIds, "index" => $index, "bookmark_count" => $bookmark_count, "bookmark_id" => $bookmark_id, "publicFormationData" => $publicFormationData, "publicResourceData" => $publicResourceData]);
        }

        $index = "active";
        $userObj = $this->user->init();
        $last_login_colloborateur_count = $this->user->getRecentLogin(Auth::user()->id);
        $getpublicFormationResourcedata = $this->modulecategory->getAllPublicFormationResource();
        return view('front1.home', ["publicFormationData" => $Formationdata, "publicResourceData" => $Resourcedata, "index" => $index, "userObj" => $userObj, "bookmark_count" => $bookmark_count, "bookmark_id" => $bookmark_id, "getpublicFormationResourcedata" => $getpublicFormationResourcedata, "last_login_colloborateur_count" => $last_login_colloborateur_count]);
    }

    /**
     * Add user From responsable
     *
     *
     * */
    public function userlist()
    {
        $data = $this->user->init();
        $userlist = "active";
        $getpublicFormationResourcedata = $this->modulecategory->getAllPublicFormationResource();
        return view('front1/userlist', ["data" => $data, "getpublicFormationResourcedata" => $getpublicFormationResourcedata, "userlist" => $userlist]);
    }

    /**
     * Get assigned CollaborateurUser.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function getCollaborateurAssigendUser(Request $request)
    {
        $collaborateurcategory = new CollaborateurCategory();
        $collaborateuruser = $collaborateurcategory->getuser('category_id', $request->id);
        return $collaborateuruser;
    }

    /**
     * Store assigned category to colloboratuer.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function insertcollaborateurAssignedCategory(Request $request)
    {

        $category_id = $request->category_id;
        $type = $request->type;
        $parent_id = $request->parent_id;
        $user_id = Auth::user()->id;
        $collaborateurcategory = new CollaborateurCategory();
        $delete = $collaborateurcategory->remove([
            'category_id' => $category_id,
        ]);

        if ($request->collaborateur_id == null) {
            $create = true;
        } else {

            foreach ($request->collaborateur_id as $collaborateur_id) {
                $collaborateurcategory = new CollaborateurCategory();
                $create = $collaborateurcategory->insert([

                    'category_id' => $category_id,
                    'collaborateur_id' => $collaborateur_id,
                    'user_id' => $user_id,
                    'type' => $type,
                    'parent_id' => $parent_id,

                ]);
            }

        }

        if (!$create) {
            $request->session()->flash('fail', __('probtp.fail_message'));
        } else {
            $request->session()->flash('success', __(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect('/index');
    }

    /**
     * Detail page of cateogry
     *
     * @return \Illuminate\Http\Response
     */

    public function detail($id, $category_id)
    {
        //print_r($category_id);exit;
        $category_array = explode(":", $category_id);
        $parent_category_data = [];
        $moduleid_data = [];
        foreach ($category_array as $categoryKey => $categoryData) {

            $moduleid_datas = $this->modulecategory->getallModulebyCategory([$categoryData]);
            //  if(count($moduleid_datas) > 0){
            $moduleid_data[$categoryKey]['category_name'] = $this->category->getCategoryByID($categoryData)->name;
            $moduleid_data[$categoryKey]['description'] = $this->category->getCategoryByID($categoryData)->description;
            $moduleid_data[$categoryKey]['category_id'] = $categoryData;
            if ($categoryKey != 0) {
                $parent_category_data[] = $this->category->getCategoryByID($categoryData);
            }

            foreach ($moduleid_datas as $key => $module) {

                $moduleid_datas[$key]['form_data'] = $this->form->getFormByID($module->Module->form_id);
                $moduleid_datas[$key]['quiz_data'] = $this->quiz->getQuizByID($module->Module->quiz_id);
                $moduleid_datas[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
                $moduleid_datas[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
            }
            $moduleid_data[$categoryKey]['moduleData'] = $moduleid_datas;
            //}

        }
        $commentsData = $this->comments->getCommentswithPaginate($category_id); // with pagination
//        $commentsData = $this->comments->getComments($category_id); without pagination
        $category['id'] = $category_id;
        $getDataofAllUserCategory = $this->getDataofAllUserCategory();
        return view('front1.detail', ["cat_id" => $id, "moduleid_data" => $moduleid_data, "parent_category_data" => $parent_category_data, "category_id" => $category_id, "commentsData" => $commentsData, "getDataofAllUserCategory" => $getDataofAllUserCategory]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeComment(Request $request)
    {
        //dd($request->all());
        $data = $this->comments->insert($request->all());

        $res['statusCode'] = 0;
        $res['data'] = "";
        if ($data) {
            $res['statusCode'] = 1;
            $commentsData = $this->comments->getComments($request->category_id);
            $res['data'] = view('front1.comments', ["commentsData" => $commentsData])->render();
            $res['commentajaxdata'] = view('layouts.front1.rightsidebar')->render();
        }

        return $res;
    }

    /**
     * update user form category module wise.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function formDataUpdate(Request $request)
    {
        $userForm = $this->user_module_category_form->addUserForm($request->all());
        $res['statusCode'] = 0;
        $res['data'] = "";
        if ($userForm) {
            $res['statusCode'] = 1;

            $category_array = explode(":", $request->category_id);
            $parent_category_data = [];
            $moduleid_data = [];
            foreach ($category_array as $categoryKey => $categoryData) {
                $moduleid_datas = $this->modulecategory->getassignedmodulebycategoryid([$categoryData]);
                //if(count($moduleid_datas) > 0){
                $moduleid_data[$categoryKey]['category_name'] = $this->category->getCategoryByID($categoryData)->name;
                $moduleid_data[$categoryKey]['description'] = $this->category->getCategoryByID($categoryData)->description;

                $moduleid_data[$categoryKey]['category_id'] = $categoryData;
                if ($categoryKey != 0) {
                    $parent_category_data[] = $this->category->getCategoryByID($categoryData);
                }

                foreach ($moduleid_datas as $key => $module) {

                    $moduleid_datas[$key]['form_data'] = $this->form->getFormByID($module->Module->form_id);
                    $moduleid_datas[$key]['quiz_data'] = $this->quiz->getQuizByID($module->Module->quiz_id);

                    $moduleid_datas[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
                    $moduleid_datas[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
                }
                $moduleid_data[$categoryKey]['moduleData'] = $moduleid_datas;
                //}

            }

            $res['data'] = view('front1.module_detail', ["cat_id" => $request->cat_id, "moduleid_data" => $moduleid_data, "parent_category_data" => $parent_category_data, "category_id" => $request->category_id])->render();
        }

        return $res;
    }

    /**
     * update user quiz category module wise.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function quizDataUpdate(Request $request)
    {
        $userQuiz = $this->user_module_category_quiz->addUserQuiz($request->all());
        $res['statusCode'] = 0;
        $res['data'] = "";
        if ($userQuiz) {
            $res['statusCode'] = 1;
            $category_array = explode(":", $request->category_id);
            $parent_category_data = [];
            $moduleid_data = [];
            foreach ($category_array as $categoryKey => $categoryData) {
                $moduleid_datas = $this->modulecategory->getassignedmodulebycategoryid([$categoryData]);
                // if(count($moduleid_datas) > 0){
                $moduleid_data[$categoryKey]['category_name'] = $this->category->getCategoryByID($categoryData)->name;
                $moduleid_data[$categoryKey]['description'] = $this->category->getCategoryByID($categoryData)->description;
                $moduleid_data[$categoryKey]['category_id'] = $categoryData;

                if ($categoryKey != 0) {
                    $parent_category_data[] = $this->category->getCategoryByID($categoryData);
                }

                foreach ($moduleid_datas as $key => $module) {

                    $moduleid_datas[$key]['form_data'] = $this->form->getFormByID($module->Module->form_id);
                    $moduleid_datas[$key]['quiz_data'] = $this->quiz->getQuizByID($module->Module->quiz_id);

                    $moduleid_datas[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
                    $moduleid_datas[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
                }
                $moduleid_data[$categoryKey]['moduleData'] = $moduleid_datas;
                //  }

            }

            $res['data'] = view('front1.module_detail', ["cat_id" => $request->cat_id, "moduleid_data" => $moduleid_data, "parent_category_data" => $parent_category_data, "category_id" => $request->category_id])->render();
        }
        return $res;
    }


    protected function validator(array $data)
    {
        //dd($data);
        return Validator::make($data, [
            'comment' => 'required',
        ]);
    }

    /**
     * Get a Download file.
     *
     * @param  file
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function download($filename)
    {

        $file_path = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'module' . DIRECTORY_SEPARATOR . $filename;

        if (file_exists($file_path)) {
            // Send Download
            return response()->download($file_path, $filename, [
                'Content-Length: ' . filesize($file_path)
            ]);
        } else {
            // Error
            return back();
        }

    }

    /**
     * Bookmark Category
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function bookmark(Request $request)
    {
        //dd($request->all());
        $data = $this->bookmark->insert($request->all());

        $res['statusCode'] = 0;
        $res['data'] = "";
        if ($data) {
            $res['statusCode'] = 1;
            $res['bookmark_count'] = $this->bookmark->getCount(Auth::user()->id);
            return $res;
        }

        return $res;
    }

    /**
     * Remove Bookmark Category
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeBookmark(Request $request)
    {
        //dd($request->all());
        $data = $this->bookmark->remove($request->all());

        $res['statusCode'] = 0;
        $res['data'] = "";
        if ($data) {
            $res['statusCode'] = 1;
            $res['bookmark_count'] = $this->bookmark->getCount(Auth::user()->id);
            return $res;
        }

        return $res;
    }

    /**
     * Get a Search keyword.
     *
     * @param  $search_keyword
     * @return \Illuminate\Http\Response
     */
    public function searchResult($search_keyword)
    {
        // For keeping h1 tag proper in detail page
        $search_style = 'style="margin-top:74px"';

        // Search Keyword
        $search_array_keyword = explode('-', $search_keyword);

        // For Public Category
        $public_category = Category::where('category_type_id', 2)->where('status', '0')->get();
        $public_id = [];
        if (!empty($public_category)) {
            foreach ($public_category as $category) {
                $public_id[] = $category->id;
            }
        }

        // Search section for Responsable user
        if (Auth::user()->user_type == 3) {

            // For Private Category
            $private_category = EntrepriseCategory::leftJoin('tbl_category', 'tbl_entreprise_category.category_id', '=', 'tbl_category.id')->where('tbl_category.status', '=', '0')->where('entreprise_id', Auth::user()->entreprise_id)->get();
            $private_id = [];
            if (!empty($private_category)) {
                foreach ($private_category as $category_data) {
                    $private_id[] = $category_data->category_id;
                }
            }

            // All private and public category id merge
            $all_category = array_merge($private_id, $public_id);

            $query = Category::select('tbl_category.*', 'mc.category_id', 'mc.module_id', 'tbl_module.title', 'tbl_module.keyword')->leftJoin('tbl_module_category as mc', 'mc.category_id', '=', 'tbl_category.id')->leftJoin('tbl_module', 'tbl_module.id', '=', 'mc.module_id')->where('tbl_category.status', '=', '0');
            $query = $query->where(function ($query) use ($search_array_keyword, $all_category) {
                foreach ($search_array_keyword as $value) {
                    if ($value != "") {
                        $query->orWhere('tbl_category.name', 'LIKE', "%" . $value . "%");
                        $query->orWhere('tbl_category.description', 'LIKE', "%" . $value . "%");
                        $query->orWhere('tbl_module.title', 'LIKE', "%" . $value . "%")->where('tbl_module.status', '=', '0');
                        $query->orWhere('tbl_module.keyword', 'LIKE', "%" . $value . "%");
                    }
                }
                $query->whereIn('tbl_category.id', $all_category);
            });
            $search_data = $query->get();
            return view('front1.search_result', ["search_data" => $search_data, "search_keyword" => $search_keyword, "search_style" => $search_style]);
        } // Search section for Collaborateur user

        elseif (Auth::user()->user_type == 4) {
            $colloaborateur_private_id = CollaborateurCategory::leftJoin('tbl_category', 'tbl_collaborateur_category.category_id', '=', 'tbl_category.id')->where('tbl_category.status', '=', '0')->where('collaborateur_id', Auth::user()->id)->get();
            $private_id = [];
            if (!empty($colloaborateur_private_id)) {
                foreach ($colloaborateur_private_id as $priv_category_data) {
                    $private_id[] = $priv_category_data->category_id;
                }
            }

            // All private and public category id
            $all_category = array_merge($private_id, $public_id);

            $query = Category::select('tbl_category.*', 'mc.category_id', 'mc.module_id', 'tbl_module.title', 'tbl_module.keyword')->leftJoin('tbl_module_category as mc', 'mc.category_id', '=', 'tbl_category.id')->leftJoin('tbl_module', 'tbl_module.id', '=', 'mc.module_id')->where('tbl_category.status', '=', '0');
            $query = $query->where(function ($query) use ($search_array_keyword, $all_category) {
                foreach ($search_array_keyword as $value) {
                    if ($value != "") {
                        $query->orWhere('tbl_category.name', 'LIKE', "%" . $value . "%");
                        $query->orWhere('tbl_category.description', 'LIKE', "%" . $value . "%");
                        $query->orWhere('tbl_module.title', 'LIKE', "%" . $value . "%");
                        $query->orWhere('tbl_module.keyword', 'LIKE', "%" . $value . "%");
                    }
                }
                $query->whereIn('tbl_category.id', $all_category);
            });
            $search_data = $query->get();
            return view('front1.search_result', ["search_data" => $search_data, "search_keyword" => $search_keyword, "search_style" => $search_style]);
        }


    }

    /**
     * Add the Module Complete status.
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */

    public function addUserModuleComplete(Request $request)
    {
        $status = $this->user_module_complete->insert($request->all());
        return $status;
    }

    /**
     * Add the comment's like
     *
     * @param  $request
     * @return \Illuminate\Http\Response
     */

    public function CommentLikeToggle(Request $request)
    {
        $id = $request['id'];
        $liked = $this->like_comments->like($id);
        $counter = $this->like_comments->getCounter($id);
        return response()->json(['counter' => $counter, 'liked' => $liked]);
    }

    /**
     * get all data of user type==3
     * @param
     * @return array of data
     */

    public function getDataofAllUserCategory()
    {
        $Formationdata = [];
        $Resourcedata = [];
        $bookmark_id = [];

        $bookmark_count = $this->bookmark->getCount(Auth::user()->id);

//         if (Auth::user()->user_type == 3 ) {
       if (Auth::user()->user_type == 3 || Auth::user()->user_type == 4) {

            $publicResourceData = $this->category->getPublicResource()->toArray();
            if (count($publicResourceData) > 0) {
                foreach ($publicResourceData as $key => $data) {
                    $childDataResourceData = $this->category->getResourceById($data['id'])->toArray();
                    $sub_child_arrs = [];
                    if (count($childDataResourceData) > 0) {
                        foreach ($childDataResourceData as $key_child => $key_child_data) {
                            $sub_child_arrs[] = $key_child_data;
                            $subChildDataResourceData = $this->category->getResourceById($key_child_data['id'])->toArray();
                            if (count($subChildDataResourceData) > 0) {
                                foreach ($subChildDataResourceData as $key_sub_child => $key_sub_child_data) {
                                    $sub_child_arrs[] = $key_sub_child_data;
                                }
                            }
                        }
                    }

                    $publicResourceData[$key]['ChildArray'] = $sub_child_arrs;
                }
            }

            $publicFormationData = $this->category->getPublicFormation()->toArray();
            if (count($publicFormationData) > 0) {
                foreach ($publicFormationData as $public_for_key => $publ_for_data) {
                    $childDataFormationData = $this->category->getFormationById($publ_for_data['id'])->toArray();
                    $sub_child_formation_arrs = [];
                    if (count($childDataFormationData) > 0) {
                        foreach ($childDataFormationData as $key_child => $key_formation_child_data) {
                            if ($key_formation_child_data['category_type_id'] == 1) {
                                $en = EntrepriseCategory::select('entreprise_id')->where('category_id', $key_formation_child_data['id'])->get()->toArray();
                                if (count($en) > 0) {
                                    foreach ($en as $e) {
                                        if (Auth::user()->entreprise_id == $e['entreprise_id']) {
                                            $sub_child_formation_arrs[] = $key_formation_child_data;
                                            $subChildDataFormationData = $this->category->getResourceById($key_formation_child_data['id'])->toArray();
                                            if (count($subChildDataFormationData) > 0) {
                                                foreach ($subChildDataFormationData as $key_sub_for_child => $key_sub_for_child_data) {
                                                    if ($key_sub_for_child_data['category_type_id'] == 1) {
                                                        $ents = EntrepriseCategory::select('entreprise_id')->where('category_id', $key_sub_for_child_data['id'])->get()->toArray();
                                                        if (count($ents) > 0) {
                                                            foreach ($ents as $ent) {
                                                                if (Auth::user()->entreprise_id == $ent['entreprise_id']) {
                                                                    $sub_child_formation_arrs[] = $key_sub_for_child_data;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                            } else {
                                $sub_child_formation_arrs[] = $key_formation_child_data;
                                $subChildDataFormationData = $this->category->getResourceById($key_formation_child_data['id'])->toArray();
                                if (count($subChildDataFormationData) > 0) {
                                    foreach ($subChildDataFormationData as $key_sub_for_child => $key_sub_for_child_data) {
                                        $sub_child_formation_arrs[] = $key_sub_for_child_data;
                                    }
                                }
                            }

                        }
                    }
                    $publicFormationData[$public_for_key]['ChildArray'] = $sub_child_formation_arrs;
                }
            }


            $privateResourceData = $this->category->getResourceByEnterpriseId(Auth::user()->entreprise_id)->toArray();
            if (count($privateResourceData) > 0) {
                foreach ($privateResourceData as $private_res_key => $data) {
                    $childPrivateDataResourceData = $this->category->getResourceById($data['id'])->toArray();
                    $sub_private_child_arrs = [];
                    if (count($childPrivateDataResourceData) > 0) {
                        foreach ($childPrivateDataResourceData as $key_private_child => $key_private_child_data) {
                            $sub_private_child_arrs[] = $key_private_child_data;
                            $subChildPrivateDataResourceData = $this->category->getResourceById($key_private_child_data['id'])->toArray();
                            if (count($subChildPrivateDataResourceData) > 0) {
                                foreach ($subChildPrivateDataResourceData as $key_sub_private_child => $key_sub_private_child_data) {
                                    $sub_private_child_arrs[] = $key_sub_private_child_data;
                                }
                            }
                        }
                    }

                }
            }

            $privateFormationData = $this->category->getFormationByEnterpriseId(Auth::user()->entreprise_id)->toArray();
            if (count($privateFormationData) > 0) {
                foreach ($privateFormationData as $private_for_key => $private_for_data) {
                    $childPrivateDataFormationData = $this->category->getFormationById($private_for_data['id'])->toArray();
                    $sub_private_for_child_arrs = [];
                    if (count($childPrivateDataFormationData) > 0) {
                        foreach ($childPrivateDataFormationData as $key_private_for_child => $key_private_for_child_data) {
                            $sub_private_for_child_arrs[] = $key_private_for_child_data;
                            $subChildPrivateDataFormationData = $this->category->getFormationById($key_private_for_child_data['id'])->toArray();
                            if (count($subChildPrivateDataFormationData) > 0) {
                                foreach ($subChildPrivateDataFormationData as $key_sub_private_child => $key_sub_private_for_child_data) {
                                    $sub_private_for_child_arrs[] = $key_sub_private_for_child_data;
                                }
                            }
                        }
                    }

                    $privateFormationData[$private_for_key]['ChildArray'] = $sub_private_for_child_arrs;
                }

            }

            $Resourcedata = array_merge($privateResourceData, $publicResourceData);
            $Formationdata = array_merge($publicFormationData, $privateFormationData);
            // For getting the list of bookmark category

            $bookmark_data = $this->bookmark->getUserBookmark(Auth::user()->id)->toArray();
            $bookmark_id = [];
            if (count($bookmark_data) > 0) {
                foreach ($bookmark_data as $bookmark) {
                    $bookmark_id[] = $bookmark['formation_id'];
                }
            }
        }

        $index = "active";
        $userObj = $this->user->init();
        $last_login_colloborateur_count = $this->user->getRecentLogin(Auth::user()->id);
        $getpublicFormationResourcedata = $this->modulecategory->getAllPublicFormationResource();
        $alldata = (["publicFormationData" => $Formationdata, "publicResourceData" => $Resourcedata, "index" => $index, "userObj" => $userObj, "bookmark_count" => $bookmark_count, "bookmark_id" => $bookmark_id, "getpublicFormationResourcedata" => $getpublicFormationResourcedata, "last_login_colloborateur_count" => $last_login_colloborateur_count]);

        return $alldata;
    }

    /**
     * Like Unlike Category.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function likeCategory(Request $request)
    {
        $data = $this->likeCategory->insert($request->all());
        if ($data) {
            $res['like_status'] = $this->likeCategory->getLikeStatus($request['category_id']);
            $res['like_count'] = $this->likeCategory->getCount($request['category_id']);
            return $res;
        }
    }

    /**
     * Like Unlike Category.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addCounterOnClick(Request $request)
    {
        $data =   $this->getmost_accessed_module->insert($request->all());
        if ($data) {
            // $res = $this->getMost_accessedModule->getCount($request['category_id']);
            //return $res;
        }
        return $data;
    }

    /**
     * show UserEntreprise
     *
     *
     * */
    public function userEntreprise()
    {
        $userEntreprise = "active";
        $getEntrepriseassignedByUSer = $this->user->getAllEntreprisebyUser();
        $entreprises = array();
        foreach ($getEntrepriseassignedByUSer as $id) {
            $entreprise = $this->entreprise->getEntrepriseByID($id);
            array_push($entreprises, $entreprise);
        }
        $parent_id = $this->entreprise->getParentEntrepise(); //collection of parent entreprise
        $sub_entreprise_id = $this->entreprise->getSubEntrepise($entreprises); //collection of child entreprise

        return view('front1/user_entreprise', ["parent_id" => $parent_id, "entreprises" => $entreprise, "getEntrepriseassignedByUSer" => $getEntrepriseassignedByUSer, "userEntreprise" => $userEntreprise, "sub_entreprise_id" => $sub_entreprise_id]);
    }

    /**
     * Show errorpage 404
     *
     *
     * */
    public function page404()
    {
        if (Auth::user()->user_type == 4 || Auth::user()->user_type == 3 ) {
            return view('front1/404');
        }
        return view('admin/404');
    }

}