<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comments;
use App\Models\ModuleCategory;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use App\Helpers\LaraHelpers;
use App\Models\Entreprise;
use Event;
use App\Events\SendMail;
use App\Models\EmailTemplate;
use App\Models\CollaborateurCategory;
use Config;
use Auth;
use Hash;
use Carbon\Carbon;
use DB;
use App\Models\Module;
use App\Models\UserModuleComplete;
use App\Models\getMostAccessedModule;
use App\Models\UserLogin;
use Log;
use Illuminate\Support\Facades\Input;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user;
    protected $entreprise;
    protected $emailTemplate;
    protected $comments;
    protected $category;
    protected $module_category;
    protected $collaborateurCategory;
    protected $userLogin;

    public function __construct(User $user,Entreprise $entreprise,EmailTemplate $emailTemplate,Comments $comments,Category $category,ModuleCategory $module_category , CollaborateurCategory $collaborateurCategory,UserLogin $userLogin)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->entreprise = $entreprise;
        $this->emailTemplate = $emailTemplate;
        $this->comments = $comments;
        $this->category = $category;
        $this->module_category = $module_category;
        $this->collaborateurCategory = $collaborateurCategory;
        $this->userLogin = $userLogin;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //echo "hi";exit;
        // Load all user data

        if(Input::get('id')){
            $data = $this->user->getUserByID(Input::get('id'));
        }else{
            $data = $this->user->init();
        }
        if(Auth::user()->user_type == 3){
            $userlist = "active";
            return view('front1.userlist', ["data"=>$data,'userlist'=>$userlist]);
        }
        return view('admin.user.index',["data"=>$data]);
    }

    /**
 * Show the form for creating a new resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function create()
    {
        // Load all user data
        $data = $this->user->init();
        $viewData['responsableData'] = $this->user->getResponsable();
        $viewData['data'] = $data;
        $viewData['userlist'] = "active";
        $viewData['userTypes'] = LaraHelpers::get_user_types();
        $viewData['entrepriseData'] = $this->entreprise->init();
        if(Auth::user()->user_type == 3){
            return view('front1.useradd',$viewData);
        }
        return view('admin.user.create',$viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all(),"add");
        if ($validator->fails()) {
            return redirect()->route('userAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all user data

        $create = $this->user->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{

            $adduser['first_name'] = $request->first_name;
            $adduser['last_name'] = $request->last_name;
            $adduser['email'] = $request->email;
            $adduser['password'] = $request->password;
            $adduser['fromEmail'] = Config::get('constant.FROM_EMAIL');
            $adduser['fromName'] = Config::get('constant.FROM_NAME');
            $adduser['subjectLine'] =  __('probtp.subjectline');
            $emailcontent = $this->emailTemplate->GetTemplateBySlug("welcome_email");
            $replaceBodyVariable = $emailcontent[0]->email_content;
            $result = str_replace('{name}',$request->first_name, $replaceBodyVariable);
            $result = str_replace('{email}',$request->email, $result);
            $result = str_replace('{password}',$request->password, $result);
            $adduser['body'] = $result;
            $adduser['viewTemplate'] = 'emails.user_signup';

            Event::fire(new SendMail($adduser));
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }
       /* if($request->user_type == 4){
            return redirect('/userlist');
        }*/
        if(isset($request->front_user)){
            return redirect('/userlist');
        }
        return redirect()->route('userIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load all user data

        $data = $this->user->init();
        $viewData['responsableData'] = $this->user->getResponsable();
        $editData = $this->user->getUserByID($id);
        $entrepriseArray= $editData->entreprise_id;
        $entrepriseId = explode(',',$entrepriseArray);
        $viewData['data'] = $data;
        $viewData['userTypes'] = LaraHelpers::get_user_types();
        $viewData['entrepriseData'] = $this->entreprise->init();
        $viewData['editData'] = $editData;
        $viewData['entrepriseId'] = $entrepriseId;
        if(Auth::user()->user_type == 3){
            return view('front1.useredit',$viewData);
        }
        return view('admin.user.edit',$viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all(),"edit");
        if ($validator->fails()) {
            return redirect()->route('userEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }
       // print_r($request->all());exit;

        // Load all user data
        $edit = $this->user->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_update')
                ]
            ));
        }
        /*if(Auth::user()->user_type == 3){
            return redirect('/userlist');
        }*/
        if(isset($request->front_user)){
            return redirect('/userlist');
        }
        return redirect()->route('userIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all user data

        $destroy = $this->user->remove($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message_user'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }
        if(Auth::user()->user_type == 3){
            return redirect('/userlist');
        }
        return redirect('/user');
    }

    /**
     * Get User Information.
     *@param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function myprofile()
    {

        $data = $this->user->init();
        $editData = $this->user->getUserByID(Auth::user()->id);
        $userTypes  = LaraHelpers::get_user_types();
        $entrepriseData = $this->entreprise->init();
        $editData['editData'] = $editData;

        if(Auth::user()->user_type == 3 || Auth::user()->user_type == 4){
            return view('front1.myprofile',[
                "data"=>$data,
                'editData'=>$editData,
                'userTypes'=>$userTypes,
                'entrepriseData'=>$entrepriseData,
            ]);
        }
        return view('admin.user.useraccount',[
            "data"=>$data,
            'editData'=>$editData,
            'userTypes'=>$userTypes,
            'entrepriseData'=>$entrepriseData,
        ]);

    }

    /**
     * Update the User Account information.
     * @param first_name and last_name update only
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateuser(Request $request)
    {


        $validator = $this->validator($request->all(),"useredit");
        if ($validator->fails()) {

            return redirect()->route('userProfile')
                ->withErrors($validator)
                ->withInput();
        }



        $edit = $this->user->edituseraccount($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_update')
                ]
            ));
        }
        $id = Auth::user()->id;
        return redirect()->route('userProfile');
    }

    /**
     * Change User Password View.
     *
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function changepassword()
    {
        $id = Auth::user()->id;
        $editData = $this->user->getUserByID($id);
        if(Auth::user()->user_type == 3 ||Auth::user()->user_type == 4 ){

            return view('front1.changepassword',["editData"=>$editData]);
        }
        return view('admin.user.changepassword',["editData"=>$editData]);
    }


    /**
     * Update the User Account Password.
     * @param password, new_password and confirm_password update only
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatepassword(Request $request)
    {
        $validator = $this->validator($request->all(),"updatepassword");
        if ($validator->fails()) {
            return redirect()->route('changePassword',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }
        $user_password = $request->old_password;
        $id = $request->id;
        $user_data = Auth::user();
        $old_password = $user_data->password;


        if (Hash::check($user_password, $old_password)){

            $updatepassword = $this->user->editpassword($request->all());
            if($updatepassword){
                $request->session()->flash('success',__(
                    'probtp.success_password',
                    [
                        'module' => __('probtp.user'),
                        'action' => __('probtp.action_update')
                    ]
                ));
            }
            else{

                $request->session()->flash('fail',__('probtp.fail_message'));
            }
        }
        else{
            $request->session()->flash('fail',__('probtp.fail_password'));
        }


        return redirect()->route('changePassword');

    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {

        // Load all module data
        $data = $this->user->trasheddata();
        return view('admin.user.userbin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all category data

        $restore = $this->user->restoreuser($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('userTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {

        $destroy = $this->user->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('userTrashIndex');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @param  string  $mode
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$mode)
    {

        $rules = array(
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'user_type' => 'required',
            'parent_id' => 'required',
            'entreprise_id' => '',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:15|Same:confirm_password'

        );
        if(isset($data['user_type'])){
//            if($data['user_type']==5){
//                $rules['parent_id'] = '';
//                $rules['entreprise_id'] = 'required';
//            }
            if($data['user_type']==5){
                $message = array(

                );
            }
            if($data['user_type']==1){
                $rules['parent_id'] = '';
            }
            if($data['user_type']==3 || $data['user_type']==2 || $data['user_type']==5){
                $rules['parent_id'] = '';
                $rules['entreprise_id'] = 'required';
            }
//            if($data['user_type']==2){
//                $rules['parent_id'] = '';
//                $rules['entreprise_id'] = 'required';
//            }
        }
        if ($mode == "edit") {
            if(isset($data['user_type'])) {
                if ($data['user_type'] == 5) {
                    $rules['parent_id'] = '';
                    $rules['entreprise_id'] = 'required';
                }
            }
            $rules['email'] = 'required|email|unique:users,email,' . $data['id'] . ',id';
            $rules['parent_id'] = '';
            $rules['password'] = '';
        }

        else if ($mode == "useredit") {
            $rules['first_name'] = 'required|max:50';
            $rules['last_name'] = 'required|max:50';
            $rules['user_type'] = '';
            $rules['parent_id'] = '';
            $rules['entreprise_id'] = '';
            $rules['email'] = '';
            $rules['password'] = '';

//            if(isset($data['user_type'])){
//                if($data['user_type']){
//                    $rules['parent_id'] = '';
//                }
//            }
        }

        else if ($mode == "updatepassword") {

            $rules['first_name'] = '';
            $rules['last_name'] = '';
            $rules['user_type'] = '';
            $rules['parent_id'] = '';
            $rules['entreprise_id'] = '';
            $rules['email'] = '';
            $rules['password'] = 'required|min:6|max:15|Same:confirm_password';
            $rules['old_password'] = 'required|min:6|max:15';

//            if(isset($data['user_type'])){
//                if($data['user_type']==5){
//                    $rules['parent_id'] = '';
//                    $rules['entreprise_id'] = '';
//                }
//            }

        }

        $messages = array(
          //  'entreprise_id.required' => "This Job Title has already been assigned to this RC",
        );




      // echo"<pre>";
//       print_r($data);exit;
       //print_r($rules);exit;
        return Validator::make($data, $rules,$messages);
    }

    /**
     * Get the community member
     *
     */
    public function getCommnunity()
    {
        $data = $this->user->getCommunityMember();

        $getpublicFormationResourcedata = $this->module_category->getAllPublicFormationResource();
        $viewData['communityData'] = $data;
        $viewData['getpublicFormationResourcedata'] = $getpublicFormationResourcedata;
        $viewData['communtiyTab'] = "active";
        return view('front1.community',$viewData);
    }

    /**
     * Get a data for statistics which
     * contains all the arrays
     * for each charts
     *
     * @return view
     */

    public function getStatistics()
    {
        $responsable = User::where('user_type',3)->count();
        $admin = User::where('user_type',2)->count();
        $formature = User::where('user_type',5)->count();
        $collaberator = User::where('user_type',4)->count();
        $user  = array();
        $user['Responsable '] = $responsable;
        $user['Administrateur'] = $admin;
        $user['Formateur'] = $formature;
        $user['Collaborateur'] = $collaberator;

        // bar chart
//        $res = $this->getResponsibleByAssignedModule();
        $ent = $this->getEntreprises();

        // bar chart for entreprise assign to user + collab assign to user
        $collectionOfres = $this->getAssignEntAndCollab();
        // $ent = $this->user->getAllEntreprisebyUserid();
        $getUserCommentData = $this->getUserCommentData();

        //get recent active user
        $mostRecent = $this->getRecentlyActiveUser();

        // get most comlpeted modules
        $mostCompleted = $this->getMostcompletedModule();

        $mostAccessed = $this->getMostAccessedModules();
        $comments = $this->getcommentsbyUserData();
        $time = $this->mostTimeSpentonSystem();
       // print_r($comments);exit;
       // $data  =  json_encode($user);  getAllColloborateurFormationResource , 'res'=>$res ,'collectionOfres'=>$collectionOfres
        return view('admin.user.statistics',['user'=>$user ,'entreprises_user_obj'=>$ent,'getUserCommentData'=>$getUserCommentData,'mostRecent'=>$mostRecent,"mostCompleted"=>$mostCompleted,'mostAccessed'=>$mostAccessed,"commentsData"=>$comments,"time" =>$time]);
    }



    /*  get all list of modules
     *  assigned to a responsible
     *
     *  @return array of data of name and count
     * */
    public function getResponsibleByAssignedModule(){
        $respo = $this->user->getUserByFieldName('user_type',3);

        $collab = [];
        foreach($respo as $res){
            $count = 0;
            $count = $this->collaborateurCategory->getAllColloborateurFormationResourceData($res->id);
            $getpublicFormationResourcedata = $this->module_category->getAllPublicFormationResource();
            $name = $this->user->getUserFirtName($res->id);

            $collab[$name] =  $count + $getpublicFormationResourcedata;
        }
        return $collab;
    }

    /*
     *   get the list of data for
     *   entreprise to assign
     *   the Collaborature
     *
     *   @return array wuth name and counter
     * */
    public function getAssignEntAndCollab(){
        $respo = $this->user->getUserByFieldName('user_type',3);
        $collaction = [];
        $ConutcollbOFresponble = 0;
        foreach($respo as $res){
            $entANDcol=[];
            $entANDcol['collaborature']=$ConutcollbOFresponble = User::select('id')->where('user_type',4)->where('parent_id',$res->id)->count();
            $entANDcol['entreprise']=$entreprise = User::select('tbl_entreprise.id')->join('tbl_entreprise','tbl_entreprise.id','=','users.entreprise_id')->where('users.id',$res->id)->count();

            $name = $this->user->getUserFirtName($res->id);


            $collaction[$name] =  $entANDcol;

        }
        return $collaction;
    }

    /*
     *   get the list of entreprise
     *
     *   @return JSON Response with name and counter
     * */
    public function getEntreprises(){
        $entreprise_user = array();
        $entreprise = array();
        $entreprises = Entreprise::where('deleted_at',Null)->get();
        foreach ($entreprises as $key => $value) {
            $responsable = User::where('entreprise_id',$value->id)->where('user_type',3)->where('deleted_at',Null)->count();
            $collaborature = User::where('entreprise_id',$value->id)->where('user_type',4)->where('deleted_at',Null)->count();

            $entreprise_user['entreprise'] = $value->name;
            $entreprise_user['responsable'] = $responsable;
            $entreprise_user['collaborateur'] = $collaborature;
            $entreprise_user['total_user'] = $responsable + $collaborature;
            $entreprise[]=$entreprise_user;
        }
      //  print_r($entreprise);exit;
       $entreprises_user_obj= json_encode($entreprise);
       return $entreprises_user_obj;
    }

    /*
    *   get the list of entreprise
    *  according to user type
    *
    *   @param \Illuminate\Http\Request  $request
    *   @return JSON Response with  name and counter
    * */
    public function getbarChartData(Request $request){
    $user_type = $request->data;
    $entreprise_user = array();
     $entreprises = Entreprise::where('deleted_at',Null)->get();
        foreach ($entreprises as $key => $value) {
            $user = User::where('entreprise_id',$value->id)
                          ->where('user_type',$user_type)
                          ->where('deleted_at',Null)
                          ->count();
            // $entreprise_user[entreprise id]=  userid;
             $entreprise_user[$value->name]=  $user;
        }
    //$user = User::where('user_type',$user_type)->count();

    return $entreprise_user;
    }

    /*
    *   get the list of user
    *   with comments list
    *
    *   @return array $userComment with user name and count of comments
    * */
    public function getUserCommentData(){
        $userComment = [];
        $user = User::where('deleted_at',Null)
                          ->get();
        foreach ($user as $key => $value) {
                $comment = Comments::where('user_id',$value->id)->count();
                $userComment[$value->name] = $comment;
                          }

    return $userComment;
    }

    /*
    *   get the list of user which are
    *   recently login
    *
    *   @return $mostRecent[]  with name and last login date
    * */
    public function getRecentlyActiveUser(){
        $val = Carbon::now();
        $mostRecent = User::select('name','last_login','user_type')
                            ->where('last_login' , '<' ,$val )
                            ->orderBy('last_login','DESC')
                            ->take(30)
                            ->get();
       // return json_encode($mostRecent);
        return ($mostRecent);
    }

    /*
    *   get the list of most completed
    *   modules by user
    *
    *   @return $modules[]  with title and conut
    * */
    public function getMostcompletedModule(){

        $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'))
            ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
            ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
            ->where('tbl_module.deleted_at',NULL)
            ->groupBy('user_module_complete.module_id')
            ->orderBy('total','DESC')
            ->take(30)->get();

        return $modules;
    }

    /*
    *   get the list of most completed
    *   modules by user with $module_type
    *
    *   @param \Illuminate\Http\Request  $request
    *   @return $modules[]  with title and conut
    * */
     public function getbarChartforMostCompletedData(Request $request){
         $module_type = $request->data;
         if($module_type != "A"){
             $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'))
                 ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
                 ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
                 ->where('tbl_module.deleted_at',NULL)
                 ->where('tbl_module.module_type',$module_type)
                 ->groupBy('user_module_complete.module_id')
                 ->orderBy('total','DESC')
                 ->take(30)
                 ->get();

            return $modules;
         }else{
                $modules = $this->getMostcompletedModule();
                 return $modules;
         }
    }

   /*
    *   get the list of most Accessed
    *   modules
    *
    *
    *  @return $modules[]  JSON Response
    * */
     public function getMostAccessedModules(){
         $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id')
           ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
           ->where('tbl_module.deleted_at','=',NULL)
           ->orderBy('get_most_accessed_module.counter','desc')
           ->take(30)->get();
      return  json_encode($modules);
    }

  /*
   *   get the list of most Accessed
   *   modules for $module_type
   *
   *  @param \Illuminate\Http\Request  $request
   *  @return $modules[]  JSON Response
   * */
     public function getbarChartforMostAccessedData(Request $request){
         $module_type = $request->data;
         if($module_type != "A"){
             $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id')
                 ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
                 ->where('tbl_module.module_type',$module_type)
                 ->where('tbl_module.deleted_at','=',NULL)
                 ->orderBy('get_most_accessed_module.counter','desc')
                 ->take(30)->get();

               return json_encode($modules);
        }else{
               return $this->getMostAccessedModules();
        }
    }

  /*
   *   get the list of comments given by user
   *
   *
   *  @return JSON Response $commentsDataArray
   * */
    public function getcommentsbyUserData(){
        $commentsData = array();
        $comments = Comments::select('category_id')->where('deleted_at',Null)->get()->toArray();

        $cat = array();

        foreach($comments as $k => $val){
            if(strpos($val['category_id'],":")){
                $cate_id = stristr($val['category_id'],":",true);
            }else{
                $cate_id = $val['category_id'];
            }
            $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
        }

        $commentsDataArray = array();
        foreach ($cat as $key => $value) {
            $cats = array();
            $counter  = Comments::select( DB::raw('count(id) as total') )
                ->where('category_id','like',$value.'%')
                ->take(30)->get()->toArray();
            $cat_name = Category::select('name','id')->where('id',$value)->where('parent_id',0)->first();
            if(!empty($cat_name->name)){
                $cats['name'] = $cat_name->name;
                $cats['total'] = $counter[0]['total'];
                $commentsDataArray[]= $cats;
            }

        }
        return json_encode($commentsDataArray);
    }

  /*
   *   get the list of comments given by user
   *
   *
   *  @return JSON Response $commentsData
   * */
    public function getbarChartforComments(Request $request){
       $module_type = $request->data;

        $commentsDataArray = array();
        if($module_type != "A"){
            $comments = Comments::select('category_id')->where('deleted_at',Null)->get()->toArray();
            $cat = array();
            foreach($comments as $k => $val){
                if(strpos($val['category_id'],":")){
                    $cate_id = stristr($val['category_id'],":",true);
                }else{
                    $cate_id = $val['category_id'];
                }
                $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
            }


            foreach ($cat as $key => $value) {
                $cats = array();
                $counter  = Comments::select( DB::raw('count(id) as total') )
                    ->where('category_id','like',$value.'%')
                    ->take(30)->get()->toArray();
                $cat_name = Category::select('name','id')->where('id',$value)->where('category_classification',$module_type)->where('parent_id',0)->first();
                if(!empty($cat_name->name)){
                    $cats['name'] = $cat_name->name;
                    $cats['total'] = $counter[0]['total'];
                    $commentsDataArray[]= $cats;
                }
            }
            return json_encode($commentsDataArray);
        }else{
            $commentsData = $this->getcommentsbyUserData();
            return ($commentsData);
        }

        //$comments = Comments::select('category_id')->where('deleted_at',Null)->distinct()->get();
//        foreach ($comments as $key => $value) {
//
//                $categoryId = explode(':', $value->category_id);
//                $count  = Comments::select( DB::raw('count(id) as total'))->where('category_id','like',$categoryId[0].'%')->get()->toArray();
//                $categoryName = Category::select('name')->where('category_classification',$module_type)->where('id',$categoryId[0])->get()->toArray();
//
//                if(!empty($categoryName)){
//                    $comment = array();
//                    $comment['name'] = $categoryName[0]['name'];
//                    $comment['total'] = $count[0]['total'];
//                    $commentsData[] = $comment;
//                }
//
//            }


    }

  /*
   *   get the list of user with
   * total time spent by them
   *
   *
   *  @return JSON Response $time
   * */

    public function mostTimeSpentonSystem(){
        $time = User::select('users.name','tbl_users_login.total_time')
                ->leftjoin('tbl_users_login','tbl_users_login.user_id','=','users.id')
                ->where('users.deleted_at',Null)
                ->orderBy('tbl_users_login.total_time','DESC')
                ->get();
      return json_encode($time);
    }


}
    