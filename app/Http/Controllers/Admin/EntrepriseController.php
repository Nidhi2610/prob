<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Entreprise;
use App\Models\Module;
use Validator;
use Auth;
use Illuminate\Support\Facades\Input;

class EntrepriseController extends Controller
{
    protected $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     $Entreprise = new Entreprise;
        // Load all category data
        if(Input::get('id')){
            $data = $Entreprise->getEntrepriseByID(Input::get('id'));
//            print_r($data);exit;
        }else{
            $data = $Entreprise->init();
//            print_r($data);exit;
        }


        return view('admin.entreprise.index',["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load all category data
        $Entreprise = new Entreprise;
        $data = $Entreprise->init();
        if(Auth::user()->user_type == 3){
            return view('front1.user_rh_add',["data"=>$data]);
        }
        return view('admin.entreprise.create',["data"=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return redirect()->route('entrepriseAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all entreprise data
        $Entreprise = new Entreprise;
        $create = $Entreprise->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('entrepriseIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load all entreprise data
        $Entreprise = new Entreprise;
        $data = $Entreprise->init();
        $editData = $Entreprise->getEntrepriseByID($id);

        return view('admin.entreprise.edit',[
            "data"=>$data,
            'editData'=>$editData
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('entrepriseEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all entreprise data
        $Entreprise = new Entreprise;
        $edit = $Entreprise->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.enterprise_update_sucess'));
        }

        return redirect()->route('entrepriseIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all entreprise data
        $Entreprise = new Entreprise;

        $destroy = $Entreprise->remove($request->id);

        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message_entreprise'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('entrepriseIndex');
    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {

        $Entreprise = new Entreprise;
        $data = $Entreprise->trasheddata();
        return view('admin.entreprise.entreprisebin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all Entreprise data
        $Entreprise = new Entreprise;
        $restore = $Entreprise->restoreuser($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('entrepriseTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroypermenant(Request  $request)
    {
        $Entreprise = new Entreprise;
        $destroy = $Entreprise->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('entrepriseTrashIndex');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'logo' => 'mimes:jpeg,jpg,png'
        ]);
    }

    /**
     * get all users with the filter of user type.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getUserOfEnterprise(Request $request)
    {
        $users = $this->user->getUserByField('user_type',$request->user_type);
        $res['statusCode'] = 0;
        $res['data'] = '';
        if(count($users) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.entreprise.assign_user',["users"=>$users,"enterprise_id"=>$request->enterprise_id])->render();
        }
        return $res;
    }

    /**
     * add enterprise to selected users with the filter of user type.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addEnterpriseToUser(Request $request)
    {
        $users = $this->user->addEnterprise($request->all());

        if (!$users) {
            $request->session()->flash('fail',__('probtp.fail_message_entreprise'));
        } else{
            $request->session()->flash('success',__(
                'probtp.enterprise_assigne_success'));
        }

        return redirect()->route('entrepriseIndex');

    }

}
