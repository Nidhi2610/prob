<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Collaborateur;
use Validator;
use Auth;

class CollaborateurController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	protected $responsable;
	 
    public function __construct(Collaborateur $collaborateur)
    {
        $this->middleware(['auth','AuthenticateUser']);
		$this->collaborateur = $collaborateur;
    }
	
	
    /**
     * Store assigned user module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
		Note: responsable_id is considered as collaborateur_id
     */
	 
    public function insertcollaborateur(Request $request)
    {
		
		$user_id = Auth::user()->id;
		$module_id = $request->module_id;
		$entreprise_id = Auth::user()->entreprise_id;
		
		$delete = $this->collaborateur->remove([
			
			'user_id' => $user_id,
			'module_id' => $module_id,
		
		]);
		
		if($request->responsable_id == null)
		{
			$create = true;
		}
		else{
			
			foreach($request->responsable_id as $collaborateur_id)
			{
				
				$create = $this->collaborateur->insert([
				 'user_id' => $user_id,
				 'module_id' => $module_id,
				 'entreprise_id' => $entreprise_id,
				 'collaborateur_id'=> $collaborateur_id
				 
				]);
			}
		}
		
        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('moduleIndex');
    }
	
	/**
     * Store assigned user module of collobarateur.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
		
     */
	
	public function getcollaborateur(Request $request)
    {
        // Load all Module data
       $module_id = $request->id;
	   $user_id = Auth::user()->id;
	   $data = $this->collaborateur->getmodule([
				"module_id" => $module_id,
				"user_id" => $user_id,
	   
	   ]);
	   return $data;				 
	}
}
