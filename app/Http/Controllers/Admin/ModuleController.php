<?php

namespace App\Http\Controllers\Admin;


use App\Models\ModuleResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Module;
use Validator;
use Auth;
use App\Models\ModuleCategory;
use App\Models\Quiz;
use App\Models\Category;
use App\Models\Form;
use App\Models\UserModuleCategoryForm;
use App\Models\UserModuleCategoryQuiz;
use Illuminate\Support\Facades\Input;



class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $module;
    protected $modulecategory;
    protected $quiz;
    protected $form;
    protected $user_module_category_form;
    protected $user_module_category_quiz;
    protected $category;
    protected $moduleresource;

    public function __construct(Module $module, ModuleCategory $modulecategory, Quiz $quiz ,Form $form,UserModuleCategoryForm $user_module_category_form,UserModuleCategoryQuiz $user_module_category_quiz,Category $category,ModuleResource $moduleresource)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->module = $module;
        $this->modulecategory = $modulecategory;
        $this->quiz = $quiz;
        $this->form = $form;
        $this->user_module_category_form = $user_module_category_form;
        $this->user_module_category_quiz = $user_module_category_quiz;
        $this->category = $category;
        $this->moduleresource = $moduleresource;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = "")
    {

        // Load all module data

        if(Input::get('id')){
            $data = $this->module->getModuleDataByID(Input::get('id'));
           // $data = $this->module->init($type);
        }else{
            $data = $this->module->init($type);
        }

        $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;


        return view('admin.module.index',["data"=>$data,"type"=>$type],["filepath" =>$filepath]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id=0,$type ="")
    {
        // Load all Module data

        $data = $this->module->init($type);

        $category = $this->category->init($type);
        $category = $category->toArray();
        $form = $this->form->init();
        $quiz = $this->quiz->init();
        return view('admin.module.create',["data"=>$data, "form" => $form, "category" => $category,"category_id" =>$id,"quiz" => $quiz,"types"=>$type ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('moduleAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all module data

        $create = $this->module->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__('probtp.module_add_sucess'));
        }

        return redirect('module/'.$request->module_type);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$type)
    {
        // Load all Module data

        $data = $this->module->init($type);
        $editData = $this->module->getModuleByID($id);
        $moduleCategory = $this->modulecategory->getCategoryByModuleId($id);
        $quiz = $this->quiz->init();
        $categoryData = $this->category->init($type);
        $categoryData = $categoryData->toArray();
        $form = Form::get();
        return view('admin.module.edit',[
            "data"=>$data,
            'editData'=>$editData,
            'categoryData'=> $categoryData,
            'form' => $form,
            'quiz' => $quiz,
            'moduleCategory' => $moduleCategory,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getmodule(Request $request)
    {
        // Load all Module data
        $data = $this->module->getModuleByID($request->id);
        if(Auth::user()->user_type == 3) {
            $data->assigned_responsable_module = $data->assigned_user_module;
        }
        if($data){
            return $data;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('moduleEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all Module data

        $edit = $this->module->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.module_update_sucess'));
        }

        return redirect('module/'.$request->module_type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all category data

        $destroy = $this->module->remove($request->id);

        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.module_delete_sucess'));
        }

        return redirect('module/'.$request->module_type);
}

    /**
     * Get a Download file.
     *
     * @param  file
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download ($filename){

        $file_path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR . $filename;
        if (file_exists($file_path))
        {
            // Send Download
            return response()->download($file_path);
        }
        else
        {
            // Error
            return redirect()->route('moduleIndex');
        }

    }

    /**
     * Get a the data of assigned module to the users.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function usermodule(Request  $request){

        $response = $this->module->insertusermodule($request->all());

        if (!$response) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_module_assigned',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_update')
                ]
            ));
        }

        return redirect()->route('moduleIndex');
    }

    /**
     * Get a the view detail of Module.
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id){

        $data = $this->module->getModuleByID($id);
        $categorydata = Category::find($data->category_id)->get();
        $categoryname = $categorydata[0]->name;
        $keyword = explode(",",$data->keyword);
        $formdata = Form::where('form_id',$data->form_id)->first(['content']);
        $form = $formdata['content'];

        // For Loading Likes of the module according the user login imported from Likes Model


        return view('admin.module.detail',[
            "data"=>$data,
            "category"=>$categoryname,
            "formdata"=>$form,
            "keyword"=>$keyword,

        ]);
    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function gettrashdata($type)
    {

        // Load all module data
        $data = $this->module->trasheddata($type);
        $filepath = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR;


        return view('admin.module.modulebin',["data"=>$data,"type"=>$type],["filepath"=>$filepath]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function restorefrombin(Request  $request)
    {
        // Load all category data
        $restore = $this->module->restoremodule($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect('gettrashdata/'.$request->module_type);
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroypermenant(Request  $request)
    {

        $destroy = $this->module->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect('gettrashdata/'.$request->module_type);
    }

    /**
     * Replicate the Module .
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function replicate($id,$type)
    {

        $duplicateid = $this->module->duplicate($id);

        if($duplicateid){

            $data = $this->module->init($type);
            $editData = $this->module->getModuleByID($duplicateid);
            $moduleCategory = $this->modulecategory->getCategoryByModuleId($duplicateid);
            $categoryData = $this->category->init($type);
            $categoryData = $categoryData->toArray();
            $form = Form::get();
            $quiz = $this->quiz->init();

            return view('admin.module.edit',[
                "data"=>$data,
                'editData'=>$editData,
                'categoryData'=> $categoryData,
                'form' => $form,
                'moduleCategory' => $moduleCategory,
                'quiz' => $quiz,

            ]);
        }
        else{

            $duplicateid->session()->flash('fail',__('probtp.fail_message'));
        }

        return redirect('module/'.$duplicateid->module_type);
    }

    /**
     * Replicate the Module from Category listing.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function replicateFromCategory(Request $request)
    {

        $duplicateid = $this->module->duplicate($request->id);

        if ($duplicateid) {

            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_copied')
                ]
            ));
            return redirect()->route('categoryIndex');
        }

    }

    /**
     * Assign Module from category Listing Popup.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertModuleCategory(Request $request){

        if($request->module_id){

            $module_data =  $this->modulecategory->storeFromCategory($request->all());

            if (!$module_data) {
                $request->session()->flash('fail',__('probtp.fail_message'));
            } else{
                $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.category'),
                        'action' => __('probtp.action_assigned')
                    ]
                ));
            }

            return redirect('category/'.$request->category_classification);

        }
        else{
            $request->session()->flash('fail',__('probtp.fail_message_module'));
        }

        return redirect('category/'.$request->category_classification);
    }

    /**
     * Assign Module from resource Listing Popup.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertModuleResource(Request $request){

        if($request->module_id){

            $module_data =  $this->moduleresource->storeFromResource($request->all());

            if (!$module_data) {
                $request->session()->flash('fail',__('probtp.fail_message'));
            } else{
                $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.resource'),
                        'action' => __('probtp.action_assigned')
                    ]
                ));
            }

            return redirect()->route('resourceIndex');

        }
        else{
            $request->session()->flash('fail',__('probtp.fail_message_module'));
        }

        return redirect()->route('resourceIndex');
    }

    /**
     * Assign Module from category Listing Popup.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteModuleCategory(Request $request){

        if($request->module_id){

            $module_data =  $this->modulecategory->deleteFromCategory($request->all());

            if (!$module_data) {
                $request->session()->flash('fail',__('probtp.fail_message'));
            } else{
                $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.category'),
                        'action' => __('probtp.action_unassigned')
                    ]
                ));
            }

            return redirect('category/'.$request->category_classification);

        }
        else{
            $request->session()->flash('fail',__('probtp.fail_message_module'));
        }

        return redirect('category/'.$request->category_classification);
    }

    /**
     * Assign Module from category Listing Popup.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteModuleResource(Request $request){

        if($request->module_id){

            $module_data =  $this->moduleresource->deleteFromResource($request->all());

            if (!$module_data) {
                $request->session()->flash('fail',__('probtp.fail_message'));
            } else{
                $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.resource'),
                        'action' => __('probtp.action_unassigned')
                    ]
                ));
            }

            return redirect()->route('resourceIndex');

        }
        else{
            $request->session()->flash('fail',__('probtp.fail_message_module'));
        }

        return redirect()->route('resourceIndex');
    }

    /**
     * Display a listing of the unassigned Module to category.
     *
     * @return \Illuminate\Http\Response
     */
    public function getunassignedmodule(Request $request)
    {
        // Load all module which are not assigned to the category
        $categorydata = $this->module->getmodulebycategoryid($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.category.categoryunassigned',["module_list" => $categorydata,"category_classification"=>$request->category_classification])->render();
        }
        return $res;
    }

    /**
     * Display a listing of the unassigned Module to resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getunassignedmoduleresource(Request $request)
    {
        // Load all module which are not assigned to the resource
        $categorydata = $this->module->getmodulebyresourceid($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.resource.resourceunassigned',["module_list" => $categorydata])->render();
        }
        return $res;
    }

    /**
     * Display a listing of the assigned module.
     *
     * @return \Illuminate\Http\Response
     */
    public function getassignedmodule(Request $request)
    {

        // Load all module which are not assigned to the category
        $categorydata = $this->modulecategory->getModalassignedmodulebycategoryid($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.category.categoryassigned',["module_list" => $categorydata,"hide"=>"hide"])->render();
        }

        return $res;

    }

    /**
     * Display a listing of the assigned module to resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getassignedmoduleresource(Request $request)
    {

        // Load all module which are not assigned to the resource
        $categorydata = $this->moduleresource->getModalassignedmodulebyresourceid($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.resource.resourceassigned',["module_list" => $categorydata,"hide"=>"hide"])->render();
        }

        return $res;

    }

    /**
     * Display a listing of the assigned module.
     *
     * @return \Illuminate\Http\Response
     */
    public function getModuleCateogry(Request $request)
    {

        // Load all module which are not assigned to the category
       $categorydata = $this->modulecategory->getModuleCategoryList($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.module.get_category',["module_list" => $categorydata])->render();
        }
        return $res;

    }

    /**
     * Display a listing of the assigned module in resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getModuleResource(Request $request)
    {

        // Load all module which are not assigned to the category
        $categorydata = $this->moduleresource->getModuleResourceList($request->id);
        $res['statusCode'] = 0;
        $res['data'] = "";
        if(count($categorydata) > 0){
            $res['statusCode'] = 1;
            $res['data'] = view('admin.module.get_resource',["module_list" => $categorydata])->render();
        }
        return $res;

    }

    /**
     * Detail page of cateogry
     *
     * @return \Illuminate\Http\Response
     */

    public function preview($module_id)
    {

        if(Auth::user()->user_type == 3 || Auth::user()->user_type == 4){
            return redirect('/home');
        }

        $moduleid_data = $this->modulecategory->getModuleByModuleId($module_id);

        foreach($moduleid_data as $key => $module){

            $moduleid_data[$key]['form_data'] = $this->form->getAdminFormByID($module->Module->form_id);
            $moduleid_data[$key]['quiz_data'] = $this->quiz->getAdminQuizByID($module->Module->quiz_id);
            $moduleid_data[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
            $moduleid_data[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
        }


        return view('admin.module.preview',["moduleid_data" => $moduleid_data]);
    }

    /**
     * Detail page of cateogry
     *
     * @return \Illuminate\Http\Response
     */

    public function deleteFile(Request $request)
    {
        $moduleid_data = $this->module->unlinkFile($request->all());
        echo 1;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    protected function validator(array $data)
    {
        //dd($data);
        return Validator::make($data, [
            /*'title' => 'required',
            'keyword' => 'required',
            'category_id' => 'required'*/


        ]);
    }
}