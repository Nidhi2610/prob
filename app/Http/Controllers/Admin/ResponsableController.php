<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Responsable;
use Validator;
use Auth;

class ResponsableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	protected $responsable;
	 
    public function __construct(Responsable $responsable)
    {
        $this->middleware(['auth','AuthenticateUser']);
		$this->responsable = $responsable;
    }
	
	
    /**
     * Store assigned user module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
    public function insertresponsabel(Request $request)
    {
		
		$user_id = Auth::user()->id;
		$module_id = $request->module_id;
		$entreprise_id = Auth::user()->entreprise_id;
		
		$delete = $this->responsable->remove([
			
			'user_id' => $user_id,
			'module_id' => $module_id,
		
		]);
		
		if($request->responsable_id == null)
		{
			$create = true;
		}
		
		else{
			
			foreach($request->responsable_id as $responsable_id)
			{
				$create = $this->responsable->insert([
				 'user_id' => $user_id,
				 'module_id' => $module_id,
				 'entreprise_id' => $entreprise_id,
				 'responsable_id'=> $responsable_id
				 
				]);
			}
			
		}
		
        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.modules'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('moduleIndex');
    }
	
	/**
     * Get assigned responsable user module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	
	public function getresponsable(Request $request)
    {
        // Load all Module data
       $module_id = $request->id;
	   $user_id = Auth::user()->id;
	   
	   $data = $this->responsable->getmodule([
				"module_id" => $module_id,
				"user_id" => $user_id,
	   
	   ]);
	   return $data;				 
	}

}
