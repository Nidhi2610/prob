<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EmailTemplate;
use Validator;
use Auth;


class EmailTemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $emailtemplate;

    public function __construct(EmailTemplate $emailtemplate)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->emailtemplate = $emailtemplate;

    }

    /**
     * Display a listing of the Email Template.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Load all module data
        $data = $this->emailtemplate->init();
        return view('admin.emailtemplate.index',["data"=>$data]);
    }

    /**
     * Show the Email .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load all Email Template data

        $data = $this->emailtemplate->init();
        return view('admin.emailtemplate.create',["data"=>$data]);
    }

    /**
     * Store a newly created Email Template in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('emailTemplateAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all Email Template data

        $create = $this->emailtemplate->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.email_template'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('emailTemplateIndex');
    }

    /**
     * Show the Email Template for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load all Email template data

        $data = $this->emailtemplate->init();
        $editData = $this->emailtemplate->getTemplateByID($id);

        return view('admin.emailtemplate.edit',[
            "data"=>$data,
            'editData'=>$editData,

        ]);
    }

    /**
     * Update the specified email template in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('emailTemplateEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all emailtemplate data

        $edit =$this->emailtemplate->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.email_template'),
                    'action' => __('probtp.action_update')
                ]
            ));
        }

        return redirect()->route('emailTemplateIndex');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd($data);
        return Validator::make($data, [
            'email_content' => 'required',
            'name' => 'required',

        ]);
    }

}