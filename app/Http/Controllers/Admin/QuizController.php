<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quiz;
use Validator;

class QuizController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $quiz;

    public function __construct(Quiz $quiz)
    {
        $this->middleware('auth');
        $this->quiz = $quiz;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Load all quiz data

        $data = $this->quiz->init();

        return view('admin.quiz.index',["data"=>$data]);
    }

    /**
     * Show the quiz for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load all quiz data

        return view('admin.quiz.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all(),"add");
        if ($validator->fails()) {
            return redirect()->route('quizAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all quiz data
        $create = $this->quiz->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{

            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.quiz'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('quizIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the quiz for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load all quiz data

        $editData = $this->quiz->getQuizByID($id);

        $viewData['editData'] = $editData;
        return view('admin.quiz.edit',$viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all(),"edit");
        if ($validator->fails()) {
            return redirect()->route('quizEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all quiz data

        $edit = $this->quiz->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.quiz'),
                        'action' => __('probtp.action_update')
                    ]
            ));
        }

        return redirect()->route('quizIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all quiz data

        $destroy = $this->quiz->remove($request->id);

        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message_quiz'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.quiz'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('quizIndex');
    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {

        // Load all module data
        $data = $this->quiz->trasheddata();
        return view('admin.quiz.quizbin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        $restore = $this->quiz->restorequiz($request->id);
        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.quiz'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('quizTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {
            dd($request->all());
        $destroy = $this->quiz->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.quiz'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('quizTrashIndex');
    }

    /**
     * Replicate the Quiz listing.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function replicate(Request $request)
    {
        $duplicateid = $this->quiz->duplicate($request->id);

        if (!$duplicateid) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.quiz'),
                    'action' => __('probtp.action_copied')
                ]
            ));
        }
        return redirect()->route('quizIndex');
    }
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @param  string  $mode
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$mode)
    {
        $rules = array(
            'quiz_name' => 'required|max:50',
        );

        return Validator::make($data, $rules);
    }
}
