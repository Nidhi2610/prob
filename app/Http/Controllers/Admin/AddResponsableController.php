<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comments;
use App\Models\ModuleCategory;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use App\Helpers\LaraHelpers;
use App\Models\Entreprise;
use Event;
use App\Events\SendMail;
use App\Models\EmailTemplate;
use App\Models\Responsable;
use Config;
use Auth;
use Hash;
class AddResponsableController extends Controller
{

    protected $user;
    protected $entreprise;
    protected $emailTemplate;

    public function __construct(User $user,Entreprise $entreprise,EmailTemplate $emailTemplate)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->entreprise = $entreprise;
        $this->emailTemplate = $emailTemplate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource as a Responsable RH.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       if(Auth::user()->user_type == 3) {

            return view('front1.user_rh_add');
        }
        //return redirect()->route('welcome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $validator = $this->validator($request->all(),"add");
               if ($validator->fails()) {
                   return redirect()->route('user_Rh_Add',['e_id'=>$request->entreprise_id])
                        ->withErrors($validator)
                        ->withInput();
        }


        // Load all user data

        $create = $this->user->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{

            $adduser['first_name'] = $request->first_name;
            $adduser['last_name'] = $request->last_name;
            $adduser['email'] = $request->email;
            $adduser['password'] = $request->password;
            $adduser['fromEmail'] = Config::get('constant.FROM_EMAIL');
            $adduser['fromName'] = Config::get('constant.FROM_NAME');
            $adduser['subjectLine'] =  __('probtp.subjectline');
            $emailcontent = $this->emailTemplate->GetTemplateBySlug("welcome_email");
            $replaceBodyVariable = $emailcontent[0]->email_content;
            $result = str_replace('{name}',$request->first_name, $replaceBodyVariable);
            $result = str_replace('{email}',$request->email, $result);
            $result = str_replace('{password}',$request->password, $result);
            $adduser['body'] = $result;
            $adduser['viewTemplate'] = 'emails.user_signup';


            Event::fire(new SendMail($adduser));
            $request->session()->flash('success',__(
                'probtp.RH_successfully_added',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }


     return redirect()->route('user_Rh_Add',['e_id'=>$request->entreprise_id]);

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // Load all user data

        $data = $this->user->init();
        $viewData['responsableData'] = $this->user->getResponsable();
        $editData = $this->user->getUserByID($id);
        $entrepriseArray= $editData->entreprise_id;
        $entrepriseId = explode(',',$entrepriseArray);
        $viewData['data'] = $data;
        $viewData['userTypes'] = LaraHelpers::get_user_types();
        $viewData['entrepriseData'] = $this->entreprise->init();
        $viewData['editData'] = $editData;
        $viewData['entrepriseId'] = $entrepriseId;
        if(Auth::user()->user_type == 3){
            return view('front1.user_rh_edit',$viewData);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all(),"edit");
        if ($validator->fails()) {
            return redirect()->route('userRHEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all user data
        $edit = $this->user->edit($request->all());


        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.rh_update_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_update')
                ]
            ));
        }

        return redirect()->route('user_Rh_Add',['e_id'=>$request->entreprise_id]);
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @param  string  $mode
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$mode)
    {
        $rules = array(
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'user_type' => 'required',
            'parent_id' => '',
            'entreprise_id' => '',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:15|Same:confirm_password'

        );
        if($mode == "add"){
            $rules['email']= 'required|email|unique:users,email';
            $rules['password'] = 'required|min:6|max:15|Same:confirm_password';
        }

        else if ($mode == "edit") {
            $rules['email'] = 'required|email|unique:users,email,' . $data['id'] . ',id';
            $rules['password'] = '';
        }

        else if ($mode == "useredit") {

            $rules['first_name'] = 'required|max:50';
            $rules['last_name'] = 'required|max:50';
            $rules['user_type'] = '';
            $rules['parent_id'] = '';
            $rules['entreprise_id'] = '';
            $rules['email'] = '';
            $rules['password'] = '';
        }

        else if ($mode == "updatepassword") {

            $rules['first_name'] = '';
            $rules['last_name'] = '';
            $rules['user_type'] = '';
            $rules['parent_id'] = '';
            $rules['entreprise_id'] = '';
            $rules['email'] = '';
            $rules['password'] = 'required|min:6|max:15|Same:confirm_password';
            $rules['old_password'] = 'required|min:6|max:15';

        }

        $messages = array(
            'entreprise_id.required' => "This Job Title has already been assigned to this RC",
        );

        return Validator::make($data, $rules,$messages);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all user data
        $e_id = User::select('entreprise_id')->where('id',$request->id)->get();
        $entreprise_id=($e_id->toArray()[0]['entreprise_id']);
        $destroy = $this->remove($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message_user'));
        } else{
            $request->session()->flash('success',__(
                'probtp.rh_action_delete',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.rh_action_delete')
                ]
            ));
        }
        return redirect()->route('user_Rh_Add',['e_id'=>$entreprise_id]);
    }

    /**
     * Delete User data base on ID
     *
     * @param array $id
     * @return  Object
     */
    public function remove($id){

        if(Auth::user()->user_type == "3"){

            $userdata = Responsable::where('responsable_id',$id)->count();
            if($userdata > 0){
                return false;
            }
            else{

                return User::where('id',$id)->delete();
            }
        }
    }
}
