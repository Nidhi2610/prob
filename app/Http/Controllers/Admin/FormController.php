<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Form;
use Validator;



class FormController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $form;

    public function __construct(Form $form)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->form = $form;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Load all form data

        $data = $this->form->init();

        return view('admin.form.index',["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Load all form data

        $data = $this->form->init();
        $viewData['data'] = $data;
        return view('admin.form.create',$viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all(),"add");
        if ($validator->fails()) {
            return redirect()->route('formAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // Load all form data

        $create = $this->form->insert($request->all());

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{

            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.form'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('formIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Load all form data

        $data = $this->form->init();
        $editData = $this->form->getFormByID($id);

        $viewData['data'] = $data;
        $viewData['editData'] = $editData;
        return view('admin.form.edit',$viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = $this->validator($request->all(),"edit");
        if ($validator->fails()) {
            return redirect()->route('formEdit',['id'=>$request->id])
                ->withErrors($validator)
                ->withInput();
        }

        // Load all form data

        $edit = $this->form->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                    'probtp.success_message',
                    [
                        'module' => __('probtp.form'),
                        'action' => __('probtp.action_update')
                    ]
            ));
        }

        return redirect()->route('formIndex');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all form data

        $destroy = $this->form->remove($request->id);

        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message_form'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.form'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('formIndex');
    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {

        // Load all module data
        $data = $this->form->trasheddata();
        return view('admin.form.formbin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all category data

        $restore = $this->form->restoreform($request->form_id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.form'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('formTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {

        $destroy = $this->form->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.form'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('formTrashIndex');
    }

    /**
     * Replicate the Form listing.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function replicate(Request $request)
    {
        $duplicateid = $this->form->duplicate($request->form_id);

        if (!$duplicateid) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.form'),
                    'action' => __('probtp.action_copied')
                ]
            ));
        }
        return redirect()->route('formIndex');
    }
	
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @param  string  $mode
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data,$mode)
    {
        $rules = array(
            'title' => 'required|max:50',
            'content' => 'required|min:10',
            'status' => 'required',
        );

        return Validator::make($data, $rules);
    }


}
