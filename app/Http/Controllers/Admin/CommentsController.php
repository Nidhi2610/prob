<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Comments;
use Validator;
use Illuminate\Support\Facades\Input;


class CommentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $comment;
    protected $category;

    public function __construct(Comments $comment,Category $category)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->comment = $comment;
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Load all form data
        if(Input::get('id')){
            $data = $this->comment->getCommentuserID(Input::get('id'));
        }else if(Input::get('cat_id')){
            $data = $this->comment->getCommentByCategory(Input::get('cat_id'));
        }else{
            $data = $this->comment->getAll();
        }

        $categoryFormation = $this->category->getAllParentFormationCategory();
        $categoryResource = $this->category->getAllParentResourceCategory();

        return view('admin.comment.index',["data"=>$data,"categoryFormation" => $categoryResource,"categoryResource" => $categoryFormation]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request  $request)
    {
        // Load all form data

        $destroy = $this->comment->remove($request->id);

        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.comment_delete_sucess'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.comment'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('commentIndex');
    }

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {

        // Load all module data
        $data = $this->comment->trasheddata();
        return view('admin.comment.commentbin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all category data

        $restore = $this->comment->restorecomment($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.comment'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('commentTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {

        $destroy = $this->comment->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.comment'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('commentTrashIndex');
    }

    /*
     * Get comments by Category Id
     *
     * */
    public function getCommentsByCategoryId(Request  $request)
    {

        $data =  $this->comment->getCommentByCategory($request->catgory_id);
        $categoryFormation = $this->category->getAllParentFormationCategory();
        $categoryResource = $this->category->getAllParentResourceCategory();
        return view('admin.comment.index',["data"=>$data,"categoryFormation" => $categoryResource,"categoryResource" => $categoryFormation,'category_id' => $request->catgory_id]);
    }

}