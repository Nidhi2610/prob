<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comments;
use App\Models\ModuleCategory;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use App\Helpers\LaraHelpers;
use App\Models\Entreprise;
use Event;
use App\Events\SendMail;
use App\Models\EmailTemplate;
use App\Models\CollaborateurCategory;
use Config;
use Auth;
use Hash;
use Carbon\Carbon;
use DB;
use App\Models\Module;
use App\Models\UserModuleComplete;
use App\Models\getMostAccessedModule;
use App\Models\UserLogin;
use Log;
use Illuminate\Http\Response;


class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user;
    protected $entreprise;
    protected $emailTemplate;
    protected $comments;
    protected $category;
    protected $module_category;
    protected $collaborateurCategory;
    protected $userLogin;

    public function __construct(User $user,Entreprise $entreprise,EmailTemplate $emailTemplate,Comments $comments,Category $category,ModuleCategory $module_category , CollaborateurCategory $collaborateurCategory,UserLogin $userLogin)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->entreprise = $entreprise;
        $this->emailTemplate = $emailTemplate;
        $this->comments = $comments;
        $this->category = $category;
        $this->module_category = $module_category;
        $this->collaborateurCategory = $collaborateurCategory;
        $this->userLogin = $userLogin;
    }


    /**
     * Get a data for statistics which @unused
     * contains all the arrays
     * for each charts
     *
     * @return view
     */

    public function getStatistics()
    {
        $responsable = User::where('user_type',3)->count();
        $admin = User::where('user_type',2)->count();
        $formature = User::where('user_type',5)->count();
        $collaberator = User::where('user_type',4)->count();
        $user  = array();
        $user['Responsable '] = $responsable;
        $user['Administrateur'] = $admin;
        $user['Formateur'] = $formature;
        $user['Collaborateur'] = $collaberator;

        // bar chart
//        $res = $this->getResponsibleByAssignedModule();
      //  $ent = $this->getEntreprises();

        // bar chart for entreprise assign to user + collab assign to user
        $collectionOfres = $this->getAssignEntAndCollab();
        // $ent = $this->user->getAllEntreprisebyUserid();
        $getUserCommentData = $this->getUserCommentData();

        //get recent active user
        $mostRecent = $this->getRecentlyActiveUser();

        // get most comlpeted modules
        $mostCompleted = $this->getMostcompletedModule();

        $mostAccessed = $this->getMostAccessedModules();
        $comments = $this->getcommentsbyUserData();
        $time = $this->mostTimeSpentonSystem();
        // print_r($comments);exit;
        // $data  =  json_encode($user);  getAllColloborateurFormationResource , 'res'=>$res ,'collectionOfres'=>$collectionOfres
        return view('admin.user.statistics.statistics',['user'=>$user ,'getUserCommentData'=>$getUserCommentData,'mostRecent'=>$mostRecent,"mostCompleted"=>$mostCompleted,'mostAccessed'=>$mostAccessed,"commentsData"=>$comments,"time" =>$time]);
    }

    /*
    *   get the list of entreprise
    *  according to user type
    *
    *   @param \Illuminate\Http\Request  $request
    *   @return JSON Response with  name and counter
    * */
    public function getbarChartData(Request $request){
        $user_type = $request->data;
        $entreprise_user = array();
        $entreprises = Entreprise::where('deleted_at',Null)->get();
        foreach ($entreprises as $key => $value) {
            $user = User::where('entreprise_id',$value->id)
                ->where('user_type',$user_type)
                ->where('deleted_at',Null)
                ->count();
            // $entreprise_user[entreprise id]=  userid;
            $entreprise_user[$value->name]=  $user;
        }
        //$user = User::where('user_type',$user_type)->count();

        return $entreprise_user;
    }

    /*
    *   get the list of user
    *   with comments list
    *
    *   @return array $userComment with user name and count of comments
    * */
    public function getUserCommentData(){
        $userComment = array();
        $user = User::where('deleted_at',Null)
            ->get();
        $userData=[];
        foreach ($user as $key => $value) {
            $comment = Comments::where('user_id',$value->id)->count();
            $userData['name'] = $value->name;
            $userData['comment'] = $comment;
            $userData['id'] = $value->id;
            $userComment[] = $userData;
        }
//        print_r($userComment);exit;
        return view('admin.user.statistics.barChartComments',['getUserCommentData'=>json_encode($userComment)]);
    }

    /*
   *   get the list of user
   *   with comments list  using date filter
   *
   *   @return array $userComment with user name and count of comments
   * */
    public function getUserCommentDataByDate(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        $userComment = [];
        $user = User::where('deleted_at',Null)
            ->get();
        if(empty($sDate) || empty($eDate)){
            $userData=[];
            foreach ($user as $key => $value) {
                $comment = Comments::where('user_id',$value->id)->count();
                $userData['name'] = $value->name;
                $userData['comment'] = $comment;
                $userData['id'] = $value->id;
                $userComment[] = $userData;
            }
        }else{

            foreach ($user as $key => $value) {
                $userData=[];
                $comment = Comments::where('user_id',$value->id)->whereBetween('created_at',array($sDate,$eDate))->count();
                $userData['name'] = $value->name;
                $userData['comment'] = $comment;
                $userData['id'] = $value->id;
                $userComment[] = $userData;
            }

        }

        return json_encode($userComment);
    }

    /*
    *   get the list of user which are
    *   recently login
    *
    *   @return $mostRecent[]  with name and last login date
    * */
    public function getRecentlyActiveUserData(){
//            if($request->isMethod('GET')){
                $val = Carbon::now();
            $mostRecent = User::select('name','last_login','user_type')
                ->where('last_login' , '<' ,$val )
                ->orderBy('last_login','DESC')
//            ->take(30) //will be removed
                ->get();
            //$mostRecentData =  json_encode($mostRecent);
            return view('admin.user.statistics.tableRecentActive',['mostRecent'=>$mostRecent]);
//        }
    }

    /*
   *   get the list of user which are
   *   recently login  ajax response
   *   filter with date nd user type
   *
   *   @return $mostRecent[]  with name and last login date
   * */
    public function getRecentlyActiveUserFilter(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        $val = Carbon::now();
        if(!empty($request->module_type)){
            $mostRecent = User::select('name','last_login','user_type')
                ->where('last_login' , '<' ,$val )
                ->where('user_type' , $request->module_type )
                ->orderBy('last_login','DESC')
//            ->take(30) //will be removed
                ->get();

            return response(view('admin.user.statistics.recentActiveDatatable',array('mostRecent'=>$mostRecent))->render());
        }else if (!empty($sDate) && !empty($eDate)){

            $mostRecent = User::select('name','last_login','user_type')
                ->where('last_login' , '<' ,$val )
                ->whereBetween('last_login' , array($sDate,$eDate) )
                ->orderBy('last_login','DESC')
//            ->take(30) //will be removed
                ->get();
            return response(view('admin.user.statistics.recentActiveDatatable',array('mostRecent'=>$mostRecent))->render());

        }else{
            $mostRecent = User::select('name','last_login','user_type')
                ->where('last_login' , '<' ,$val )
                ->orderBy('last_login','DESC')
//            ->take(30) //will be removed
                ->get();
            return response(view('admin.user.statistics.recentActiveDatatable',array('mostRecent'=>$mostRecent))->render());

        }

        return response(view('admin.user.statistics.recentActiveDatatable',array('mostRecent'=>$mostRecent))->render());
    }

    /*
    *   get the list of most completed
    *   modules by user
    *
    *   @return $modules[]  with title and conut
    * */
    public function getMostcompletedModule(){

        $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'),'tbl_module.id','tbl_module.module_type')
            ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
            ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
            ->where('tbl_module.deleted_at',NULL)
            ->groupBy('user_module_complete.module_id')
            ->orderBy('total','DESC')
            ->take(30)->get();
        return view('admin.user.statistics.barChartcompletedModule',["mostCompleted"=>$modules]);
    }

    /*
    *   get the list of most completed
    *   modules by user with $module_type
    *   contains filter of Start and end date
    *
    *   @param \Illuminate\Http\Request  $request
    *   @return $modules[]  with title and conut
    * */
    public function getbarChartforMostCompletedData(Request $request){
        $module_type = $request->module_type;
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        if($module_type != "A"){
                if(empty($sDate) || empty($eDate)) {
                    $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'),'tbl_module.id','tbl_module.module_type')
                        ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
                        ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
                        ->where('tbl_module.deleted_at',NULL)
                        ->where('tbl_module.module_type',$module_type)
                        ->groupBy('user_module_complete.module_id')
                        ->orderBy('total','DESC')
                        ->take(30)->get();
                    return $modules;
                }
            $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'),'tbl_module.id','tbl_module.module_type')
                ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
                ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
                ->where('tbl_module.deleted_at',NULL)
                ->where('tbl_module.module_type',$module_type)
                ->whereBetween('tbl_module.created_at',array($sDate,$eDate))
                ->groupBy('user_module_complete.module_id')
                ->orderBy('total','DESC')
                ->take(30)
                ->get();
            return $modules;
        }else{
            if(empty($sDate) || empty($eDate)) {
                $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'))
                    ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
                    ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
                    ->where('tbl_module.deleted_at',NULL)
                    ->groupBy('user_module_complete.module_id')
                    ->orderBy('total','DESC')
                    ->take(30)->get();
                return $modules;
            }
            $modules = Module::select('tbl_module.title',DB::raw('count(user_module_complete.module_id) as total'))
                ->leftjoin('tbl_module_category','tbl_module_category.module_id','=','tbl_module.id')
                ->leftjoin('user_module_complete','user_module_complete.module_id','=','tbl_module_category.id')
                ->where('tbl_module.deleted_at',NULL)
                ->whereBetween('tbl_module.created_at',array($sDate,$eDate))
                ->groupBy('user_module_complete.module_id')
                ->orderBy('total','DESC')
                ->take(30)->get();
            return $modules;
        }
    }

    /*
     *   get the list of most Accessed
     *   modules
     *
     *
     *  @return $modules[]  JSON Response
     * */
    public function getMostAccessedModules(){
        $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id','tbl_module.id','tbl_module.module_type')
            ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
            ->where('tbl_module.deleted_at','=',NULL)
            ->orderBy('get_most_accessed_module.counter','desc')
            ->take(30)->get();

        $mostAccessed =   json_encode($modules);
        return view('admin.user.statistics.barChartAccessedModule',['mostAccessed'=>$mostAccessed]);

    }

    /*
     *   get the list of most Accessed
     *   modules for $module_type with
     *  filter for start date and end date
     *
     *  @param \Illuminate\Http\Request  $request
     *  @return $modules[]  JSON Response
     * */
    public function getbarChartforMostAccessedData(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        $module_type = $request->module_type;
        if($module_type != "A"){
            if(empty($sDate) || empty($eDate)) {
                $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id','tbl_module.id','tbl_module.module_type')
                    ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
                    ->where('tbl_module.module_type',$module_type)
                    ->where('tbl_module.deleted_at','=',NULL)
                    ->orderBy('get_most_accessed_module.counter','desc')
                    ->take(30)->get();
                return json_encode($modules);
            }
            $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id','tbl_module.id','tbl_module.module_type')
                ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
                ->where('tbl_module.module_type',$module_type)
                ->where('tbl_module.deleted_at','=',NULL)
                ->whereBetween('tbl_module.created_at',array($sDate,$eDate))
                ->orderBy('get_most_accessed_module.counter','desc')
                ->take(30)->get();
           // print_r($modules);exit;
            return json_encode($modules);
        }else{
            if(empty($sDate) || empty($eDate)) {
                $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id','tbl_module.id','tbl_module.module_type')
                    ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
                    ->where('tbl_module.deleted_at','=',NULL)
                    ->orderBy('get_most_accessed_module.counter','desc')
                    ->take(30)->get();
                return json_encode($modules);
            }
            $modules = Module::select('tbl_module.title','get_most_accessed_module.counter','get_most_accessed_module.module_id','tbl_module.id','tbl_module.module_type')
                ->leftjoin('get_most_accessed_module','get_most_accessed_module.module_id','=','tbl_module.id')
                ->where('tbl_module.deleted_at','=',NULL)
                ->whereBetween('get_most_accessed_module.created_at',array($sDate,$eDate))
                ->orderBy('get_most_accessed_module.counter','desc')
                ->take(30)->get();

            $mostAccessed =   json_encode($modules);
            return $mostAccessed;
        }
    }

    /*
     *   get the list of comments given by category
     *
     *
     *  @return JSON Response $commentsDataArray
     * */
    public function getcommentsbyCategoryData(){
        $commentsData = array();
        $comments = Comments::select('category_id')->where('deleted_at',Null)->get()->toArray();

        $cat = array();

        foreach($comments as $k => $val){
            if(strpos($val['category_id'],":")){
                $cate_id = stristr($val['category_id'],":",true);
            }else{
                $cate_id = $val['category_id'];
            }
            $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
        }

        $commentsDataArray = array();
        foreach ($cat as $key => $value) {
            $cats = array();
            $counter  = Comments::select( DB::raw('count(id) as total') )
                ->where('category_id','like',$value.'%')
                ->take(30)->get()->toArray();
            $cat_name = Category::select('name','id')->where('id',$value)->where('parent_id',0)->first();
            if(!empty($cat_name->name)){
                $cats['name'] = $cat_name->name;
                $cats['cat_id'] = $cat_name->id;
                $cats['total'] = $counter[0]['total'];
                $commentsDataArray[]= $cats;
            }

        }
      return json_encode($commentsDataArray);
    }

    /*
    *   get the list of comments given by category
    *
    *
    *  @return JSON Response $commentsDataArray
    * */
    public function getcommentsbyCategory(){

        $comments = $this->getcommentsbyCategoryData();
        return view('admin.user.statistics.barChartscommentsbyCat',["commentsData"=>$comments]);
    }

    /*
     *   get the list of comments given by user
     *
     *
     *  @return JSON Response $commentsData
     * */
    public function getbarChartforCommentsByDate(Request $request){

        $module_type = $request->module_type;
        $startDate = date("Y-m-d", strtotime($request->startDate));
        $endDate = date("Y-m-d", strtotime($request->endDate));

        if($module_type != "A"){
            $comments = Comments::select('category_id')->where('deleted_at',Null)->whereBetween('created_at',array($startDate,$endDate))->get()->toArray();

            $cat = array();
            $commentsDataArray = array();
            foreach($comments as $k => $val){
                if(strpos($val['category_id'],":")){
                    $cate_id = stristr($val['category_id'],":",true);
                }else{
                    $cate_id = $val['category_id'];
                }
                $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
            }
            foreach ($cat as $key => $value) {
                $cats = array();
                $counter  = Comments::select( DB::raw('count(id) as total') )
                    ->where('category_id','like',$value.'%')
                    ->take(30)->get()->toArray();
                $cat_name = Category::select('name','id')->where('id',$value)->where('category_classification',$module_type)->where('parent_id',0)->first();

                if(!empty($cat_name->name)){
                    $cats['name'] = $cat_name->name;
                    $cats['cat_id'] = $cat_name->id;
                    $cats['total'] = $counter[0]['total'];
                    $commentsDataArray[]= $cats;
                }
            }
            return json_encode($commentsDataArray);
        }else{
            $data['startDate']=$startDate;
            $data['endDate']= $endDate ;

            $comments = Comments::select('category_id')->where('deleted_at',Null)->whereBetween('created_at',array($startDate,$endDate))->get()->toArray();

            $cat = array();
            $commentsDataArray = array();
            foreach($comments as $k => $val){
                if(strpos($val['category_id'],":")){
                    $cate_id = stristr($val['category_id'],":",true);
                }else{
                    $cate_id = $val['category_id'];
                }
                $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
            }

            foreach ($cat as $key => $value) {
                $cats = array();
                $counter  = Comments::select( DB::raw('count(id) as total') )
                    ->where('category_id','like',$value.'%')
                    ->take(30)->get()->toArray();
                $cat_name = Category::select('name','id')->where('id',$value)->where('parent_id',0)->first();

                if(!empty($cat_name->name)){
                    $cats['name'] = $cat_name->name;
                    $cats['cat_id'] = $cat_name->id;
                    $cats['total'] = $counter[0]['total'];
                    $commentsDataArray[]= $cats;
                }
            }

//            $commentsData = $this->getcommentsbyUserData();
            return json_encode($commentsDataArray);
        }

        //$comments = Comments::select('category_id')->where('deleted_at',Null)->distinct()->get();
//        foreach ($comments as $key => $value) {
//
//                $categoryId = explode(':', $value->category_id);
//                $count  = Comments::select( DB::raw('count(id) as total'))->where('category_id','like',$categoryId[0].'%')->get()->toArray();
//                $categoryName = Category::select('name')->where('category_classification',$module_type)->where('id',$categoryId[0])->get()->toArray();
//
//                if(!empty($categoryName)){
//                    $comment = array();
//                    $comment['name'] = $categoryName[0]['name'];
//                    $comment['total'] = $count[0]['total'];
//                    $commentsData[] = $comment;
//                }
//
//            }


    }

    /*
     *   get the list of comments given
     *  by user filter for start and end date
     *
     *  @return JSON Response $commentsData
     * */
    public function getbarChartforCommentsByType(Request $request){
        $module_type = $request->module_type;
        $date['startDate'] = $startDate = date("Y-m-d", strtotime($request->startDate)) ;
        $date['endDate'] =  $endDate= date("Y-m-d", strtotime($request->endDate));

        $commentsDataArray = array();
        if($module_type != "A"){
            if(empty($startDate) || empty($endDate)) {
                $comments = Comments::select('category_id')->where('deleted_at',Null)->get()->toArray();
            }else{
                $comments = Comments::select('category_id')->where('deleted_at',Null)->whereBetween('created_at',array($startDate,$endDate))->get()->toArray();
            }

            $cat = array();
            foreach($comments as $k => $val){
                if(strpos($val['category_id'],":")){
                    $cate_id = stristr($val['category_id'],":",true);
                }else{
                    $cate_id = $val['category_id'];
                }
                $d =   (in_array($cate_id,$cat))?  : array_push($cat,$cate_id) ;
            }


            foreach ($cat as $key => $value) {
                $cats = array();
                $counter  = Comments::select( DB::raw('count(id) as total') )
                    ->where('category_id','like',$value.'%')
                    ->take(30)->get()->toArray();
                $cat_name = Category::select('name','id')->where('id',$value)->where('category_classification',$module_type)->where('parent_id',0)->first();
                if(!empty($cat_name->name)){
                    $cats['name'] = $cat_name->name;
                    $cats['cat_id'] = $cat_name->id;
                    $cats['total'] = $counter[0]['total'];
                    $commentsDataArray[]= $cats;
                }
            }
            return json_encode($commentsDataArray);
        }else{
//            $commentsData = $this->getcommentsbyCategoryData($date);
            $commentsData = $this->getcommentsbyCategoryData();
            return ($commentsData);
        }


        //$comments = Comments::select('category_id')->where('deleted_at',Null)->distinct()->get();
//        foreach ($comments as $key => $value) {
//
//                $categoryId = explode(':', $value->category_id);
//                $count  = Comments::select( DB::raw('count(id) as total'))->where('category_id','like',$categoryId[0].'%')->get()->toArray();
//                $categoryName = Category::select('name')->where('category_classification',$module_type)->where('id',$categoryId[0])->get()->toArray();
//
//                if(!empty($categoryName)){
//                    $comment = array();
//                    $comment['name'] = $categoryName[0]['name'];
//                    $comment['total'] = $count[0]['total'];
//                    $commentsData[] = $comment;
//                }
//
//            }


    }

    /*
     *   get the list of user with
     * total time spent by them
     *
     *  @return JSON Response $time
     * */
    public function mostTimeSpentonSystem(){
        $time = User::select('users.name','tbl_users_login.total_time','users.id')
            ->leftjoin('tbl_users_login','tbl_users_login.user_id','=','users.id')
            ->where('users.deleted_at',Null)
            ->orderBy('tbl_users_login.total_time','DESC')
            ->get();
        $total_time =  json_encode($time);
        return view('admin.user.statistics.barCharTotalTime',['time'=>$total_time]);

    }

    /*
     *   get the list of user with
     * total time spent by them by date filter
     *
     *  @return JSON Response $time
     * */
    public function mostTimeSpentonSystemByDate(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        if(empty($sDate) || empty($eDate)) {
            $time = User::select('users.name','tbl_users_login.total_time','users.id')
                ->leftjoin('tbl_users_login','tbl_users_login.user_id','=','users.id')
                ->where('users.deleted_at',Null)
                ->orderBy('tbl_users_login.total_time','DESC')
                ->get();
            return json_encode($time);
        }
        $time = User::select('users.name','tbl_users_login.total_time','users.id')
            ->leftjoin('tbl_users_login','tbl_users_login.user_id','=','users.id')
            ->where('users.deleted_at',Null)
            ->whereBetween('users.created_at',array($sDate,$eDate))
            ->orderBy('tbl_users_login.total_time','DESC')
            ->get();
        $total_time =  json_encode($time);
       return $total_time;

    }

    /*
     *  user list for pie chart
     *
     *
     *  @return view
     * */
    public function getPieChart(){
        $responsable = User::where('user_type',3)->count();
        $admin = User::where('user_type',2)->count();
        $formature = User::where('user_type',5)->count();
        $collaberator = User::where('user_type',4)->count();
        $user  = array();
        $user['Responsable '] = $responsable;
        $user['Administrateur'] = $admin;
        $user['Formateur'] = $formature;
        $user['Collaborateur'] = $collaberator;
        return view('admin.user.statistics.piechart',['user'=>json_encode($user)]);
    }

    /*
     *  user list for pie chart filter for start dte and end date
     *
     *
     *  @return view
     * */
    public function getPieChartByDate(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));
        $responsable = User::where('user_type',3)->whereBetween('created_at',array($sDate,$eDate))->count();
        $admin = User::where('user_type',2)->whereBetween('created_at',array($sDate,$eDate))->count();
        $formature = User::where('user_type',5)->whereBetween('created_at',array($sDate,$eDate))->count();
        $collaberator = User::where('user_type',4)->whereBetween('created_at',array($sDate,$eDate))->count();
        $user  = array();
        $user['Responsable '] = $responsable;
        $user['Administrateur'] = $admin;
        $user['Formateur'] = $formature;
        $user['Collaborateur'] = $collaberator;
        return json_encode($user);
    }

    /*
     *   get the list of entreprise
     *
     *   @return JSON Response with name and counter
     * */
    public function getEntrepriseData(){
        $entreprise_user = array();
        $entreprise = array();
        $entreprises = Entreprise::where('deleted_at',Null)->get();
        foreach ($entreprises as $key => $value) {
            $responsable = User::where('entreprise_id',$value->id)->where('user_type',3)->where('deleted_at',Null)->count();
            $collaborature = User::where('entreprise_id',$value->id)->where('user_type',4)->where('deleted_at',Null)->count();

            $entreprise_user['id'] = $value->id;
            $entreprise_user['entreprise'] = $value->name;
            $entreprise_user['responsable'] = $responsable;
            $entreprise_user['collaborateur'] = $collaborature;
            $entreprise_user['total_user'] = $responsable + $collaborature;
            $entreprise[]=$entreprise_user;
        }
        //  print_r($entreprise);exit;
        $entreprises_user_obj= json_encode($entreprise);
        return view('admin.user.statistics.barChartEntreprise',['entreprises_user_obj'=>$entreprises_user_obj]);
    }

    /*
    *   get the list of data for
    *   entreprise to assign
    *   the Collaborature   ajax - filter for start& end date
    *
    *   @return array wuth name and counter
    * */
    public function getEntreprisDataByDate(Request $request){
        $sDate = date("Y-m-d", strtotime($request->startDate));
        $eDate = date("Y-m-d", strtotime($request->endDate));

        $entreprise_user = array();
        $entreprise = array();
        if(empty($sDate) || empty($eDate)) {
            $entreprises = Entreprise::where('deleted_at',Null)->get();
        }else{
            $entreprises = Entreprise::where('deleted_at',Null)->whereBetween('created_at',array($sDate,$eDate))->get();
        }
        foreach ($entreprises as $key => $value) {
            $responsable = User::where('entreprise_id',$value->id)->where('user_type',3)->where('deleted_at',Null)->count();
            $collaborature = User::where('entreprise_id',$value->id)->where('user_type',4)->where('deleted_at',Null)->count();

            $entreprise_user['entreprise'] = $value->name;
            $entreprise_user['responsable'] = $responsable;
            $entreprise_user['collaborateur'] = $collaborature;
            $entreprise_user['total_user'] = $responsable + $collaborature;
            $entreprise[]=$entreprise_user;
        }
        //  print_r($entreprise);exit;
        $entreprises_user_obj= json_encode($entreprise);
        return $entreprises_user_obj;
    }
}
