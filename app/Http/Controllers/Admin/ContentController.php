<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Content;
use Validator;

class ContentController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $content;

    public function __construct(Content $content)
    {
        $this->middleware(['auth','AuthenticateUser']);
        $this->content = $content;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Load all form data

        $data = $this->content->init();

        return view('admin.content.edit',["data"=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $edit = $this->content->edit($request->all());

        if (!$edit) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.update_success'));
        }

        return redirect()->route('getContent');
    }
}
