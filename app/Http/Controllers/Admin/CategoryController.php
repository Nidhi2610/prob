<?php

namespace App\Http\Controllers\Admin;
use Log;
use  App\Http\Controllers\Controller;
use App\Models\Collaborateur;
use App\Models\EntrepriseCategory;
use App\Models\CollaborateurCategory;
//use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Entreprise;
use App\Helpers\LaraHelpers;
use App\Models\ModuleCategory;
use App\Models\User;
use App\Models\Form;
use App\Models\Quiz;
use App\Models\Module;
use App\Models\Comments;
use Validator;
use Config;
use Auth;
use App\Models\UserModuleCategoryForm;
use App\Models\UserModuleCategoryQuiz;
class CategoryController extends Controller
{
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $category;
    protected $modulecategory;
    protected $module;
    protected $form;
    protected $quiz;
    protected $user_module_category_form;
    protected $user_module_category_quiz;
    protected $comments;

    public function __construct(User $user,  ModuleCategory $modulecategory,UserModuleCategoryForm $user_module_category_form,UserModuleCategoryQuiz $user_module_category_quiz,Form $form,Quiz $quiz,Module $module,Category $category, Comments $comments)
	{
		$this->middleware(['auth','AuthenticateUser']);
        $this->user = $user;
        $this->modulecategory = $modulecategory;
        $this->category = $category;
        $this->module = $module;
        $this->form = $form;
        $this->quiz = $quiz;
        $this->user_module_category_form = $user_module_category_form;
        $this->user_module_category_quiz = $user_module_category_quiz;
        $this->comments = $comments;
    }

    /**
     * Display a listing of the resource.
     * @param $type
     * @return \Illuminate\Http\Response
     */
	public function index($type = "")
	{

		// Load all category data
		$Category = new Category;
		// $data = $Category->getCategoryByEnterpriseId(Auth::user()->entreprise_id);
        $data = $Category->init($type);
        $Entreprise = new Entreprise();
		$entObj = $Entreprise->getallentreprise();

		foreach($data as $key => $category)
        {
            $categorydata = $this->modulecategory->getAdminassignedmodulebycategoryid($category->id);
            $data[$key]['IsModuledata'] = 0;
            if($categorydata->count() > 0)
            {
                $data[$key]['IsModuledata'] = 1;

            }
        }

       return view('admin.category.index',["data"=>$data,"entObj" => $entObj,"type" => $type]);
	}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create($type = "")
	{
		// Load all category data
		$Category = new Category;
		$data = $Category->init($type);
		$data = $data->toArray();
        $viewData = LaraHelpers::get_category_type();
		return view('admin.category.create',["data"=>$data,"viewData" => $viewData,"type" => $type]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function store(Request $request)
	{

		$validator = $this->validator($request->all());
		if ($validator->fails()) {
			//return redirect()->route('categoryAdd')
			return redirect('category/create/'.$request->category_classification)
				->withErrors($validator)
				->withInput();
		}

		// Load all category data
		$Category = new Category;
		$create = $Category->insert($request->all());

		if (!$create) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
				'probtp.category_add_success'
			));
		}

		return redirect('category/'.$request->category_classification);
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	public function show($id)
	{
		//
	}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		 
	public function edit($id,$type)
	{
		// Load all category data
		$Category = new Category;
		$data = $Category->init($type);
        $data = $data->toArray();
		$editData = $Category->getCategoryByID($id);
        $viewData = LaraHelpers::get_category_type();
        return view('admin.category.edit',[
			"data"=>$data,
			'editData'=>$editData,
            'viewData' =>$viewData,
            'type' => $type,
		]);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function update(Request $request)
	{
		$validator = $this->validator($request->all());
		if ($validator->fails()) {
			return redirect()->route('categoryEdit',['id'=>$request->id,'type'=>$request->category_classification])
				->withErrors($validator)
				->withInput();
		}

		// Load all category data
		$Category = new Category;
		$edit = $Category->edit($request->all());

		if (!$edit) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
					'probtp.category_update_sucess'));
		}

		return redirect('category/'.$request->category_classification);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function destroy(Request  $request)
	{
		// Load all category data
		$Category = new Category;
		$destroy = $Category->remove($request->id);
        $type = $request->category_classification;
		if (!$destroy) {
			session()->flash('fail',__('probtp.fail_message_category'));
		} else{
			session()->flash('success',__(
				'probtp.category_delete_success'
			));
		}
	}

    /**
     * Update Category Tree.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	
	public function updatecategorytree(Request $request)
	{

		$Category = new Category;
		$categoryData = $Category->updateAllCategory($request->all());
		
		if (!$categoryData) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
				'probtp.success_message',
				[
					'module' => __('probtp.record'),
					'action' => __('probtp.action_update')
				]
			));
		}

		return 1;
	}

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata($type)
    {
        // Load all module data
        $Category = new Category;
        $data = $Category->trasheddata($type);
        return view('admin.category.categorybin',["data"=>$data,"type"=>$type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all category data
        $Category = new Category;
        $restore = $Category->restorecategory($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.record'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect('category/gettrashdata/'.$request->category_classification);
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {
        $Category = new Category;
        $destroy = $Category->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.category'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect('category/gettrashdata/'.$request->category_classification);
    }

    /**
     * Replicate the Category .
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function replicate($id)
    {
        $Category = new Category;
        $duplicateid = $Category->duplicate($id);

        if($duplicateid){

            session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.category'),
                    'action' => __('probtp.action_copied')
                ]
            ));
        }
        else{

            session()->flash('fail',__('probtp.fail_message'));
        }

        return redirect('category/'.$duplicateid->category_classification);
    }

    /**
     * Get assigned Entreprise.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getEntreprise(Request  $request)
    {
        $entreprise = new EntrepriseCategory();
        $entreprisedata = $entreprise->getEntrepriseById('category_id',$request->id);
        return $entreprisedata;
    }

    /**
     * Store assigned Enterprise module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function entrepriseCategory(Request $request)
    {

        $category_id = $request->category_id;
        $entreprise = new EntrepriseCategory();

        $delete = $entreprise->remove([
            'category_id' => $category_id,
        ]);

        if($request->entreprise_id == null)
        {
            $create = true;
        }

        else{

            foreach($request->entreprise_id as $entreprise_id)
            {
                $entreprise = new EntrepriseCategory();
                $create = $entreprise->insert([

                    'category_id' => $category_id,
                    'entreprise_id' => $entreprise_id,

                ]);
            }

        }

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect('category/'.$request->category_classification);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
	protected function validator(array $data)
	{  $rules = array(
        'name' => 'required',
        'parent_category' => 'required',
       // 'category_type_id' => 'required',
        );
		return Validator::make($data,$rules);
	}

    /**
 * Get List of Category assigned to responsable.
 *
 *
 * @return \Illuminate\Http\Response
 */

    public function getResponsableCategory()
    {

        $Category = new Category;
        $data =  $Category->getCategoryByEnterpriseId(Auth::user()->entreprise_id);
        $userObj = $this->user->init();
        return view('admin.category.responsablecategory',["data"=>$data,"userObj" => $userObj]);
    }

    /**
     * Get List of Category assigned to responsable.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function getResponsableResource()
    {
        $Category = new Category;
        $data =  $Category->getResourceByEnterpriseId(Auth::user()->entreprise_id);
        return $data;
    }

    /**
     * Get List of Category assigned to responsable.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function getResponsableFormation()
    {
        $Category = new Category;
        $data =  $Category->getFormationByEnterpriseId(Auth::user()->entreprise_id);
        return $data;
    }

    /**
     * Store assigned category to colloboratuer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function insertcollaborateurcategory(Request $request){

        $category_id = $request->category_id;
        $user_id = Auth::user()->id;
        $collaborateurcategory = new CollaborateurCategory();
        $delete = $collaborateurcategory->remove([
            'category_id' => $category_id,
        ]);

        if($request->collaborateur_id == null)
        {
            $create = true;
        }

        else{

            foreach($request->collaborateur_id as $collaborateur_id)
            {
                $collaborateurcategory = new CollaborateurCategory();
                $create = $collaborateurcategory->insert([

                    'category_id' => $category_id,
                    'collaborateur_id' => $collaborateur_id,
                    'user_id' => $user_id,

                ]);
            }

        }

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect('/index');
    }

    /**
     * Get assigned CollaborateurUser.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getCollaborateurUser(Request  $request)
    {
        $collaborateurcategory = new CollaborateurCategory();
        $collaborateuruser = $collaborateurcategory->getuser('category_id',$request->id);
        return $collaborateuruser;
    }

    /**
     * Detail page of cateogry
     *
     * @return \Illuminate\Http\Response
     */

    public function detail($id,$category_id)
    {
        $parent_category_data = [];
        if(Auth::user()->user_type == 3 || Auth::user()->user_type == 4){
            return redirect('/home');
        }

//        $moduleid_data = $this->modulecategory->getAdminassignedmodulebycategoryid($category_id);
//
//        foreach($moduleid_data as $key => $module){
//            $moduleid_data[$key]['form_data'] = $this->form->getAdminFormByID($module->Module->form_id);
//            $moduleid_data[$key]['quiz_data'] = $this->quiz->getAdminQuizByID($module->Module->quiz_id);
//            $moduleid_data[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
//            $moduleid_data[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
//        }
        $category_array = explode(":", $category_id);

        foreach ($category_array as $categoryKey => $categoryData) {

            $moduleid_datas = $this->modulecategory->getallModulebyCategory([$categoryData]);
            //  if(count($moduleid_datas) > 0){
            $moduleid_data[$categoryKey]['category_name'] = $this->category->getCategoryByID($categoryData)->name;
            $moduleid_data[$categoryKey]['description'] = $this->category->getCategoryByID($categoryData)->description;
            $moduleid_data[$categoryKey]['category_id'] = $categoryData;
            if ($categoryKey != 0) {
                $parent_category_data[] = $this->category->getCategoryByID($categoryData);
            }

            foreach ($moduleid_datas as $key => $module) {

                $moduleid_datas[$key]['form_data'] = $this->form->getFormByID($module->Module->form_id);
                $moduleid_datas[$key]['quiz_data'] = $this->quiz->getQuizByID($module->Module->quiz_id);
                $moduleid_datas[$key]['form_user_data'] = $this->user_module_category_form->getUserForm($module);
                $moduleid_datas[$key]['quiz_user_data'] = $this->user_module_category_quiz->getUserQuiz($module);
            }
            $moduleid_data[$categoryKey]['moduleData'] = $moduleid_datas;
            //}

        }

        $commentsData = $this->comments->getCommentswithPaginate($category_id);
       // $parent_category_data = $this->category->getChildCategoryByParentId($category_id);
        return view('admin.category.preview',["cat_id" => $id,"moduleid_data" => $moduleid_data,"parent_category_data" => $parent_category_data,"category_id"=>$category_id,"commentsData"=>$commentsData]);
    }

    /**
     * Get a Download file.
     *
     * @param  file
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download ($filename){

        $file_path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR . $filename;

        if (file_exists($file_path))
        {
            // Send Download
            return response()->download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        }
        else
        {
            // Error
            return back();
        }

    }
    /**
     * Update Category Tree.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatemoduletree(Request $request)
    {
        $ModuleCategory = new ModuleCategory();
        $categoryData = $ModuleCategory->updateAllModule($request->all());
        echo 1;
    }

    /**
     * Get all Category for search.
     *
     * @return \Illuminate\Http\Response
     */

    public function getIndexSearch($type = "")
    {

        // Load all category data
        $Category = new Category;
        // $data = $Category->getCategoryByEnterpriseId(Auth::user()->entreprise_id);
        $data = $Category->init($type);
        $Entreprise = new Entreprise();
        $entObj = $Entreprise->getallentreprise();

        foreach($data as $key => $category)
        {
            $categorydata = $this->modulecategory->getAdminassignedmodulebycategoryid($category->id);
            $data[$key]['IsModuledata'] = 0;
            if($categorydata->count() > 0)
            {
                $data[$key]['IsModuledata'] = 1;

            }
            if($category->parent_id == 0){
                $data[$key]['ParentCheck'] = 1;
            }elseif($category->parent_id > 0){
                $parentData = $Category->getCategoryByID($category->parent_id);
                if($parentData){
                    $childParentData = $Category->getCategoryByID($parentData->parent_id);
                    if($childParentData){
                        $data[$key]['ParentCheck'] = 3;
                    }else{
                        $data[$key]['ParentCheck'] = 2;
                    }
                }else{
                    $data[$key]['ParentCheck'] = 2;
                }
            }

        }
        return view('admin.category.index_search',["data"=>$data,"entObj" => $entObj,"type" => $type]);
    }

}
