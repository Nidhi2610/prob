<?php

namespace App\Http\Controllers\Admin;

use  App\Http\Controllers\Controller;
use App\Models\Collaborateur;
use App\Models\EntrepriseResource;
use App\Models\CollaborateurResource;
//use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Models\Resource;
use App\Models\Entreprise;
use App\Helpers\LaraHelpers;
use App\Models\ModuleResource;
use App\Models\User;
use App\Models\Form;
use App\Models\Quiz;
use App\Models\Module;
use Validator;
use Config;
use Auth;
use App\Models\UserModuleResourceForm;
use App\Models\UserModuleResourceQuiz;
class ResourceController extends Controller
{
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    protected $resource;
    protected $moduleresource;
    protected $module;
    protected $form;
    protected $quiz;
    protected $user_module_resource_form;
    protected $user_module_resource_quiz;

	public function __construct(User $user,  ModuleResource $moduleresource,UserModuleResourceForm $user_module_resource_form,UserModuleResourceQuiz $user_module_resource_quiz,Form $form,Quiz $quiz,Module $module,Resource $resource)
	{
		$this->middleware(['auth','AuthenticateUser']);
        $this->user = $user;
        $this->moduleresource = $moduleresource;
        $this->resource = $resource;
        $this->module = $module;
        $this->form = $form;
        $this->quiz = $quiz;
        $this->user_module_resource_form = $user_module_resource_form;
        $this->user_module_resource_quiz = $user_module_resource_quiz;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index()
	{
		// Load all resource data
		$Resource = new Resource;
		// $data = $Resource->getResourceByEnterpriseId(Auth::user()->entreprise_id);
        $data = $Resource->init();
        $Entreprise = new Entreprise();
		$entObj = $Entreprise->getallentreprise();

		foreach($data as $key => $resource)
        {
            $resourcedata = $this->moduleresource->getAdminassignedmodulebyresourceid($resource->id);
            $data[$key]['IsModuledata'] = 0;
            if($resourcedata->count() > 0)
            {
                $data[$key]['IsModuledata'] = 1;

            }
        }
       return view('admin.resource.index',["data"=>$data,"entObj" => $entObj]);
	}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function create()
	{
		// Load all resource data
		$Resource = new Resource;
		$data = $Resource->init();
		$data = $data->toArray();
        $viewData = LaraHelpers::get_resource_type();
		return view('admin.resource.create',["data"=>$data,"viewData" => $viewData]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function store(Request $request)
	{
       $validator = $this->validator($request->all());
		if ($validator->fails()) {
			return redirect()->route('resourceAdd')
				->withErrors($validator)
				->withInput();
		}

		// Load all resource data
		$Resource = new Resource;
		$create = $Resource->insert($request->all());

		if (!$create) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
				'probtp.resource_add_success'
			));
		}

		return redirect()->route('resourceIndex');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	 
	public function show($id)
	{
		//
	}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		 
	public function edit($id)
	{
		// Load all resource data
		$Resource = new Resource;
		$data = $Resource->init();
        $data = $data->toArray();
		$editData = $Resource->getResourceByID($id);
        $viewData = LaraHelpers::get_resource_type();

		return view('admin.resource.edit',[
			"data"=>$data,
			'editData'=>$editData,
            'viewData' =>$viewData,
		]);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function update(Request $request)
	{
		$validator = $this->validator($request->all());
		if ($validator->fails()) {
			return redirect()->route('resourceEdit',['id'=>$request->id])
				->withErrors($validator)
				->withInput();
		}

		// Load all resource data
		$Resource = new Resource;
		$edit = $Resource->edit($request->all());

		if (!$edit) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
					'probtp.resource_update_sucess'));
		}

		return redirect()->route('resourceIndex');
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	 
	public function destroy(Request  $request)
	{
		// Load all resource data
		$Resource = new Resource;
		$destroy = $Resource->remove($request->id);

		if (!$destroy) {
			$request->session()->flash('fail',__('probtp.fail_message_resource'));
		} else{
			$request->session()->flash('success',__(
				'probtp.resource_delete_success'
			));
		}

		return redirect()->route('resourceIndex');
	}

    /**
     * Update Resource Tree.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	
	public function updateresourcetree(Request $request)
	{
		$Resource = new Resource;
		$resourceData = $Resource->updateAllResource($request->all());
		
		if (!$resourceData) {
			$request->session()->flash('fail',__('probtp.fail_message'));
		} else{
			$request->session()->flash('success',__(
				'probtp.success_message',
				[
					'module' => __('probtp.resource'),
					'action' => __('probtp.action_update')
				]
			));
		}

		return redirect()->route('resourceIndex');
	}

    /**
     * Get a list of Softdeleted data.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function gettrashdata()
    {
        // Load all module data
        $Resource = new Resource;
        $data = $Resource->trasheddata();
        return view('admin.resource.resourcebin',["data"=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function restorefrombin(Request  $request)
    {
        // Load all resource data
        $Resource = new Resource;
        $restore = $Resource->restoreresource($request->id);

        if (!$restore) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.resource'),
                    'action' => __('probtp.action_restored')
                ]
            ));
        }

        return redirect()->route('resourceTrashIndex');
    }

    /**
     * Remove the Softdata permenantly.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function destroypermenant(Request  $request)
    {
        $Resource = new Resource;
        $destroy = $Resource->removetrash($request->id);
        if (!$destroy) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.resource'),
                    'action' => __('probtp.action_delete')
                ]
            ));
        }

        return redirect()->route('resourceTrashIndex');
    }

    /**
     * Replicate the Resource .
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function replicate($id)
    {
        $Resource = new Resource;
        $duplicateid = $Resource->duplicate($id);

        if($duplicateid){

            session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.resource'),
                    'action' => __('probtp.action_copied')
                ]
            ));
        }
        else{

            session()->flash('fail',__('probtp.fail_message'));
        }

        return redirect()->route('resourceIndex');
    }

    /**
     * Get assigned Entreprise.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getEntreprise(Request  $request)
    {
        $entreprise = new EntrepriseResource();
        $entreprisedata = $entreprise->getEntrepriseById('resource_id',$request->id);

        return $entreprisedata;
    }

    /**
     * Store assigned Enterprise module.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function entrepriseResource(Request $request)
    {


        $resource_id = $request->resource_id;
        $entreprise = new EntrepriseResource();

        $delete = $entreprise->remove([
            'resource_id' => $resource_id,
        ]);

        if($request->entreprise_id == null)
        {
            $create = true;
        }

        else{

            foreach($request->entreprise_id as $entreprise_id)
            {
                $entreprise = new EntrepriseResource();
                $create = $entreprise->insert([

                    'resource_id' => $resource_id,
                    'entreprise_id' => $entreprise_id,

                ]);
            }

        }

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.entreprise'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('resourceIndex');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required',
		]);
	}

    /**
     * Get List of Resource assigned to responsable.
     *
     *
     * @return \Illuminate\Http\Response
     */

    public function getResponsableResource()
    {

        $Resource = new Resource;
        $data =  $Resource->getResourceByEnterpriseId(Auth::user()->entreprise_id);
        $userObj = $this->user->init();
        return view('admin.resource.responsableresource',["data"=>$data,"userObj" => $userObj]);
    }

    /**
     * Store assigned resource to colloboratuer.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function insertcollaborateurresource(Request $request){

        $resource_id = $request->resource_id;
        $user_id = Auth::user()->id;
        $collaborateurresource = new CollaborateurResource();
        $delete = $collaborateurresource->remove([
            'resource_id' => $resource_id,
        ]);

        if($request->collaborateur_id == null)
        {
            $create = true;
        }

        else{

            foreach($request->collaborateur_id as $collaborateur_id)
            {
                $collaborateurresource = new CollaborateurResource();
                $create = $collaborateurresource->insert([

                    'resource_id' => $resource_id,
                    'collaborateur_id' => $collaborateur_id,
                    'user_id' => $user_id,

                ]);
            }

        }

        if (!$create) {
            $request->session()->flash('fail',__('probtp.fail_message'));
        } else{
            $request->session()->flash('success',__(
                'probtp.success_message',
                [
                    'module' => __('probtp.user'),
                    'action' => __('probtp.action_add')
                ]
            ));
        }

        return redirect()->route('responsableResourceIndex');
    }

    /**
     * Get assigned CollaborateurUser.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getCollaborateurUser(Request  $request)
    {
        $collaborateurresource = new CollaborateurResource();
        $collaborateuruser = $collaborateurresource->getuser('resource_id',$request->id);
        return $collaborateuruser;
    }

    /**
     * Detail page of cateogry
     *
     * @return \Illuminate\Http\Response
     */

    public function detail($resource_id)
    {

        if(Auth::user()->user_type == 3 || Auth::user()->user_type == 4){
            return redirect('/home');
        }

        $moduleid_data = $this->moduleresource->getAdminassignedmodulebyresourceid($resource_id);
        foreach($moduleid_data as $key => $module){


            $moduleid_data[$key]['form_data'] = $this->form->getAdminFormByID($module->Module->form_id);
            $moduleid_data[$key]['quiz_data'] = $this->quiz->getAdminQuizByID($module->Module->quiz_id);
            $moduleid_data[$key]['form_user_data'] = $this->user_module_resource_form->getUserForm($module);
            $moduleid_data[$key]['quiz_user_data'] = $this->user_module_resource_quiz->getUserQuiz($module);
        }

        $parent_resource_data = $this->resource->getChildResourceByParentId($resource_id);
        return view('admin.resource.preview',["moduleid_data" => $moduleid_data,"parent_resource_data" => $parent_resource_data,"resource_id"=>$resource_id]);
    }

    /**
     * Get a Download file.
     *
     * @param  file
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function download ($filename){

        $file_path = storage_path() . DIRECTORY_SEPARATOR .'app'. DIRECTORY_SEPARATOR .'public'.DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR .'module' . DIRECTORY_SEPARATOR . $filename;

        if (file_exists($file_path))
        {
            // Send Download
            return response()->download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        }
        else
        {
            // Error
            return back();
        }

    }
    /**
     * Update Resource Tree.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatemoduletree(Request $request)
    {
        $ModuleResource = new ModuleResource();
        $resourceData = $ModuleResource->updateAllModule($request->all());
        echo 1;
    }

    /**
     * Get all Resource for search.
     *
     * @return \Illuminate\Http\Response
     */

    public function getIndexSearch()
    {

        // Load all resource data
        $Resource = new Resource;
        // $data = $Resource->getResourceByEnterpriseId(Auth::user()->entreprise_id);
        $data = $Resource->init();
        $Entreprise = new Entreprise();
        $entObj = $Entreprise->getallentreprise();

        foreach($data as $key => $resource)
        {

            $resourcedata = $this->moduleresource->getAdminassignedmodulebyresourceid($resource->id);
            $data[$key]['IsModuledata'] = 0;
            if($resourcedata->count() > 0)
            {
                $data[$key]['IsModuledata'] = 1;

            }
            if($resource->parent_id == 0){
                $data[$key]['ParentCheck'] = 1;
            }elseif($resource->parent_id > 0){
                $parentData = $Resource->getResourceByID($resource->parent_id);
                if($parentData){
                    $childParentData = $Resource->getResourceByID($parentData->parent_id);
                    if($childParentData){
                        $data[$key]['ParentCheck'] = 3;
                    }else{
                        $data[$key]['ParentCheck'] = 2;
                    }
                }else{
                    $data[$key]['ParentCheck'] = 2;
                }
            }

        }
        return view('admin.resource.index_search',["data"=>$data,"entObj" => $entObj]);
    }

}
