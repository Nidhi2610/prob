-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2017 at 05:37 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `probtp`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_04_02_160942_create_category_table', 2),
(4, '2017_04_02_163507_update_category_table_columns', 3),
(5, '2017_04_02_192914_create_entreprise_table', 4),
(6, '2017_05_15_101826_add_logo_to_tbl_entreprise', 5),
(7, '2017_05_22_125621_create_tbl_entreprise_category', 6),
(8, '2017_05_25_093131_create_quiz_table', 7),
(9, '2017_05_25_094656_create_quiz_que_ans_table', 7),
(11, '2017_05_27_061758_add_colom_quiz_id_to_tbl_module', 8),
(13, '2017_05_29_093828_tbl_module_comment', 9),
(14, '2017_05_31_131012_create_user_module_category_form_table', 10),
(15, '2017_06_01_102720_create_user_module_category_quiz_table', 11),
(16, '2017_06_07_071226_add_colom_delete_at_quiz', 12),
(17, '2017_06_09_063924_update_tbl_module_nullable', 13),
(18, '2017_06_09_064320_update_tbl_module_category', 14),
(19, '2017_06_10_055855_update_user_entreprise_id', 15),
(21, '2017_06_13_073145_update_parent_id_nullable', 16),
(22, '2017_06_15_130616_add_sort_order_module_category', 17),
(29, '2017_06_29_042456_update_tbl_module', 18),
(31, '2017_07_12_114113_add_profile_image_to_user', 19),
(33, '2017_07_13_085052_create_tbl_extra', 20),
(35, '2017_07_14_051715_create_resource', 21),
(36, '2017_07_17_044847_create_tbl_module_resource', 22),
(37, '2017_07_17_045925_create_tbl_user_module_resource_quiz', 23),
(38, '2017_07_17_050105_create_tbl_module_resource_form', 23),
(39, '2017_07_17_051023_create_tbl_entreprise_resource', 24),
(40, '2017_07_18_121131_add_module_type_tbl_module', 25),
(41, '2017_07_19_060140_add_module_category_type_tbl_category', 26),
(42, '2017_07_31_131531_tbl_my_bookmark', 27),
(43, '2017_08_01_093005_add_type_collaborateur_category', 27),
(44, '2017_08_02_072437_add_parent_id_colloborateur_category', 28),
(45, '2017_08_03_064631_update_comment', 29);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('vishalpatel@gmail.com', '$2y$10$Vm/E6awzTAcTIi7714ON6urYeXkidT7dR4fnMXSpQLF9OTFVD6sEW', '2017-08-17 07:05:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bookmark`
--

CREATE TABLE `tbl_bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `formation_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_bookmark`
--

INSERT INTO `tbl_bookmark` (`id`, `user_id`, `formation_id`, `created_at`, `updated_at`) VALUES
(99, 62, 16, '2017-09-01 05:01:22', '2017-09-01 05:01:22'),
(98, 62, 15, '2017-08-31 09:18:00', '2017-08-31 09:18:00'),
(97, 87, 3, '2017-08-31 06:30:24', '2017-08-31 06:30:24'),
(96, 62, 1, '2017-08-31 06:25:13', '2017-08-31 06:25:13'),
(95, 62, 14, '2017-08-31 06:25:01', '2017-08-31 06:25:01'),
(100, 62, 14, '2017-09-02 06:50:51', '2017-09-02 06:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `color` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `font_awesome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sort_order` tinyint(3) NOT NULL DEFAULT '0',
  `category_type_id` int(10) NOT NULL,
  `category_classification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`, `parent_id`, `color`, `description`, `font_awesome`, `status`, `sort_order`, `category_type_id`, `category_classification`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Publice Formatin 1', 0, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 0, 2, 'F', '2017-08-01 04:25:10', '2017-08-18 00:15:18', NULL),
(2, 'Publice Formatin 2', 1, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 4, 2, 'F', '2017-08-01 04:25:14', '2017-08-18 00:15:18', NULL),
(3, 'Private Formatin 1', 21, '#FFFFFF', 'werewqrewrewr', NULL, '0', 10, 1, 'F', '2017-08-01 04:25:34', '2017-08-18 00:15:18', NULL),
(4, 'Private Formatin 2', 0, '#FFFFFF', 'qqqqqq', NULL, '0', 8, 1, 'F', '2017-08-01 04:25:57', '2017-08-18 00:15:18', NULL),
(5, 'Public Resource 1', 0, '#FFFFFF', 'sadfasfadsf', NULL, '0', 14, 2, 'R', '2017-08-01 04:26:51', '2017-08-04 23:06:52', NULL),
(6, 'Public Resource 2', 5, '#FFFFFF', 'sadfasfadsf', NULL, '0', 15, 2, 'R', '2017-08-01 04:26:56', '2017-08-04 23:06:52', NULL),
(7, 'Private Resource 1', 0, '#FFFFFF', 'sadfasfadsf', NULL, '0', 2, 1, 'R', '2017-08-01 04:27:09', '2017-08-04 23:06:52', NULL),
(8, 'Private Resource 2', 7, '#FFFFFF', 'sadfasfadsf', NULL, '0', 3, 1, 'R', '2017-08-01 04:28:10', '2017-08-04 23:06:52', NULL),
(9, 'Public Resource 1-Copy', 6, '#FFFFFF', 'sadfasfadsf', NULL, '0', 16, 2, 'R', '2017-08-01 05:27:08', '2017-08-04 23:06:52', NULL),
(11, 'Public Resource 2-Copy', 0, '#FFFFFF', 'sadfasfadsf', 'fa-ban', '0', 0, 2, 'R', '2017-08-01 05:27:42', '2017-08-05 04:32:11', NULL),
(10, 'Public Resource 1-Copy-Copy', 11, '#FFFFFF', 'sadfasfadsf', 'fa-arrows', '0', 1, 2, 'R', '2017-08-01 05:27:27', '2017-08-05 04:33:02', NULL),
(12, 'Publice Formatin 2-Copy', 2, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 5, 2, 'F', '2017-08-01 07:07:46', '2017-08-18 00:15:18', NULL),
(13, 'Publice Formatin 2-Copy-Copy', 2, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 6, 2, 'F', '2017-08-01 07:07:55', '2017-08-18 00:15:18', NULL),
(14, 'Publice Formatin 2-Copy-Copy-Copy', 1, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 1, 1, 'F', '2017-08-01 07:08:01', '2017-08-31 04:54:40', NULL),
(15, 'Publice Formatin 2-Copy-Copy-Copy-Copy', 14, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 2, 2, 'F', '2017-08-01 07:08:08', '2017-08-18 00:15:18', NULL),
(16, 'Publice Formatin 2-Copy-Copy-Copy-Copy-Copy', 14, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 3, 2, 'F', '2017-08-01 07:08:18', '2017-08-18 00:15:18', NULL),
(17, 'Publice Formatin 2-Copy-Copy-Copy-Copy-Copy-Copy', 1, '#FFFFFF', 'dsfdsafdasfs', NULL, '0', 7, 2, 'F', '2017-08-01 07:08:28', '2017-08-18 00:15:18', NULL),
(18, 'Private Resource 2-Copy', 8, '#FFFFFF', 'sadfasfadsf', NULL, '0', 4, 1, 'R', '2017-08-02 02:00:16', '2017-08-04 23:06:52', NULL),
(19, 'Private Resource 2-Copy-Copy', 7, '#FFFFFF', 'sadfasfadsf', NULL, '0', 5, 1, 'R', '2017-08-02 02:00:21', '2017-08-04 23:06:52', NULL),
(20, 'Private Resource 2-Copy-Copy-Copy', 19, '#FFFFFF', 'sadfasfadsf', NULL, '0', 6, 1, 'R', '2017-08-02 02:00:39', '2017-08-04 23:06:52', NULL),
(21, 'Private Formatin 1-Copy', 4, '#FFFFFF', 'werewqrewrewr', NULL, '0', 9, 1, 'F', '2017-08-02 02:01:54', '2017-08-18 00:15:18', NULL),
(22, 'Private Formatin 1-Copy-Copy', 4, '#FFFFFF', 'werewqrewrewr', NULL, '0', 11, 1, 'F', '2017-08-02 02:01:59', '2017-08-18 00:15:18', NULL),
(23, 'Private Formatin 1-Copy-Copy-Copy', 22, '#FFFFFF', NULL, NULL, '0', 12, 1, 'F', '2017-08-02 02:02:02', '2017-08-30 01:27:42', NULL),
(24, 'Private Formatin 1-Copy-Copy-Copy-Copy', 4, '#FFFFFF', NULL, NULL, '0', 13, 1, 'F', '2017-08-02 02:02:06', '2017-08-30 01:27:25', NULL),
(25, 'Private Resource 2-Copy-Copy-Copy-Copy', 19, '#FFFFFF', 'sadfasfadsf', NULL, '0', 7, 1, 'R', '2017-08-02 05:11:18', '2017-08-04 23:06:52', NULL),
(26, 'Private Resource 1-Copy', 0, '#FFFFFF', 'sadfasfadsf', NULL, '0', 10, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(27, 'Private Resource 2-Copy', 0, '#FFFFFF', 'sadfasfadsf', NULL, '0', 8, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(28, 'Private Resource 2-Copy-Copy', 27, '#FFFFFF', 'sadfasfadsf', NULL, '0', 9, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(29, 'Private Resource 2-Copy-Copy-Copy', 26, '#FFFFFF', 'sadfasfadsf', NULL, '0', 11, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(30, 'Private Resource 2-Copy-Copy-Copy-Copy', 29, '#FFFFFF', 'sadfasfadsf', NULL, '0', 12, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(31, 'Private Resource 2-Copy-Copy-Copy-Copy-Copy', 29, '#FFFFFF', 'sadfasfadsf', NULL, '0', 13, 1, 'R', '2017-08-02 07:01:30', '2017-08-04 23:06:52', NULL),
(32, 'Private Formatin 1-Copy-Copy-Copy-Copy-Copy', 0, '#FFFFFF', 'werewqrewrewr', NULL, '0', 14, 1, 'F', '2017-08-02 23:17:39', '2017-08-18 00:15:18', NULL),
(33, 'test1', 0, '#FFFFFF', 'Brijesh', NULL, '0', 15, 2, 'F', '2017-08-18 00:14:53', '2017-08-18 00:15:18', NULL),
(34, 'test1-Copy', 33, '#FFFFFF', 'dsafadsa', NULL, '0', 16, 2, 'F', '2017-08-18 00:15:06', '2017-08-18 00:15:18', NULL),
(35, 'test1-Copy-Copy', 34, '#FFFFFF', 'dsafadsa', NULL, '0', 17, 2, 'F', '2017-08-18 00:15:11', '2017-08-18 00:15:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_collaborateur`
--

CREATE TABLE `tbl_collaborateur` (
  `id` int(10) UNSIGNED NOT NULL,
  `collaborateur_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `module_id` int(10) NOT NULL,
  `entreprise_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_collaborateur_category`
--

CREATE TABLE `tbl_collaborateur_category` (
  `id` int(11) NOT NULL,
  `collaborateur_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_collaborateur_category`
--

INSERT INTO `tbl_collaborateur_category` (`id`, `collaborateur_id`, `user_id`, `type`, `parent_id`, `category_id`, `created_at`, `updated_at`) VALUES
(17, 76, 62, 'R', NULL, 7, '2017-08-28 07:57:18', '2017-08-28 07:57:18'),
(2, 76, 62, 'R', NULL, 8, '2017-08-02 04:15:11', '2017-08-02 04:15:11'),
(3, 76, 62, 'R', NULL, 19, '2017-08-02 04:15:20', '2017-08-02 04:15:20'),
(4, 76, 62, 'R', NULL, 18, '2017-08-02 04:15:29', '2017-08-02 04:15:29'),
(5, 76, 62, 'R', NULL, 20, '2017-08-02 04:15:38', '2017-08-02 04:15:38'),
(6, 76, 62, 'R', NULL, 25, '2017-08-02 05:11:54', '2017-08-02 05:11:54'),
(12, 76, 62, 'F', NULL, 4, '2017-08-22 04:48:27', '2017-08-22 04:48:27'),
(14, 76, 62, 'F', NULL, 32, '2017-08-22 04:48:37', '2017-08-22 04:48:37'),
(20, 76, 62, 'F', NULL, 3, '2017-08-29 06:54:01', '2017-08-29 06:54:01'),
(13, 87, 62, 'F', NULL, 4, '2017-08-22 04:48:27', '2017-08-22 04:48:27'),
(15, 87, 62, 'F', NULL, 32, '2017-08-22 04:48:37', '2017-08-22 04:48:37'),
(16, 87, 62, 'F', NULL, 21, '2017-08-27 23:44:48', '2017-08-27 23:44:48'),
(18, 87, 62, 'R', NULL, 7, '2017-08-28 07:57:18', '2017-08-28 07:57:18'),
(19, 87, 62, 'R', NULL, 27, '2017-08-28 07:57:25', '2017-08-28 07:57:25'),
(21, 87, 62, 'F', NULL, 3, '2017-08-29 06:54:01', '2017-08-29 06:54:01'),
(22, 87, 62, 'F', NULL, 22, '2017-08-29 06:54:08', '2017-08-29 06:54:08'),
(23, 87, 62, 'F', NULL, 23, '2017-08-29 06:54:13', '2017-08-29 06:54:13'),
(24, 87, 62, 'F', NULL, 24, '2017-08-29 06:54:19', '2017-08-29 06:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emailtemplate`
--

CREATE TABLE `tbl_emailtemplate` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_content` longtext NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_emailtemplate`
--

INSERT INTO `tbl_emailtemplate` (`id`, `name`, `email_content`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Test Template', '<div style="text-align: center;display: table-cell;vertical-align: middle;">\r\n<div style="width: 450px;margin: 0 auto;">\r\n<div style="text-align: center; border-bottom: 1px lightgrey solid;">&nbsp;</div>\r\n\r\n<div style="text-align: center;border-bottom: 1px lightgrey solid;">\r\n<h2><strong>User Login Information</strong></h2>\r\n</div>\r\n\r\n<div style=" text-align: center;display: inline-block;border-bottom: 1px lightgrey solid;">\r\n<p>Hello {name},</p>\r\n\r\n<p>Welcome to Probtp. Please find your login details below.</p>\r\n\r\n<p><strong>Username:</strong> {email}</p>\r\n\r\n<p><strong>Password:</strong> {password}</p>\r\n&nbsp;\r\n\r\n<p><a href="{{url(\'/login\')}}">Click here</a> to Login</p>\r\n</div>\r\n\r\n<p>Thanks,</p>\r\n\r\n<p>Probtp</p>\r\n</div>\r\n</div>', 'welcome_email', '2017-05-16 04:02:19', '2017-05-16 07:05:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entreprise`
--

CREATE TABLE `tbl_entreprise` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_entreprise`
--

INSERT INTO `tbl_entreprise` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`, `logo`) VALUES
(1, 'Big Bazar', NULL, '2017-06-13 01:55:45', '2017-08-28 00:49:45', NULL, '1503901184_download_1.jpg'),
(2, 'Alphaone', NULL, '2017-06-13 01:56:07', '2017-06-13 01:56:07', NULL, '1497338767_images (1).jpg'),
(4, 'Brandfactory', NULL, '2017-06-13 01:56:35', '2017-06-13 01:56:35', NULL, '1497338795_images (2).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entreprise_category`
--

CREATE TABLE `tbl_entreprise_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `entreprise_id` int(11) NOT NULL COMMENT 'Enterprise id!',
  `category_id` int(11) NOT NULL COMMENT 'Category Id Instance!',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_entreprise_category`
--

INSERT INTO `tbl_entreprise_category` (`id`, `entreprise_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 2, 3, '2017-08-01 04:26:23', '2017-08-01 04:26:23'),
(2, 2, 3, '2017-08-01 04:26:23', '2017-08-01 04:26:23'),
(3, 4, 3, '2017-08-01 04:26:23', '2017-08-01 04:26:23'),
(48, 4, 4, '2017-08-04 23:02:24', '2017-08-04 23:02:24'),
(47, 2, 4, '2017-08-04 23:02:24', '2017-08-04 23:02:24'),
(46, 1, 4, '2017-08-04 23:02:24', '2017-08-04 23:02:24'),
(7, 1, 7, '2017-08-01 04:28:23', '2017-08-01 04:28:23'),
(8, 2, 7, '2017-08-01 04:28:23', '2017-08-01 04:28:23'),
(9, 4, 7, '2017-08-01 04:28:23', '2017-08-01 04:28:23'),
(10, 1, 8, '2017-08-01 04:28:29', '2017-08-01 04:28:29'),
(11, 2, 8, '2017-08-01 04:28:29', '2017-08-01 04:28:29'),
(12, 4, 8, '2017-08-01 04:28:29', '2017-08-01 04:28:29'),
(13, 1, 18, '2017-08-02 02:01:10', '2017-08-02 02:01:10'),
(14, 2, 18, '2017-08-02 02:01:10', '2017-08-02 02:01:10'),
(15, 4, 18, '2017-08-02 02:01:10', '2017-08-02 02:01:10'),
(16, 1, 19, '2017-08-02 02:01:19', '2017-08-02 02:01:19'),
(17, 2, 19, '2017-08-02 02:01:19', '2017-08-02 02:01:19'),
(18, 4, 19, '2017-08-02 02:01:19', '2017-08-02 02:01:19'),
(19, 1, 20, '2017-08-02 02:01:27', '2017-08-02 02:01:27'),
(20, 2, 20, '2017-08-02 02:01:27', '2017-08-02 02:01:27'),
(21, 4, 20, '2017-08-02 02:01:27', '2017-08-02 02:01:27'),
(22, 1, 25, '2017-08-02 05:11:36', '2017-08-02 05:11:36'),
(23, 2, 25, '2017-08-02 05:11:36', '2017-08-02 05:11:36'),
(24, 4, 25, '2017-08-02 05:11:36', '2017-08-02 05:11:36'),
(25, 1, 26, '2017-08-02 07:01:43', '2017-08-02 07:01:43'),
(26, 2, 26, '2017-08-02 07:01:43', '2017-08-02 07:01:43'),
(27, 4, 26, '2017-08-02 07:01:43', '2017-08-02 07:01:43'),
(28, 1, 27, '2017-08-02 07:01:50', '2017-08-02 07:01:50'),
(29, 2, 27, '2017-08-02 07:01:50', '2017-08-02 07:01:50'),
(30, 4, 27, '2017-08-02 07:01:50', '2017-08-02 07:01:50'),
(31, 1, 28, '2017-08-02 07:01:55', '2017-08-02 07:01:55'),
(32, 2, 28, '2017-08-02 07:01:55', '2017-08-02 07:01:55'),
(33, 4, 28, '2017-08-02 07:01:55', '2017-08-02 07:01:55'),
(34, 1, 29, '2017-08-02 07:02:05', '2017-08-02 07:02:05'),
(35, 2, 29, '2017-08-02 07:02:05', '2017-08-02 07:02:05'),
(36, 4, 29, '2017-08-02 07:02:05', '2017-08-02 07:02:05'),
(37, 1, 30, '2017-08-02 07:02:14', '2017-08-02 07:02:14'),
(38, 2, 30, '2017-08-02 07:02:14', '2017-08-02 07:02:14'),
(39, 4, 30, '2017-08-02 07:02:14', '2017-08-02 07:02:14'),
(40, 1, 31, '2017-08-02 07:02:21', '2017-08-02 07:02:21'),
(41, 2, 31, '2017-08-02 07:02:21', '2017-08-02 07:02:21'),
(42, 4, 31, '2017-08-02 07:02:21', '2017-08-02 07:02:21'),
(43, 1, 32, '2017-08-02 23:17:52', '2017-08-02 23:17:52'),
(44, 2, 32, '2017-08-02 23:17:52', '2017-08-02 23:17:52'),
(45, 4, 32, '2017-08-02 23:17:52', '2017-08-02 23:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entreprise_resource`
--

CREATE TABLE `tbl_entreprise_resource` (
  `id` int(10) UNSIGNED NOT NULL,
  `entreprise_id` int(11) NOT NULL DEFAULT '0',
  `resource_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_entreprise_resource`
--

INSERT INTO `tbl_entreprise_resource` (`id`, `entreprise_id`, `resource_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-07-18 18:30:00', '2017-07-18 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_form_manager`
--

CREATE TABLE `tbl_form_manager` (
  `form_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_form_manager`
--

INSERT INTO `tbl_form_manager` (`form_id`, `title`, `identifier`, `content`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
(1, 'name', 'test', '[\n	{\n		"type": "text",\n		"label": "Name",\n		"subtype": "text",\n		"placeholder": "Name",\n		"className": "form-control",\n		"name": "text-1493013396212"\n	}\n]', 0, '2017-04-24 05:57:09', '2017-04-24 05:57:09', 1, 1, NULL),
(3, 'namess', 'testss', '[\r\n	{\r\n		"type": "text",\r\n		"label": "Name",\r\n		"subtype": "text",\r\n		"placeholder": "Name",\r\n		"className": "form-control",\r\n		"name": "text-1493013396212"\r\n	},\r\n	{\r\n		"type": "radio-group",\r\n		"label": "Des boutons radio",\r\n		"className": "radio-group",\r\n		"name": "radio-group-1498814977222",\r\n		"values": [\r\n			{\r\n				"label": "Option 1",\r\n				"value": "option-1",\r\n				"selected": true\r\n			},\r\n			{\r\n				"label": "Option 2",\r\n				"value": "option-2"\r\n			},\r\n			{\r\n				"label": "Option 3",\r\n				"value": "option-3"\r\n			}\r\n		]\r\n	}\r\n]', 0, '2017-04-24 05:57:09', '2017-06-30 09:29:41', 1, 1, NULL),
(6, 'namess-Copy', 'testss', '[\r\n	{\r\n		"type": "text",\r\n		"label": "Name",\r\n		"subtype": "text",\r\n		"placeholder": "Name",\r\n		"className": "form-control",\r\n		"name": "text-1493013396212"\r\n	}\r\n]', 0, '2017-06-26 09:39:06', '2017-06-26 09:39:06', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module`
--

CREATE TABLE `tbl_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `summary` text,
  `file_name` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `form_id` int(10) DEFAULT '0',
  `quiz_id` int(11) DEFAULT '0',
  `status` int(10) NOT NULL,
  `module_type` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_module`
--

INSERT INTO `tbl_module` (`id`, `title`, `keyword`, `summary`, `file_name`, `original_name`, `form_id`, `quiz_id`, `status`, `module_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(34, 'Hello', 'brij,dhanani', '<p>wwwwww</p>', '1501758583_aaa.xlsx', 'aaa.xlsx', 1, 1, 0, 'F', '2017-08-03 05:39:43', '2017-08-03 05:39:43', NULL),
(35, 'dhanani', 'rrrrrrrrr', '<p>rrrrr</p>', '1503905463_animated_2d_cartoon_funny_bunny_rabbit_video_short_animation_clip_for_kids_youtube.MP4', '(Animated 2D Cartoon) Funny Bunny Rabbit Video - Short Animation Clip for Kids - YouTube.MP4', 1, 1, 0, 'F', '2017-08-03 05:40:06', '2017-08-28 02:01:03', NULL),
(33, 'dhanani', 'qqq', '<p>qqq heelo htppadsfj</p>\r\n<p>&nbsp;ljldksfj afdslkjlk dsfdsdsfdsaf lkdsfmj dsafjlkdsf jdslkfjldskf lksdjflk dsflkdj sdflkjdskf dsf</p>\r\n<p>jslkdfjlk dsfljdslkflksd jflkdsaflkjds f</p>\r\n<p>dflk jsdflkj slkfjlkd lkjlkf lkfjlkdsf jlkdfjlkdsfuoiweufori jflkmjoiujoiefr ,.lnmlkdsfuoiej ;sadfljlk oijlkmdsf&nbsp;</p>', '1501758546_1.png', '1.png', 1, 1, 0, 'F', '2017-08-03 05:39:06', '2017-08-18 23:48:09', NULL),
(31, 'dfasfdsafdsafdsafdsaf', 'dsfadsfdsafdsaf', '<p>ddsafdsafdsafdsaf</p>', '', '', 1, 1, 0, 'F', '2017-08-03 05:30:23', '2017-08-03 05:30:23', NULL),
(32, 'qqqqqqqq', 'qqq', '<p>qqq</p>', '1501758472_1.png', '1.png', 1, 1, 0, 'F', '2017-08-03 05:37:53', '2017-08-03 05:37:53', NULL),
(30, 'dsfassfsafdsaf', 'asdfdsafs', '<p>dsafdsafs</p>', '', '', 1, 1, 0, 'R', '2017-08-03 01:36:01', '2017-08-03 01:36:01', NULL),
(29, 'sadfasfsafs', 'asdfdsafdsaf', '<p>dsafdsafdsaf</p>', '', '', 3, 1, 0, 'F', '2017-08-03 01:33:37', '2017-08-03 01:33:37', NULL),
(26, 'fadsfadsfdsaf', 'dsfdsafdsaf', '<p>sadfdsafsaf</p>', '', '', 3, 1, 0, 'F', '2017-08-03 00:30:39', '2017-08-03 00:30:39', NULL),
(27, 'Resouvccc', 'werqewr', '<p>qsadfasdfdsa</p>', '', '', 1, 1, 0, 'R', '2017-08-03 00:31:01', '2017-08-03 00:31:01', NULL),
(28, 'wreqewrqewrqewr', 'qewrqewrwqrqew', '<p>rewrewrewrw</p>', '', '', 3, 1, 0, 'R', '2017-08-03 00:31:16', '2017-08-03 00:31:16', NULL),
(25, 'Fist Module`', 'dsfdsfdsaf', '<p>adsfdsafdsa</p>', '', '', 3, 1, 0, 'F', '2017-08-03 00:30:26', '2017-08-03 01:35:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_category`
--

CREATE TABLE `tbl_module_category` (
  `id` int(11) NOT NULL,
  `module_id` int(11) DEFAULT '0',
  `category_id` int(11) DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_module_category`
--

INSERT INTO `tbl_module_category` (`id`, `module_id`, `category_id`, `sort_order`, `created_at`, `updated_at`) VALUES
(11, 25, 13, 0, '2017-08-03 01:35:13', '2017-08-03 01:35:13'),
(10, 25, 2, 0, '2017-08-03 01:35:13', '2017-08-03 01:35:13'),
(3, 26, 14, 0, '2017-08-03 00:30:39', '2017-08-03 00:30:39'),
(4, 26, 16, 0, '2017-08-03 00:30:39', '2017-08-03 00:30:39'),
(5, 27, 11, 0, '2017-08-03 00:31:01', '2017-08-03 00:31:01'),
(6, 27, 10, 0, '2017-08-03 00:31:01', '2017-08-04 23:14:27'),
(7, 28, 10, 1, '2017-08-03 00:31:16', '2017-08-04 23:14:27'),
(8, 28, 18, 1, '2017-08-03 00:31:16', '2017-08-04 23:14:40'),
(9, 29, 4, 0, '2017-08-03 01:33:37', '2017-08-03 01:33:37'),
(12, 30, 18, 0, '2017-08-03 01:36:01', '2017-08-04 23:14:40'),
(13, 31, 2, 0, '2017-08-03 05:30:23', '2017-08-03 05:30:23'),
(19, 33, 1, 0, '2017-08-18 23:48:09', '2017-08-18 23:48:09'),
(15, 34, 2, 0, '2017-08-03 05:39:43', '2017-08-03 05:39:43'),
(21, 35, 14, 0, '2017-08-28 02:01:03', '2017-08-28 02:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_comment`
--

CREATE TABLE `tbl_module_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entreprise_id` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_module_comment`
--

INSERT INTO `tbl_module_comment` (`id`, `user_id`, `category_id`, `entreprise_id`, `comment`, `created_at`, `updated_at`) VALUES
(25, 62, '2', 1, 'sdfsadfdsafdsaf', '2017-09-02 07:26:39', '2017-09-02 07:26:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_likes`
--

CREATE TABLE `tbl_module_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) NOT NULL,
  `module_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_resource`
--

CREATE TABLE `tbl_module_resource` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(11) DEFAULT '0',
  `resource_id` int(11) DEFAULT '0',
  `sort_order` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz`
--

CREATE TABLE `tbl_quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `quiz_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_quiz`
--

INSERT INTO `tbl_quiz` (`id`, `quiz_name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Indain quizz', 0, 1, 1, '2017-05-26 23:33:23', '2017-06-13 05:02:24', NULL),
(2, 'fsaddsaf', 0, 1, 1, '2017-06-07 01:51:13', '2017-06-07 01:51:30', '2017-06-07 01:51:30'),
(10, 'Indain quizz-Copy', 0, 1, 1, '2017-06-07 03:46:40', '2017-06-07 03:47:11', '2017-06-07 03:47:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz_que_ans`
--

CREATE TABLE `tbl_quiz_que_ans` (
  `id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(11) NOT NULL DEFAULT '0',
  `question` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answers` text COLLATE utf8mb4_unicode_ci,
  `true_answers` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_quiz_que_ans`
--

INSERT INTO `tbl_quiz_que_ans` (`id`, `quiz_id`, `question`, `answers`, `true_answers`) VALUES
(16, 1, 'How are you today', '{"A":"fine","B":"just fine","C":"very good","D":"not good","desc":"ddddd"}', 'A,B,C,D');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resource`
--

CREATE TABLE `tbl_resource` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `font_awesome` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `sort_order` tinyint(4) NOT NULL DEFAULT '0',
  `resource_type_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_responsable`
--

CREATE TABLE `tbl_responsable` (
  `id` int(10) UNSIGNED NOT NULL,
  `responsable_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `module_id` int(10) NOT NULL,
  `entreprise_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `content`, `created_at`, `updated_at`) VALUES
(1, '<p>Hello how are uyou this greate</p>', '2017-07-13 04:12:17', '2017-07-13 04:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_module_category_form`
--

CREATE TABLE `tbl_user_module_category_form` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_category_id` int(11) NOT NULL DEFAULT '0',
  `form_id` int(11) NOT NULL DEFAULT '0',
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_value` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_module_category_form`
--

INSERT INTO `tbl_user_module_category_form` (`id`, `module_category_id`, `form_id`, `form_name`, `form_value`, `user_id`, `created_at`, `updated_at`) VALUES
(32, 19, 1, 'Name', 'fksdkdf kdgndfg brijesh', 62, '2017-09-01 06:47:19', '2017-09-01 06:47:19'),
(30, 3, 3, 'Name', 'dddddd', 62, '2017-09-01 06:42:00', '2017-09-01 06:42:00'),
(31, 3, 3, 'Des boutons radio', 'option-1', 62, '2017-09-01 06:42:00', '2017-09-01 06:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_module_category_quiz`
--

CREATE TABLE `tbl_user_module_category_quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_category_id` int(11) NOT NULL DEFAULT '0',
  `quiz_id` int(11) NOT NULL DEFAULT '0',
  `quiz_que` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quiz_ans` text COLLATE utf8mb4_unicode_ci,
  `true_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_point` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user_module_category_quiz`
--

INSERT INTO `tbl_user_module_category_quiz` (`id`, `module_category_id`, `quiz_id`, `quiz_que`, `quiz_ans`, `true_ans`, `user_ans`, `user_point`, `user_id`, `created_at`, `updated_at`) VALUES
(15, 10, 1, 'How are you today', '{"A":"fine","B":"just fine","C":"very good","D":"not good","desc":"ddddd"}', 'A,B,C,D', 'A,B', 0, 62, '2017-09-01 06:56:06', '2017-09-01 06:56:06'),
(14, 11, 1, 'How are you today', '{"A":"fine","B":"just fine","C":"very good","D":"not good","desc":"ddddd"}', 'A,B,C,D', 'A,B', 0, 62, '2017-09-01 06:55:50', '2017-09-01 06:55:50'),
(13, 3, 1, 'How are you today', '{"A":"fine","B":"just fine","C":"very good","D":"not good","desc":"ddddd"}', 'A,B,C,D', 'A', 0, 62, '2017-09-01 06:55:22', '2017-09-01 06:55:22'),
(12, 19, 1, 'How are you today', '{"A":"fine","B":"just fine","C":"very good","D":"not good","desc":"ddddd"}', 'A,B,C,D', 'A', 0, 62, '2017-09-01 06:55:08', '2017-09-01 06:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_module_resource_form`
--

CREATE TABLE `tbl_user_module_resource_form` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_resource_id` int(11) NOT NULL DEFAULT '0',
  `form_id` int(11) NOT NULL DEFAULT '0',
  `form_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_value` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_module_resource_quiz`
--

CREATE TABLE `tbl_user_module_resource_quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_resource_id` int(11) NOT NULL DEFAULT '0',
  `quiz_id` int(11) NOT NULL DEFAULT '0',
  `quiz_que` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quiz_ans` text COLLATE utf8mb4_unicode_ci,
  `true_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_ans` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_point` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'User first name!',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'User last name!',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `entreprise_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `profile_image`, `original_profile_image`, `user_type`, `entreprise_id`, `parent_id`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin Admin', 'Admin', 'Admin', 'admin@probtp.com', '1499924838_images.jpg', 'images.jpg', 1, '0', 0, '$2y$10$dHFvYRCFh5.yqtdmDHMPdexUXXZT3x/3d2fbIK1nbT4D6jL9CgkNu', 'Zmr2vzZFUmZsirXb6NWstb6uhE6ztpfL7Z2s9A2hBBhwGgl72zfSVcHfEO5n', '2017-03-26 09:22:47', '2017-07-13 00:17:18', NULL),
(62, 'Vishal patel', 'Vishal', 'patel', 'vishalpatel@gmail.com', '1501215728_background_img_min.jpg', 'background_img-min.jpg', 3, '1', NULL, '$2y$10$93DwXDGC0crrMQ5Cl4Uv7ukBiTQz82ys8tT24BlgrR932ROSz3wvC', 'Ri6bKFElnQDaQ0fZpd64usF6chMxYV9bwGGYVaSE4H5IpETOj8J1MyVXwK7f', '2017-06-13 02:51:05', '2017-07-28 00:46:54', NULL),
(61, 'Nikhil Jain', 'Nikhil', 'Jain', 'nikhil@gmail.com', NULL, NULL, 4, '1', 60, '$2y$10$Qqwo2XoFegKlQYuZTzgWiuJXCnamjkHzWpTOovhanKSmyNp5IGI1y', 'PU7rvHUmzukHsx54gpX0deWJ1C1xxvmgVx0T72IyaeALG1N8oJYoq9P3sD58', '2017-06-13 02:23:41', '2017-06-13 02:23:41', NULL),
(60, 'Brijesh Dhananieesss', 'Brijesh', 'Dhananieesss', 'bjpatel2001@gmail.com', NULL, NULL, 3, '1', NULL, '$2y$10$Xh9VCrSdRRGkN/sPCq4mwutA.L7acTmjj5WzUEqIacyO3uer5Wf/y', 'tpvShQ3I8fQR89b7e7wSptQUOJlf3J1LEFaioa2xTKO4NOvhZUmnFr22BQp9', '2017-06-13 02:22:57', '2017-06-16 06:02:47', NULL),
(71, 'asfdsaf adfad sfdsaf', 'asfdsaf', 'adfad sfdsaf', 'asdfdsa@gmail.com', NULL, NULL, 3, '2', 0, '$2y$10$S9LxkCqvxiOOEkPPkXaaCeINHzxw8bq1wMysa250mmyUkvTtFvQ9u', NULL, '2017-06-22 04:47:06', '2017-06-22 04:47:06', NULL),
(70, 'afslkdsajflkajsfa1111 lkafjsadf', 'afslkdsajflkajsfa1111', 'lkafjsadf', 'ajsdfsa@gmail.com', NULL, NULL, 3, '1', 0, '$2y$10$nYshBtufz/ifuO//e2CT7.tV05oxZbv4SS76ngMzaTd0TOI20BqQO', NULL, '2017-06-16 05:12:01', '2017-06-16 05:12:01', NULL),
(69, 'adsfdsafads dsafdsafdsaf', 'adsfdsafads', 'dsafdsafdsaf', 'asssaaaaaadmin@ets.com', NULL, NULL, 4, '1', 60, '$2y$10$IPknpQzc2GKO/d3rs1abQu9a68oKov6BmkpiJ6F3jD2UFnFZD5Lim', NULL, '2017-06-16 05:05:28', '2017-06-19 06:39:37', NULL),
(72, 'ajsflk;jdsaf lkdsajfds', 'ajsflk;jdsaf', 'lkdsajfds', 'alkfsjdsaf@gmail.com', NULL, NULL, 3, '2', 0, '$2y$10$/16ixq/VQMYAOlowlWOL8eT/Yxq7z/smVvg7qvBECVoyzCPxuG2bS', 'e9l7Mzytc0vedvHwH5Ll1tcEMkl2bNBNo3mtu3WDFBWcjPSzlTeMyYPkJUEt', '2017-06-22 04:47:34', '2017-06-22 04:47:34', NULL),
(73, 'asdfdsafdsa dsafadsfs', 'asdfdsafdsa', 'dsafadsfs', 'asfdsa@gmail.com', NULL, NULL, 2, '1', NULL, '$2y$10$uuDfDq5b53OJ5dUAngK5WeC98D5zDQGiRD907yqQbFT19KA5tFzpG', NULL, '2017-06-22 22:35:45', '2017-06-22 22:41:50', NULL),
(75, 'Responsable dddd', 'Responsable', 'dddd', 'respon@gmail.com', NULL, NULL, 3, '1', 0, '$2y$10$dwwQQnVkxN88uQxX.KWXg.ZDtpGClgjQ/HGwHG4F.XsTjPFHPbzm2', NULL, '2017-07-13 00:49:16', '2017-07-13 00:49:16', NULL),
(76, 'Brij kljlkajdsfsad', 'Brij', 'kljlkajdsfsad', 'brij@gmail.com', '1501067278_background_img_min.jpg', 'background_img-min.jpg', 4, '1', 62, '$2y$10$TAzjVsPNby/gfjeYQMxhtOT5r3pq86RTKRGgBkNKa40O.sui6m25.', '0BTk5MOb91CgpFn0YdbQDShstBl4KRsTMjpIQ63xCLjx5YQUHeSuPN3FYfQ7', '2017-07-26 04:01:27', '2017-08-31 06:09:44', '2017-08-31 06:09:44'),
(77, 'dsfdsafdsaf dsafdsafsafads', 'dsfdsafdsaf', 'dsafdsafsafads', 'abcdd@gmail.com', NULL, NULL, 3, '1', 0, '$2y$10$Y4dMQJA16oTZrmtj2PWrvekEyduFcxm1PMfZNev8eZqdF2PXLgWlW', NULL, '2017-07-26 04:03:17', '2017-07-26 04:03:17', NULL),
(78, 'sdfadsfdsaf dsafdsafdsaf', 'sdfadsfdsaf', 'dsafdsafdsaf', 'sadfsad@gmail.com', NULL, NULL, 4, '3', 62, '$2y$10$kct9IYPX5Q8X8i.Of1HVaOJJI9JNi2tAfdwHKHkFFMy1TTSjzcwki', NULL, '2017-07-26 04:44:35', '2017-07-26 05:23:43', '2017-07-26 05:23:43'),
(79, 'sadfdsafdsads dsafdsafdsafdsaf', 'sadfdsafdsads', 'dsafdsafdsafdsaf', 'asfdsafdsa@gmail.com', NULL, NULL, 4, '2', 62, '$2y$10$wz6auAAOYfM5G.TR09YusubXjKg7xybHD63dhwaJEQEue3LOr6qym', NULL, '2017-07-26 04:44:52', '2017-07-29 04:07:14', '2017-07-29 04:07:14'),
(80, 'fadfasasdfas Asfdsaf', 'fadfasasdfas', 'Asfdsaf', 'asfddddsa@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$ynFcnls6hWDda5rG1zShee0dbZGxIWgg110k8m6Elg/2x9yl2rWNy', NULL, '2017-07-29 04:07:36', '2017-07-29 04:10:28', '2017-07-29 04:10:28'),
(81, 'ewqrqewdddf werqwrwq', 'ewqrqewdddf', 'werqwrwq', 'qwreqwrqw@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$W46kCmpwRRGUM5mohCX2KelYm3d6ZoW76yHcYXQ8sSzIItwkfL13u', NULL, '2017-07-29 04:10:55', '2017-07-29 06:00:01', '2017-07-29 06:00:01'),
(82, 'dfadsasfsa adfsadsfdsafdsafdsafs', 'dfadsasfsa', 'adfsadsfdsafdsafdsafs', 'dsafdsafdsafdsaf@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$W21XfivgzG839rKvg3xG4OB/yFsykX01x4TmhCsFVRRsncTUvbN/a', NULL, '2017-07-29 05:26:22', '2017-07-29 05:59:47', '2017-07-29 05:59:47'),
(83, 'adsfdsaf asdfdsaf', 'adsfdsaf', 'asdfdsaf', 'dsafdsafsafd@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$6vR9.iA0ruYR.ZM9ujkBR.gshRFty0z45glNpJOPNifhxhR/PHXhG', NULL, '2017-07-29 06:07:40', '2017-07-31 01:04:30', '2017-07-31 01:04:30'),
(85, 'Demo test', 'Demo', 'test', 'abcd@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$RlgJ3R9e8XsjKVyj9wvCR.V7RMi5FheVqEF6nHYUHdDTfPQk7cKVW', NULL, '2017-08-03 02:17:21', '2017-08-04 04:26:22', '2017-08-04 04:26:22'),
(86, 'dfsadfsa asdfsaf', 'dfsadfsa', 'asdfsaf', 'dsafdsafsafddddddd@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$HHhdmC9V9pyRzbCABnj2K.Kpb5WgDQ9pdeHxWYRymg6Lm1fXFu3ba', NULL, '2017-08-04 07:40:45', '2017-08-31 06:08:55', '2017-08-31 06:08:55'),
(87, 'colloborateur jjdj', 'colloborateur', 'jjdj', 'coll@gmail.com', NULL, NULL, 4, '1', 62, '$2y$10$jjkvFu0gTn9.5lWJVXUdm.kaQTs8Yhhnx.dQ.2MQZHpc/bo4bj.WS', 'R5Z7o1ncbgP0AXGn970zOLLpMefHVQdJJKdBN2f8pwMY5FULJNXXky8s4qB3', '2017-08-22 04:38:05', '2017-08-22 04:38:05', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_bookmark`
--
ALTER TABLE `tbl_bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_collaborateur`
--
ALTER TABLE `tbl_collaborateur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_collaborateur_category`
--
ALTER TABLE `tbl_collaborateur_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emailtemplate`
--
ALTER TABLE `tbl_emailtemplate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_entreprise`
--
ALTER TABLE `tbl_entreprise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_entreprise_category`
--
ALTER TABLE `tbl_entreprise_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_entreprise_category_entreprise_id_foreign` (`entreprise_id`),
  ADD KEY `tbl_entreprise_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `tbl_entreprise_resource`
--
ALTER TABLE `tbl_entreprise_resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_form_manager`
--
ALTER TABLE `tbl_form_manager`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `tbl_module`
--
ALTER TABLE `tbl_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_module_category`
--
ALTER TABLE `tbl_module_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_module_comment`
--
ALTER TABLE `tbl_module_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_module_likes`
--
ALTER TABLE `tbl_module_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_module_resource`
--
ALTER TABLE `tbl_module_resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_quiz_que_ans`
--
ALTER TABLE `tbl_quiz_que_ans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_resource`
--
ALTER TABLE `tbl_resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_responsable`
--
ALTER TABLE `tbl_responsable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_module_category_form`
--
ALTER TABLE `tbl_user_module_category_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_module_category_quiz`
--
ALTER TABLE `tbl_user_module_category_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_module_resource_form`
--
ALTER TABLE `tbl_user_module_resource_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_module_resource_quiz`
--
ALTER TABLE `tbl_user_module_resource_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tbl_bookmark`
--
ALTER TABLE `tbl_bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_collaborateur`
--
ALTER TABLE `tbl_collaborateur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_collaborateur_category`
--
ALTER TABLE `tbl_collaborateur_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_emailtemplate`
--
ALTER TABLE `tbl_emailtemplate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_entreprise`
--
ALTER TABLE `tbl_entreprise`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_entreprise_category`
--
ALTER TABLE `tbl_entreprise_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `tbl_entreprise_resource`
--
ALTER TABLE `tbl_entreprise_resource`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_form_manager`
--
ALTER TABLE `tbl_form_manager`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_module`
--
ALTER TABLE `tbl_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_module_category`
--
ALTER TABLE `tbl_module_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbl_module_comment`
--
ALTER TABLE `tbl_module_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tbl_module_likes`
--
ALTER TABLE `tbl_module_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_module_resource`
--
ALTER TABLE `tbl_module_resource`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_quiz_que_ans`
--
ALTER TABLE `tbl_quiz_que_ans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_resource`
--
ALTER TABLE `tbl_resource`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_responsable`
--
ALTER TABLE `tbl_responsable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user_module_category_form`
--
ALTER TABLE `tbl_user_module_category_form`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tbl_user_module_category_quiz`
--
ALTER TABLE `tbl_user_module_category_quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_user_module_resource_form`
--
ALTER TABLE `tbl_user_module_resource_form`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user_module_resource_quiz`
--
ALTER TABLE `tbl_user_module_resource_quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
