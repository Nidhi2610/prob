<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblModuleResourceForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_module_resource_form', function (Blueprint $table){
            $table->increments('id');
            $table->integer('module_resource_id')->default('0');
            $table->integer('form_id')->default('0');
            $table->string('form_name')->nullable();
            $table->text('form_value')->nullable();
            $table->integer('user_id')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_user_module_resource_form');
    }
}
