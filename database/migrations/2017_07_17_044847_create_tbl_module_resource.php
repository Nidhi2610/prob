<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblModuleResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_module_resource', function (Blueprint $table){
            $table->increments('id');
            $table->integer('module_id')->default('0')->nullable();
            $table->integer('resource_id')->default('0')->nullable();
            $table->tinyInteger('sort_order')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_module_resource');
    }
}
