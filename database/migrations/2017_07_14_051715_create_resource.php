<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_resource', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('parent_id')->default('0');
            $table->string('color')->nullable();
            $table->longText('description')->nullable();
            $table->string('font_awesome')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->tinyInteger('sort_order')->default('0');
            $table->integer('resource_type_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_resource');
    }
}
