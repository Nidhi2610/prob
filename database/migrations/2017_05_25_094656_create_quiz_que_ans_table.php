<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizQueAnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_quiz_que_ans', function (Blueprint $table){
            $table->increments('id');
            $table->integer('quiz_id')->default('0');
            $table->string('question')->nullable();
            $table->text('answers')->nullable();
            $table->string('true_answers',30)->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_quiz_que_ans');
    }
}
