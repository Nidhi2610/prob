<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserModuleCategoryQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_user_module_category_quiz', function (Blueprint $table){
            $table->increments('id');
            $table->integer('module_category_id')->default('0');
            $table->integer('quiz_id')->default('0');
            $table->string('quiz_que')->nullable();
            $table->text('quiz_ans')->nullable();
            $table->string('true_ans')->nullable();
            $table->string('user_ans')->nullable();
            $table->tinyInteger('user_point')->default('0');
            $table->integer('user_id')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_user_module_category_quiz');
    }
}
