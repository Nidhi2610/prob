<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentIdColloborateurCategory extends Migration
{
    public function up()
    {
        //
        Schema::table('tbl_collaborateur_category', function (Blueprint $table) {
            
            $table->integer('parent_id')->nullable()->after('type');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tbl_collaborateur_category');
    }
}
