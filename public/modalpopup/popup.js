
// For DataTable in Modal Popup for checkAll

	$('#checkAll2').on('click',function(){
			$('.modalcheckbox').prop('checked',$(this).is(":checked"));
		});
		
		$('.modalcheckbox').on('click',function(){
			if($(this).is(":checked")){									
				if($('.modalcheckbox').length === $('.modalcheckbox:checked').length){
					$('#checkAll2').prop('checked',$(this).is(":checked"));
				}
			}else{
				$('#checkAll2').prop('checked',$(this).is(":checked"));
			}
		});

	/*function checkAll(theForm, cName, allNo_stat) {
		var n=theForm.elements.length;
		for (var i=0;i<n;i++){
			if (theForm.elements[i].className.indexOf(cName) !=-1){
				if (allNo_stat.checked) {
				theForm.elements[i].checked = true;
				} else {
				theForm.elements[i].checked = false;
				}
			}
		}
	}*/

// Data Tables

	$('#data-table2').DataTable( 
		{
		"pageLength": 100,    
		"order": [],
		"language": {
			"paginate": {
			  "previous": "Précédente",
			  "next": "Suivante"
			},
			'lengthMenu' : '_MENU_ par page',
			"info": "_START_ à _END_ sur _TOTAL_ réponses",
			'search' : 'Rechercher : ',
		  },
		"columnDefs": [ {
			  "targets": 'no-sort',
			  "orderable": false
		} ]}
	);

