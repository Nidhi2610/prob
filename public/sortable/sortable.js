$(window).on('load', function() {
     type = $("#category_classification").val();

  /**
  * Function Generate OL/LI for nested category
  * @params integrr parentID
  *
  * @return HTML  
  */
  function getMenu(parentID) {

    return data.filter(function(node) {
		return (node.parent_id == parentID); console.log(parentID);
	}).map(function(node) {
        var category_type_id = node.category_type_id;
        var status = node.status;
        var font_awesome = node.font_awesome;
        var IsModuledata = node.IsModuledata;
		var exists = data.some(function(childNode) {
			return childNode.parent_id == node.id;
		});
		var font_view = (font_awesome == "")?'':'<i class="fa '+font_awesome+'"></i>'
        var mode = (status == 0) ? '<i class="fa fa-circle category-active-status"></i>' : '<i class="fa fa-circle category-inactive-status"></i>';
		var subMenu = (exists) ? '<ol>' + getMenu(node.id).join('')+ '</ol>' : "";
        // var preview = '<a type="button" href="'+app.config.SITE_PATH+'category/detail_page/'+node.id+'/'+node.id+'#category_'+node.id+'"><span title="Visualiser la page" class="fa fa-eye"></span></a>';
        var preview = '<a id ="'+node.id+'" type="button" href="javascript:void(0);"><span title="Visualiser la page" class="fa fa-eye"></span></a>';
		var assigned = (IsModuledata > 0) ? '<a  type="button" href="javascript:void(0);" onclick="getassignedmodule(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Les modules attribués" class="fa fa-list"></span></a>' : '<span style = "color:grey" class="fa fa-list"></span>';
	    var lock =(category_type_id == 2) ? '<span title="Public" class="fa fa-unlock-alt"></span>': '<span title ="Privé" class="fa fa-lock"></span>';
        var entreprise =(category_type_id == 2) ? '': '<a  type="button" href="javascript:void(0);" onclick="getentreprise(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Assigner une entreprise" id="probtp_form" class="fa fa-etsy"></span></a>';
	    var liString = '<li id=list_'+node.id+' data-id="'+node.id+'"><div>'+font_view+' '+ node.name+lock+entreprise +'<a type="button" id="'+node.id+'" data-id ="'+node.id+'" href="javascript:void(0);" onclick="deleteCate('+node.id +');" class="delete"><span title="Supprimer" class="fa fa-btn fa-trash"></span></a><a type="button" href="javascript:void(0);" onclick="duplicate(this.id);" id="'+node.id+'"><span title="Copier" class="fa fa-btn fa-files-o"></span></a><a href = "'+app.config.SITE_PATH+'category/edit/'+node.id+'/'+ type +'"><span title = "Modifier" class="fa fa-btn fa-pencil"></span></a><a href = "'+app.config.SITE_PATH+'module/create/'+node.id+'/'+ type +'"><span title="Ajouter un nouveau module" class="fa fa-plus-square"></span></a><a  type="button" href="javascript:void(0);" onclick="getunassignedmodule(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Assigner un module" id="probtp_form" class="fa fa-plug"></span></a>'+ assigned + preview+mode+'</div>' + subMenu +'</li>';
		return liString ;
    });
  }
  
  // Generate OL/LI for nested category
  var endMenu = getMenu("0");  
  $('#categoryList').html('<ol id="sortable" class="sortable">' + endMenu.join('') + '</ol>');
	
  // Nested sortable function	 
  $('#sortable').nestedSortable({
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		update: function () {
			order = $('#sortable').nestedSortable('serialize');
			$.ajax({
				data: order,
				type: 'POST',
				url: 'updatecategory',
				headers: {
					'X-CSRF-TOKEN': window.Probtp.csrfToken
				},
				success: function( data ) {
				},
				error: function(xhr, status, error) {
				// check status && error
				}
			});
		}
	});
});


/*
* For Duplication of category
* @param int id
*/

    function duplicate(id) {

       var confirmstatus = confirm("Êtes-vous sûr de vouloir dupliquer cette catégorie ?");

        var path = app.config.SITE_PATH+'category/replicate/'+id;

        if(confirmstatus){
            window.location = path;
        }
    }

/*
 * For Delete the category
 * @param int id
 */

function deleteCate(id) {

        var confirmstatus = confirm("Êtes-vous sûr de vouloir supprimer cela?");
        var url = app.config.SITE_PATH+'category/destroy';
        var cat_type = $('#category_classification').val();
    if(confirmstatus){
        $.ajax({
            data: {id:id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {
                window.location = app.config.SITE_PATH+'category/'+cat_type;
            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });
    }

    }

/*
* For Getting Unassigned module list
* @param int id
*
*/
    function getunassignedmodule(id){

        var url = app.config.SITE_PATH+'module/getunassignedmodule';
        app.showLoader();
        $("#category_id").val(id);
        $.ajax({

            data: {id:id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {
                app.hideLoader();
                if(data.statusCode == 1){
                    $(".module_list").html(data.data);
                    app.dataTable3();
                    $("#myModal").modal("show");
                    $("#category_class").val(type);
                    $('#attached').show();
                    $('#detached').hide();
                    $("#add_form").attr("action", app.config.SITE_PATH+"/module/insertModuleCategory");

                }else{
                    $(".module_list").html("");
                    alert("No record found");
                }

            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });
    }

/*
 * For Getting assigned  module list
 * @param int id
 *
 */
function getassignedmodule(id){
    var url = app.config.SITE_PATH+'module/getassignedmodule';
    app.showLoader();
    $.ajax({

        data: {id:id},
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        success: function (data) {
            app.hideLoader();
            if(data.statusCode == 1){
                $(".module_list").html(data.data);
                $("#myModal").modal("show");
                $("#category_class").val(type);
                app.dataTable3();
                $('#attached').hide();
                $('#detached').show();
                $("#add_form").attr("action", app.config.SITE_PATH+"/module/deleteModuleCategory");
                $("tbody.module_category").sortable({
                    helper: fixHelperModified,
                    stop: updateIndex,
                    update: function () {
                        var module_category_ids = $(".module_category_ids").map(function () {
                            return $(this).val();
                        }).get();
                       var order = module_category_ids.join(",");

                        $.ajax({
                            data:{list:order},
                            type: 'POST',
                            url: 'updatemoduletree',
                            headers: {
                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                            },
                            success: function( data ) {
                            },
                            error: function(xhr, status, error) {
                                // check status && error
                            }
                        });



                    }
                }).disableSelection();

            }else{
                $(".module_list").html("");
            }

        },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
        }
    });
}

function getentreprise(id) {

    $("#categoryid").val(id);
    $("#category_type").val(type);

        var url = app.config.SITE_PATH+'category/get_entreprise';

    $.ajax({

        data: {id: id},
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        success: function (data) {
            $('.modalcheckbox').prop('checked', false);  //alert();

                if(data.length > 0) {
                    $.each(data, function (key, val) {

                        $('.enterprises[value="' + val.entreprise_id + '"]').prop('checked', true);
                    });
                }
            $("#myModalCategoryEntreprise").modal("show");
            $("#category_id").val(id);
            },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
        }
    });


}

// For reorder

var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    },
    updateIndex = function(e, ui) {
        $('td.index', ui.item.parent()).each(function (i) {
            $(this).html(i + 1);
        });
    };


// for


$('.modalcheckbox ').on('click',function(){
    var id = $(this).val();
    $('.parent_entreprise_id_' + id).prop('checked',$(this).is(":checked"));
});

$('.childmodalcheckbox').on('click',function(){
    var id = $(this).attr('id');
    if($(this).is(":checked")){

        if($('.parent_entreprise_id_'+ id).length === $('.parent_entreprise_id_'+ id +':checked').length){
            $('#entreprise_id_'+id).prop('checked',$(this).is(":checked"));
        }
    }else{
        $('#entreprise_id_'+ id).prop('checked', $(this).is(":checked"));
    }
});

$(document).ready(function(){  
    $(document).on('click','.fa-eye',function(){
        var clickedID = $(this).parent().attr('id');
        var url = '';
        var parentId ;
        var siblingsArray = [];
        var getAllParentLI = $(this).parents('li:last');

        parentId = getAllParentLI.attr('data-id');

        if($(this).parents('li').length === 1){
            siblingsArray.push(clickedID);
        }

        getAllParentLI.find('li').each(function () {
            siblingsArray.push($(this).attr('data-id'));
        });

        if(siblingsArray.length === 0){
           url = app.config.SITE_PATH + 'category/detail_page/' + parentId+'/'+ parentId +"#category_"+clickedID;
        }else if($(this).parents('li:last').attr('data-id') === clickedID){
            url = app.config.SITE_PATH + 'category/detail_page/' + parentId+'/'+ siblingsArray.join(':')+"#category_"+clickedID;
        }else{
            url = app.config.SITE_PATH + 'category/detail_page/' + parentId+'/'+ parentId + ":" + siblingsArray.join(':')+"#category_"+clickedID;
        }
      window.location.href = url;
    });
});




