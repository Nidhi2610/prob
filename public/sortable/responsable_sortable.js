$(window).on('load', function() {

    /**
     * Function Generate OL/LI for nested category
     * @params integrr parentID
     *
     * @return HTML
     */
    function getMenu(parentID) {

        return data.filter(function(node) {
            node.parent_id = 0;
            return (node.parent_id == parentID);
        }).map(function(node) {
            var category_type_id = node.category_type_id;
            var exists = data.some(function(childNode) {
                return childNode.parent_id == node.id;
            });

            var subMenu = (exists) ? '<ol>' + getMenu(node.id).join('')+ '</ol>' : "";
            var lock =(category_type_id == 2) ? '<span title="private" class="fa fa-unlock-alt"></span>': '<span class="fa fa-lock"></span>';
            var entreprise =(category_type_id == 2) ? '': '<a  type="button" href="javascript:void(0);" onclick="getusers(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Attach Enterprise" id="probtp_form" class="fa fa-etsy"></span></a>';
            var liString = '<li id=list_'+node.id+'><div>' + node.name+ entreprise  + lock +'</div>' + subMenu +'</li>';

            return liString ;

        });
    }

    // Generate OL/LI for nested category
    var endMenu = getMenu("0");
    $('#categoryList').html('<ol id="sortable" class="sortable">' + endMenu.join('') + '</ol>');

});

// For getting List of the collaborateur user

function getusers(id) {

    $("#categoryids").val(id);

    var url = app.config.SITE_PATH+'category/get_user';

    $.ajax({

        data: {id: id},
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        success: function (data) {

            $('.modalcheckbox').prop('checked', false);
            $.each(data, function (key, val) {
                $('.users[value="' + val.collaborateur_id + '"]').prop('checked', true);
            });
            $("#myModalCollaborateuruser").modal("show");

        },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
        }
    });


}

