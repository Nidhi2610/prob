$(window).on('load', function() {

  /**
  * Function Generate OL/LI for nested resource
  * @params integrr parentID
  *
  * @return HTML  
  */
  function getMenu(parentID) {

    return data.filter(function(node) {
		return (node.parent_id == parentID);
	}).map(function(node) {
        var resource_type_id = node.resource_type_id;
        var IsModuledata = node.IsModuledata;
		var exists = data.some(function(childNode) {
			return childNode.parent_id == node.id;
		});

		var subMenu = (exists) ? '<ol>' + getMenu(node.id).join('')+ '</ol>' : "";
		var preview = '<a type="button" href="'+app.config.SITE_PATH+'resource/detail_page/'+node.id+'"><span title="Visualiser la page" class="fa fa-eye"></span></a>';
		var assigned = (IsModuledata > 0) ? '<a  type="button" href="javascript:void(0);" onclick="getassignedmodule(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Les modules attribués" class="fa fa-list"></span></a>' : '<span style = "color:grey" class="fa fa-list"></span>';
	    var lock =(resource_type_id == 2) ? '<span title="Public" class="fa fa-unlock-alt"></span>': '<span title ="Privé" class="fa fa-lock"></span>';
        var entreprise =(resource_type_id == 2) ? '': '<a  type="button" href="javascript:void(0);" onclick="getentreprise(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Assigner une entreprise" id="probtp_form" class="fa fa-etsy"></span></a>';
	    var liString = '<li id=list_'+node.id+'><div>' + node.name+lock+ entreprise +'<a type="button" data-id ="'+node.id+'" href="javascript:void(0);" class="delete"><span title="Supprimer" class="fa fa-btn fa-trash"></span></a><a type="button" href="javascript:void(0);"onclick="duplicate(this.id);"id="'+node.id+'"><span title="Copier" class="fa fa-btn fa-files-o"></span></a><a href = "'+app.config.SITE_PATH+'resource/edit/'+node.id+'"><span title = "Modifier" class="fa fa-btn fa-pencil"></span></a><a href = "'+app.config.SITE_PATH+'module/create/'+node.id+'"><span title="Ajouter un nouveau module" class="fa fa-plus-square"></span></a><a  type="button" href="javascript:void(0);" onclick="getunassignedmodule(this.id);" data-toggle="modal" id="'+node.id+'"><span title="Assigner un module" id="probtp_form" class="fa fa-plug"></span></a>'+ assigned + preview+'</div>' + subMenu +'</li>';

		return liString ;
		
    });
  }
  
  // Generate OL/LI for nested resource
  var endMenu = getMenu("0");  
  $('#resourceList').html('<ol id="sortable" class="sortable">' + endMenu.join('') + '</ol>');
	
  // Nested sortable function	 
  $('#sortable').nestedSortable({
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		update: function () {
			order = $('#sortable').nestedSortable('serialize');			 
			$.ajax({
				data: order,
				type: 'POST',
				url: 'resource/updateresource',
				headers: {
					'X-CSRF-TOKEN': window.Probtp.csrfToken
				},
				success: function( data ) {
				},
				error: function(xhr, status, error) {
				// check status && error
				}
			});
		}
	});
});


/*
* For Duplication of resource
* @param int id
*/

    function duplicate(id) {

       var confirmstatus = confirm("Êtes-vous sûr de vouloir dupliquer cette resource ?");

        var path = app.config.SITE_PATH+'resource/replicate/'+id;

        if(confirmstatus){
            window.location = path;
        }
    }
/*
* For Getting Unassigned module list
* @param int id
*
*/
    function getunassignedmodule(id){
        var url = app.config.SITE_PATH+'module/getunassignedmoduleresource';
        app.showLoader();
        $("#resource_id").val(id);
        $.ajax({

            data: {id:id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {
                app.hideLoader();
                if(data.statusCode == 1){
                    $(".module_list").html(data.data);
                    app.dataTable3();
                    $("#myModal").modal("show");
                    $('#attached').show();
                    $('#detached').hide();
                    $("#add_form").attr("action", app.config.SITE_PATH+"/module/insertModuleResource");

                }else{
                    $(".module_list").html("");
                    alert("No record found");
                }

            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });
    }

/*
 * For Getting assigned  module list
 * @param int id
 *
 */
function getassignedmodule(id){
    var url = app.config.SITE_PATH+'module/getassignedmoduleresource';
    app.showLoader();
    $.ajax({

        data: {id:id},
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        success: function (data) {
            app.hideLoader();
            if(data.statusCode == 1){
                $(".module_list").html(data.data);
                $("#myModal").modal("show");
                app.dataTable3();
                $('#attached').hide();
                $('#detached').show();
                $("#add_form").attr("action", app.config.SITE_PATH+"/module/deleteModuleResource");
                $("tbody.module_resource").sortable({
                    helper: fixHelperModified,
                    stop: updateIndex,
                    update: function () {
                        var module_resource_ids = $(".module_resource_ids").map(function () {
                            return $(this).val();
                        }).get();
                       var order = module_resource_ids.join(",");

                        $.ajax({
                            data:{list:order},
                            type: 'POST',
                            url: 'resource/updatemoduletree',
                            headers: {
                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                            },
                            success: function( data ) {
                            },
                            error: function(xhr, status, error) {
                                // check status && error
                            }
                        });



                    }
                }).disableSelection();

            }else{
                $(".module_list").html("");
            }

        },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
        }
    });
}

function getentreprise(id) {

    $("#resourceid").val(id);

        var url = app.config.SITE_PATH+'resource/get_entreprise';

    $.ajax({

        data: {id: id},
        type: 'POST',
        url: url,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        success: function (data) {
            $('.modalcheckbox').prop('checked', false);
            $.each(data, function (key, val) {
                $('.enterprises[value="' + val.entreprise_id + '"]').prop('checked', true);
            });
            $("#myModalResourceEntreprise").modal("show");
            $("#resource_id").val(id);
        },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
        }
    });


}

// For reorder

var fixHelperModified = function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
            $(this).width($originals.eq(index).width())
        });
        return $helper;
    },
    updateIndex = function(e, ui) {
        $('td.index', ui.item.parent()).each(function (i) {
            $(this).html(i + 1);
        });
    };