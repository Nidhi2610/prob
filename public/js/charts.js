
var appCharts = appCharts || {};
$(window).on('load',function(){
    /*
     *  for converting it in
     *  Day.Hours.Minuts.Seconds format from seconds
     *
     *  @param $time
     *  @return $result string
     * */
    function destructMS(sec) {
        if(typeof(sec) !== "undefined"){
            var tabTemps = {"jours": 86400,"heures": 3600,"minutes": 60};
            // var tabTemps = {"jours": 86400,"heures": 3600,"minutes": 60,"secondes": 1};
            var result= "";

            $.each(tabTemps,function(k,v){
           // for(tabTemps in time){
                var unit = Math.floor(sec/v);
                sec = sec%v;
                if(unit === 1 && k === 'heures'){
                    k = 'heure';
                }else if( unit>0 || result.length!==0 ){
                    result +=unit+k+" ";
                }
            });

            return result;
        }

    }


    /**
     * Function that generates data for Comment by user
     *  @param total_time
     *  @return totalTime object
     */
	function generateMosttimeSpentonSystemDiv(total_time){
    var totalTime = [];   
      $.each(JSON.parse(total_time),function(k,v){
         var day =  destructMS(v.total_time);

          totalTime.push({
          "Name": v.name,
          "Time" :v.total_time
          });
      });
    //  console.log(totalTime);
      return totalTime;
	}

    /**
     * Function that generates data for Comment by user
     *  @param nArray
     *  @return chartData object
     */
	function generateChartData(nArray){  
    var chartData = [];   
      $.each(JSON.parse(nArray),function(k,v){
          console.log(v);
       chartData.push({
        "Entreprise": v.entreprise,
         "Responsable" : v.responsable,
         "Collaborateur" : v.collaborateur,
         "Total User" : v.total_user
        });
      });   
console.log(chartData);
    return chartData;
 }


    /**
     * Function  generates charts for "Total time spent by user"
     *
     */
var timeSpentonSystemDiv = AmCharts.makeChart( "timeSpentonSystemDiv", {
          "type": "serial",
          "theme": "none",
          "categoryField": "Name",
          "rotate": true,
          "startDuration": 1,
           "fontFamily":" Raleway,sans-serif",
          // "autoMarginOffset": 40,
          "categoryAxis": {
            "gridPosition": "start",
            "position": "left",
            "title": "user" ,
            "labelFunction": function(label, item, axis) {
                  var chart = axis.chart;

                  if ((chart.realWidth <= 1050 ) && ( label.length > 10 )) {
                      return label.substr(0, 10) + '...';
                  } else {
                      return label;
                  }


              }
          },
          "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10,
            "marginTop": 20
          },
          "trendLines": [],
          "graphs": [ {
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "showBalloon": true,
                        "valueField": "Time",
                      //  "balloonText": "Time:[[Time]]",
                        "valueAxis": "durationAxis",
                        "title": Login_time,
                        "labelText": "[[Time]]",
                        "labelFunction": function(item, label) {
                          // return label == "0" ? "" : label;
                          var sec =  label == "0" ? "" : label;
                          return  destructMS(sec);
                        },
                        "fixedColumnWidth": 5,
                        "balloonText": "[[Name]] ",
                    } ],                     
          "guides": [],          
          "valueAxes": [
            {
              "id": "ValueAxis-1",
               "duration": "ss",
               "durationUnits": 	{DD:"d. ", hh:":", mm:":",ss:""},
              "position": "top",
              "axisAlpha": 0,
              "title": totalTimeSpentByUser,
            }
          ],
          "allLabels": [],
        "balloon": {
            "borderThickness": 1,
            "borderAlpha": 1,
            "fillAlpha": 1,
            "horizontalPadding": 4,
            "verticalPadding": 2,
            "shadowAlpha": 0,

            "textAlign":"middle",
            //  "cornerRadius":4,
            "pointerOrientation":"down",
            "fixedPosition": true,
            "maxWidth": 10000
        },
          "titles":"",
            "chartCursor": {
                "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
            },
          "dataProvider":generateMosttimeSpentonSystemDiv(total_time),
            "export": {
              "enabled": false
             },
        "responsive": {
            "enabled": false,
            "addDefaultRules": true,
            "rules": [
                {
                    "maxWidth": 400,
                    "maxHeight": 400,
                    "minWidth":200,
                    "overrides": {
                        "marginLeft":-30,
                        "legend": {
                            "enabled": true,
                            "marginRight":100,
                            "marginLeft":10
                        },

                    }
                },
                {
                    "maxWidth": 1440,
                    "minHeight": 900,
                    "overrides": {
                        "legend": {
                            "enabled": true,
                            // "marginRight":150,
                            // "marginLeft":20,
                        }
                    }
                }
            ]
        }
} );



    /**
     * amCharts Plugin: Order items by value
     * This plugin will look for setting `orderByField` and will re-order items
     * in ascending order before chart loads
     */

    AmCharts.addInitHandler( function( chart ) {

        // Check if `orderByField` is set
        if ( chart.orderByField === undefined ) {
            // Nope - do nothing
            return;
        }

        // Re-order the data provider
        chart.dataProvider.sort( function( a, b ) {
            if ( a[ chart.orderByField ] > b[ chart.orderByField ] ) {
                return -1;
            } else if ( a[ chart.orderByField ] == b[ chart.orderByField ] ) {
                return 0;
            }
            return 1;
        } );

    }, [ "serial" ] );

    /**
     * Function  generates charts for "Assigned Entreprise to User"
     *
     */
    var chart = AmCharts.makeChart("chartdiv", {
          "type": "serial",
          "theme": "none",
          "categoryField": "Entreprise",
          "rotate": true,
          "orderByField": "Total User",
          "startDuration": 1,
          "fontFamily":" Raleway,sans-serif",
          "categoryAxis": {
            "gridPosition": "start",
            "position": "left",
            "title":entreprise,
            "labelFunction": function(label, item, axis) {
                  var chart = axis.chart;

                  if ((chart.realWidth <= 1050 ) && ( label.length > 10 )) {
                      return label.substr(0, 10) + '...';
                  }else if((chart.realWidth <= 768 ) && ( label.length > 10 )){
                      return label.substr(0, 8) + '...';
                  } else {
                      return label;
                  }
              }
          },
          "sortColumns":true,
          "legend": {
            "horizontalGap": 10,
           // "maxColumns": 3,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10,
            "marginTop": 20
          },
          "trendLines": [],
          "graphs": [
            {
              "labelText": "[[value]]",
              "labelFunction": function(item, label) {
                  return label == "0" ? "" : label;
                },
              "fillAlphas": 0.8,
              "id": "AmGraph-1",
              "lineAlpha": 0.2,
              "title": "Responsable",
              "type": "column",
          //      "color": "#000000",
              "valueField": "Responsable",
              "showBalloon": true,
              "fixedColumnWidth": 15,
              "integersOnly":true ,
                "balloonText": "[[Entreprise]] : Responsable "
            },
            {
              "labelText": "[[value]]",
              "labelFunction": function(item, label) {
                  return label == "0" ? "" : label;
                },
              "fillAlphas": 0.8,
              "id": "AmGraph-2",
              "lineAlpha": 0.2,
              "title": "Collaborateur",
              "type": "column",
              "color": "#000000",
              "valueField": "Collaborateur",
              "showBalloon": true,
              "fixedColumnWidth": 15,
              "integersOnly":true,
                "balloonText": "[[Entreprise]] : Collaborateur"
            }
            // ,{
            //       "labelText": "[[value]]",
            //       "labelFunction": function(item, label) {
            //           return label == "0" ? "" : label;
            //       },
            //       "fillAlphas": 0.8,
            //       "id": "AmGraph-2",
            //       "lineAlpha": 0.2,
            //       "title": "Total User",
            //       "type": "column",
            //       //"color": "#000000",
            //       "valueField": "Total User",
            //       "showBalloon": false,
            //       "fixedColumnWidth": 15,
            //       "integersOnly":true
            //   }
          ],
          "guides": [],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.5,
                "position": "top",
                "title" : valueAxisTitle
               // "gridAlpha": 0
            }],
          // "valueAxes": [
          //   {
          //     "id": "ValueAxis-1",
          //     "position": "top",
          //     "axisAlpha": 0,
          //     "title": assigned_entreprise
          //   },
          //   {
          //     "id": "ValueAxis-2",
          //     "position": "top",
          //     "axisAlpha": 0,
          //     "title": "  {{ __('probtp.assigned_entreprise') }} "
          //   }
          // ],
          "allLabels": [],
                    "balloon": {
                    "borderThickness": 1,
                    "borderAlpha": 1,
                    "fillAlpha": 1,
                    "horizontalPadding": 4,
                    "verticalPadding": 2,
                    "shadowAlpha": 0,
                    "textAlign":"middle",
                    //  "cornerRadius":4,
                    "pointerOrientation":"down",
                        "fixedPosition": true,
                        "maxWidth": 10000
                },
        "chartCursor": {
            "cursorAlpha": 0,
            "oneBalloonOnly": true,
            "categoryBalloonText": "&raquo;"
        },
          "titles":"",
          "dataProvider": generateChartData(arrValue),
          "export": {
              "enabled": false
          },
        "responsive": {
            "enabled": false,
            "addDefaultRules": true,
            "rules": [
                {
                    "maxWidth": 400,
                    "maxHeight": 400,
                    "minWidth":200,
                    "overrides": {
                        "marginLeft":-30,
                        "legend": {
                            "enabled": true,
                            "marginRight":100,
                            "marginLeft":10,
                        },

                    }
                },
                {
                    "maxWidth": 1440,
                    "minHeight": 900,
                    "overrides": {
                        "legend": {
                            "enabled": true,
                            // "marginRight":150,
                            // "marginLeft":20,
                        }
                    }
                }
            ]
        }


});


});



















//  window.onload = function () {
//         var chart = AmCharts.makeChart( "pieChart", {
//             "libs": { "autoLoad": false },
//             "type": "pie",
//             "theme": "light",
//             "dataProvider": [
//                 // <?php
//                 // foreach ($user as $key => $val){
//                 //      echo'{"user": "'.$key.'" , "total": '.$val.' },';
//                 //      echo "\r\n";
//                 // }
//                 // ?>
//             ],
//             "valueField": "total",
//             "titleField": "user",
//             "outlineAlpha": 0.4,
//             "depth3D": 15,
//             "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
//             "angle": 30,
//             "export": {
//                 "enabled": false
//             }
//         } );


//         var barChart = AmCharts.makeChart( "chartdiv", {
//             "type": "serial",
//             "theme": "light",
//             "marginRight": 70,
//             "dataProvider": [
//                // <?php
//                //  foreach ($res as $key => $val){
//                //      echo'{"user": "'.$key.'" , "AssignedMmodule": '.$val.' },';
//                //      echo "\r\n";
//                //  }
//                //  ?>

//                  ],
//             "valueAxes": [ {
//                 "gridColor": "#FFFFFF",
//                 "gridAlpha": 1,
//                 "dashLength": 0,
//                 "integersOnly":true,
// //                "position": "left",
//                 "title": " {{ __('probtp.assigned_module') }} "
//             } ],
//             "gridAboveGraphs": true,
//             "startDuration": 1,
//             "graphs": [ {
//                 "balloonText": "[[user]]: <b>[[AssignedMmodule]]</b>",
//                 "fillAlphas": 0.8,
//                 "lineAlpha": 0.2,
//                 "type": "column",
//                 "valueField": "AssignedMmodule"
//             } ],
//             "chartCursor": {
//                 "categoryBalloonEnabled": true,
//                 "cursorAlpha": 0.5,
//                 "zoomable": true
//             },
//             "categoryField": "user",
//             "categoryAxis": {
//                 "gridPosition": "start",
//                 "gridAlpha": 0,
//                 "tickPosition": "middle",
//                 "tickLength": 10,
//                 "labelRotation": 30
//             },
//             "export": {
//                 "enabled": false
//             }

//         } );

//         var doubleBarchart = AmCharts.makeChart("doublechartdiv", {
//             "type": "serial",
//             "theme": "light",
//             "categoryField": "user",
//             "mouseWheelZoomEnabled": true,
//             "rotate": true,
//             "startDuration": 1,
//             "categoryAxis": {
//                 "gridPosition": "start",
//                 "position": "left"
//             },

//             "chartCursor": {
// //                "categoryBalloonEnabled": true,
// //                "cursorAlpha": 0.5,
//                 "zoomable": true,
// //                "selectWithoutZooming": true
//             },
//             "trendLines": [],
//             "dataProvider": [
//                 // <?php
//                 // foreach($collectionOfres as $key => $val){
//                 //     echo'{"user": "'.$key.'" , "Collaborateur": '.$val['collaborature'].',"Entreprise" : '.$val['entreprise'].' },';
//                 //     echo "\r\n";
//                 // }
//                 // ?>
//             ],
//             "graphs": [
//                 {
//                     "balloonText": "AssignedCollaborateur:[[Collaborateur]]",
//                     "fillAlphas": 0.8,
//                     "id": "AmGraph-1",
//                     "lineAlpha": 1,
//                     "title": "Collaborateur",
//                     "type": "column",
//                     "valueField": "Collaborateur",
//                     "showAllValueLabels":true,
//                      "fixedColumnWidth": 1
//                 },
//                 {
//                     "balloonText": "AssignedEntreprise:[[Entreprise]]",
//                     "fillAlphas": 0.8,
//                     "id": "AmGraph-2",
//                     "lineAlpha": 1,
//                     "title": "Entreprise",
//                     "type": "column",
//                     "valueField": "Entreprise",
//                     "showAllValueLabels":true,
//                     "fixedColumnWidth": 1
//                 }
//             ],
//             "guides": [],
//             "valueAxes": [
//                 {
//                     "id": "ValueAxis-1",
//                     "position": "top",
//                     "axisAlpha": 0.3,
//                     "showFirstLabel": true,
//                     "showLastLabel": true,
//                     "autoGridCount":true,
//                     "integersOnly":true
//                 }
//             ],
//             "allLabels": [],
//             "balloon": {},
//             "titles": [],

//             "export": {
//                 "enabled": false
//             }

//         });
//     }