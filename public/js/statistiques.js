
function addLabel(chart){
    console.log( chart.dataProvider.length);
    console.log( chart.dataProvider);
    if (0 === chart.dataProvider.length) {
        // set min/max on the value axis
        chart.valueAxes[0].minimum = 0;
        chart.valueAxes[0].maximum = 100;
        // add dummy data point
        var dataPoint = {
            dummyValue: 0
        };
        dataPoint[chart.categoryField] = '';
        chart.dataProvider = [dataPoint];

        // add label
        chart.allLabels = [];
        var a={x:0,y:'50%',text:"Désolé, aucun résultat n'a pas été trouvé !",align:'center',size:'24',color:'#ea6212',enabled:!0};
        chart.allLabels.push(a);

        // set opacity of the chart div
        chart.chartDiv.style.opacity = 0.5;

        // redraw it
        chart.validateNow();
    }
}