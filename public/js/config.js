/**
 * @file
 * A JavaScript file for Application configuration
 *
 */

// Define App and datatable variable 
var app = app || {};
var dataTable;

app.config = {
    SITE_PATH: window.Probtp.baseUrl,
    init: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};

// Log data in console
function l(data) {
    console.log(data);
}

// Check string is valid json or not
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


// Show loading mask
app.showLoader = function (id) {
    $((id) ? id : 'body').mask("<img src='" + app.config.SITE_PATH + "public/img/svg/loading.svg'");
}

// Hide loading mask 
app.hideLoader = function (id) {
    $((id) ? id : 'body').unmask();
}

// Remove loading mask
app.removeLoader = function () {
    $('.loadmask').remove();
    $('.loadmask-msg').remove();
    $('.masked').removeClass('masked');
    $('.masked-relative').removeClass('masked-relative');
}

// Data Table
app.dataTable = function(){
	
	if($('#data-table').length){

		dataTable =   $('#data-table').DataTable(
					{
					"pageLength": 100,    
					"order": [],
					"language": {
						"paginate": {
						  "previous": "Précédente",
						  "next": "Suivante",
                          "emptyTable": " Désolé, aucun résultat n'a pas été trouvé !"
						},
						'lengthMenu' : '_MENU_ par page',
						"info": "_START_ à _END_ sur _TOTAL_ réponses",
						'search' : 'Rechercher : ',
					  },
					"columnDefs": [ {
						  "targets": 'no-sort',
						  "orderable": false
					} ]}
		 );

	}

    if($('#data-table4').length){

        dataTable4 =   $('#data-table4').DataTable(
            {
                "pageLength": 100,
                "order": [],
                "language": {
                    "paginate": {
                        "previous": "Précédente",
                        "next": "Suivante",
                        "emptyTable": " Désolé, aucun résultat n'a pas été trouvé !"
                    },
                    'lengthMenu' : '_MENU_ par page',
                    "info": "_START_ à _END_ sur _TOTAL_ réponses",
                    'search' : 'Rechercher : ',
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ]}
        );

    }

    function searchDate(){
        /* Custom filtering function which will search data in column four between two values */


        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = $('#startDate').val();
                var max = $('#endDate').val();
                var age =(data[2]); // use data for the age column

                // //  var date = age.getFullYear() + '-'+ (((age.getMonth()+1).toString().length === 1) ? '0'+ (age.getMonth()+1) : (age.getMonth()+1))  +'-'+((age.getDate().toString().length === 1) ? '0'+ age.getDate() : age.getDate()) ;
                //   console.log(age);
                //   console.log(min.toLocaleString('Europe/Paris'));

                if ( ( ( min === null) && ( max === null ) ) ||
                    ( ( min === null) && age <= max ) ||
                    ( min <= age   && ( max === null ) ) ||
                    ( min <= age   && age <= max ) ||
                    ( min === age )  || ( max === age )    )
                {
                    return true;
                }
                return false;
            }
        );


        // $('#endDate').keyup( function() { dataTable4.draw(); } );
    }
};


app.dataTable3 = function(){
    if($('#data-table3').length){

        dataTable3 =   $('#data-table3').DataTable(
            {
                "pageLength": 100,
                "order": [],
                "language": {
                    "paginate": {
                        "previous": "Précédente",
                        "next": "Suivante",
                        "emptyTable": " Désolé, aucun résultat n'a pas été trouvé !"
                    },
                    'lengthMenu' : '_MENU_ par page',
                    "info": "_START_ à _END_ sur _TOTAL_ réponses",
                    'search' : 'Rechercher : ',
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ]}
        );

    }
};

// Check all check box
app.checkAll = function(){
	$('#checkAll').on('click',function(){
		$('.checkAllChild').prop('checked',$(this).is(":checked"));
	});
	
	$('.checkAllChild').on('click',function(){
		if($(this).is(":checked")){									
			if($('.checkAllChild').length === $('.checkAllChild:checked').length){
				$('#checkAll').prop('checked',$(this).is(":checked"));
			}
		}else{
			$('#checkAll').prop('checked',$(this).is(":checked"));
		}
	});
}

// Check all check box
app.delete = function(message){
    $('.delete').on('click',function(){
        var r = confirm(message);
        if (r == true) {
            $('#delete_id').val($(this).attr('data-id'));
            $('#deleteForm').submit();
        }
    });
}


app.restore = function(message){
    $('.restore').on('click',function(){
 var r = confirm(message);
        if (r == true) {
            $('#restore_id').val($(this).attr('data-id'));
            $('#restoreForm').submit();
        }
    });
}

app.duplicate = function(message){
    $('.duplicate').on('click',function(){
        var r = confirm(message);
        if (r == true) {
            $('#duplicate_id').val($(this).attr('data-id'));
            $('#duplicateForm').submit();
        }
    });
}


setTimeout(function(){
	$('.alert').fadeOut('slow');
},2500);