/**
 * @file
 * A JavaScript file for Application configuration
 *
 */

// Define App and datatable variable 
var app = app || {};
var dataTable, searchBtn;

app.config = {
    SITE_PATH: baseUrl,
    init: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};

// Log data in console
function l(data) {
    console.log(data);
}

// Check string is valid json or not
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


// Show loading mask
app.showLoader = function (id) {
    $((id) ? id : 'body').mask("<img src='" + app.config.SITE_PATH + "/image/svg/loading.svg'");
}

// Hide loading mask 
app.hideLoader = function (id) {
    $((id) ? id : 'body').unmask();
}

// Remove loading mask
app.removeLoader = function () {
    $('.loadmask').remove();
    $('.loadmask-msg').remove();
    $('.masked').removeClass('masked');
    $('.masked-relative').removeClass('masked-relative');
}

// Set ToolTip
app.toolTip = function () {
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        trigger: 'hover'
    });
}
