<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // Dashboard
    'dashboard' => 'Tableau de bord',
    'you_are_logged_in' => 'Vous êtes connecté/e',
    'home' => 'Accueil',


    // General Translation
    'site_title' => "PRO BTP",
    'name' => "Nom",
    'to' => "à",
    'first_name' => "Prénom",
    'last_name' => "Nom",
    'email' => "E-mail",
    'select' => "Sélectionner",
    'status' => "Statut",
    'parent' => "Parent",
    'type' => "Type",
    'assign' => "Assigner",
    'token_mismatch_error' => 'Le temps s\'est écoulé. Merci de réessayer.',
    'all' => 'Tous',
    'detail' => 'Détails',
    'keyword' => 'Mots clés',
    'file' => 'Fichier',
    'summary' => 'Sommaire',
    'back' => 'Retour',
    'likes' => 'Like',
    'responsable' => 'Responsable',
    'count' => 'Count',
    'collaborateur' => 'Collaborateur',
    'edit' => 'Modifier',
    'copy' => 'Copie',
    'delete' => 'Supprimer',
    'addmodule' => 'Ajouter un module',
    'attach' => 'Liste des modules attachés',
    'unattach' => 'Attacher un nouveau module',
    'logo' =>'Logo',
    'subjectline'=> 'Bienvenue au PRO BTP Formation',
    'email_note'=> 'Merci de ne pas modifier des textes qui se trouve à l\'intérier des accolades. Example : {name}',
    'description'=>'Résumé',
    'color'=>'Affecter une couleur',
    'font_awesome' => 'Icône',
    'close' => 'Fermer',
    'enable' => 'Activer',
    'disable' => 'Désactiver',
    'reset' => 'Rétablir',
    'management' => 'Gestion',
    'group' => 'Groupe',
    'last_updated' => 'Dernière mise à jour',
    'administrator' => 'Administrateur',
    'formateur' => 'Formateur',
    'quiz' => 'Jeu-concours',
    'max_char' => 'Un maximum de 140 caractères sont autorisés',
    'attach_submit' => 'Attacher',
    'detached_submit' => 'Detacher',
    'add' => ' Ajouter',
    'duplicate' => 'Dupliquer',
    'preview' => 'Aperçu',
    'list' => 'Liste',
    'restore' => 'Restaurer',
    'login_message' => 'Vous êtes connecté/e !',
    'remember_me' => 'Se souvenir de moi',
    'link' => 'lien',
    'send' => 'Envoyer',
    'forgot_password' => 'Mot de passe oublié ?',
    'update_success' => 'Mise à jour réussie',
    'content' => 'Présentation du site',
    'content_update' => 'Mise à jour du contenu',
    'profile_image' => 'Photo de profil',


    // Email Template
    'email_template' => 'Modèles d\'emails',
    'template_name' => 'Titre',
    // Trash Module

    'trash' => 'Poubelle',
    'trash_module' => 'Gestion des corbeilles',

    // Category Module

    'category' => 'Formations',
    'category_manager' => 'Gestion des formations',
    'category_name' => 'Nom du formation',
    'parent_name' => 'Nom du parent',
    'parent_category' => 'Formations parente',
    'select_category' => 'Sélectionner',
    'master_management' => 'Gestion d\'interface',
    'fail_message_category' => 'Cette formations est déjà associée avec une autre dossier',
    'assign_category' => 'Assigner une formations',

    // Entreprise Module

    'entreprise' => 'Enterprise',
    'entreprise_manager' => 'Gestion des entreprises',
    'entreprise_name' => 'Nom de l\'entreprise',
    'fail_message_entreprise' => 'Cet entreprise est déjà associé avec une autre dossier',
    'attach_enterprise' => 'Attribuer',

    // Module module

    'modules' => 'Module',
    'modules_manager' => 'Gestion des modules',
    'modules_title' => 'Les libellés',
    'modules_keyword' => 'Mot-clés',
    'modules_file_name' => 'Nom du fichier',
    'modules_file_download' => 'Télécharger',
    'modules_summary' => 'Sommaire',
    'success_module_assigned' => 'Ce module a été assigné avec succès',
    'fail_message_module' => 'Sélectionner au moins un module pour l\'attacher',
    'attach_module' => 'Attacher un module',


    // form management
    'form_management' => 'Gestion des formulaires',
    'form' => 'Formulaire',
    'form_manager' => 'Gestion des formulaire',
    'select_form' => 'Selectionner un formulaire',
    'title' => 'Titre',
    'identifier' => 'Identifier',
    'fail_message_form' => 'Ce formulaire est associé avec un autre dossier',
    'duplicate_message' => 'Êtes-vous sûr de vouloir dupliquer ce jeu-concours ?',

    // User Module
    'user' => 'Membre',
    'user_management' => 'Membres',
    'user_name' => 'Nom d\'utilisateur',
    'password' => 'Mot de passe',
    'confirm_password' => 'Confirmer le mot de passe',
    'myaccount' => 'Mon Profil',
    'change_password' => 'Confirmer le mot de passe',
    'current_password' => 'Mot de passe actuel',
    'new_password' => 'Nouveau mot de passe',
    'fail_password' => 'Veuillez entrer le bon mot de passe',
    'success_password' => 'Le mot de passe à été changé avec succès',
    'fail_message_user' => 'Cet utilisateur est associé avec un autre compte',
    'user_list' => 'Gestion des membres',
    'user_add' => 'Ajouter un membre',
    'user_edit' => 'Modifier un membre',

    // Default module

    'module_add'  => ':module Ajouter',
    'module_edit' => ':module Modifier',
    'module_list' => ':module Liste',
    'action_add'  => 'ajouté',
    'action_update' => 'modifié',
    'action_delete' => 'supprimé',
    'action_copied' => 'copié',
    'action_assigned' => 'attribué',
    'action_unassigned' => 'détacher',
    'action_restored' => 'restorer',
    'fail_message' => 'Une erreur est survenue. Veuillez réessayer ultérieurement!',
    'success_message' => ':module record :action successfully!',
    'success_assigned_module' => ':module assigned successfully',
    'delete_message' => "Êtes-vous sûr de vouloir supprimer cela?",
    'restore_message' => "Êtes-vous sûr de vouloir le restaurer?",

    'action' => 'Action',
    'submit' => 'Enregistrer',
    'update' => 'Mettre à jour',
    'login'  => 'Connexion',
    'logout' => 'Se déconnecter',
    'cancel' => 'Annuler',
    'to_login' => 'Se connecter',

    // Quiz
    'quiz_managment' => 'Gestion des jeu-concours',
    'questions' => 'Question',
    'answers' => "Réponses",
    'select_quiz' => 'Sélectionner un jeu-concour',
    'fail_message_quiz' => 'Ce jeu-concour est associé avec un autre dossier',

    // Extra List

    'module_success_messsage' => 'Un nouveau membre a bien été ajouté',
    'reset_password' => 'Rétablir le mot de passe',
    'category_list' => 'Gestion des formations',
    'add_category' => 'Ajouter',
    'assign_enterprise' => 'Assigner une enterprise',
    'select_enterprise' => 'Tout cocher / tout décocher',
    'modules_list' => 'Liste des modules',
    'category_edit' => 'Modifier une formation',
    'parent_category_type' => 'Choisir la formations',
    'category_type' => 'Type de formations',
    'category_update_sucess' => 'La formations a été modifiée',
    'category_add_success' => 'Une formations a été ajoutée',
    'category_delete_success' => 'La record a été supprimée',
    'group_management' => 'Gestion des Quizzs',
    'password_change' => 'Modifier le mot de passe',

    // Enterprise

    'enterprise_list' => 'Gestion des entreprises',
    'assign_enterprise_admin' => 'Assigner un administrateur',
    'select_all_user' => 'Tout cocher / tout décocher',
    'enterprise_assigne_success' => 'Modification de la liste des administrateurs est fait.',
    'enterprise_edit' => 'Modifier une entreprise',
    'enterprise_update_sucess' => 'L’entreprise a été modifiée',
    'enterprise_create_sucess' => '',
    'enterprise_create' => 'Ajouter une entreprise',
    'enterprise_parent' => 'Choisir un parent',
    'assign_enterprise_formature'=>'Assigner un formateur',

    // Email

    'email_list'  => 'Gestion des modèles des e-mails',
    'add_email_template' => 'Modifier un modèle d’e-mail',

    // Quiz
    'quiz_list' => 'Gestion des jeu-concours',
    'quiz_edit' => 'Modifier un jeu-concours',
    'quiz_add' => 'Ajouter un jeu-concours',

    // Form
    'form_list' => 'Gestion des formulaires',
    'form_edit' => 'Modifier ce formulaire',

    // Module
    'module_index_list' => 'Gestion des modules',
    'assign_category_list' => 'Liste des formations',
    'module_index_edit' => 'Modifier un module',
    'module_update_sucess' => 'Ce module a bien été modifié',
    'module_delete_sucess' => 'Ce module a bien été supprimé',
    'module_index_create' => 'Ajouter un module',
    'module_add_sucess' => 'Ce module a bien été ajouté',

    //Trash
    'trash_module_list' => 'Gestion des modules supprimés',
    'trash_category_list' => 'Gestion des formations supprimés',
    'trash_user_list' => 'Gestion des membres supprimés',
    'trash_enterprise_list' => 'Gestion des entreprises supprimés',
    'trash_form_list' => 'Gestion des formulaires supprimés',
    'trash_quiz_list' => 'Gestion des jeu-concours supprimés',

    // Category
    'back_category_list' => 'Changer le visuel',
    'category_search_list' => 'Ajouter une formations',
    'category_add_index' => 'Ajouter une formation',
    'category_parent' => 'Choisir un parent',

    // Resource
    'resource'=>'Ressource',
    'resources'=>'Ressources',
    'resource_add_index' => 'Ajouter une ressource',
    'resource_add_success' => 'Une ressource a été ajoutée',
    'resource_list' => 'Gestion des ressources',
    'resource_edit' => 'Modifier une ressource',
    'resource_name' => 'Nom du resource',
    'resource_update_sucess' => 'La ressource a été modifiée',
    'resource_delete_success' => 'La ressource a été supprimée',
    'fail_message_resource' => 'Cette ressource est déjà associée avec une autre dossier',
    'resource_manager' => 'Gestion des ressources',
    'trash_resource_list' => 'Gestion des resource supprimés',
    'record' => 'Record',

    // Comment Section

    'comment_delete_sucess' => 'Ce commentaire a bien été supprimé',
    'comment' => 'Commentaire',

    // add RH
    'RH_successfully_added' => 'Le responsable est bien été ajouté',
    'rh_action_delete' => 'Le responsable est bien été supprimé',
    'rh_update_message' => ' Le responsable est bien été modifié',

    //for Statistiques in admin panel
    'statistiques' => 'Statistiques',
    'total_user' => 'Vision globale',
    'assigned_module' => 'Assigned Module to User',
    'assigned_entreprise' => 'Utilisateurs par entreprise',
    'assigned_entreprises_caterory_titte' => 'Entreprises',
    'assigned_entreprise_and_collab' => 'Assigned Entreprise and Collaborature of Responsable',
    'CommentsbyUSer' => 'Commentaires déposés',
    'numberofCommentsbyUSer' => 'Commentaires',
    'totalTimeSpentOnSystem' => 'Temps passé par les utilisateurs',
    'totalTimeSpentByUser' => 'Temps passé',
    'mostCompletedModuleDiv' => 'Modules réalisés',
    'mostCompletedModule' => 'Modules',
    'mostRecentLoginuser' => 'Derniers accès au site',
    'last_login' => 'Derniers accès',
    'mostAccessedModuleDiv' => 'Modules les plus consultés',
    'mostAccessedModule' => 'Modules',
    'commentsonModulesDiv' => 'Commentaires par catégrie',
    'commentsonModules' => 'Commentaires',
    'total' => 'Total',
    'total_login' => 'Total login time',
    'ressources' =>'ressources',
    'All_Users' => 'Vue d\'ensemble',
    'valueAxisTitle' =>'Utilisateurs',
	'Utilisateurs' =>'Entreprises',
    'Are you sure to logout?' => 'Êtes-vous sûr de vouloir vous déconnecter?',
    'select Period' => 'Sur la période',
    'Start Date' => 'du',
    'End Date' => 'au',
    'Show Chart' => 'Voir les statistiques',
    'ResourceList' => 'Tous les ressources ',
    'entrepriseList' => 'Tous les entreprises',
    'CommentList' => 'Tous les commentaires',
    'UserList' => 'Tous les  Membres',
    'FormationList' => 'Tous les formations',
    'Data_not_found!' =>'Désolé, aucun résultat n\'a pas été trouvé !', // need to change this in public/js/statistiques.js
    ];
