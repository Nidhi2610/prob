<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
	
	// Dashboard
    'dashboard' => 'Tableau de bord',
    'you_are_logged_in' => 'Vous êtes connecté/e',

    // General Translation
    'site_title' => "PRO BTP",
    'name' => "Nom",
    'to' => "à",
    'first_name' => "Prénom",
    'last_name' => "Nom",
    'email' => "E-mail",
    'select' => "Sélectionner",
    'status' => "Statut",
    'parent' => "Parent",
    'type' => "Type",
	'assign' => "Assigner",
    'token_mismatch_error' => 'Le temps s\'est écoulé. Merci de réessayer.',
	'all' => 'Tous',
	'detail' => 'Détails',
	'keyword' => 'Mots clés',
	'file' => 'Ficher',
	'summary' => 'Sommaire',
	'back' => 'Retour',
	'comment' => 'Commentaire',
	'likes' => 'Like',
    'responsable' => 'Responsable',
    'count' => 'Count',
    'collaborateur' => 'Collaborateur',
    'edit' => 'Modifier',
    'copy' => 'Copie',
    'delete' => 'Supprimer',
    'addmodule' => 'Ajouter un module',
    'attach' => 'Liste des modules attachés',
    'unattach' => 'Attacher un nouveau module',
    'logo' =>'Logo',
    'subjectline'=> 'Bienvenue au PRO BTP Formation',
    'email_note'=> 'Merci de ne pas modifier des textes qui se trouve à l\'intérier des accolades. Example : {name}',
    'description'=>'Résumé',
    'color'=>'Couleur pour le catégorie',
    'font_awesome' => 'Icône',
    'close' => 'Fermer',
    'enable' => 'Activer',
    'disable' => 'Désactiver',
    'reset' => 'Rétablir',
    'management' => 'Gestion',
    'group' => 'Groupe',
    'last_updated' => 'Dernière mise à jour',
    'administrator' => 'Administrateur',
    'formateur' => 'Formateur',
    'quiz' => 'Jeu-concours',
    'max_char' => 'Un maximum de 140 caractères sont autorisés',
    'attach_submit' => 'Attacher',
    'detached_submit' => 'Detacher',
    'add' => ' Ajouter',
    'duplicate' => 'Dupliquer',
    'preview' => 'Aperçu',
    'list' => 'Liste',
    'restore' => 'Restaurer',
    'login_message' => 'Vous êtes connecté/e !',
    'remember_me' => 'souviens-toi de moi',
    'link' => 'lien',
    'send' => 'envoyer',


    // Email Template
    'email_template' => 'Modèles d\'emails',
    'template_name' => 'Nom du modèle',
    'content' => 'Contenu',

    // Trash Module

    'trash' => 'Poubelle',
    'trash_module' => 'Gestion des poubelles',

    // Category Module

    'category' => 'Catégorie',
    'category_manager' => 'Gestion des catégories',
    'category_name' => 'Nom du catégorie',
    'parent_name' => 'Nom du parent',
    'parent_category' => 'Catégorie parente',
    'select_category' => 'Selectionner une Categorie',
    'master_management' => 'Gestion d\'interface',
	'fail_message_category' => 'Cette categorie est déjà associée avec une autre dossier',
    'assign_category' => 'Assigner une categorie',

    // Entreprise Module

    'entreprise' => 'Enterprise',
    'entreprise_manager' => 'Gestion des entreprises',
    'entreprise_name' => 'Nom de l\'entreprise',
    'fail_message_entreprise' => 'Cet entreprise est déjà associé avec une autre dossier',
    'attach_enterprise' => 'Ajouter un entreprise',
	
	// Module module
	
	'modules' => 'Module',
	'modules_manager' => 'Gestion des modules',
	'modules_title' => 'Les libellés',
	'modules_keyword' => 'Mot-clé',
	'modules_file_name' => 'Nom du fichier',
	'modules_file_download' => 'Télécharger',
	'modules_summary' => 'Sommaire',
	'success_module_assigned' => 'Ce module a été assigné avec succès',
    'fail_message_module' => 'Sélectionner au moins un module pour l\'attacher',
    'attach_module' => 'Attacher un module',
	

    // form management
    'form' => 'Formulaire',
    'form_manager' => 'Gestion des formulaire',
	'select_form' => 'Selectionner un formulaire',
    'title' => 'Les libellés',
    'identifier' => 'Identifier',
    'fail_message_form' => 'Ce formulaire est associé avec un autre dossier',
    'duplicate_message' => 'Êtes-vous sûr de vouloir dupliquer ce dossier ?',

    // User Module
    'user' => 'Membre',
    'user_management' => 'Membres',
    'user_name' => 'Nom d\'utilisateur',
    'password' => 'Mot de passe',
    'confirm_password' => 'Confirmer le mot de passe',
	'myaccount' => 'Mon Profil',
	'change_password' => 'Confirmer le mot de passe',
	'current_password' => 'Mot de passe actuel',
	'new_password' => 'Nouveau mot de passe',
	'fail_password' => 'Veuillez entrer le bon mot de passe',
	'success_password' => 'Le mot de passe à été changé avec succès',
    'fail_message_user' => 'Cet utilisateur est associé avec un autre compte',

    // Default module

    'module_add'  => ':module Ajouter',
    'module_edit' => ':module Modifier',
    'module_list' => ':module Liste',
    'action_add'  => 'ajouté',
    'action_update' => 'modifié',
    'action_delete' => 'supprimé',
    'action_copied' => 'copié',
    'action_assigned' => 'attribué',
    'action_unassigned' => 'détacher',
	'action_restored' => 'restorer',
    'fail_message' => 'Une erreur est survenue. Veuillez réessayer ultérieurement!',
    'success_message' => ':module record :action successfully!',
    'success_assigned_module' => ':module assigned successfully',
    'delete_message' => "Êtes-vous sûr de vouloir supprimer ces données?",
    'restore_message' => "Êtes-vous sûr de vouloir restorer ces données?",

    'action' => 'Action',
    'submit' => 'Enregistrer',
    'update' => 'Mettre à jour',
    'login'  => 'Se connecter',
    'logout' => 'Se déconnecter',
    'cancel' => 'Annuler',

    // Quiz
    'quiz' => 'Jeu-concours',
    'questions' => 'Questions',
    'answers' => "Réponses",
    'select_quiz' => 'Sélectionner un jeu-concour',
    'fail_message_quiz' => 'Ce jeu-concour est associé avec un autre dossier',


];
