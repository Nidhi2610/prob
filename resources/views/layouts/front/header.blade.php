<header>
    <div class="container">
        <nav class="navbar header-top">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><img src = "{{ url('public/front/images/logo_probtp.jpg')}}"></a>
            </div>
            @if(isset($entreprise_log->logo))
                <img src="{{ url('storage/app/public/files/enterprise/'.$entreprise_log->logo)}}" style="height: 40px;width:70px;padding-left: 20px">
            @endif
            <!-- <ul class="nav navbar-nav header-mid">
                <li><div class="seprator-height"><a href="#">Particuliers</a><hr> </div></li>
                <li><div class="seprator-height separator"><a  href="#">artisans</a> <hr></div></li>
                <li><div class="seprator-height separator"><a href="#">Entreprises</a> <hr></div></li>
                <li><div class="seprator-height separator"><a href="#">Experts-comptables </a><hr> </div></li>
                <li><div class="seprator-height separator"><a href="#">Groupe | RH </a><hr> </div></li>
            </ul>-->
            <ul class="nav navbar-nav navbar-right header-left">
                @if(Auth::user()->user_type == 4 )
                <li>
                    <div class="seprator-height">

                        <a class="description_boutonNav" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out" aria-hidden="true" style = "color:#fff;"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
                    @elseif(Auth::user()->user_type == 1 || Auth::user()->user_type == 5)
                    <li>
                        <div class="seprator-height">

                            <a class="description_boutonNav" href="{{ url()->previous() }}">
                                <i class="fa fa-arrow-left" aria-hidden="true" style = "color:#fff;"></i>
                            </a>
                        </div>
                    </li>
                @endif
                <!--<li>
                    <div class="seprator-height separator">
                        <a href="#" id="probtp1" title="Login">
                            <i class="fa fa-sign-out" aria-hidden="true" style = "color:#fff;"></i>
                        </a>
                    </div>
                </li>-->
            </ul>
        </nav>
    </div>
</header>