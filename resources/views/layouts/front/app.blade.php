<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ProBtp') }}</title>
    <!-- Styles -->
    <link href="{{ asset('public/front/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" media="screen" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/front/css/video-js.css') }}" rel="stylesheet">

    <!-- If you'd like to support IE8 -->
    <script src="{{ asset('public/front/js/videojs-ie8.min.js') }}"></script>

@stack('css')
<!-- Scripts -->
    <script>
        window.Probtp = {!! json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' =>  url('/') . "/"
        ]) !!};

    </script>
</head>
<body>

    @include('layouts.front.header')
    @yield('content')
    @include('layouts.front.footer')

<!-- Scripts -->

    <script src="{{ asset('public/js/app.js') }}"></script>

<script src="{{ asset('public/front/js/config.js') }}"></script>
<script src="{{ asset('public/front/js/video.js') }}"></script>
    <script src="{{ asset('public/html5lightbox/html5lightbox.js') }}"></script>
@stack('external_script')
@stack('script')

</body>
</html>
