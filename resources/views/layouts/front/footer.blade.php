<div class = "container">
   @if(Auth::user()->user_type == 4 )
    <footer>
        <div class = "row">
            <div class = "pull-left">
                <!--<ul class = "footer-left">
                    <li><a href = "#"><i class="fa fa-user-circle" aria-hidden="true"></i><span>Prendre rendez-vous</span></a></li>
                    <li><a href = "#"><i class="fa fa-user-circle" aria-hidden="true"></i><span>Nous contacter</span></a></li>
                </ul>-->
            </div>
            <div class = "pull-right col-sm-push-3">
                <div class="boutonNav" style="position: relative; margin: 0; opacity: 1;" title="{{ __('probtp.logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    <a class="description_boutonNav" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('probtp.logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                   </div>
            </div>
        </div>
    </footer>
    @endif
    <div class = "row" style = "background:#dfdfdf;">
        <p class = "footer-link">
            <a href="http://www.mediadrive.fr" target="_blank"> Création de projet web</a> : <a href="http://www.mediadrive.fr" target="_blank"> Media Drive Paris</a>
        </p>
    </div>
</div>