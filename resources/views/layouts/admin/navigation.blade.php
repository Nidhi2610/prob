@inject('userComments', '\App\Models\Comments')
@inject('userList', '\App\Models\User')
@inject('entreprise', '\App\Models\Entreprise')
<?php $colloborateurCount = $userList->init(); ?>
<?php $communityCount = $userList->getCommunityMember();?>
<?php $entrepriseCount = $entreprise->getEntrepriseCount();?>
<?php $comments = $userComments->getRecentComments();  ?>
<div class="col-sm-2 col-lg-1">
    {{--<div id="side-nav-bar">--}}
    <nav class="navbar navbar-default navbar-fixed-side sidebar-head" id="header-nav">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <div class="col-md-3">
                        <button class="navbar-toggle navbar-btn-bg" data-target=".navbar-collapse"
                                data-toggle="collapse">
                            <span>MENU</span>
                        </button>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <a class="navbar-brand" href="/index">
                                @if(!empty(Auth::user()->Entreprise->logo))
                                    <img class="title_logo"
                                         src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ Auth::user()->Entreprise->logo }}"/>
                                @else
                                    <img src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                                @endif
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dropdown responcive-toggle">
                            <li class="dropdown">
                                <span class="badge">@if(!empty($comments)) {{$comments->count()}} @endif</span>
                                <a href="#" class="dropdown-toggle dropbtn" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                    {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ Auth::user()->first_name }} </span><span class="caret"></span>--}}
                                </a>


                                <ul class="dropdown-menu drop-active dropdown-content" role="menu">
                                    @if(isset($comments) && !empty($comments))
                                        @foreach($comments as $comment_data)
                                            <a href="{{url('details/'.$comment_data->category_id.'/'.$comment_data->category_id.'/#comments_'.$comment_data->id)}}">
                                                <li>
                                                    <div>
                                                        <span>{{$comment_data->user->name}}</span>
                                                    </div>
                                                    <div class="orange-cir">
                                                                <span><i class="fa fa-circle" aria-hidden="true"></i>
                                                                    {{$comment_data->comment}}</span>
                                                    </div>
                                                    {{--<div>
                                                        <span>Barbara Denis</span>
                                                    </div>--}}
                                                    <div>
                                                        <span>{!! utf8_encode(strftime('%e %B %Y' , strtotime ($comment_data->created_at->format('d-M-Y'))) )  !!}</span>
                                                    </div>
                                                </li>
                                            </a>
                                        @endforeach
                                    @endif

                                </ul>

                            </li>
                        </div>
                    </div>

                </div>
            </div>
            <div class="collapse navbar-collapse nal-image">
                <ul class="nav navbar-nav">
                    <li class="{{$index or ""}}">
                        @if(Auth::user()->user_type == 5 )
                        <a href="{{url('/index')}}"><p
                                    style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                </p><br>
                            <span>Mon offre de formation</span></a>
                        @else
                            <a href="javascript:void(0);"><p
                                        style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                    </p><br>
                                <span>Mon offre de formation</span></a>
                        @endif
                    </li>

                    <li class="{{$userEntreprise  or ""}}">
                        @if(Auth::user()->user_type == 5)
                        <a href="{{url('user_entreprise')}}"><p
                                    style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                </p><br>

                            @if($entrepriseCount != 0)
                                <span class="counter badge">{{$entrepriseCount}}</span>
                            @endif
                            <span>Gestion groupe </span></a>
                        @else
                            <a href="javascript:void(0);"><p
                                        style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                    </p><br>

                                @if($entrepriseCount != 0)
                                    <span class="counter badge">{{$entrepriseCount}}</span>
                                @endif
                                <span>Gestion groupe </span></a>
                        @endif
                    </li>
                    @if(Auth::user()->user_type == "3")
                        <li class="{{$userlist or ""}}">
                            @if(Auth::user()->user_type == 5)
                                <a href="{{url('userlist')}}">
                                    <p style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                        </p><br>

                                    @if($colloborateurCount->count() != 0)
                                        <span class="counter badge">{{$colloborateurCount->count()}}</span>
                                    @endif
                                    <span>Mon équipe</span></a>
                            @else
                                <a href="javascript:void(0);">
                                    <p style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                        </p><br>

                                    @if($colloborateurCount->count() != 0)
                                        <span class="counter badge">{{$colloborateurCount->count()}}</span>
                                    @endif
                                    <span>Mon équipe</span></a>
                            @endif
                        </li>
                    @endif
                    <li class="{{$communtiyTab or ""}}">
                        @if(Auth::user()->user_type == 5)
                        <a href="{{url('user/community')}}">
                            <p
                                    style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                </p><br>
                            @if($communityCount->count() != 0)
                                <span class="counter badge">{{$communityCount->count()}}</span>
                            @endif
                            <span>Ma communauté</span></a>

                        @else
                       <a href="javascript:void(0);">
                           <p
                                   style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                               </p><br>
                           @if($communityCount->count() != 0)
                               <span class="counter badge">{{$communityCount->count()}}</span>
                           @endif
                           <span>Ma communauté</span></a>

                        @endif

                    </li>
                    {{--<li class="{{$setting or ""}}">
                        <a href="javascript:void(0);"><p style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;"></p><br>
                            <span>Les challenges</span></a>

                    </li>--}}
                    <li class="dropdown">
                        <a href="javascript:void(0);"><p
                                    style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;">
                                </p><br>
                            <span>Paramètres</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                @if(Auth::user()->user_type == 5)
                                    <a href="{{ route('userProfile')}}">
                                        <i class="fa fa-user-o"> </i> Mon Profil
                                    </a>
                                @else
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-user-o"> </i> Mon Profil
                                    </a>
                                @endif
                            </li>
                            <li>
                                @if(Auth::user()->user_type == 5)
                                <a href="{{ route('changePassword')}}">
                                    <i class="fa fa-exchange"> </i> Mot de passe
                                </a>
                                @else
                                    <a href="javascript:void(0);">
                                        <i class="fa fa-exchange"> </i> Mot de passe
                                    </a>
                                @endif
                            </li>
                            <li>

                                <a href="{{ route('probtp_logout') }}">
                                    <i style="padding-right: 5px" class="fa fa-sign-out"> </i> {{ __('probtp.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('probtp_logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                    <li class="logo-bottom">
                        <a href="javascript:void(0);">
                            <img src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    {{-- </div>--}}
</div>

<div class="col-sm-10 col-lg-11 col-header-nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-header-nopadding header-bottom">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="/index">
                                <?php $id = Auth::user()->entreprise_id;
                                $entreprises = \App\Models\Entreprise::where('id', $id)->where('parent_id', 0)->get();  ?>
                                @foreach($entreprises as $entreprise)
                                    @if( !empty($entreprise) )
                                        <img class="title_logo"
                                             src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ $entreprise['logo'] }}"/>

                                    @else
                                        <img class="title_logo" src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                                    @endif
                                @endforeach
                            </a>
                        </div>
                        <ul class="nav navbar-nav">

                            <input type="text" id="search_box" name="search"
                                   placeholder="Rechercher un sujet, un mot clé...">

                        </ul>
                        <ul class="nav navbar-nav navbar-right head">


                            <li class="dropdown" id="header-notification">
                                <span class="badge"></span>
                                <a href="#" class="dropdown-toggle dropbtn" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                    {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ Auth::user()->first_name }} </span><span class="caret"></span>--}}
                                </a>
                            </li>


                            <li class="dropdown user-profile">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    @if(!empty(Auth::user()->profile_image))
                                        @if(file_exists('storage/app/public/files/module/'.Auth::user()->profile_image))
                                            <img src="{{ URL::to('/') }}/storage/app/public/files/module/{{ Auth::user()->profile_image }}
                                                    "
                                                 style="width: 40px;height: 40px;cursor: pointer;margin-top: -10px;border-radius: 50%;"/>
                                        @endif
                                    @else
                                        <i class="fa fa-user-circle-o fa-2x" style="width: 40px;height: 40px;cursor: pointer;margin-top: -10px;border-radius: 50%;"></i>
                                    @endif
                                    {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ Auth::user()->first_name }} </span><span class="caret"></span>--}}
                                </a>
                                <ul class="dropdown-menu" role="menu">

                                    <li>
                                        <a href="{{ route('userProfile') }}">
                                            <i style="padding-right: 5px"
                                               class="fa fa-user-o"> </i> {{ __('probtp.myaccount', ['module' =>'']) }}
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('changePassword') }}">
                                            <i style="padding-right: 5px"
                                               class="fa fa-exchange"> </i> {{ __('probtp.password_change') }}
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('probtp_logout') }}">
                                            <i style="padding-right: 5px"
                                               class="fa fa-sign-out"> </i> {{ __('probtp.logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('probtp_logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
// Show hide popover
        $(document).on('click', ".dropdown", function () {
            $(this).find(".dropdown-menu").slideToggle("fast");
        });
        $("#search_box").keyup(function (event) {
            if (event.keyCode == 13) {
                var search_keyword = $('#search_box').val();
                if (search_keyword == "" || search_keyword == null) {
                    alert('Entrez le mot-clé de recherche')
                } else {
                    var replaced_keyword = search_keyword.split(' ').join('-');
                    window.location = app.config.SITE_PATH + 'search/' + replaced_keyword;
                }

            }
        });
    });

</script>
@endpush