<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
	
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{ asset('public/front/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front1/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/front1/css/navbar-fixed-side.css') }}" rel="stylesheet">
    <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/mask/jquery.loadmask.css') }}" media="screen" rel="stylesheet" type="text/css">

@stack('css')
<!-- Scripts -->
    <script>
        var csrf_token = "<?php echo csrf_token(); ?>";
        window.Probtp = {!! json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' =>  url('/') . "/"
        ]) !!};

    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        @include('layouts.admin.navigation')
        <div class="col-sm-10 " >
            @if(Session::has('fail'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('fail') }} </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('success') }} </div>
            @endif
        </div>

        @yield('content')

       @include('layouts.admin.rightsidebar')
    </div>
</div>
</div>
</div>
</div>


<!-- Scripts -->
<script src="{{ asset('public/js/app.js') }}"></script>
<script src="{{ asset('public/html5lightbox/html5lightbox.js') }}"></script>
<script type="text/javascript" src="{{asset('public/mask/jquery.loadmask.min.js') }}"></script>
<script src="{{ asset('public/js/config.js') }}"></script>
@stack('external_script')
@stack('script')
</body>
</html>
