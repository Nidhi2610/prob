@inject('userComments', 'App\Models\Comments')
@inject('category', 'App\Models\Category')
@inject('module_category', 'App\Models\ModuleCategory')
@inject('getMostAccssedModule', 'App\Models\getMostAccessedModule')
@inject('user', 'App\Models\User')
<?php Carbon\Carbon::setLocale('fr');?>
<?php $comments = $userComments->getRecentComments(Auth::user()->id); ?>
<?php
    $user_entreprise = $user->getAllparentChildEntreprise();
    $categoryArray = array();
    $getModules = '';
    $moduleId = array();

    if(count($user_entreprise['ent']) > 0){
        $getCategory = $category->getCategoryByEnterpriseId(implode(',',$user_entreprise['ent']));
        if($getCategory->count() > 0){
            $getCategoryArray = $getCategory->toArray();
            foreach ($getCategoryArray as $categoryKey => $value ){
                $categoryArray[] = $value['id'];
            }
        }
    }


    $modules = $module_category->getLoggedInUsersModuleList($categoryArray);
    $com = $userComments->getLoggedInUsersCommentsList($categoryArray);
?>
<style>
    .custom_badge{
        position: relative !important;
        left: 22px !important;
        top: -50px !important;
    }
    .custom_side_btn{
        width: 50px;
        height: 50px;
    }
    .fa-newspaper-o:before{
        padding-right: 10px ;
        font-size: 15px !important;
    }
    .module-bell{
        padding-bottom:0px;
    }
    .module-title{
        line-height: 40px;
    }

</style>
<div id="ajaxloadeddiv">
<div class="col-sm-2" >
    <div class="left-bar">
        <nav class="navbar navbar-default navbar-fixed-side">

            <div class="container">
                <div class="left-text">

                    <div class="text-badge module">
                        <div class="side-btn">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                            <span class="badge ">@if(sizeof($com)>0) {{  sizeof($com)}} @endif</span>
                        </div>

                        <div id = "first-colum" class="title-comment comment-title">Commentaires</div><br>

                    </div>
                </div>
                <div class="lft-con">
                    <ul class="drop-active">
                        @if(isset($com) && !empty($com))
                        @foreach($com as $comment_data)
                            <li >

                                <?php
                                 $id = explode(":",$comment_data['category_id']);
//                                $category_name = $category->getCategoryByID($id[0]);
                                ?>

{{--                                    <a href="{{url('details/'.$comment_data->category_id.'/'.$comment_data->category_id.'/#comments_'.$comment_data->id)}}" >--}}
                                    <a href="{{url('details/'.$id[0].'/'.$comment_data['category_id'].'/#comments_'.$comment_data['id'])}}" >
                                    <div class="md-title">
                                        <span>{{$comment_data['categryName']}}</span>
                                    </div>
                                    <div class="orange-cir">
                                        <span><i class="fa fa-circle" aria-hidden="true"></i>
                                                {{$comment_data['comment']}}</span>
                                    </div>

                                    <div class="md-replay">
                                        {{--<span>{!! (strftime('%e %B %Y' , strtotime ($comment_data->created_at->format('d-M-Y'))) )  !!} </span>--}}
                                        <span>@if(!empty($comment_data['userName'])){{$comment_data['userName']}}@endif</span>
                                    </div>


                                </a>
                            </li>
                            @endforeach
                      @endif

                    </ul>
                </div>
                <div class="left-text">
                    <div class="text-badge module ">
                        <div class="side-btn">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                        </div>

                        <div id = "second-colum" class="title-comment comment-title">Modules les plus consultés</div><br>

                    </div>
                </div>

                <div class="lft-con">
                    <ul class="drop-active most_accessed_module">
                        <?php
                            //$moduleData = \App\Models\getMostAccessedModule::orderBy('counter','desc')->take(3)->get();
                            $moduleData =$getMostAccssedModule->getMostViewedModule($categoryArray);
                        ?>
                        @foreach($moduleData as $module)
                            <?php $link =   \App\Helpers\LaraHelpers::setHrefforLinkRightBar($module); ?>
                        @endforeach
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="left-text">
                    <div class="text-badge module">
                        <div class="side-btn">
                            <i class="fa fa-bell-o" aria-hidden="true" ></i>
                        </div>

                        <div id = "third-colum" class="title-comment comment-title">Dernières mises à jour</div><br>

                    </div>
                </div>
                <div class="lft-con">
                    <ul class="drop-active most_accessed_module">
                        <?php

                        if(count($modules) > 0){
                        ?>
                        <?php //$modules = \App\Models\Module::where('deleted_at',NULL)->orderBy('updated_at','desc')->where('status',0)->take(3)->get(); ?>
                        @foreach($modules as $module)
                            <?php
                                $category_name = $module_category->getCategoryByModuleId($module->module_id); ?>
                            @if(sizeof($category_name) > 0)
                            <?php
                                    $cat_id =  $category_name[0]['category_id'];
                                    $cat_data = $category->getActiveCategoryByID($cat_id);
                                    $url = '';
                                    $sub_url = '';
                                   ?>
                                    @if($cat_data['parent_id'])
                                    <?php $par_cat_id = \App\Models\Category::select('parent_id')->where('id',$cat_id)->get()->toArray();?>

                                        @if(sizeof($par_cat_id))
                                            <?php   $main_category = $par_cat_id[0]['parent_id']; ?>
                                        @if($par_cat_id[0]['parent_id']!= 0)
                                            <?php
                                            $parent_id = \App\Models\Category::select('parent_id')->where('id',$par_cat_id[0]['parent_id'])->get()->toArray();
                                            if(count($parent_id) > 0 &&  $parent_id[0]['parent_id']!= 0) {
                                                $main_category = $parent_id[0]['parent_id'];
                                                $mod_id= \App\Models\Category::select('id')->where('parent_id',$parent_id)->get()->toArray();
                                                ?>
                                                @foreach($mod_id as $mid)
                                                    <?php  $url .= ':'.$mid['id'];?>
                                                    <?php $sub_id = \App\Models\Category::select('id')->where('parent_id',$mid['id'])->get()->toArray();?>
                                                    @foreach($sub_id as $s)
                                                        <?php $sub_url .=  ':'.$s['id'];?>
                                                    @endforeach
                                                @endforeach
                                            <?php }else{
                                                    $mod_id= \App\Models\Category::select('id')->where('parent_id',$main_category)->where('category_type_id',2)->get()->toArray();?>
                                                @foreach($mod_id as $mid)
                                                    <?php  $url .= ':'.$mid['id'];?>
                                                    <?php $sub_id = \App\Models\Category::select('id')->where('parent_id',$mid['id'])->where('category_type_id',2)->get()->toArray();?>
                                                    @foreach($sub_id as $s)
                                                        <?php $sub_url .=  ':'.$s['id'];?>
                                                    @endforeach
                                                @endforeach
                                            <?php    }?>
                                        @endif
                                        @endif
                                    @else
                                        <?php
                                        $main_category = $cat_id;
                                        $module_id = \App\Models\Category::select('id')->where('parent_id',$main_category)->get()->toArray();
                                        // we need to get parent cat of module+id
                                        ?>
                                        @if(count($module_id))
                                            @foreach($module_id as $mid)
                                                <?php  $url .= ':'.$mid['id'];?>
                                                <?php $sub_id = \App\Models\Category::select('id')->where('parent_id',$mid['id'])->get()->toArray();?>
                                                @foreach($sub_id as $s)
                                                    <?php $sub_url .=  ':'.$s['id'];?>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endif

                                    @if(sizeof($category_name))
                                        <li>
                                            <div class="orange-cir">
                                            <span> <i class="fa fa-newspaper-o iconsize" aria-hidden="true" ></i>
                                                <a class="clickCount scroll-module" data-id="<?php echo $cat_id ;?>" id="{{$module->module_id}}" href="javascript:void(0)" data-link="{{ url('details/'.$main_category.'/'.$main_category.$url.$sub_url .'#subcategory_'.$module->module_id) }}">
                                                <?php echo $module->title; ?>

                                                </a>
                                            </span>
                                            </div>
                                        </li>
                                    @endif
                            @endif
                        @endforeach
                        <?php  } ?>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </nav>
    </div>
</div>
</div>
@push('script')
<script>
    $(window).scroll(function(){

        //var left_content_height = $(".left-bar .navbar-fixed-side").height() + parseInt('60');
        //var scroll_height = $(".left-bar .navbar-fixed-side").height() + $(window).scrollTop();
        if($(window).width() > 991){

            //if(left_content_height > scroll_height){
            var height = $(window).scrollTop() - parseInt('75');
            if($(window).scrollTop() > 75){
                $(".left-bar .navbar-fixed-side").closest("div.col-sm-2").animate({marginTop:height},0);
            }else{
                $(".left-bar .navbar-fixed-side").closest("div.col-sm-2").animate({marginTop:0},0);
            }


        }
    });
$(document).ready(function() {

    $(document).on('click', ".clickCount", function () {
        var module_id = $(this).attr('id');
        var url = app.config.SITE_PATH + '/count_accessby_user';
        $.ajax({

            data: {id: module_id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {
                console.log(data);

            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });
    });

    $(".scroll-module").click(function() {
        var href = $(this).attr('data-link');
        console.log(href);
        var id = $(this).attr('id');
        var parentId = $(this).attr('data-id');
        var target = $('html').find('#subcategory_'+id);

        console.log(target);
        window.location.href = href;

    });
});
</script>
@endpush
