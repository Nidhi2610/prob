@inject('userComments', '\App\Models\Comments')
@inject('userList', '\App\Models\User')
@inject('entreprise', '\App\Models\Entreprise')
@inject('category', 'App\Models\Category')
@inject('module_category', 'App\Models\ModuleCategory')
@inject('getMostAccssedModule', 'App\Models\getMostAccessedModule')
<?php $colloborateurCount = $userList->init(); ?>
<?php $communityCount = $userList->getCommunityMember();?>
<?php $entrepriseCount = $entreprise->getEntrepriseCount();?>
<?php $comments = $userComments->getRecentComments();  ?>
<?php $parententreprise = $entreprise->getParentEntrepise();  ?>
<div class="col-sm-2 col-lg-1">
    {{--<div id="side-nav-bar">--}}
    <nav class="navbar navbar-default navbar-fixed-side sidebar-head" id="header-nav">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <div class="col-md-3">
                        <button class="navbar-toggle navbar-btn-bg" data-target=".navbar-collapse" data-toggle="collapse">
                            <span>MENU</span>
                        </button>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <a class="navbar-brand" href="/index">
                                @if(!empty(Auth::user()->Entreprise->logo))
                                    <img class="title_logo" src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ Auth::user()->Entreprise->logo }}"/>
                                @else
                                    <img src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                                @endif
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dropdown responcive-toggle">
                            <li class="dropdown">
                                {{--<span class="badge">@if(!empty($comments)) {{$comments->count()}} @endif</span>--}}
                                <a href="#" class="dropdown-toggle dropbtn" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                    {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ Auth::user()->first_name }} </span><span class="caret"></span>--}}
                                </a>

                                <ul class="dropdown-menu drop-active dropdown-content" role="menu">
                                    <div class="text-badge module">
                                        <div class="side-btn">
                                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                                            <span class="badge ">@if(!empty($comments)) {{$comments->count()}} @endif</span>
                                        </div>

                                        <div id = "first-colum" class="title-comment comment-title">Commentaires</div><br>

                                    </div>
                                    @if(isset($comments) && !empty($comments))
                                        @foreach($comments as $comment_data)
                                            <a href="{{url('details/'.$comment_data->category_id.'/'.$comment_data->category_id.'/#comments_'.$comment_data->id)}}">
                                                <li>
                                                    <div>
                                                        <span>{{$comment_data->user->name}}</span>
                                                    </div>
                                                    <div class="orange-cir">
                                                                <span><i class="fa fa-circle" aria-hidden="true"></i>
                                                                    {{$comment_data->comment}}</span>
                                                    </div>
                                                    {{--<div>
                                                        <span>Barbara Denis</span>
                                                    </div>--}}
                                                    <div>
                                                        <span>{!! utf8_encode(strftime('%e %B %Y' , strtotime ($comment_data->created_at->format('d-M-Y'))) )  !!}</span>
                                                    </div>
                                                </li>
                                            </a>
                                        @endforeach
                                    @endif

                                        <div class="left-text">
                                            <div class="text-badge module ">
                                                <div class="side-btn">
                                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                                </div>

                                                <div id = "second-colum" class="title-comment comment-title">Modules les plus consultés</div><br>

                                            </div>
                                        </div>
                                        <ul class=" most_accessed_module " role="menu">
                                            <?php $modules = \App\Models\getMostAccessedModule::orderBy('counter','desc')->take(3)->get();?>
                                                @if(count($modules) && isset($modules))
                                                    @foreach($modules as $module)
                                                    <?php $link =   \App\Helpers\LaraHelpers::setHrefforLink( $module); ?>
                                                     @endforeach
                                                @endif
                                        </ul>

                                        <div class="left-text">
                                            <div class="text-badge module">
                                                <div class="side-btn">
                                                    <i class="fa fa-bell-o" aria-hidden="true" ></i>
                                                </div>

                                                <div id = "third-colum" class="title-comment comment-title">Dernières mises à jour</div><br>

                                            </div>
                                        </div>
                                    <ul class="drop-active most_accessed_module ">
                                    <?php $modules = \App\Models\Module::where('deleted_at',NULL)->orderBy('updated_at','desc')->where('status',0)->take(3)->get(); ?>
                                    @foreach($modules as $module)
                                        <?php $category_name = $module_category->getCategoryByModuleId($module['id']);  ?>
                                        @if(sizeof($category_name) > 0)
                                            <?php   $cat_id =  $category_name[0]['category_id'];
                                            $cat_data = $category->getActiveCategoryByID($cat_id);
                                            $url = '';
                                            $sub_url = '';?>
                                            <!--                                if $cat_data['parent_id'] is 0 then it is parent category-->
                                                @if($cat_data['parent_id'])
                                                    <?php $par_cat_id = \App\Models\Category::select('parent_id')->where('id',$cat_id)->get()->toArray();?>
                                                    @if(count($par_cat_id))
                                                        <?php $main_category = $par_cat_id[0]['parent_id'];
                                                        $mod_id= \App\Models\Category::select('id')->where('parent_id',$par_cat_id)->get()->toArray();

                                                        ?>

                                                        @foreach($mod_id as $mid)
                                                            <?php  $url .= ':'.$mid['id'];
                                                            $sub_id = \App\Models\Category::select('id')->where('parent_id',$mid['id'])->get()->toArray(); ?>
                                                            @foreach($sub_id as $s)
                                                                <?php $sub_url .=  ':'.$s['id'];?>
                                                            @endforeach
                                                        @endforeach
                                                    @endif
                                                @else
                                                    <?php $main_category = $cat_id;?>
                                                @endif

                                                @foreach($category_name as $c)
                                                    <?php    $c_id = $c->category_id; ?>
                                                @endforeach
                                                @if(sizeof($category_name))
                                                    <li>
                                                        <div class="orange-cir">
                                                <span> <i class="fa fa-newspaper-o iconsize" aria-hidden="true" ></i>
                                                    <a class="clickCount scroll-module" data-id="<?php echo $cat_id ;?>" id="{{$module['id']}}" href="javascript:void(0)" data-link="{{ url('details/'.$main_category.'/'.$main_category.$url.$sub_url .'#subcategory_'.$module['id']) }}">
                                                    {{--<a class="clickCount" id="{{$module['id']}}" href="{{ url('details/'.$module['id'].'/'.$c_id .'#category_'.$module['id']) }}">--}}
                                                        <?php echo $module['title']; ?>

                                                    </a>
                                                </span>
                                                        </div>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </ul>


                            </li>


                        </div>
                    </div>

                </div>
            </div>
            <div class="collapse navbar-collapse nal-image">
                <ul class="nav navbar-nav">

                        {{--<li class="{{$index or ""}} scroll-class">--}}
                        <li class="{{$index or ""}}">
                            <a href="{{url('/index')}}" data-toggle="tooltip"  title=""><p class="nav-items-a"></p>
                                <span>Mon offre de formation</span></a>
                        </li>

                    @if(Auth::user()->user_type == "3")
                    @if($parententreprise)
                        <li class="{{$userEntreprise  or ""}} ">
                        <a href="{{url('user_entreprise')}}"><p class="nav-items-a"></p>

                            @if($entrepriseCount != 0)
                                <span class="counter badge">{{$entrepriseCount}}</span>
                            @endif
                            <span>Gestion groupe </span></a>
                    </li>
                    @endif
                    @endif
                    @if(Auth::user()->user_type == "3")
                    <li class="{{$userlist or ""}}">
                        <a href="{{url('userlist')}}"><p class="nav-items-a"></p>

                            @if($colloborateurCount->count() != 0)
                                <span class="counter badge">{{$colloborateurCount->count()}}</span>
                            @endif
                            <span>Mon équipe</span></a>
                    </li>
                    @endif
                    <li class="{{$communtiyTab or ""}}">
                        <a href="{{url('user/community')}}"><p class="nav-items-a"></p>
                            @if($communityCount->count() != 0)
                                <span class="counter badge">{{$communityCount->count()}}</span>
                            @endif
                            <span>Ma communauté</span></a>
                    </li>
                    {{--<li class="{{$setting or ""}}">
                        <a href="javascript:void(0);"><p style="font-family:btp_icons;font-size: 25px;margin-bottom: -10px;color:#ffffff;"></p><br>
                            <span>Les challenges</span></a>

                    </li>--}}

                    <li class="dropdown customtoggle">
                        <a href="javascript:void(0);"><p class="nav-items-a"></p>
                            <span>Paramètres</span></a>
                        <ul class="dropdown-menu ">

                            <li>
                            <a href="{{ route('userProfile') }}">
                                <i class="fa fa-user-o"> </i>   Mon Profil
                            </a>
                            </li>
                            <li>
                            <a href="{{ route('changePassword') }}">
                                <i class="fa fa-exchange"> </i>   Mot de passe
                            </a>
                            </li>
                            <li>
                                <a href="{{ route('probtp_logout') }}">
                                    <i  class="fa fa-sign-out"> </i>   {{ __('probtp.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('probtp_logout') }}" method="POST" class="dsply-non">
                                {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>

                    </li>

                    <li class="logo-bottom">
                        <a href="javascript:void(0);">
                            <img src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
   {{-- </div>--}}
</div>

<div class="col-sm-10 col-lg-11 col-header-nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-header-nopadding header-bottom">
            <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="/index">


                        <?php $id = Auth::user()->entreprise_id ;
                            $entreprises = \App\Models\Entreprise::where('id', $id)->where('parent_id',0)->get();  ?>

                            @if(sizeof($entreprises)==0)
                                 <img class="title_logo" src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                            @else
                                @foreach($entreprises as $entreprise)
                                    @if( !empty($entreprise) )
                                         <img class="title_logo" src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ $entreprise['logo'] }}"/>

                                    @else
                                        <img class="title_logo" src="{{ url('public/front1/images/logo_probtp.jpg')}}"/>
                                        @endif
                                @endforeach
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-md-5">
                    <ul class="nav navbar-nav">
                        <input type="text" id="search_box" name="search" placeholder="Rechercher un sujet, un mot clé..." >
                    </ul>
                </div>
                <div class="col-md-4">

                    <ul class="nav navbar-nav navbar-right head">


                        <li class="dropdown" id="header-notification">
                            <span class="badge"></span>
                            <a href="#" class="dropdown-toggle dropbtn" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-bell-o" aria-hidden="true"></i>
                                <span style="font-family: Raleway,sans-serif;padding-left: 5px">   </span><span class="caret"></span>
                            </a>
                        </li>

                    </ul>
                    <div class="user-profile ">

                           @if(!empty(Auth::user()->profile_image))
                                    <img src="{{ URL::to('/') }}/storage/app/public/files/module/{{ Auth::user()->profile_image }}"
                                          class="profile-image"    />
                                @else
                                    <i class="fa fa-user-circle-o fa-2x"></i>
                                @endif
                               <span>Bonjour</span>
                                <span>  {{ Auth::user()->first_name }}  {{ Auth::user()->last_name }} </span>
                                    {{--<span class="caret"></span>--}}

                            {{--<ul class="dropdown-menu" role="menu">--}}

                                {{--<li>--}}
                                    {{--<a href="{{ route('userProfile') }}">--}}
                                        {{--<i  class="fa fa-user-o"> </i>      {{ __('probtp.myaccount', ['module' =>'']) }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li>--}}
                                    {{--<a href="{{ route('changePassword') }}">--}}
                                        {{--<i  class="fa fa-exchange"> </i>   {{ __('probtp.password_change') }}--}}
                                    {{--</a>--}}
                                {{--</li>--}}

                                {{--<li>--}}
                                    {{--<a href="{{ route('probtp_logout') }}">--}}
                                        {{--<i  class="fa fa-sign-out"> </i>   {{ __('probtp.logout') }}--}}
                                    {{--</a>--}}

                                    {{--<form id="logout-form" action="{{ route('probtp_logout') }}" method="POST" class="dsply-non">--}}
                                        {{--{{ csrf_field() }}--}}
                                    {{--</form>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                       </div>

                </div>


            </div>






        </div>
    </nav>
            </div>
         </div>
    </div>

@push('script')
<script type="text/javascript">
        $(document).ready(function(){
            // Show hide popover
            $(document).on('click',".dropdown",function(){
                $(this).find(".dropdown-menu").slideDown("fast");
            });
            $("#search_box").keyup(function(event){
                if(event.keyCode == 13){
                    var search_keyword = $('#search_box').val();
                    if(search_keyword == "" || search_keyword == null){
                        alert('Entrez le mot-clé de recherche')
                    }else{
                        var replaced_keyword = search_keyword.split(' ').join('-');
                        window.location = app.config.SITE_PATH+'search/'+replaced_keyword;
                    }

                }
            });

            $(document).on('click','.col-header-nopadding' ,function(){
                $(".dropdown-menu").slideUp("fast");
            });

            $.fn.clickOff = function(callback, selfDestroy) {
                var clicked = false;
                var parent = this;
                var destroy = selfDestroy || true;

                parent.click(function() {
                    clicked = true;
                });

                $(document).click(function(event) {
                    if (!clicked) {
                        callback(parent, event);
                    }
                    if (destroy) {

                    };
                    clicked = false;
                });
            };

            $(".customtoggle").clickOff(function() {
                $(".dropdown-menu").slideUp("fast");
            });

            $(document).on('click', ".clickCount", function () {
                var module_id = $(this).attr('id');
                var url = app.config.SITE_PATH + '/count_accessby_user';
                $.ajax({

                    data: {id: module_id},
                    type: 'POST',
                    url: url,
                    headers: {
                        'X-CSRF-TOKEN': window.Probtp.csrfToken
                    },
                    success: function (data) {
                        console.log(data);

                    },
                    error: function (xhr, status, error) {
                        // check status && error
                        console.log(status);
                    }
                });
            });

            $(".scroll-class").click(function(e) {
                var child = $("li.scroll-class").find("a"); console.log(child);
                 var path = $(location).attr('href'); console.log(path);
                var subpath = path.indexOf('/details/');

               if(subpath > 0) {
//                   $(location).attr('title','Click Here to go the Modules and Resourses');
//                   $(".scroll-class").hover(function(e) { alert('tool');
//                       $('a[title]').tooltip();
//                   });

                   $('html,body').animate({
                           scrollTop: $("#showalldetail").offset().top
                       },
                       'slow');
                   e.preventDefault();
                   e.stopPropagation();
               }else{

               }
                });





//                var child = $("li.scroll-class").find("a"); console.log(child);
//                 var path = $(location).attr('href'); console.log(path);
//                var subpath = path.indexOf('/details/');
//
//               if(subpath > 0) {
//
//                  path = $(child).attr('href','#showalldetail');
//                  $(path).on('setpath',function(){
//                      path = $(child).attr('href','/index');
//                  })
//                  $('.scroll-class').trigger('setpath');
//               }


        });

</script>
@endpush