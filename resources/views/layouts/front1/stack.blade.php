@push('css')
    <link href="{{ asset('public/datatable/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/datatable/responsive.dataTables.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/sortable/jquery-ui.css') }}" rel="stylesheet">
	<link href="{{ asset('public/sortable/sortable.min.css') }}" rel="stylesheet">
	<link href="{{ asset('public/modalpopup/popupstyle.css') }}" rel="stylesheet">
@endpush

@push('script')
    <script src="{{ asset('public/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/datatable/dataTables.responsive.min.js') }}"></script>
    {!! $extra !!}

    <script>
        $(document).ready(function () {
            app.dataTable();
            app.checkAll();
            app.restore('{!! __('probtp.restore_message') !!}');
            app.duplicate('{!! __('probtp.duplicate_message') !!}');
        });
    </script>
	
	
@endpush