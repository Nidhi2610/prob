<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
{{--    <meta name="viewport" content="width=device-width, initial-scale=1">--}}
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!--link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet"-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/mask/jquery.loadmask.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href=" {{ asset('public/css/plugins/amchart/export.css') }}" rel="stylesheet" type="text/css" />
<link href=" {{ asset('public/css/plugins/jquery-ui-1.12.1/jquery-ui.css') }}" rel="stylesheet" type="text/css" />
<link href=" {{ asset('public/css/plugins/jquery-ui-1.12.1/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />

    @stack('css')
   
    <!-- Scripts -->
    <script>
        var csrf_token = "<?php echo csrf_token(); ?>";
        window.Probtp = {!! json_encode([
            'csrfToken' => csrf_token(),
            'baseUrl' =>  url('/') . "/"
        ]) !!};

    </script>
</head>
<body>
    <div id="app">
        @include('layouts.navigation')

        <div class="col-md-12">
            @if(Session::has('fail'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('fail') }} </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session::get('success') }} </div>
            @endif
        </div>
        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="{{ asset('public/js/jquery-3.3.1.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/html5lightbox/html5lightbox.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('public/mask/jquery.loadmask.min.js') }}" ></script>
    <script src="{{ asset('public/js/jquery-ui-1.12.1/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/js/jquery-ui-1.12.1/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/js/amcharts/amcharts.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/js/amcharts/themes/light.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/js/amcharts/serial.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/js/amcharts/pie.js')}}" type="text/javascript"></script>
    <script src="{{ asset('public/js/amcharts/plugins/responsive/responsive.js')}}" type="text/javascript"></script>
{{--    <script src="{{ asset('public/js/amchart/responsive.min.js.map')}}" type="text/javascript"></script>--}}
    {{--<script src="{{ asset('public/js/amchart/export.min.js')}}" type="text/javascript"></script>--}}
    <script>

        /*
         *   this script is for manage the logout of timeout
         *   if user is inactive for 15 min
         *   he will be logout : Nidhi
         *
         * */
        var logout = '{{ __("probtp.Are you sure to logout") }}';
        var timeout; console.log(logout);
        var url = app.config.SITE_PATH+'/logout';
        document.onmousemove = function(){
            clearTimeout(timeout);
                timeout = setTimeout(function () {
                    var confirmstatus = confirm( logout );
                    if(confirmstatus === true) {

                        var redirect = $.ajax({
                            cache: false,
                            url: url,
                            type: "GET",
                            headers: {
                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                            },
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                window.location.href = url;
                            }
                        });
                    }
                }, 60000*15);
            };
    </script>
    @stack('script')
    <div class="col-md-1"> &nbsp; </div>
    <div class="col-md-10 text-center" id="footer_text">
        <a href="http://www.mediadrive.fr" target="_blank"> Création de projet web</a> :
        <a href="http://www.mediadrive.fr" target="_blank"> Media Drive Paris</a>
    </div>
    <div class="col-md-1"> &nbsp; </div>
</body>
</html>
