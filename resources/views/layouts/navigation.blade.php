<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Probtp') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                   {{-- <li><a href="{{ route('login') }}">{{ __('probtp.login') }}</a></li>--}}
                @else

                    @if(Auth::user()->user_type == 1|| Auth::user()->user_type == 2|| Auth::user()->user_type == 3)
                    <li class="dropdown">
                        <a class="fa fa-users" href="{{ route('userIndex') }}">
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px"> {{ __('probtp.user_management', ['module' =>'']) }} </span>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle fa fa-pie-chart" href="{{ route('getStatistics') }}" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px"> {{ __('probtp.statistiques') }}</span><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('getPieChart') }}">
                                    <i style="padding-right: 5px" class="fa fa-pie-chart"></i>   {{ __('probtp.total_user') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getEntreprise') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.assigned_entreprise', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getUserCommentData') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.CommentsbyUSer', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('gettimespentByuser') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.totalTimeSpentOnSystem', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getMostcompletedModule') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.mostCompletedModuleDiv', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getMostAccessedModules') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.mostAccessedModuleDiv', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getCommentsbyCategoryData') }}">
                                    <i style="padding-right: 5px" class="fa fa-bar-chart"></i>   {{ __('probtp.commentsonModulesDiv', ['module' =>'']) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getRecentlyActiveUser') }}">
                                    <i style="padding-right: 5px" class="fa fa-table"></i>   {{ __('probtp.mostRecentLoginuser', ['module' =>'']) }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->user_type == 3)
                        <li class="dropdown">
                        <a href="{{ route('responsableCategoryIndex') }}">
                            {{ __('probtp.category_manager') }}
                        </a>
                    @endif

                @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 2 ||Auth::user()->user_type == 5 )
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle fa fa-etsy" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ __('probtp.group_management')}}</span> <span class="caret"></span>--}}
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px">Entreprises</span> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('entrepriseIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-list-ul"></i>   {{ __('probtp.module_list', ['module' =>'']) }}
                                </a>
                            </li>
                            @if(Auth::user()->user_type == 1 ||Auth::user()->user_type == 2||Auth::user()->user_type == 5 )
                                <li>
                                    <a href="{{ route('entrepriseAdd') }}">
                                        <i style="padding-right: 5px" class="fa fa-plus"></i>    {{ __('probtp.module_add', ['module' =>'']) }}
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif

                @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 2)
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle fa fa-cogs" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px">Interfaces</span> <span class="caret"></span>
                            {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">{{ __('probtp.master_management') }}</span> <span class="caret"></span>--}}
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{ url('category/F') }}">
                                    <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{ __('probtp.category_manager') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('category/R') }}">
                                    <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{ __('probtp.resource_manager') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('comment') }}">
                                    <i style="padding-right: 5px" class="fa fa-sticky-note"></i>    Gestion des commentaires
                                </a>
                            </li>


                            <li>
                                <a href="{{ route('emailTemplateIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-envelope-open"></i>     {{ __('probtp.email_template') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('getContent') }}">
                                    <i style="padding-right: 5px" class="fa fa-bookmark"></i>    {{ __('probtp.content', ['module' =>'']) }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle fa fa-th-list" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span style="font-family: Raleway,sans-serif;padding-left: 5px">Modules</span> <span class="caret"></span>
                                {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px"> {{ __('probtp.modules_manager') }}</span> <span class="caret"></span>--}}
                            </a>

                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a class="test"  href="#">Formations<span class="caret"></span></a>
                                    <ul class="dropdown-menu" style=" top: 0;left: 100%;margin-top: -1px; min-width:185px;">
                                        <li><a href="{{ url('module/F') }}">
                                                <i style="padding-right: 5px" class="fa fa-list-ul"></i>   {{ __('probtp.module_list', ['module' =>'']) }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('module/create/0/F')}}">
                                                <i style="padding-right: 5px" class="fa fa-plus"></i>    {{ __('probtp.module_add', ['module' =>'']) }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="dropdown-submenu">
                                    <a class="test" href="#">Ressources <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style=" top: 0;left: 100%;margin-top: -1px;min-width:185px">
                                        <li><a href="{{ url('module/R') }}">
                                                <i style="padding-right: 5px" class="fa fa-list-ul"></i>   {{ __('probtp.module_list', ['module' =>'']) }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{url('module/create/0/R')}}">
                                                <i style="padding-right: 5px" class="fa fa-plus"></i>    {{ __('probtp.module_add', ['module' =>'']) }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>

                        </li>

                        @if(Auth::user()->user_type == 5)
                            <li class="dropdown ">
                                <a href="#" class="dropdown-toggle fa fa-cogs" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <span style="font-family: Raleway,sans-serif;padding-left: 5px">Interfaces</span> <span class="caret"></span>
                                    {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">{{ __('probtp.master_management') }}</span> <span class="caret"></span>--}}
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                        <li>
                                        <a href="{{ url('category/F') }}">
                                            <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{ __('probtp.category_manager') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('category/R') }}">
                                            <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{ __('probtp.resource_manager') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 2 ||Auth::user()->user_type == 5 )
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle fa fa-snowflake-o" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{--<span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ __('probtp.group_management')}}</span> <span class="caret"></span>--}}
                                <span style="font-family: Raleway,sans-serif;padding-left: 5px">Quizzs</span> <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('quizIndex') }}" role="button" aria-expanded="false">
                                     <i style="padding-right: 5px" class="fa fa-graduation-cap"></i>   {{ __('probtp.quiz_managment') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('formIndex') }}">
                                        <i style="padding-right: 5px" class="fa fa-wpforms"></i>    {{ __('probtp.form_management') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif



                    @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 2)
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle fa fa-trash" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px">Corbeilles</span> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('gettrashdata/F') }}">
                                    <i style="padding-right: 5px" class="fa fa-list-ul"></i>    Formations Module
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('gettrashdata/R') }}">
                                    <i style="padding-right: 5px" class="fa fa-list-ul"></i>    Ressources Module
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('category/gettrashdata/F') }}">
                                    <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{__('probtp.category') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('category/gettrashdata/R') }}">
                                    <i style="padding-right: 5px" class="fa fa-cubes"></i>    {{__('probtp.resources') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('userTrashIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-user"></i>     {{ __('probtp.user') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('entrepriseTrashIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-etsy"></i>      {{ __('probtp.entreprise') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('formTrashIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-wpforms"></i>   {{ __('probtp.form') }}
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('quizTrashIndex') }}">
                                    <i style="padding-right: 5px" class="fa fa-graduation-cap"> </i> {{ __('probtp.quiz') }}
                                </a>
                            </li>

                        </ul>


                    </li>
                    @endif

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle fa fa-user-circle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span style="font-family: Raleway,sans-serif;padding-left: 5px">  {{ Auth::user()->first_name }} </span><span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            
							<li>
								<a href="{{ route('userProfile') }}">
                                    <i style="padding-right: 5px" class="fa fa-user-o"> </i>      {{ __('probtp.myaccount', ['module' =>'']) }}
                                </a>
                            </li>


							<li>
                                <a href="{{ route('changePassword') }}">
                                        <i style="padding-right: 5px" class="fa fa-exchange"> </i>   {{ __('probtp.password_change') }}
                                </a>
                            </li>
							<li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                     <i style="padding-right: 5px" class="fa fa-sign-out"> </i>   {{ __('probtp.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@push('script')

<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.test').on("click", function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).parents('ul.dropdown-menu').find('ul.dropdown-menu').not($(this).next('ul')).hide();
            $(this).next('ul').toggle();
        });

//        console.log(window.location.href);


    });

</script>

@endpush
@push('css')
<style>

    .dropdown-submenu .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: 10px;
        min-width: 185px;
    }
    #app-navbar-collapse ul li ul li{
        position: relative !important;
    }

</style>
@endpush