<?php $i = 0;
//print_r($status);
?>
<style>
    .fa-hand-o-right {
        font-size: 31px;
        padding-left: 20px;
        color: #337ab7;
        padding-top: 6px;
    }
</style>
@foreach($moduleid_data as $categoryData)
    <?php
    if(isset($categoryData['category_id']))   {
    $collapse_class = "";
    if ($cat_id == $categoryData['category_id']) {
        $collapse_class = "in";
    }
    ?>
    <div class="row">
        <?php  if ($i == 0) {
            $col_md = 'col-md-10';
        } else {
            $col_md = "";
        } ?>
        <?php if($col_md == ""){    ?>
        <div class="category_title {{$col_md}}">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
               href="#collapse{{$categoryData['category_id']}}" aria-expanded="true">
                <?php
                $categoryName = \App\Helpers\LaraHelpers::headingTitle($categoryData['category_id']); ?>
            </a>
        </div>
        <?php  }else{?>
        <div class="category_title {{$col_md}}">
            <a class="titletag" id="{!! $categoryData['category_id'] !!}">
                <?php
                $categoryName = \App\Helpers\LaraHelpers::headingTitle($categoryData['category_id']);  ?>
            </a>
        </div>
        <?php } ?>


        @if($i == 0)
            <div class="col-md-2">
            <?php
            $categoryLikeStatus = \App\Helpers\LaraHelpers::likeStatus($categoryData['category_id']);
            $totalLike = \App\Helpers\LaraHelpers::likeCount($categoryData['category_id']);
            if ($categoryLikeStatus == 1) {
                $likeColor = '#337ab7';
            } else {
                $likeColor = 'grey';
            }
            ?>
            <a href="javascript:void(0);" class="like_category" id="{{$categoryData['category_id']}}"
               style="color:{{$likeColor}}">
                <i class="fa fa-thumbs-up p-right"></i><span
                        class="like_counter">{{$totalLike}}</span>
            </a>
            </div>
            <nav class="module-detail-nav">
                <div id="category_name"class="category_name"></div>
                <div class="steps">
                    <div class="row">
                        @if(count($parent_category_data))
                            <div class="col-md-1">
                                <i class="fa fa-hand-o-right"></i>
                                {{-- <a href="javascript:void(0);">
                             <span class="fa-stack fa-2x" aria-hidden="true">
                                 <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                             </span>
                                 </a>--}}
                            </div>
                        @endif
                        <div class="col-md-10 stepline">
                            <ul>

                                <?php
                                $width = "";
                                if (count($parent_category_data)) {
                                    $width = 95 / count($parent_category_data);
                                    /*if(count($parent_category_data) == 1 || count($parent_category_data) == 2){
                                      $margin = "-21px";
                                    }
                                    else{
                                        $margin = "-10px";
                                    }*/
                                }
                                ?>

                                @foreach($parent_category_data as $child_category)
                                    <a href="{{url('details/'.$cat_id.'/'.$category_id.'#category_'.$child_category->id)}}">
                                        <li class="childli" style="width:{{$width}}%;" onmouseleave="leave()"
                                            onmouseover='hover("{{ $child_category->name}}")'
                                            id="{{$child_category->id}}"></li>
                                    </a>
                                @endforeach
                            </ul>
                        </div>
                        @if(count($parent_category_data))
                            <div class="col-md-1">
                                {{--<div class="col-md-1" style="margin-left: {{$margin}};">--}}
                                {{--<a href="javascript:void(0);">
                                    <span class="fa-stack fa-2x right" aria-hidden="true">
                                        <i class="fa fa-flag fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>--}}
                            </div>
                        @endif

                    </div>
                </div>

            </nav>

            <?php $i = 1; ?>
        @endif
        @if(!empty($categoryData['description']))
            <div class="resume">
                {{$categoryData['description']}}
            </div>
        @endif
        <?php $array = explode(" ", $categoryData['category_name']); ?>
        @if(ctype_digit($array[0]))
            <div id="collapse{{$categoryData['category_id']}}" class="collapse out">
                @else
                    <div id="collapse{{$categoryData['category_id']}}" class="collapse {{$collapse_class}}">
                        @endif

                        @foreach($categoryData['moduleData'] as $module)
                            <div class="content">

                                <div class="col-md-12 module_data">
                                    <div class="panel panel-default">

                                        <div class="panel-heading" id="module_title">
                                            <h2>{{$module->Module->title}}</h2>
                                        </div>

                                        <div class="panel-body">

                                            <div class="col-sm-12 col-header-nopadding">
                                                <div class="attachement">

                                                    @if (pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'mp4' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MOV' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'AVI' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'wmv'|| pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MP4')
                                                        <video class="video-js" controls preload="auto" width="80%"
                                                               height=""
                                                               data-setup="{}">
                                                            <source src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                                                    type='video/mp4'>
                                                        </video>

                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'txt')
                                                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                            <i class="fa fa-btn fa-file-text fa-4x"></i>
                                                        </a>
                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpg' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) == 'png' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpeg' )
                                                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                            <img src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                                                 width="80%"/>
                                                        </a>
                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pdf')
                                                        {{-- <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                             <i class="fa fa-btn fa-file-pdf-o fa-2x"></i>
                                                         </a>--}}
                                                        <iframe id="fred" class="fred"
                                                                title="PDF in an i-Frame"
                                                                src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                                                frameborder="1" scrolling="auto" height="600"
                                                                width="80%"></iframe>
                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'doc'||pathinfo($module->file_name, PATHINFO_EXTENSION) === 'docx')
                                                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                            <i class="fa fa-file-word-o fa-4x "></i>
                                                        </a>
                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pptx')
                                                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                            <i class="fa fa-file-powerpoint-o fa-4x "></i>
                                                        </a>
                                                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'xlsx')
                                                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                                            <i class="fa fa-file-excel-o fa-4x "></i>
                                                        </a>

                                                    @endif

                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="summary">
                                                    {!! $module->Module->summary !!}
                                                </div>
                                            </div>
                                            <!-------button is cretaed here.--->
                                            <div class="col-md-12">
                                                <div class="panel-default">

                                                    <div class="row">
                                                        <form id="module_submit_form{{$module->id}}" role="form"
                                                              method="POST" action=""
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <input type="hidden" name="module_id"
                                                                       id="form_module_id"
                                                                       value="{{$module->id}}"/>
                                                                <input type="hidden" name="user_id"
                                                                       id="form_user_id"
                                                                       value="{{Auth::user()->id}}"/>
                                                                <div class="col-md-9"></div>
                                                                <div class="col-md-3 ">
                                                                    <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                                    @if(count($status) > 0)
                                                                        <button type="submit"
                                                                                id="module_submit_button_{{$module->id}}"
                                                                                class="btn btn-primary  disabled submit_module">
                                                                            Module déjà terminé
                                                                        </button>

                                                                    @else
                                                                        <button type="submit"
                                                                                id="module_submit_button_{{$module->id}}"
                                                                                class="btn btn-primary module_submit">
                                                                            Terminer ce module
                                                                        </button>
                                                                        {{--<p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p> --}}
                                                                    @endif

                                                                </div>
                                                                <br/>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            @push('script')
                                            <script>
                                                $(document).ready(function () {
                                                    var module_id = '{{$module->id}}';
                                                    $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        if (!$("#module_submit_button_" + module_id).valid()) {
                                                            return false;
                                                        }
                                                        var form_data = {
                                                            'module_id': module_id,
                                                        };

                                                        var path = app.config.SITE_PATH + 'add_module_count';

                                                        $.ajax({
                                                            data: form_data,
                                                            type: 'POST',
                                                            url: path,
                                                            headers: {
                                                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                                                            },
                                                            success: function (data) {
                                                                if (data.status == 1) {
                                                                    console.log(data);

                                                                    $('#module_submit_button_' + module_id).attr('disabled', true);

                                                                } else {
                                                                    $('#module_submit_button_' + module_id).attr('disabled', true);
                                                                    //$(".submit_successfully_"+ module_id).css('display','block');
                                                                    $('#module_submit_button_' + module_id).text('Module déjà terminé');

                                                                }
                                                            },
                                                            error: function (xhr, status, error) {
                                                                // check status && error
                                                                console.log(status);
                                                            }
                                                        });

                                                    });

                                                });
                                            </script>
                                            @endpush
                                            <div class="col-md-12">
                                                <div class="form_title">
                                                    <div class="form_content">
                                                        @if(!empty($module->form_data))
                                                            <?php $form_content = json_decode($module->form_data->content);?>
                                                            <div class="row">
                                                                <div class="col-md-12 containttestform">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading"><i
                                                                                    class="fa fa-tasks fa-lg fa-fw"
                                                                                    aria-hidden="true"></i>{{ $module->form_data->title }}
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            @if(count($module['form_user_data']) > 0)
                                                                                @foreach($module['form_user_data'] as $formData)

                                                                                    <div class="form-group">
                                                                                        <div class="finalanswers">
                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal">{{$formData->form_name}}</label>
                                                                                            <div class="col-md-6">
                                                                                                <label class="font-normal" >{{$formData->form_value}}</label><br>
                                                                                            </div>
                                                                                        </div>
                                                                                        <br></div>
                                                                                @endforeach

                                                                            @else
                                                                                <form class="form-horizontal"
                                                                                      id="dynamic_form_{{$module->id}}"
                                                                                      role="form"
                                                                                      method="POST"
                                                                                      action="javascript:void(0);"
                                                                                      enctype="multipart/form-data">
                                                                                    {{ csrf_field() }}
                                                                                    @foreach($form_content as $key => $row)
                                                                                        <div class="form-group">
                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal">{{$row->label}}</label>
                                                                                            <div class="col-md-6">
                                                                                                <input type="hidden"
                                                                                                       name="form_data[{{$key}}][name]"
                                                                                                       value="{{$row->label}}"/>
                                                                                                @if($row->type == "text")
                                                                                                    <input type="text"
                                                                                                           class="form-control required"
                                                                                                           name="form_data[{{$key}}][value]"
                                                                                                           value=""/>
                                                                                                @elseif($row->type == "textarea")
                                                                                                    <textarea
                                                                                                            name="form_data[{{$key}}][value]"
                                                                                                            class="form-control required"></textarea>
                                                                                                @elseif($row->type == "select")
                                                                                                    <select name="form_data[{{$key}}][value]"
                                                                                                            class="form-control required">
                                                                                                        @foreach($row->values as $select)
                                                                                                            <option value="{{$select->value}}">{{$select->label}}</option>
                                                                                                        @endforeach

                                                                                                    </select>
                                                                                                @elseif($row->type == "checkbox-group")
                                                                                                    <div class="input-group">
                                                                                                        @foreach($row->values as $check_box)
                                                                                                            <input type="checkbox"
                                                                                                                   class="required"
                                                                                                                   name="form_data[{{$key}}][value][]"
                                                                                                                   id="check_{{$check_box->value}}"
                                                                                                                   value="{{$check_box->value}}"/>
                                                                                                            <label class="font-normal"
                                                                                                                   for="check_{{$check_box->value}}">{{$check_box->label}}</label>
                                                                                                            <br/>
                                                                                                        @endforeach
                                                                                                    </div>
                                                                                                @elseif($row->type == "radio-group")
                                                                                                    <div class="input-group">
                                                                                                        @foreach($row->values as $radio_box)
                                                                                                            <input type="radio"
                                                                                                                   class="required"
                                                                                                                   name="form_data[{{$key}}][value]"
                                                                                                                   id="radio_{{$radio_box->value}}"
                                                                                                                   value="{{$radio_box->value}}"/>
                                                                                                            <label class="font-normal"
                                                                                                                   for="radio_{{$radio_box->value}}">{{$radio_box->label}}</label>
                                                                                                            <br/>
                                                                                                        @endforeach
                                                                                                    </div>
                                                                                                @else
                                                                                                    <input type="text"
                                                                                                           class="form-control required"
                                                                                                           name="form_data[{{$key}}][value]"
                                                                                                           value=""/>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                    <input type="hidden" name="form_id"
                                                                                           value="{{$module->form_data->form_id}}"/>
                                                                                    <input type="hidden"
                                                                                           name="category_id"
                                                                                           value="{{$category_id}}"/>
                                                                                    <input type="hidden" name="cat_id"
                                                                                           value="{{$cat_id}}"/>
                                                                                    <input type="hidden"
                                                                                           name="module_category_id"
                                                                                           value="{{$module->id}}"/>
                                                                                    <div class="form-group">
                                                                                        <div class="col-md-8 col-md-offset-4">
                                                                                            <button type="submit"
                                                                                                    id="dynamic_button_{{$module->id}}"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                Poster
                                                                                            </button>
                                                                                            &nbsp; &nbsp;
                                                                                            <button type="reset"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                {{ __('probtp.reset') }}
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <!-------button is cretaed here.--->
                                                                    <div class="panel-default">
                                                                        <div class="row">
                                                                            <form id="module_submit_form{{$module->id}}"
                                                                                  role="form"
                                                                                  method="POST" action=""
                                                                                  enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-group">
                                                                                    <input type="hidden"
                                                                                           name="module_id"
                                                                                           id="form_module_id"
                                                                                           value="{{$module->id}}"/>
                                                                                    <input type="hidden" name="user_id"
                                                                                           id="form_user_id"
                                                                                           value="{{Auth::user()->id}}"/>
                                                                                    <div class="col-md-9"></div>
                                                                                    <div class="col-md-3">
                                                                                        <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                                                        @if(count($status) > 0)
                                                                                            <button type="submit"
                                                                                                    id="module_submit_button_{{$module->id}}"
                                                                                                    class="btn btn-primary disabled  module_submit">
                                                                                                Module déjà terminé
                                                                                            </button>

                                                                                        @else
                                                                                            <button type="submit"
                                                                                                    id="module_submit_button_{{$module->id}}"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                Terminer ce module
                                                                                            </button>
                                                                                            {{---  <p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p> --}}
                                                                                        @endif

                                                                                    </div>
                                                                                    <br/>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @push('script')
                                                                <script>
                                                                    $(document).ready(function () {
                                                                        var module_id = '{{$module->id}}';
                                                                        $("#dynamic_form_" + module_id).validate({
                                                                            errorPlacement: function (error, element) {
                                                                                if ($(element).attr('type') == "checkbox" || $(element).attr('type') == "radio") {
                                                                                    $(element).closest("div").after(error);
                                                                                } else {
                                                                                    $(element).after(error);
                                                                                }
                                                                            }
                                                                        });

                                                                        $(document).on('submit', "#dynamic_form_" + module_id, function (event) {

                                                                            var module_id = '{{$module->id}}';
                                                                            event.preventDefault();
                                                                            event.stopPropagation();
                                                                            if (!$("#dynamic_form_" + module_id).valid()) {
                                                                                return false;
                                                                            }
                                                                            $('#dynamic_button_' + module_id).attr('disabled', true);
                                                                            var form_data = new FormData(this);
                                                                            $.ajax({
                                                                                cache: false,
                                                                                url: '{{url('form_data_update')}}',
                                                                                type: "POST",
                                                                                data: form_data,
                                                                                contentType: false,
                                                                                processData: false,
                                                                                success: function (response) {
                                                                                    if (response.statusCode == '1') {
                                                                                        $(".category_modules").html(response.data);
                                                                                        $('#dynamic_button_' + module_id).attr('enabled', true);

                                                                                    }
                                                                                },
                                                                                error: function (xhr) {
                                                                                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                                                                                }
                                                                            });
                                                                        });

                                                                        $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                                                            event.preventDefault();
                                                                            event.stopPropagation();
                                                                            if (!$("#module_submit_button_" + module_id).valid()) {
                                                                                return false;
                                                                            }
                                                                            var form_data = {
                                                                                'module_id': module_id,
                                                                            };

                                                                            var path = app.config.SITE_PATH + 'add_module_count';

                                                                            $.ajax({
                                                                                data: form_data,
                                                                                type: 'POST',
                                                                                url: path,
                                                                                headers: {
                                                                                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                                                                                },
                                                                                success: function (data) {
                                                                                    if (data.status == 1) {
                                                                                        console.log(data);

                                                                                        $('#module_submit_button_' + module_id).attr('disabled', true);

                                                                                    } else {
                                                                                        $('#module_submit_button_' + module_id).attr('disabled', true);
                                                                                        // $(".submit_successfully_"+ module_id).css('display','block');
                                                                                        $('#module_submit_button_' + module_id).text('Module déjà terminé');
                                                                                    }
                                                                                },
                                                                                error: function (xhr, status, error) {
                                                                                    // check status && error
                                                                                    console.log(status);
                                                                                }
                                                                            });

                                                                        });

                                                                    });

                                                                </script>
                                                                @endpush

                                                            </div>

                                                        @endif
                                                        @if(!empty($module->quiz_data->QuizQueAns))
                                                            <div class="row">
                                                                <div class="col-md-12 containttestform">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading"><i
                                                                                    class="fa fa-tasks fa-lg fa-fw"
                                                                                    aria-hidden="true"></i>{{ $module->quiz_data->quiz_name }}
                                                                        </div>
                                                                        <div class="panel-body">
                                                                            @if(count($module['quiz_user_data']) > 0)
                                                                                <?php $user_ponts = [];?>
                                                                                @foreach($module['quiz_user_data'] as $quiz_key => $quizData)
                                                                                    <?php
                                                                                    $answers = json_decode($quizData->quiz_ans);
                                                                                    $true_ans = explode(",", $quizData->true_ans);
                                                                                    $user_ans = explode(",", $quizData->user_ans);
                                                                                    $user_ponts[] = $quizData->user_point;
                                                                                    $ans_class = "fa-times-circle";
                                                                                    if ($quizData->user_point) {
                                                                                        $ans_class = "fa-check-circle";
                                                                                    }

                                                                                    ?>
                                                                                    <div class="quizmark">
                                                                                        <div class="form-group">

                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal">Question {{$quiz_key + 1}}
                                                                                                .</label>
                                                                                            <div class="col-md-6">
                                                                                                <label >{{$quizData->quiz_que}}</label><i
                                                                                                        class="fa {{$ans_class}} fa-lg fa-fw font-normal"
                                                                                                        aria-hidden="true"></i>
                                                                                            </div>

                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal"></label>
                                                                                            <div class="col-md-6 ansoption">
                                                                                                @if(!empty($answers->A))
                                                                                                    <span><input
                                                                                                                type="checkbox"
                                                                                                                class="required"
                                                                                                                @foreach($user_ans as $row) @if($row == "A") {{"checked"}} @endif @endforeach value="A"/></span>{{$answers->A}}   @foreach($true_ans as $row) @if($row == "A")
                                                                                                        <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                                           aria-hidden='true'></i> @endif @endforeach
                                                                                                    <br>
                                                                                                @endif
                                                                                                @if(!empty($answers->B))
                                                                                                    <span><input
                                                                                                                type="checkbox"
                                                                                                                class="required"
                                                                                                                @foreach($user_ans as $row) @if($row == "B") {{"checked"}} @endif @endforeach value="B"/></span>{{$answers->B}} @foreach($true_ans as $row) @if($row == "B")
                                                                                                        <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                                           aria-hidden='true'></i> @endif @endforeach
                                                                                                    <br>
                                                                                                @endif
                                                                                                @if(!empty($answers->C))
                                                                                                    <span><input
                                                                                                                type="checkbox"
                                                                                                                class="required"
                                                                                                                @foreach($user_ans as $row) @if($row == "C") {{"checked"}} @endif @endforeach value="C"/></span>{{$answers->C}} @foreach($true_ans as $row) @if($row == "C")
                                                                                                        <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                                           aria-hidden='true'></i> @endif @endforeach
                                                                                                    <br>
                                                                                                @endif
                                                                                                @if(!empty($answers->D))
                                                                                                    <span><input
                                                                                                                type="checkbox"
                                                                                                                class="required"
                                                                                                                @foreach($user_ans as $row) @if($row == "D") {{"checked"}} @endif @endforeach value="D"/></span>{{$answers->D}} @foreach($true_ans as $row) @if($row == "D")
                                                                                                        <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                                           aria-hidden='true'></i> @endif @endforeach
                                                                                                    <br>
                                                                                                @endif
                                                                                            </div>
                                                                                        </div>
                                                                                        @if(!empty($answers->desc))
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                       for="email"
                                                                                                       class="col-md-4 control-label font-normal">{{__('probtp.description')}}</label>
                                                                                                <div class="col-md-6 ansoption">
                                                                                                    <p>{{$answers->desc}}</p>
                                                                                                </div>
                                                                                            </div>
                                                                                        @endif
                                                                                    </div>
                                                                                @endforeach



                                                                            @else
                                                                                <form class="form-horizontal"
                                                                                      id="dynamic_quiz_{{$module->id}}"
                                                                                      role="form"
                                                                                      method="POST"
                                                                                      action="javascript:void(0);"
                                                                                      enctype="multipart/form-data">
                                                                                    @foreach($module->quiz_data->QuizQueAns as $quiz_key => $quiz_value)
                                                                                        {{ csrf_field() }}
                                                                                        <?php $answers = json_decode($quiz_value->answers);?>
                                                                                        <div class="form-group">
                                                                                            <input type="hidden"
                                                                                                   name="quiz_data[{{$quiz_key}}][quiz_que]"
                                                                                                   value="{{$quiz_value->question}}"/>
                                                                                            <input type="hidden"
                                                                                                   name="quiz_data[{{$quiz_key}}][quiz_ans]"
                                                                                                   value="{{$quiz_value->answers}}"/>
                                                                                            <input type="hidden"
                                                                                                   name="quiz_data[{{$quiz_key}}][true_ans]"
                                                                                                   value="{{$quiz_value->true_answers}}"/>
                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal">Question {{$quiz_key + 1}}
                                                                                                .</label>
                                                                                            <div class="col-md-6">
                                                                                                <label class="font-normal">{{$quiz_value->question}}</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label
                                                                                                   for="email"
                                                                                                   class="col-md-4 control-label font-normal"></label>
                                                                                            <div class="col-md-6">
                                                                                                <div class="input-group">
                                                                                                    @if(!empty($answers->A))

                                                                                                        <div class="col-md-2">
                                                                                                            <input type="checkbox"
                                                                                                                   name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                                                   id="quiz_{{$module->id}}_{{$quiz_key}}_1"
                                                                                                                   class="required"
                                                                                                                   value="A"/>
                                                                                                        </div>
                                                                                                        <div class="col-md-10">
                                                                                                            <label class="font-normal"
                                                                                                                   for="quiz_{{$module->id}}_{{$quiz_key}}_1">{{$answers->A}}</label>
                                                                                                            <br>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    @if(!empty($answers->B))
                                                                                                        <div class="col-md-2">
                                                                                                            <input type="checkbox"
                                                                                                                   name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                                                   id="quiz_{{$module->id}}_{{$quiz_key}}_2"
                                                                                                                   class="required"
                                                                                                                   value="B"/>
                                                                                                        </div>
                                                                                                        <div class="col-md-10">
                                                                                                            <label class="font-normal"
                                                                                                                   for="quiz_{{$module->id}}_{{$quiz_key}}_2">{{$answers->B}}</label>
                                                                                                            <br>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    @if(!empty($answers->C))
                                                                                                        <div class="col-md-2">
                                                                                                            <input type="checkbox"
                                                                                                                   name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                                                   id="quiz_{{$module->id}}_{{$quiz_key}}_3"
                                                                                                                   class="required"
                                                                                                                   value="C"/>
                                                                                                        </div>
                                                                                                        <div class="col-md-10">
                                                                                                            <label class="font-normal"
                                                                                                                   for="quiz_{{$module->id}}_{{$quiz_key}}_3">{{$answers->C}}</label>
                                                                                                            <br>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    @if(!empty($answers->D))
                                                                                                        <div class="col-md-2">
                                                                                                            <input type="checkbox"
                                                                                                                   name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                                                   id="quiz_{{$module->id}}_{{$quiz_key}}_4"
                                                                                                                   class="required"
                                                                                                                   value="D"/>
                                                                                                        </div>
                                                                                                        <div class="col-md-10">
                                                                                                            <label class="font-normal"
                                                                                                                   for="quiz_{{$module->id}}_{{$quiz_key}}_4">{{$answers->D}}</label>
                                                                                                            <br>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                    <input type="hidden" name="quiz_id"
                                                                                           value="{{$module->quiz_data->id}}"/>
                                                                                    <input type="hidden"
                                                                                           name="category_id"
                                                                                           value="{{$category_id}}"/>
                                                                                    <input type="hidden" name="cat_id"
                                                                                           value="{{$cat_id}}"/>
                                                                                    <input type="hidden"
                                                                                           name="module_category_id"
                                                                                           value="{{$module->id}}"/>
                                                                                    <div class="form-group">
                                                                                        <div class="col-md-8 col-md-offset-4">
                                                                                            <button type="submit"
                                                                                                    id="quiz_button_{{$module->id}}"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                Poster
                                                                                            </button>
                                                                                            &nbsp; &nbsp;
                                                                                            <button type="reset"
                                                                                                    id="resetbtn"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                {{ __('probtp.reset') }}
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <!-------button is cretaed here.--->

                                                                    <div class="panel-default">
                                                                        <div class="row">
                                                                            <form id="module_submit_form{{$module->id}}"
                                                                                  role="form"
                                                                                  method="POST" action=""
                                                                                  enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-group">
                                                                                    <input type="hidden"
                                                                                           name="module_id"
                                                                                           id="form_module_id"
                                                                                           value="{{$module->id}}"/>
                                                                                    <input type="hidden" name="user_id"
                                                                                           id="form_user_id"
                                                                                           value="{{Auth::user()->id}}"/>
                                                                                    <div class="col-md-9"></div>
                                                                                    <div class="col-md-3">
                                                                                        <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                                                        @if(count($status) > 0)
                                                                                            <button type="submit"
                                                                                                    id="module_submit_button_{{$module->id}}"
                                                                                                    class="btn btn-primary  disabled module_submit">
                                                                                                Module déjà terminé
                                                                                            </button>

                                                                                        @else
                                                                                            <button type="submit"
                                                                                                    id="module_submit_button_{{$module->id}}"
                                                                                                    class="btn btn-primary module_submit">
                                                                                                Terminer ce module
                                                                                            </button>
                                                                                            {{--<p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p>--}}
                                                                                        @endif

                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    @push('script')
                                                                    <script>
                                                                        $(document).ready(function () {
                                                                            var module_id = '{{$module->id}}';
                                                                            $("#dynamic_quiz_" + module_id).validate({
                                                                                errorPlacement: function (error, element) {
                                                                                    if ($(element).attr('type') == "radio") {
                                                                                        $(element).closest("div").after(error);
                                                                                    } else if ($(element).attr('type') == "checkbox") {
                                                                                        $(element).closest(".input-group").after(error);
                                                                                    } else {
                                                                                        $(element).after(error);
                                                                                    }
                                                                                }
                                                                            });

                                                                            $(document).on('click', "#resetbtn", function () {
                                                                                $("#dynamic_quiz_" + module_id).find("label.error").remove();
                                                                            });

                                                                            $(document).on('submit', "#dynamic_quiz_" + module_id, function (event) {

                                                                                var module_id = '{{$module->id}}';
                                                                                $('#quiz_button_' + module_id).attr('disabled', true);
                                                                                event.preventDefault();
                                                                                event.stopPropagation();
                                                                                if (!$("#dynamic_quiz_" + module_id).valid()) {
                                                                                    return false;
                                                                                }
                                                                                var form_data = new FormData(this);

                                                                                $.ajax({
                                                                                    cache: false,
                                                                                    url: '{{url('quiz_data_update')}}',
                                                                                    type: "POST",
                                                                                    data: form_data,
                                                                                    contentType: false,
                                                                                    processData: false,
                                                                                    success: function (response) {
                                                                                        if (response.statusCode == '1') {
                                                                                            $(".category_modules").html(response.data);
                                                                                            $('#quiz_button_' + module_id).attr('enabled', false);
                                                                                        }
                                                                                    }
                                                                                });
                                                                            });
                                                                            $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                                                                event.preventDefault();
                                                                                event.stopPropagation();
                                                                                if (!$("#module_submit_button_" + module_id).valid()) {
                                                                                    return false;
                                                                                }
                                                                                var form_data = {
                                                                                    'module_id': module_id,
                                                                                };

                                                                                var path = app.config.SITE_PATH + 'add_module_count';

                                                                                $.ajax({
                                                                                    data: form_data,
                                                                                    type: 'POST',
                                                                                    url: path,
                                                                                    headers: {
                                                                                        'X-CSRF-TOKEN': window.Probtp.csrfToken
                                                                                    },
                                                                                    success: function (data) {
                                                                                        if (data.status == 1) {
                                                                                            console.log(data);

                                                                                            $('#module_submit_button_' + module_id).attr('disabled', true);

                                                                                        } else {
                                                                                            $('#module_submit_button_' + module_id).attr('disabled', true);
                                                                                            // $(".submit_successfully_"+ module_id).css('display','block');
                                                                                            $('#module_submit_button_' + module_id).text('Module déjà terminé');
                                                                                        }
                                                                                    },
                                                                                    error: function (xhr, status, error) {
                                                                                        // check status && error
                                                                                        console.log(status);
                                                                                    }
                                                                                });

                                                                            });

                                                                        });

                                                                    </script>
                                                                    @endpush
                                                                </div>
                                                            </div>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
            </div>
            <?php } ?>
            @endforeach
            @push('script')
            <script>
                $(document).ready(function () {
                    var nd = $(".down-arrow").parent();
                    nd.css('cursor', 'pointer');

                    var divid = $(".titletag").attr('id');
                    $("#collapse" + divid).removeClass('collapse');
                    $("#collapse" + divid).removeClass('in');
                    $(document).on('click', '.childli', function () {
                        var id = $(this).attr('id');
                        $("#collapse" + id).addClass("in");

                    });
                    if ($(".category_title h1").length == 0) {
                        $('.category_title').css('margin-top', '74px');
                    }
                    $(document).on('click', '.like_category', function () {
                        var id = $(this).attr('id');

                        $.ajax({
                            data: {category_id: id},
                            type: 'POST',
                            url: app.config.SITE_PATH + '/like_category',
                            headers: {
                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                            },
                            success: function (data) {
                                $('.like_counter').contents().first().replaceWith(data.like_count);
                                if (data.like_status == 1) {
                                    $('.like_category').css('color', '#337ab7');
                                } else {
                                    $('.like_category').css('color', 'grey');
                                }


                            },
                            error: function (xhr, status, error) {
                                // check status && error
                                console.log(status);
                            }
                        });

                    });
                });
            </script>
    @endpush