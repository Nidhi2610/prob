@foreach($moduleid_data as $categoryData)
    <div class="row">
        <h2 class="part-title category-title">{{$categoryData['category_name']}}</h2>
    @foreach($categoryData['moduleData'] as $module)
        <div class="content">
            <div class="col-sm-12 col-header-nopadding">
                <h2 class="part-title">{{$module->Module->title}}</h2>

                <div class="attachement">
                    @if (pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'mp4' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MOV' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'AVI' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'wmv'|| pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MP4')
                        <video class="video-js" controls preload="auto" width="100%" height=""
                               data-setup="{}">
                            <source src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                    type='video/mp4'>
                        </video>

                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'txt')
                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <i class="fa fa-btn fa-file-text fa-2x"></i>
                        </a>
                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpg' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'png' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpeg' )
                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <img src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}" width="600px" />
                        </a>
                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pdf')
                       {{-- <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <i class="fa fa-btn fa-file-pdf-o fa-2x"></i>
                        </a>--}}
                        <iframe id="fred" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}" frameborder="1" scrolling="auto" height="600" width="100%" ></iframe>
                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'doc'||pathinfo($module->file_name, PATHINFO_EXTENSION) === 'docx')
                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <i class="fa fa-file-word-o fa-2x "></i>
                        </a>
                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pptx')
                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <i class="fa fa-file-powerpoint-o fa-2x "></i>
                        </a>
                    @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'xlsx')
                        <a href="{{url('category_module/'.$module->Module->file_name)}}">
                            <i class="fa fa-file-excel-o fa-2x "></i>
                        </a>

                    @endif

                </div>
            </div>

            <div class="col-md-12">
                <div class="summary">
                    {!! $module->Module->summary !!}
                </div>
            </div>


            <div class="col-md-12">
                <div class="form_title">
                    <div class="form_content">
                        @if(!empty($module->form_data))
                            <?php $form_content = json_decode($module->form_data->content);?>
                            <div class="row">
                                <div class="col-md-12 clearfix containttestform">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><i class="fa fa-tasks fa-lg fa-fw" aria-hidden="true"></i> {{ $module->form_data->title }}</div>
                                        <div class="panel-body">
                                            @if(count($module['form_user_data']) > 0)
                                                @foreach($module['form_user_data'] as $formData)

                                                    <div class="form-group">
                                                        <div class="finalanswers">
                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label">{{$formData->form_name}}</label>
                                                            <div class="col-md-6">
                                                                <label style="font-weight: normal">{{$formData->form_value}}</label><br>
                                                            </div>
                                                        </div><br></div>
                                                @endforeach

                                            @else
                                                <form class="form-horizontal" id="dynamic_form_{{$module->id}}" role="form" method="POST" action="javascript:void(0);" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    @foreach($form_content as $key => $row)
                                                        <div class="form-group">
                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label">{{$row->label}}</label>
                                                            <div class="col-md-6">
                                                                <input type="hidden" name="form_data[{{$key}}][name]" value="{{$row->label}}" />
                                                                @if($row->type == "text")
                                                                    <input type="text" class="form-control required" name="form_data[{{$key}}][value]" value=""/>
                                                                @elseif($row->type == "textarea")
                                                                    <textarea name="form_data[{{$key}}][value]" class="form-control required"></textarea>
                                                                @elseif($row->type == "select")
                                                                    <select name="form_data[{{$key}}][value]" class="form-control required">
                                                                        @foreach($row->values as $select)
                                                                            <option value="{{$select->value}}">{{$select->label}}</option>
                                                                        @endforeach

                                                                    </select>
                                                                @elseif($row->type == "checkbox-group")
                                                                    <div class="input-group">
                                                                        @foreach($row->values as $check_box)
                                                                            <input type="checkbox" class="required" name="form_data[{{$key}}][value][]" id="check_{{$check_box->value}}" value="{{$check_box->value}}" /><label style="font-weight: normal" for="check_{{$check_box->value}}">{{$check_box->label}}</label><br/>
                                                                        @endforeach
                                                                    </div>
                                                                @elseif($row->type == "radio-group")
                                                                    <div class="input-group">
                                                                        @foreach($row->values as $radio_box)
                                                                            <input type="radio" class="required" name="form_data[{{$key}}][value]" id="radio_{{$radio_box->value}}" value="{{$radio_box->value}}" /> <label style="font-weight: normal" for="radio_{{$radio_box->value}}">{{$radio_box->label}}</label><br/>
                                                                        @endforeach
                                                                    </div>
                                                                @else
                                                                    <input type="text" class="form-control required" name="form_data[{{$key}}][value]" value=""/>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <input type="hidden" name="form_id" value="{{$module->form_data->form_id}}" />
                                                    <input type="hidden" name="category_id" value="{{$category_id}}" />
                                                    <input type="hidden" name="module_category_id" value="{{$module->id}}" />
                                                    <div class="form-group">
                                                        <div class="col-md-8 col-md-offset-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Poster
                                                            </button>
                                                            &nbsp; &nbsp;
                                                            <button type="reset" class="btn btn-primary">
                                                                {{ __('probtp.reset') }}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @push('script')
                                <script>
                                    $(document).ready(function () {
                                        var module_id = '{{$module->id}}';
                                        $("#dynamic_form_"+module_id).validate({
                                            errorPlacement: function (error, element) {
                                                if ($(element).attr('type') == "checkbox" || $(element).attr('type') == "radio") {
                                                    $(element).closest("div").after(error);
                                                }else{
                                                    $(element).after(error);
                                                }
                                            }
                                        });

                                        $(document).on('submit',"#dynamic_form_"+module_id,function (event) {
                                            var module_id = '{{$module->id}}';
                                            event.preventDefault();
                                            event.stopPropagation();
                                            if(!$("#dynamic_form_"+module_id).valid()){
                                                return false;
                                            }

                                            var form_data=new FormData(this);

                                            $.ajax({
                                                cache:false,
                                                url: '{{url('form_data_update')}}',
                                                type: "POST",
                                                data: form_data,
                                                contentType: false,
                                                processData:false,
                                                success: function(response)
                                                {
                                                    if(response.statusCode == '1'){
                                                        $(".category_modules").html(response.data);
                                                    }
                                                }
                                            });
                                        });
                                    });

                                </script>
                                @endpush
                            </div>
                        @endif
                        @if(!empty($module->quiz_data->QuizQueAns))
                            <div class="row">
                                <div class="col-md-12 clearfix containttestform">
                                    <div class="panel panel-default">
                                        <div     class="panel-heading"><i class="fa fa-tasks fa-lg fa-fw" aria-hidden="true"></i>{{ $module->quiz_data->quiz_name }}</div>
                                        <div class="panel-body">
                                            @if(count($module['quiz_user_data']) > 0)
                                                <?php $user_ponts = [];?>
                                                @foreach($module['quiz_user_data'] as $quiz_key => $quizData)
                                                    <?php
                                                    $answers = json_decode($quizData->quiz_ans);
                                                    $true_ans = explode(",",$quizData->true_ans);
                                                    $user_ans = explode(",",$quizData->user_ans);
                                                    $user_ponts[] = $quizData->user_point;
                                                    $ans_class = "fa-times-circle";
                                                    if($quizData->user_point){
                                                        $ans_class = "fa-check-circle";
                                                    }

                                                    ?>
                                                    <div class="quizmark">
                                                        <div class="form-group">

                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label">Question {{$quiz_key + 1}}.</label>
                                                            <div class="col-md-6">
                                                                <label style="font-weight: normal">{{$quizData->quiz_que}}</label><i class="fa {{$ans_class}} fa-lg fa-fw" aria-hidden="true"></i>
                                                            </div>

                                                        </div>
                                                        <div class="form-group">
                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label"></label>
                                                            <div class="col-md-6 ansoption">
                                                                @if(!empty($answers->A))
                                                                    <span><input type="checkbox" class="required" @foreach($user_ans as $row) @if($row == "A") {{"checked"}} @endif @endforeach value="A" /></span>{{$answers->A}}   @foreach($true_ans as $row) @if($row == "A") <i class='fa fa-check-circle fa-lg fa-fw' aria-hidden='true'></i> @endif @endforeach <br>
                                                                @endif
                                                                @if(!empty($answers->B))
                                                                    <span><input type="checkbox" class="required" @foreach($user_ans as $row) @if($row == "B") {{"checked"}} @endif @endforeach value="B" /></span>{{$answers->B}} @foreach($true_ans as $row) @if($row == "B") <i class='fa fa-check-circle fa-lg fa-fw' aria-hidden='true'></i> @endif @endforeach <br>
                                                                @endif
                                                                @if(!empty($answers->C))
                                                                    <span><input type="checkbox" class="required" @foreach($user_ans as $row) @if($row == "C") {{"checked"}} @endif @endforeach value="C" /></span>{{$answers->C}} @foreach($true_ans as $row) @if($row == "C") <i class='fa fa-check-circle fa-lg fa-fw' aria-hidden='true'></i> @endif @endforeach <br>
                                                                @endif
                                                                @if(!empty($answers->D))
                                                                    <span><input type="checkbox" class="required" @foreach($user_ans as $row) @if($row == "D") {{"checked"}} @endif @endforeach value="D" /></span>{{$answers->D}} @foreach($true_ans as $row) @if($row == "D") <i class='fa fa-check-circle fa-lg fa-fw' aria-hidden='true'></i> @endif @endforeach <br>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if(!empty($answers->desc))
                                                            <div class="form-group">
                                                                <label style="font-weight: normal" for="email" class="col-md-4 control-label">{{__('probtp.description')}}</label>
                                                                <div class="col-md-6 ansoption">
                                                                    <p>{{$answers->desc}}</p>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                @endforeach

                                                <div class="youroints">
                                                    <div class="form-group">
                                                        <label style="font-weight: normal" for="your_point" class="col-md-11 youroint control-label clearfix">Your Points</label>
                                                        <div class="col-md-1 pts">
                                                            <label style="font-weight: normal">{{array_sum($user_ponts)}}</label>
                                                        </div>
                                                    </div>
                                                </div>

                                            @else
                                                <form class="form-horizontal" id="dynamic_quiz_{{$module->id}}" role="form" method="POST" action="javascript:void(0);" enctype="multipart/form-data">
                                                    @foreach($module->quiz_data->QuizQueAns as $quiz_key => $quiz_value)
                                                        {{ csrf_field() }}
                                                        <?php $answers = json_decode($quiz_value->answers);?>
                                                        <div class="form-group">
                                                            <input type="hidden" name="quiz_data[{{$quiz_key}}][quiz_que]" value="{{$quiz_value->question}}" />
                                                            <input type="hidden" name="quiz_data[{{$quiz_key}}][quiz_ans]" value="{{$quiz_value->answers}}" />
                                                            <input type="hidden" name="quiz_data[{{$quiz_key}}][true_ans]" value="{{$quiz_value->true_answers}}" />
                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label">Question {{$quiz_key + 1}}.</label>
                                                            <div class="col-md-6">
                                                                <label style="font-weight: normal">{{$quiz_value->question}}</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label style="font-weight: normal" for="email" class="col-md-4 control-label"></label>
                                                            <div class="col-md-6">
                                                                <div class="input-group">
                                                                    @if(!empty($answers->A))
                                                                        <input type="checkbox" name="quiz_data[{{$quiz_key}}][user_ans][]" id="quiz_{{$module->id}}_{{$quiz_key}}_1" class="required" value="A" /><label style="font-weight: normal" for="quiz_{{$module->id}}_{{$quiz_key}}_1">{{$answers->A}}</label> <br>
                                                                    @endif
                                                                    @if(!empty($answers->B))
                                                                        <input type="checkbox" name="quiz_data[{{$quiz_key}}][user_ans][]" id="quiz_{{$module->id}}_{{$quiz_key}}_2" class="required" value="B" /><label style="font-weight: normal" for="quiz_{{$module->id}}_{{$quiz_key}}_2">{{$answers->B}}</label> <br>
                                                                    @endif
                                                                    @if(!empty($answers->C))
                                                                        <input type="checkbox" name="quiz_data[{{$quiz_key}}][user_ans][]" id="quiz_{{$module->id}}_{{$quiz_key}}_3" class="required" value="C" /><label  style="font-weight: normal"for="quiz_{{$module->id}}_{{$quiz_key}}_3">{{$answers->C}}</label> <br>
                                                                    @endif
                                                                    @if(!empty($answers->D))
                                                                        <input type="checkbox" name="quiz_data[{{$quiz_key}}][user_ans][]" id="quiz_{{$module->id}}_{{$quiz_key}}_4" class="required" value="D" /><label style="font-weight: normal" for="quiz_{{$module->id}}_{{$quiz_key}}_4">{{$answers->D}}</label> <br>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <input type="hidden" name="quiz_id" value="{{$module->quiz_data->id}}" />
                                                    <input type="hidden" name="category_id" value="{{$category_id}}" />
                                                    <input type="hidden" name="module_category_id" value="{{$module->id}}" />
                                                    <div class="form-group">
                                                        <div class="col-md-8 col-md-offset-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Poster
                                                            </button>
                                                            &nbsp; &nbsp;
                                                            <button type="reset" class="btn btn-primary">
                                                                {{ __('probtp.reset') }}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                    @push('script')
                                    <script>
                                        $(document).ready(function () {
                                            var module_id = '{{$module->id}}';
                                            $("#dynamic_quiz_"+module_id).validate({
                                                errorPlacement: function (error, element) {
                                                    if ($(element).attr('type') == "checkbox" || $(element).attr('type') == "radio") {
                                                        $(element).closest("div").after(error);
                                                    }else{
                                                        $(element).after(error);
                                                    }
                                                }
                                            });

                                            $(document).on('submit',"#dynamic_quiz_"+module_id,function (event) {
                                                var module_id = '{{$module->id}}';
                                                event.preventDefault();
                                                event.stopPropagation();
                                                if(!$("#dynamic_quiz_"+module_id).valid()){
                                                    return false;
                                                }
                                                var form_data=new FormData(this);

                                                $.ajax({
                                                    cache:false,
                                                    url: '{{url('quiz_data_update')}}',
                                                    type: "POST",
                                                    data: form_data,
                                                    contentType: false,
                                                    processData:false,
                                                    success: function(response)
                                                    {
                                                        if(response.statusCode == '1'){
                                                            $(".category_modules").html(response.data);
                                                        }
                                                    }
                                                });
                                            });

                                        });

                                    </script>
                                    @endpush
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
@endforeach
    </div>
@endforeach
