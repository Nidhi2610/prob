@extends('layouts.front1.app')
@push('css')
@endpush
@inject('ColloCat','App\Models\CollaborateurCategory')
@inject('timeSoent','App\Models\UserLogin')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-header-nopadding">
                <section class="pro-bg-img">
                    <div>
                        <h1>Mon offre de formation</h1>
                    </div>
                    <div>
                        <div class="tabbable-panel">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs category-tab">
                                    <li class="active">
                                        <a href="#tab_default_1" data-toggle="tab" data-img="bg1.jpg">
                                            <span class="Fill-1 font-icon"></span>
                                            <span class="Mes-modules">Mes Modules</span>
                                            {{--<span class="oval-2 formation_count">{{$bookmark_count}}</span>--}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_default_2" data-toggle="tab" data-img="bg2.jpg">
                                            <span class="Fill-1 font-icon" style="font-size: 22px;"></span>
                                            <span class="Mes-modules">Statistiques</span>
                                            {{--<span class="badge">0</span>--}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_default_3" data-toggle="tab" data-img="bg3.jpg">
                                            <span class="Fill-1 font-icon" style="font-size: 23px;"></span>
                                            <span class="Mes-modules">Ressources</span>
                                            {{--<span class="badge">0</span>--}}
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </section>

                <div class="col-sm-12">
                    <section class="margin-30 ">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_default_1">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="first-text">
                                                <p id="training_text font-bold">
                                                    Vous n'avez encore sélectionné aucun thème à votre offre de
                                                    formation
                                                </p>
                                                <p>
                                                    Vous pouvez consulter le détail d'un thème, l'ajouter à votre offre
                                                    de formation
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <?php $publicFormationData = \App\Helpers\LaraHelpers::partition($publicFormationData, 2);?>
                                    <div class="col-md-6">
                                        @foreach($publicFormationData[0] as $formation)
                                            <div class="col-md-12 no-padding">
                                                @if(!empty($formation['id']))
                                                    <div class="panel-group" id="accordion{{$formation['id']}}">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading fa_fa_color">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                                       data-parent="#accordion"
                                                                       href="#collapse{{$formation['id']}}">
                                                                        <i class="fa {{$formation['font_awesome']}} i-class"
                                                                           aria-hidden="true"></i>
                                                                        {{--<span class="text_heading category-name">{{$formation['name']}}</span>--}}
                                                                        <a class="Blanc get_category"
                                                                           href="javascript:void(0);"
                                                                           id="{{$formation['id']}}"
                                                                           role="{{$formation['id']}}"> <span
                                                                                    class="text_heading category-name">{{$formation['name']}}</span></a>
                                                                        <?php $categoryLikeStatus = \App\Helpers\LaraHelpers::likeStatus($formation['id']);
                                                                        $totalLike = \App\Helpers\LaraHelpers::likeCount($formation['id']);?>
                                                                        <i class="fa fa-thumbs-up thumb-class"></i><span
                                                                                class="like_counter">{{$totalLike}}</span>
                                                                        <span>
                                            @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)

                                                                                <i class="fa fa-user get_users star-bookmark-b cursor-pointer"
                                                                                   id="{{$formation['id']}}"
                                                                                   data-parent_id="{{$formation['id']}}"
                                                                                   data-id="F"></i>
                                                                            @endif
                                                                            {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$formation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                            </span>

                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse{{$formation['id']}}"
                                                                 class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <ul class="collapse-ul">
                                                                        @if(!empty($formation['ChildArray']))
                                                                            @foreach($formation['ChildArray'] as $childFormation)
                                                                                <li>
                                                                                    {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                                                    <div class="row">
                                                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                                                            <a class="Blanc get_category star_bookmark"
                                                                                               id="{{$childFormation['id']}}"
                                                                                               href="javascript:void(0);"
                                                                                               role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>

                                                                                        </div>
                                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span class="float-left">
                                                                @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b cursor-pointer"
                                                                              id="{{$childFormation['id']}}"
                                                                              data-parent_id="{{$childFormation['id']}}"
                                                                              data-id="F"></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star-o star-bookmark-a"
                                                                          id="{{$childFormation['id']}}"
                                                                          aria-hidden="true"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star formation_active star-bookmark-a"
                                                                          id="{{$childFormation['id']}}"
                                                                          aria-hidden="true"
                                                                          style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                                                        </div>
                                                                                    </div>


                                                                                </li>
                                                                            @endforeach
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                @endif
                                            </div>


                                        @endforeach
                                    </div>

                                    <div class="col-md-6">
                                        @foreach($publicFormationData[1] as $formation)
                                            <div class="col-md-12 no-padding">
                                                @if(!empty($formation['id']))
                                                    <div class="panel-group" id="accordion{{$formation['id']}}">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading fa_fa_color">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse"
                                                                       data-parent="#accordion"
                                                                       href="#collapse{{$formation['id']}}">
                                                                        <i class="fa {{$formation['font_awesome']}} i-class"
                                                                           aria-hidden="true"></i>
                                                                        {{--<span class="text_heading category-name">{{$formation['name']}}</span>--}}

                                                                        <a class="Blanc get_category"
                                                                           id="{{$formation['id']}}"
                                                                           href="javascript:void(0);"
                                                                           role="{{$formation['id']}}"> <span
                                                                                    class="text_heading category-name">{{$formation['name']}}</span></a>

                                                                        <?php $categoryLikeStatus = \App\Helpers\LaraHelpers::likeStatus($formation['id']);
                                                                        $totalLike = \App\Helpers\LaraHelpers::likeCount($formation['id']);?>
                                                                        <i class="fa fa-thumbs-up thumb-class"></i><span
                                                                                class="like_counter">{{$totalLike}}</span>

                                                                        <span>
                                                                     @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)

                                                                                <i class="fa fa-user get_users star-bookmark-b cursor-pointer margin"
                                                                                   id="{{$formation['id']}}"
                                                                                   data-parent_id="{{$formation['id']}}"
                                                                                   data-id="F"></i>
                                                                            @endif
                                                                            {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$formation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                                                </span>


                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse{{$formation['id']}}"
                                                                 class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <ul class="collapse-ul">
                                                                        @if(!empty($formation['ChildArray']))
                                                                            @foreach($formation['ChildArray'] as $childFormation)
                                                                                <li>
                                                                                    {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                                                    <div class="row">
                                                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                                                            <a class="Blanc get_category"
                                                                                               href="javascript:void(0);"
                                                                                               id="{{$childFormation['id']}}"
                                                                                               role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                                                        </div>
                                                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span class="float-left">
                                                                @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b cursor-pointer"
                                                                              id="{{$childFormation['id']}}"
                                                                              data-parent_id="{{$childFormation['id']}}"
                                                                              data-id="F"></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star-o  star-bookmark-a"
                                                                          id="{{$childFormation['id']}}"
                                                                          aria-hidden="true"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star formation_active star-bookmark-a"
                                                                          id="{{$childFormation['id']}}"
                                                                          aria-hidden="true"
                                                                          style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                                                        </div>
                                                                                    </div>


                                                                                </li>
                                                                            @endforeach
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                @endif
                                            </div>


                                        @endforeach
                                    </div>


                                </div>

                            </div>
                            <div class="tab-pane" id="tab_default_2">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-4">
                                            <div class="username_mon_rect">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="tempse">
                                                        <span>
                                                            Nombre de modules complétés
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        {{--<div class="progress blue">
                                                                        <span class="progress-left">
                                                                            <span class="progress-bar"></span>
                                                                        </span>
                                                            <span class="progress-right">
                                                                            <span class="progress-bar"></span>
                                                                        </span>
                                                            <div class="progress-value">9</div>
                                                        </div>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-4">
                                            <div class="username_mon_rect">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="tempse">
                                                            <span>Temps passé sur la plateforme</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="hour">
                                                            <?php $t = $timeSoent->getTotalTime(Auth::user()->id); ?>
                                                            <span> </span>
                                                            {{--<span>{{ $t }} 1h</span><span>22min</span>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-4">
                                            <div class="username_mon_rect_last">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="nombre">
                                                            <div>
                                                                Nombre d'utilisateurs actifs
                                                            </div>
                                                            <div>
                                                                <span>{{$last_login_colloborateur_count->count()}}
                                                                    /</span><span>{{$userObj->count()}}</span><span>La semaine dernière</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <div class="row">

                                        @foreach($userObj as $user)

                                            <div class="col-md-6 col-sm-6 col-lg-4">
                                                <div class="username_mon_index">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="adress">
                                                                <div>
                                                                    <h4>{{$user->name}}</h4>
                                                                </div>
                                                                <div>
                                                                    <span>{{$user->email}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="user">
                                                                @if(!empty($user->profile_image))
                                                                    <img class="home-profile-image"
                                                                         src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $user->profile_image }}"/>
                                                                @else
                                                                    <img class="home-profile-image"
                                                                         src="{{ url('public/front1/images/noimage.png')}}"/>
                                                                @endif
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="module-border">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="Modules">
                                                                    <div>
                                                                        <span>Modules suivis</span>
                                                                    </div>
                                                                    <div>
                                                                        <span class="review">{{$ColloCat->getAllColloborateurFormationResource($user->id) + $getpublicFormationResourcedata}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="Commentaires">
                                                                    <div>
                                                                        <span>Commentaires</span>
                                                                    </div>
                                                                    <div>
                                                                        {{$user->Comments->count()}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="denniel-border">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="dennied-bor">
                                                                    <div class="Commentaires">
                                                                        <span>Dernière connexion</span>
                                                                    </div>
                                                                    <div>
                                                                        {{$user->created_at->format('d-M-Y')}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane resource-tab" id="tab_default_3">
                                <div class="">
                                    <div class="row">
                                        @foreach($publicResourceData as $resource)
                                            <div class="col-md-12">
                                                <div class="panel-group" id="accordion{{$resource['id']}}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <div class="resources">
                                                                    <a href="javascript:void(0);"
                                                                       class="Blanc get_category"
                                                                       id="{{$resource['id']}}"
                                                                       role="{{$resource['id']}}"><span
                                                                                class="resource_heading">{{$resource['name']}}</span>
                                                                    </a>
                                                                    @if(Auth::user()->user_type == 3 && $resource['category_type_id'] == 1)
                                                                        <i class="fa fa-user get_users"
                                                                           id="{{$resource['id']}}" data-id="R"></i>
                                                                    @endif
                                                                </div>
                                                                <a class="accordion-toggle" data-toggle="collapse"
                                                                   data-parent="#accordion"
                                                                   href="#collapse{{$resource['id']}}">

                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$resource['id']}}"
                                                             class="panel-collapse collapse resource-pad">
                                                            <div class="panel-body">
                                                                @if(isset($resource['ChildArray']) && count($resource['ChildArray']) > 0)
                                                                    <div class="row">
                                                                        @foreach($resource['ChildArray'] as $resourceChild)
                                                                            <div class="col-md-6">
                                                                                <div class="dum">
                                                                                    <i class="fa fa-file-text-o"
                                                                                       aria-hidden="true"></i> <a
                                                                                            class="Blanc get_category"
                                                                                            href="javascript:void(0);"
                                                                                            role="{{$resourceChild['id']}}"><span>{{$resourceChild['name']}}</span></a>
                                                                                    @if(Auth::user()->user_type == 3 && $resource['category_type_id'] == 1)
                                                                                        <i class="fa fa-user get_users"
                                                                                           id="{{$resourceChild['id']}}"
                                                                                           data-id="R"></i>
                                                                                    @endif
                                                                                    {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category" role="{{$resourceChild['id']}}" aria-hidden="true"></i></a>--}}
                                                                                </div>
                                                                            </div>
                                                                        @endforeach

                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        @endforeach
                                        <input type="hidden" name="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>

@endsection
            @include('admin.category.collaborateuruser',['userObj' => $userObj])
@push('script')

<script>
    $(document).ready(function () {

        var time = '{!! $t !!}';
        var stime = time.slice(-7);
        var ftime= time.slice(0,-7);
        var newTime = "<span>" + ftime +"</span><span>" + stime + "</span>";
        $(".hour").html(newTime);
        $(document).on('click',".category-tab li a", function () {
             var img_path = app.config.SITE_PATH + 'public/front1/images/'+$(this).attr('data-img');
             $(".pro-bg-img").css("background-image",'url('+img_path+')');
        });

        $(document).on('click',".get_category",function () {
            var get_cat_ids = $(this).closest(".panel.panel-default").find(".get_category").map(function () {
                return $(this).attr('role');
            }).get();


            var module_id = $(this).attr('id');
            var url = app.config.SITE_PATH+'/count_accessby_user';

            $.ajax({

                data: {id: module_id},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    console.log(data);

                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
            window.location = app.config.SITE_PATH + 'details/'+$(this).attr('role')+'/'+ get_cat_ids.join(':')+'#category_'+$(this).attr('role');

        });

        // For Opening the Popup for assigning the category

        $(document).on('click','.get_users',function () {

            $("#categoryids").val($(this).attr('id'));
            $("#type").val($(this).attr('data-id'));
            var type = $(this).attr('data-id');
           var id = $(this).attr('id');
           var url = app.config.SITE_PATH+'/collaborateur_user';

            $.ajax({

                data: {id: id,type:type},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    $('.modalcheckbox').prop('checked', false);
                    $.each(data, function (key, val) {
                        $('.users[value="' + val.collaborateur_id + '"]').prop('checked', true);
                    });
                    $("#myModalCollaborateuruser").modal("show");

                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
        });

        // For Bookmarking the Category
        $(document).on('click','.star_bookmark',function () {
            var category_bookmark_id = $(this).attr('id');

            if($(this).hasClass("formation_active")){
                $(this).removeClass("formation_active fa-star").addClass('fa-star-o');
                var url = app.config.SITE_PATH+'/remove_bookmark';
            }else{
                $(this).addClass("formation_active fa-star").removeClass('fa-star-o');
                var url = app.config.SITE_PATH+'/bookmark';
            }

            $.ajax({
                data: {category_bookmark_id:category_bookmark_id},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    console.log(data);
                    if(data.statusCode == 1){
                        $(".category-tab .active").find(".formation_count").html(data.bookmark_count);
                    }
                    var updated_bookmark = data.bookmark_count;
                    if(updated_bookmark > 0){
                        $('#training_text').hide()
                    }else{
                        $('#training_text').show()
                    }

                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
        });

        $('.accordion-toggle' ).click(function(){
            if($(this).hasClass('icon-toggle')){ console.log($(this).hasClass('icon-toggle'));
                $('.icon-toggle').removeClass('icon-toggle');
            }else {
                $(this).removeProp('content');
                $(this).addClass('icon-toggle');
            }
            });



        // For showing and hiding  .first-text div
       var current_bookmark = '<?php echo $bookmark_count ?>';
        if(current_bookmark == 0){
            $('#training_text').show()
        }else{
            $('#training_text').hide()
        }
    });

</script>
@endpush
