@extends('layouts.front1.app')
@inject('Enterprise','App\Models\Entreprise')
@section('content')
    <div class="">
        <div class="col-sm-10">
        <div class="row">
            <div class="row">

                <div class="col-md-12">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>Modifier un responsable
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">
                        <div class="title-heading">
                            <h1>{{ __('probtp.module_edit', ['module' => __('probtp.user')]) }}</h1>
                        </div>
                    </div>--}}

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('userRHUpdate') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}" />
                            <input type="hidden" name="dbAction" value="Edit" />
                            <input type="hidden" name="front_user" value="1">

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">{{ __('probtp.first_name') }}</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $editData->first_name }}" placeholder="{{ __('probtp.first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">{{ __('probtp.last_name') }}</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $editData->last_name }}" placeholder="{{ __('probtp.last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
   <?php   $parent_id =  \App\Models\Entreprise::select('parent_id')->where('id',$editData->entreprise_id)->get();?>
                            <input type="hidden" name="parent_id" id="parent_id" value="{{$parent_id}}" />
                            <input type="hidden" name="user_type" id="user_type" value="3" />
                            <input type="hidden" name="entreprise_id" id="entreprise_id" value="{{ $editData->entreprise_id }}" />






                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control"  name="email" value="{{ $editData->email }}" placeholder="{{ __('probtp.email') }}" required >

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                    &nbsp; &nbsp;
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
@push('script')
<script src="{{ asset('public/multiselect/select2.min.js') }}"></script>
<link href="{{ asset('public/multiselect/select2.css') }}" rel="stylesheet">
<script>

//    $('#user_type').on('change', function() {
//        var user_value = $(this).val();
//        if(user_value == 2 || user_value == 5){
//            $('#parent_div').hide();
//            $('#multiple_enterpriseid').show();
//            $('#single_enterpriseid').hide();
//            $('#singleid').prop('disabled','disabled');
//
//        }else if(user_value == 3){
//            $('#parent_div').hide();
//            $('#single_enterpriseid').show();
//            $('#singleid').prop('disabled',false);
//            $('#multiple_enterpriseid').hide();
//            $('#singleid').attr('required','required');
//        }else if(user_value == 4){
//            $('#parent_div').show();
//            $('#multiple_enterpriseid').hide();
//            $('#single_enterpriseid').hide();
//            $('#singleid').prop('disabled','disabled');
//        }
//    });

    // For Multi select
    $(window).on('load', function() {
        $('#user_type').change();
        $("#entreprise_id").select2();

    });

</script>
@endpush