@extends('layouts.front1.app')
   @inject('ColloCat','App\Models\CollaborateurCategory')
@push('css')
<style>
    /*.margin-30{    margin: 15px 15px 30px 15px;}*/
</style>
@endpush
@section('content')
    <div class="col-sm-12 col-md-10 " >
   <section class="margin-30">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading" id="community">
                        <div class="title-heading">
                            <?php
                            $parent_entreprise = \App\Models\Entreprise::select('name','id','logo')-> where('id',  Auth::user()->entreprise_id)->first();  ?>
                            <h1>Gestion groupe : {{$parent_entreprise->name}}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row">

                @foreach($sub_entreprise_id as $entre)
                    @foreach($entre as $entreprise)
                    <div class="col-md-6 col-sm-6 col-lg-4">
                        <div class="username_mon">
                            <div class="row">
                                <div class="col-md-9">
                                    <div>
                                            <h4 class="entreprise_name.'{{$entreprise->name}}'">{{$entreprise->name}}</h4>
                                    </div>


                                </div>
                                <div class="col-md-3">
                                    <div class="user">

                                        @if(!empty($entreprise->logo))
                                            <img src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ $entreprise->logo }}" style="width: 100px;height: 60px;cursor: pointer;border-radius: 50%;margin-top: 0px;margin-bottom: 10px;"  />
                                        @else
                                            <img src="{{ url('public/front1/images/noimage.png')}}" style="width: 100px;height: 60px;cursor: pointer;border-radius: 50%;margin-top: 0px;margin-bottom: 10px;"/>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="add-mem">
                                        <div class="add-membre">
                                             {{--<a href="{{'user/responsable/create/?'.$entreprise->name.'/?'.$entreprise->id}}">--}}
                                             <a href="{{route('user_Rh_Add',['id'=> $entreprise->id] )}}">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <span>
                                     Gestion des responsables
                                    </span>
                                    </div>
                                    @push('script')
                                    {{--<script text="text/javascript">--}}
                                        {{--$(".fa-plus").click( function(){--}}
                                            {{--var name = '{{$entreprise->name}}';--}}
                                            {{--var id = '{{$entreprise->id}}';--}}
                                            {{--$.ajax({--}}
                                                {{--data: {id: id,--}}
                                                    {{--name: name },--}}
                                                {{--type: 'POST',--}}
                                                {{--url: '{{'user/responsable/create'}}',--}}

                                                {{--success: function (data) {--}}
                                                   {{--// console.log(data);--}}
                                                {{--},--}}
                                                {{--error: function (xhr, status, error) {--}}
                                                    {{--// check status && error--}}
                                                    {{--console.log(status);--}}
                                                {{--}--}}
                                            {{--});--}}
                                        {{--});--}}
                                    {{--</script>--}}
                                    @endpush

                                </div>
                            </div>
                        </div>
                    </div>
                     @endforeach
                @endforeach
            </div>
        </>
    </section>
    </div>
@endsection

