@if(count($commentsData) > 0)
<div >
    <table class="table table-bordered table-stripped">
    <h3 class="page-header">Commentaires</h3>
        {{ \Illuminate\Support\Facades\Log::Info($commentsData) }}
           
        <div class="container clearfix">
  <div class="row">
    <div class="col-md-12 clearfix">
      
        <section class="comment-list">
          
         
          @foreach($commentsData as $comment)
          <!-- Fourth Comment -->
          <article class="row" id="comments_{{$comment->id}}">
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thumbnail">
                @if(!empty($comment->User->profile_image))
                  <img class="img-responsive" src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $comment->User->profile_image}}"   />

                @else
                  <img  class="img-responsive" src="{{ url('public/front1/images/avtar.jpg')}}" />
                @endif
                <figcaption class="text-center">{{$comment->User->name}}</figcaption>
              </figure>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="panel panel-default arrow left">
                <div class="panel-body comment-arrow">
                  <header class="text-left">
                    <div class="comment-user"><i class="fa fa-user fa-fw" ></i> {{$comment->User->name}}</div>
                      <?php $new_date =  $comment->created_at->format('d-M-Y');
                      $newDt = Carbon\Carbon::parse($new_date)->timezone('Europe/Paris')->format('d-M-Y');
                      ?>
                    <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o fa-fw"></i> {{$newDt}}</time>
                  </header>
                  <div class="comment-post">
                    <p>
                      {{$comment->comment}}
                    </p>
                  </div>

                        <div class="comment-like">
                            <?php $likedcounter = \App\Models\LikeComments::where('user_id', Auth::user()->id)->where('comment_id',$comment->id)->where('counter',1)->first();?>
                            @if(!empty($likedcounter))
                            <div class="like-div" id="like_div_{{$comment->id}}">
                                <i class="fa fa-thumbs-up fa-fw like" style="color:#00a3b4;" id="{{$comment->id}}" ></i>
                            </div>
                            @else <div class="like-div" id="like_div_{{$comment->id}}">
                                    <i class="fa fa-thumbs-up fa-fw like" style="color:#D3D3D3;" id="{{$comment->id}}" ></i>
                                </div>
                            @endif
                            <p id="counter_{{$comment->id}}" class="display-counter" >
                                  @if( ($comment->LikeComments->where('counter',1)->count() ) !== 0 )
                                      {{ $comment->LikeComments->where('counter',1)->count()}}
                                  @endif
                              </p>

                  </div>
                </div>
              </div>
            </div>
          </article>

          @endforeach
          
        </section>
    </div>
  </div>
</div>
    </table>
</div>
@endif

@push('script')
<script>

    $(document).ready( function() {

        $(document).on('click', ".like", function (event) {
               var id = $(this).attr('id');
               var old_counter = $("#counter_"+id).text();

            var path = app.config.SITE_PATH +'like_comments';
            $.ajax({
                cache: false,
                url: path,
                type: 'POST',
                data: {id:id},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (response) {console.log(response);
                    if (response) {

                        var responsecounter = parseInt(response.counter);

                        if (responsecounter == 0){
                            $("#counter_"+ id).css("opacity", 0);
                            $("#like_div_"+id).children().css("color", "#D3D3D3");
                            $("#counter_"+id).html(response.counter);
                        }else{
                            $("#counter_"+ id).css("opacity", 1);

                            if( responsecounter > old_counter ){

                               $("#like_div_"+id).children().css("color", "#00a3b4");
                            }else{
                                $("#like_div_"+id).children().css("color", "#D3D3D3");
                                 }
                            $("#counter_"+id).html(response.counter);

                        }
                    }

                }
            });
        });
    });

</script>
@endpush