@extends('layouts.front1.app')
@push('css')
<style>
    .category-title{background-color: orange; color: white; padding: 15px; border-radius: 0px;}

</style>
@endpush
@section('content')

        <div class="col-sm-10">
            <div class="col-md-12">

                <li class="btn_ajtr">
                    <a href="{{ url()->previous() }}"><i class="fa fa-arrow-left left_arrow_back"></i>Retour</a>
                </li>

            </div>

        <section class="category_modules" id="myGroup">
            @include('front1.module_detail',["moduleid_data"=>$moduleid_data,"category_id"=>$category_id])
        </section>
        <div class="container">
            <div class="row">

            <div class="comment">
                <div class="col-md-12 all_comments">
                    @include('front1.comments',["comments"=>$comments])
                </div>
                <form class="form-horizontal" id="comment_form" role="form" action="javascript:void(0);" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="category_id" name="category_id" value="{{$category_id}}" />
                    

                   
                    <div class = "col-md-12">
                     <div class="addcomment">
                        <h3 class="page-header">Ajouter un commentaire</h3>
                        <div class="form-group" {{ $errors->has('comment') ? ' has-error' : '' }}>
                            <div class="col-md-12">
                                <textarea name="comment" id="comment" class="form-control required" cols="100" style="resize: none" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 ">
                                <button class="btn btn-primary right" id="comment_button"  type="submit">
                                    Poster
                                </button>
                                
                            </div>
                        </div>
                    </div></div>
                </form>

            </div>

        </div>
        </div>
  <div class="col-sm-12">
     <section class="margin-30">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
        <div class="col-md-12">
            <div class="row">
                <?php $bookmark_id = $getDataofAllUserCategory["bookmark_id"];
                    //dd($aftercommentdivdata);
                ?>


                <div class="col-md-6">
                    <div class="col-md-12 no-padding">
                                <h2 class="part-title category-title " style="text-align:center;font-weight:bold;">Mes Modules
                                </h2>
                        </div>

                    <?php // $aftercommentdivdata = \App\Helpers\LaraHelpers::partition($aftercommentdivdata,2)?>
                    @foreach($getDataofAllUserCategory[ "publicFormationData"]  as $publicationformation)

                    <div class="col-md-12 no-padding">
                        @if(!empty($publicationformation['id']))
                        <div class="panel-group" id="accordion{{$publicationformation['id']}}">
                            <div class="panel panel-default">
                                <div class="panel-heading fa_fa_color">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle collapsed" id="idforremoveafter" data-toggle="collapse"
                                           data-parent="#accordion" >
                                            {{--<span class="text_heading category-name">{{$publicationformation['name']}}</span>--}}
                                            <a class="Blanc get_category" href="javascript:void(0);" role="{{$publicationformation['id']}}"> <span class="text_heading category-name">{{$publicationformation['name']}}</span></a>
                                            <?php $categoryLikeStatus = \App\Helpers\LaraHelpers::likeStatus($publicationformation['id']);
                                            $totalLike = \App\Helpers\LaraHelpers::likeCount($publicationformation['id']);?>
                                            <i class="fa fa-thumbs-up" style="padding-left:5px;padding-right:3px;"></i><span class="like_counter">{{$totalLike}}</span>

                                            <span>
                                            @if(Auth::user()->user_type == 3 && $publicationformation['category_type_id'] == 1)

                                             <i class="fa fa-user get_users star-bookmark-b" id="{{$publicationformation['id']}}" data-parent_id="{{$publicationformation['id']}}" data-id="F" style="display:none;//cursor: pointer;//color:red;margin-top: -14px" ></i>
                                            @endif
                                            {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{publicationformation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                            </span>

                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$publicationformation['id']}}_list" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                           <ul class="collapse-ul">
                                            @if(!empty($publicationformation['ChildArray']))
                                               @foreach($publicationformation['ChildArray'] as $childFormation)
                                               <li>
                                                   {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                   <div class="row">
                                                       <div class="col-md-10 col-sm-10 col-xs-10">
                                                           <a class="Blanc get_category" href="javascript:void(0);" role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                       </div>
                                                       <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span style="float: left;">
                                                                @if(Auth::user()->user_type == 3 && $publicationformation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b" id="{{$childFormation['id']}}" data-parent_id="{{$childFormation['id']}}" data-id="F" style="cursor: pointer;color:red"></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star formation_active star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="color: yellow;"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star-o star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                       </div>
                                                   </div>


                                               </li>
                                               @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 no-padding">
                        <h2 class="part-title category-title" style="text-align:center;font-weight:bold;">Ressources
                        </h2>
                    </div>
                        <?php // $aftercommentdivdata = \App\Helpers\LaraHelpers::partition($aftercommentdivdata,2)?>
                        @foreach($getDataofAllUserCategory[ "publicResourceData"]  as $publicationformation)

                        <div class="col-md-12 no-padding">
                            @if(!empty($publicationformation['id']))
                            <div class="panel-group" id="accordion{{$publicationformation['id']}}">
                                <div class="panel panel-default">
                                    <div class="panel-heading fa_fa_color">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" id="idforremoveafter" data-toggle="collapse"
                                               data-parent="#accordion" >
                                                {{--<span class="text_heading category-name">{{$publicationformation['name']}}</span>--}}
                                                <a class="Blanc get_category" href="javascript:void(0);" role="{{$publicationformation['id']}}"> <span class="text_heading category-name" >{{$publicationformation['name']}}</span></a>
                                                <?php $categoryLikeStatus = \App\Helpers\LaraHelpers::likeStatus($publicationformation['id']);
                                                $totalLike = \App\Helpers\LaraHelpers::likeCount($publicationformation['id']);?>
                                                <i class="fa fa-thumbs-up" style="padding-left:5px;padding-right:3px;"></i><span class="like_counter">{{$totalLike}}</span>
                                                <span>
                                            @if(Auth::user()->user_type == 3 && $publicationformation['category_type_id'] == 1)

                                             <i class="fa fa-user get_users star-bookmark-b" id="{{$publicationformation['id']}}" data-parent_id="{{$publicationformation['id']}}" data-id="F" style="display:none;//cursor: pointer;//color:red;margin-top: -14px" ></i>
                                            @endif
                                            {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{publicationformation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                            </span>

                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$publicationformation['id']}}_list" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="panel-body">
                                            <ul class="collapse-ul">
                                                @if(!empty($publicationformation['ChildArray']))
                                                @foreach($publicationformation['ChildArray'] as $childFormation)
                                                <li>
                                                    {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                    <div class="row">
                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                            <a class="Blanc get_category" href="javascript:void(0);" role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span style="float: left;">
                                                                @if(Auth::user()->user_type == 3 && $publicationformation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b" id="{{$childFormation['id']}}" data-parent_id="{{$childFormation['id']}}" data-id="F" style="cursor: pointer;color:red"></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star formation_active star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="color: yellow;"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star-o star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                        </div>
                                                    </div>


                                                </li>
                                                @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
            </div>
        </div>
     </section>
  </div>
    </div>

    <a href="{{url('/index')}}" id="back-to-home" title="Back to Home">Accueil</a>
    <a href="#" id="back-to-top" title="Back to top">Haut de page </a>


@endsection
@push('external_script')
<script src="{{url('public/js/jquery.validate.min.js')}}"></script>
@endpush
@push('script')
<script>

    $("#comment_form").validate();

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 500);
        });
    }

    function hover(description) {
        document.getElementById('category_name').innerHTML = description;
    }
    function leave() {
        document.getElementById('category_name').innerHTML = "";
    }

    $('#comment_form').on('submit', function(event) {
        event.preventDefault();
        event.stopPropagation();
        if(!$("#comment_form").valid()){
            return false;
        }
        var form_data = new FormData(this);
        var comment_counter=0;
        $('#comment_button').attr('disabled',true);
        $.ajax({
            cache:false,
            url: '{{url('store_comment')}}',
            type: "POST",
            data: form_data,
            contentType: false,
            processData:false,
            success: function(response)
            {
                if(response.statusCode == '1'){
                    $(".all_comments").html(response.data);
                    $('#comment_button').attr('disabled',false);
                    comment_counter=$("#commentsul").children('a').length;
                    if(comment_counter==5){
                        comment_counter=5;
                    }else{
                        comment_counter++;
                    }
                    $("#commentcounter").html(comment_counter);
                    $("#ajaxloadeddiv").html(response.commentajaxdata);
                }
                $("#comment").val("");
            }
        });

    });

    var $myGroup = $('#myGroup');
    $myGroup.on('show.bs.collapse','.collapse', function() {
        $myGroup.find('.collapse.in').collapse('hide');
    });

    $(document).ready(function(){
        $(document).on('click','.get_category',function () {
            var get_cat_ids = $(this).closest(".panel.panel-default").find(".get_category").map(function () {
                return $(this).attr('role');
            }).get();

            window.location = app.config.SITE_PATH + 'details/'+$(this).attr('role')+'/'+ get_cat_ids.join(':')+'#category_'+$(this).attr('role');

        });
    });

</script>
@endpush