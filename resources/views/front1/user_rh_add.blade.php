@extends('layouts.front1.app')
@inject('Enterprise','App\Models\Entreprise')
@inject('User','App\Models\User')
@section('content')

<?php //$e_id =0;
////
//if(isset($_GET['id'])){
//    $e_id=($_GET['id']); echo $e_id;
//}


    $parent_ent_id = 0;
    $e_id =    Request::segment(4);
    $parent_id =  \App\Models\Entreprise::select('parent_id')->where('id',$e_id)->get();
    foreach($parent_id as $i){
        $parent_ent_id = ($i->parent_id);
    }

    ?>
<form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('userRHDelete') }}">
    {!! csrf_field() !!}
    <input type="hidden" value="" name="id" id="delete_id"  />
</form>

    <div class="">
        <div class="col-sm-10">
            <div class="row">

                <div class="col-md-6">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>Ajouter un responsable </h1>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                   {{-- <div class="panel-heading">
                        <div class="title-heading">
                            <h1>{{ __('probtp.module_add', ['module' => __('probtp.user')]) }}</h1>
                        </div>
                    </div>--}}
                    <div class="panel-body">
                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('userStore') }}">--}}
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('userRHStore') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">
                            <input type="hidden" name="front_user" value="1">


                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">{{ __('probtp.first_name') }}</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="{{ __('probtp.first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">{{ __('probtp.last_name') }}</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="{{ __('probtp.last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>






                                {{--<label for="user_type" class="col-md-4 control-label">{{ __('probtp.user')." ".__('probtp.type')}}</label>--}}
                                <input type="hidden" name="user_type" id="user_type"  value="{{Auth::user()->user_type}}" />
                                {{--<div class="col-md-6">--}}
                                    {{--<select type class="form-control" id="user_name"  >--}}
                                       {{--@if(Auth::user()->user_type == 3)--}}
                                            {{--<option >Responsable RH</option>--}}
                                       {{--@endif--}}
                                    {{--</select>--}}

                                {{--</div>--}}


                             <input type="hidden" id="entreprise_id" name="entreprise_id"  value="{!!    $e_id  !!}" />

                            <input type="hidden" name="parent_id" value="{{ $parent_ent_id }}  ">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('probtp.email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">{{ __('probtp.password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="{{ __('probtp.password') }}" required autofocus>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                <label for="confirm_password" class="col-md-4 control-label">{{ __('probtp.confirm_password') }}</label>

                                <div class="col-md-6">
                                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}" placeholder="{{ __('probtp.confirm_password') }}" required autofocus>

                                    @if ($errors->has('confirm_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                    &nbsp; &nbsp;
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                         <div class="panel-heading">
                             <div class="title-heading">
                                 <h1>Liste des responsables</h1>
                             </div>
                         </div>
                        <?php  $users = \App\Models\User::where('user_type',3)->where('entreprise_id', $e_id)->get(); ?>
                        <div class="panel-body">
                            <table class="table table-striped module_list  table-bordered" cellspacing="0" id="">

                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="no-sort">Nom</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody class="module_category" style="cursor: move">
                                <div style="display: none!important;"> {{$i=1}}</div>
                                <?php $i = 1;?>
                                @foreach($users as $user)

                                    <tr>
                                        <td >{{$i}} </td>
                                        <td>{{$user->name}}</td>
                                        <td>
                                            <a href="{{route('userRHEdit',['id'=>$user->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i></a>
                                            <a href="javascript:void(0)" data-id ="{{$user->id}}"  class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a>

                                            {{--<a href="{{route('userRHDelete',['id'=>$user->id])}}"    class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a>--}}
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach

                                </tbody>
                                <input type="hidden" id="savebutton" value="1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
{{--<script src="{{ asset('public/multiselect/select2.min.js') }}"></script>--}}
{{--<link href="{{ asset('public/multiselect/select2.css') }}" rel="stylesheet">--}}
<script>
    $(document).ready(function () {
        app.delete('Êtes-vous sûr de vouloir supprimer définitivement ce responsable');
    });

</script>
@endpush
