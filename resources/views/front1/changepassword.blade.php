@extends('layouts.front1.app')
@push('css')
<style>
    .panel.panel-default{border: none;}
</style>
@endpush
@section('content')
    <div class="">
        <div class="col-sm-12 col-md-10 "  >
            <div class="row">

                <div class="col-md-12">
                    <div class="panel-heading" id="change_password">
                        <div class="title-heading">
                            <h1>{{  __('probtp.password_change') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    {{--<div class="panel-heading">
                        <div class="title-heading">
                            <h1>{{  __('probtp.password_change') }}</h1>
                        </div>
                    </div>--}}

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('updatePassword') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}" />
                            <!--<input type="hidden" name="dbAction" value="Edit" />-->

                            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                <label for="old_password" class="col-md-4 control-label">{{ __('probtp.current_password') }}</label>

                                <div class="col-md-6">
                                    <input id="old_password" type="password" class="form-control" name="old_password" value="" placeholder="{{ __('probtp.current_password') }}" required autofocus>

                                    @if ($errors->has('old_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">{{ __('probtp.new_password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="" placeholder="{{ __('probtp.new_password') }}" required autofocus>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                <label for="confirm_password" class="col-md-4 control-label">{{ __('probtp.confirm_password') }}</label>

                                <div class="col-md-6">
                                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" value="" placeholder="{{ __('probtp.confirm_password') }}" required autofocus>

                                    @if ($errors->has('confirm_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="{{Auth::user()->id}}" />


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                    &nbsp; &nbsp;
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
