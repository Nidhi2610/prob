@extends('layouts.front1.app')
    @inject('ColloCat','App\Models\CollaborateurCategory')
@push('css')
<style>
    .margin-30{margin: 15px;}
</style>
@endpush
@section('content')
    <div class="col-sm-12 col-md-10" >
   <section class="margin-30">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <ul class="mon-li">
                        <li>
                            <h1>Mon équipe</h1>
                        </li>
                        <li class="btn_ajtr">
                            <a href="{{url('user/create')}}">+ Ajouter un collaborateur</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    {{--<div class="mon-tab">
                        <div class="tabbable-panel">
                            <div class="tabbable-line">
                                <ul class="nav nav-tabs ">
                                    <li class="active">
                                        <a href="#tab_default_1" data-toggle="tab">
                                            <span>Liste</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab_default_2" data-toggle="tab">
                                            <span>Fonction</span>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="row">

                    @foreach($data as $user)
                    <div class="col-md-6 col-sm-6 col-lg-4">
                    <div class="username_mon">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="adress">
                                    <div>
                                        <h4>{{$user->first_name}}</h4>
                                        <h4>{{$user->last_name}}</h4>
                                    </div>
                                    <div>
                                        <span>{{$user->email}}</span>
                                    </div>
                                    <div>
                                        <span>@if($user->user_type == 4) Collaborateur @endif</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="user">
                                    @if(!empty($user->profile_image))
                                        <img src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $user->profile_image }}"
                                             style="" class="moin-image"  />
                                    @else
                                        <img src="{{ url('public/front1/images/noimage.png')}}" style="" class="moin-image"/>
                                    @endif

                                </div>
                            </div>

                        </div>
                        <div class="module-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="Modules">
                                        <div>
                                            <span>Modules suivis</span>
                                        </div>
                                        <div>
                                            <span class="review">{{$user->ModuleQuiz->count()}}</span>
                                            <span class="read">/{{$ColloCat->getAllColloborateurFormationResource($user->id) + $getpublicFormationResourcedata}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="Commentaires">
                                        <div>
                                            <span>Commentaires</span>
                                        </div>
                                        <div>
                                            {{$user->Comments->count()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="denniel-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="dennied-bor">
                                        <div class="Commentaires">
                                            <span>Dernière connexion</span>
                                        </div>
                                        <div>
                                            {{--{{$user->created_at->format('d-M-Y')}}--}}
                                            {!! (strftime('%e %B %Y' , strtotime ($user->created_at->format('d-M-Y'))) )  !!}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="icon">
                                    <li>
                                        <a href="javascript:void(0)" onclick="deletedata(this.id);" id ="{{$user->id}}" class="member-delete delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{url('user/edit/'.$user->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach

                <div class="col-md-4 col-md-6 col-sm-6">
                    <div class="username_mon_add">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="add-mem">

                                    <div class="add-membre">
                                        <a href="{{url('user/create')}}">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <span>
                                                  Ajouter un collaborateur
                                              </span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    </div>
@endsection
@push('script')

<script>


    function deletedata (id) {
        var confirmstatus = confirm("Êtes-vous sûr de vouloir supprimer ?");
        if(confirmstatus){
            $.ajax({

                data: {id: id},
                type: 'POST',
                url: '{{'user/destroy'}}',
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    window.location = '{{'userlist'}}';
                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
        }
    }
</script>
@endpush
