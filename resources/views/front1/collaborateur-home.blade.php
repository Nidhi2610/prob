@extends('layouts.front1.app')
@push('css')
<style>
    .formation_active{color: gold;}
    .font-icon{font-family: btp_icons}
    .i-class{margin-right: 10px;float: left;}
</style>
@endpush
@section('content')
    @inject('categoryObj', 'App\Models\Category')
    <div class="container-fluid">
        <div class="row">
    <div class="col-sm-10 col-header-nopadding">
        <section class="pro-bg-img">
            <div>
                <h1>Mon offre de formation</h1>
            </div>
            <div>
                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs category-tab">
                            <li class="active" >
                                <a href="#tab_default_1" data-toggle="tab" data-img="bg1.jpg">
                                    <span class="Fill-1 font-icon" ></span>
                                    <span class="Mes-modules">Mes Modules</span>
                                    {{--<span class="oval-2 formation_count">{{$bookmark_count}}</span>--}}
                                </a>
                            </li>
                            <li>
                                <a href="#tab_default_2" data-toggle="tab" data-img="bg2.jpg">
                                    <span class="Fill-1 font-icon" style="font-size: 22px;"></span>
                                    <span class="Mes-modules">Statistiques</span>
                                    {{--<span class="badge">0</span>--}}
                                </a>
                            </li>
                            <li>
                                <a href="#tab_default_3" data-toggle="tab" data-img="bg3.jpg">
                                    <span class="Fill-1 font-icon" style="font-size: 23px;"></span>
                                    <span class="Mes-modules">Ressources</span>
                                    {{--<span class="badge">0</span>--}}
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>

    <div class="col-sm-12">
    <section>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_default_1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                <div class="first-text">
                    <p>
                        Vous pouvez consultez le détail d'un thème, l'ajoutez à votre offre de formation
                    </p>
                </div>
                    </div>
                    </div>
            </div>

                            <div class="row">

                                <!-- for Private Formation Data Starts !-->

                                @if(count($finalFormationParentIdData) > 0)
                                @foreach($finalFormationParentIdData as $formation)

                                    <?php
                                    $getchildFormationData = \App\Helpers\LaraHelpers::getParentData('parent_id',[$formation['id']]);
                                    $findChildFormationArray=[];
                                    if(count($getchildFormationData)>0){
                                        foreach ($getchildFormationData as $childFormationData){
                                            $findChildFormationArray[]= $childFormationData['id'];
                                            $getSubchildFormationData = \App\Helpers\LaraHelpers::getParentData('parent_id',[$childFormationData['id']]);
                                            if(count($getSubchildFormationData)>0){
                                                foreach ($getSubchildFormationData as $childSubFormationData){
                                                    $findChildFormationArray[]= $childSubFormationData['id'];
                                                }
                                            }
                                        }
                                    }
                                    $getFinalFormationChild = array_intersect($assignedFormationCategoryIds,$findChildFormationArray);
                                    $getFinalChildFormationData = \App\Helpers\LaraHelpers::getParentData('id',$getFinalFormationChild);
                                    ?>
                                    <div class="col-md-6">
                                        @if(!empty($formation['id']))
                                            <div class="panel-group" id="accordion{{$formation['id']}}">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading fa_fa_color">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle" data-toggle="collapse"
                                                               data-parent="#accordion" href="#collapse{{$formation['id']}}">
                                                                <i  class="fa {{$formation['font_awesome']}} i-class" aria-hidden="true"></i>
                                                                <a class="Blanc get_category" href="javascript:void(0);" role="{{$formation['id']}}"> <span class="text_heading category-name">{{$formation['name']}}</span></a>
                                                                <span>
                                                                {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category" role="{{$formation['id']}}" aria-hidden="true"></i></a>--}}
                                                                </span>
                                                            </a>

                                                        </h4>
                                                    </div>
                                                    <div id="collapse{{$formation['id']}}" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <ul class="collapse-ul">
                                                                @if(count($getFinalChildFormationData)>0)
                                                                    @foreach($getFinalChildFormationData as $childFormation)
                                                                        <li>
                                                                        <div class="row">
                                                                            <div class="col-md-10 col-sm-10 col-xs-10">
                                                                                <a class="Blanc get_category star_bookmark" id="{{$childFormation['id']}}" href="javascript:void(0);" role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span style="float: left;">
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star formation_active star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true"></i>
                                                                       <?php }else{ ?>
                                                                       <i class="fa fa-star-o star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                                        </span>
                                                                            </div>
                                                                        </div>
                                                                        </li>

                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        @endif
                                        <!-- for Private Formation Data Ends !-->


                                    </div>

                                    @endforeach
                                    @endif


                                <?php $publicFormationData = \App\Helpers\LaraHelpers::partition($publicFormationData,2);?>
                                <div class="col-md-6">
                                    @foreach($publicFormationData[0] as $formation)

                                        <div class="col-md-12 no-padding">
                                            @if(!empty($formation['id']))
                                                <div class="panel-group" id="accordion{{$formation['id']}}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading fa_fa_color">
                                                            <h4 class="panel-title">
                                                                <a class="accordion-toggle" data-toggle="collapse"
                                                                   data-parent="#accordion" href="#collapse{{$formation['id']}}">
                                                                    <i  class="fa {{$formation['font_awesome']}} i-class" aria-hidden="true"></i>
                                                                    {{--<span class="text_heading category-name">{{$formation['name']}}</span>--}}
                                                                    <a class="Blanc get_category" href="javascript:void(0);" role="{{$formation['id']}}"> <span class="text_heading category-name">{{$formation['name']}}</span></a>

                                                                    <span>
                                            @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)

                                                                            <i class="fa fa-user get_users star-bookmark-b cursor-pointer margin" id="{{$formation['id']}}" data-parent_id="{{$formation['id']}}" data-id="F"  ></i>
                                                                        @endif
                                                                        {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$formation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                            </span>

                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$formation['id']}}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <ul class="collapse-ul">
                                                                    @if(!empty($formation['ChildArray']))
                                                                        @foreach($formation['ChildArray'] as $childFormation)
                                                                            <li>
                                                                                {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                                                <div class="row">
                                                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                                                        <a class="Blanc get_category star_bookmark" id="{{$childFormation['id']}}" href="javascript:void(0);" role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span style="float: left;">
                                                                @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b cursor-pointer" id="{{$childFormation['id']}}" data-parent_id="{{$childFormation['id']}}" data-id="F" ></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star formation_active star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star-o star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                                                    </div>
                                                                                </div>


                                                                            </li>
                                                                        @endforeach
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>


                                    @endforeach
                                </div>

                                <div class="col-md-6">
                                    @foreach($publicFormationData[1] as $formation)

                                        <div class="col-md-12 no-padding">
                                            @if(!empty($formation['id']))
                                                <div class="panel-group" id="accordion{{$formation['id']}}">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading fa_fa_color">
                                                            <h4 class="panel-title">
                                                                <a class="accordion-toggle" data-toggle="collapse"
                                                                   data-parent="#accordion" href="#collapse{{$formation['id']}}">
                                                                    <i  class="fa {{$formation['font_awesome']}} i-class" aria-hidden="true"></i>
                                                                    {{--<span class="text_heading category-name">{{$formation['name']}}</span>--}}
                                                                    <a class="Blanc get_category" href="javascript:void(0);" role="{{$formation['id']}}"> <span class="text_heading category-name">{{$formation['name']}}</span></a>

                                                                    <span>
                                            @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)

                                                                            <i class="fa fa-user get_users star-bookmark-b cursor-poiner margin" id="{{$formation['id']}}" data-parent_id="{{$formation['id']}}" data-id="F" ></i>
                                                                        @endif
                                                                        {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$formation['id']}}" aria-hidden="true" style="margin-top: -22px"></i></a>--}}
                                            </span>

                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse{{$formation['id']}}" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <ul class="collapse-ul">
                                                                    @if(!empty($formation['ChildArray']))
                                                                        @foreach($formation['ChildArray'] as $childFormation)
                                                                            <li>
                                                                                {{--<span class="category-name">{{$childFormation['name']}} </span>--}}
                                                                                <div class="row">
                                                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                                                        <a class="Blanc get_category" href="javascript:void(0);" role="{{$childFormation['id']}}"> {{$childFormation['name']}}</a>
                                                                                    </div>
                                                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                                   <span style="float: left;">
                                                                @if(Auth::user()->user_type == 3 && $formation['category_type_id'] == 1)
                                                                           <i class="fa fa-user get_users star-bookmark-b cursor-pointer" id="{{$childFormation['id']}}" data-parent_id="{{$childFormation['id']}}" data-id="F" ></i>
                                                                       @endif
                                                                       {{-- <a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category star-bookmark" role="{{$childFormation['id']}}" aria-hidden="true"></i></a>--}}
                                                                       <?php if(in_array($childFormation['id'], $bookmark_id)){ ?>
                                                                       <i class="fa fa-star formation_active star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true"></i>
                                                                       <?php }else{ ?>


                                                                       <i class="fa fa-star-o star-bookmark-a" id="{{$childFormation['id']}}" aria-hidden="true" style="cursor: pointer"></i></a>
                                                                       <?php }?>
                                                        </span>
                                                                                    </div>
                                                                                </div>


                                                                            </li>
                                                                        @endforeach
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            @endif
                                        </div>


                                    @endforeach
                                </div>

                            </div>
            </div>



            <div class="tab-pane resource-tab" id="tab_default_3">
                <div class="container">
                <div class="row">

                    <!-- for Private Resource Data Starts !-->

                    @if(count($finalParentIdData)>0)
                    @foreach($finalParentIdData as $resource)
                        <?php
                            $getchildData = \App\Helpers\LaraHelpers::getParentData('parent_id',[$resource['id']]);
                            $findChildArray=[];
                             if(count($getchildData)>0){
                                 foreach ($getchildData as $childData){
                                     $findChildArray[]= $childData['id'];
                                     $getSubchildData = \App\Helpers\LaraHelpers::getParentData('parent_id',[$childData['id']]);
                                     if(count($getSubchildData)>0){
                                         foreach ($getSubchildData as $childSubData){
                                             $findChildArray[]= $childSubData['id'];
                                         }
                                     }
                                 }
                             }
                        $getFinalChild = array_intersect($assignedCategoryIds,$findChildArray);
                        $getFinalChildData = \App\Helpers\LaraHelpers::getParentData('id',$getFinalChild);
                        ?>
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion{{$resource['id']}}">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <div class="resources">
                                            <a href="javascript:void(0);" class="Blanc get_category" role="{{$resource['id']}}"><span class="resource_heading">{{$resource['name']}}</span> </a>
                                            @if(Auth::user()->user_type == 3 && $resource['category_type_id'] == 1)
                                                <i class="fa fa-user get_users" id="{{$resource['id']}}" data-id ="R"></i>
                                            @endif
                                        </div>
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$resource['id']}}">

                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$resource['id']}}" class="panel-collapse collapse resource-pad">
                                    <div class="panel-body">
                                        @if(count($getFinalChildData) > 0)
                                        <div class="row">
                                            @foreach($getFinalChildData as $resourceChild)
                                            <div class="col-md-6">
                                                <div class="dum">
                                                    <i class="fa fa-file-text-o" aria-hidden="true"></i> <a class="Blanc get_category" href="javascript:void(0);" role="{{$resourceChild['id']}}"><span>{{$resourceChild['name']}}</span></a>
                                                    {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category" role="{{$resourceChild['id']}}" aria-hidden="true"></i></a>--}}
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                            @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                        @endif

                <!-- for Private Formation Data Ends !-->
                    @foreach($publicResourceData as $resource)
                        <div class="col-md-12">
                            <div class="panel-group" id="accordion{{$resource['id']}}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <div class="resources">
                                                <a href="javascript:void(0);" class="Blanc get_category" role="{{$resource['id']}}"><span class="resource_heading">{{$resource['name']}}</span> </a>
                                                @if(Auth::user()->user_type == 3 && $resource['category_type_id'] == 1)
                                                    <i class="fa fa-user get_users" id="{{$resource['id']}}" data-id ="R"></i>
                                                @endif
                                            </div>
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$resource['id']}}">

                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{$resource['id']}}" class="panel-collapse collapse resource-pad">
                                        <div class="panel-body">
                                            @if(isset($resource['ChildArray']) && count($resource['ChildArray']) > 0)
                                                <div class="row">
                                                    @foreach($resource['ChildArray'] as $resourceChild)

                                                        <div class="col-md-6">
                                                            <div class="dum">
                                                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <a class="Blanc get_category" href="javascript:void(0);" role="{{$resourceChild['id']}}"><span>{{$resourceChild['name']}}</span></a>
                                                                @if(Auth::user()->user_type == 3 && $resource['category_type_id'] == 1)
                                                                    <i class="fa fa-user get_users" id="{{$resourceChild['id']}}"  data-id ="R"></i>
                                                                @endif
                                                                {{--<a class="Blanc" href="javascript:void(0);"><i class="fa fa-eye get_category" role="{{$resourceChild['id']}}" aria-hidden="true"></i></a>--}}
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    @endforeach

                </div>
                </div>
            </div>

        </div>
    </section>
    </div>

    </div>
@endsection
@push('script')
<script>
    $(document).ready(function () {

        $(document).on('click',".category-tab li a", function () {
            var img_path = app.config.SITE_PATH + 'public/front1/images/'+$(this).attr('data-img');
            $(".pro-bg-img").css("background-image",'url('+img_path+')');
        });

        $(document).on('click','.get_category',function () {
            var get_cat_ids = $(this).closest(".panel.panel-default").find(".get_category").map(function () {
                return $(this).attr('role');
            }).get();

            window.location = app.config.SITE_PATH + 'details/'+$(this).attr('role')+'/' + get_cat_ids.join(':')+'#category_'+$(this).attr('role');

        });

        // For Bookmarking the Category
        $(document).on('click','.star_bookmark',function () {
            var category_bookmark_id = $(this).attr('id');

            if($(this).hasClass("formation_active")){
                $(this).removeClass("formation_active fa-star").addClass('fa-star-o');
                var url = app.config.SITE_PATH+'/remove_bookmark';
            }else{
                $(this).addClass("formation_active fa-star").removeClass('fa-star-o');
                var url = app.config.SITE_PATH+'/bookmark';
            }

            $.ajax({
                data: {category_bookmark_id:category_bookmark_id},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    console.log(data);
                    if(data.statusCode == 1){
                        $(".category-tab .active").find(".formation_count").html(data.bookmark_count);
                    }
                    var updated_bookmark = data.bookmark_count;
                    if(updated_bookmark > 0){
                        $('#training_text').hide()
                    }else{
                        $('#training_text').show()
                    }

                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
        });

        // For showing and hiding  .first-text div
        var current_bookmark =  <?php echo $bookmark_count ?>;
        if(current_bookmark == 0){
            $('#training_text').show()
        }else{
            $('#training_text').hide()
        }
    });
</script>
@endpush
