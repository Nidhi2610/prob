@extends('layouts.front1.app')
@push('css')
@endpush
@section('content')
    @inject('Entcategor','App\Models\EntrepriseCategory')
    <div class="col-sm-12 col-md-10" >
   <section class="margin-30 ">
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-heading" id="community" >
                        <div class="title-heading">
                            <h1>Ma communauté</h1>
                        </div>
                    </div>
                </div>
             </div>
            <div class="row">
                    @foreach($communityData as $user)
                    <div class="col-md-6 col-sm-6 col-lg-4">
                    <div class="username_mon">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="adress">
                                    <div>
                                        <h4>{{$user->first_name}}</h4>
                                        <h4>{{$user->last_name}}</h4>
                                    </div>
                                    <div>
                                        <span>{{$user->email}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="user">
                                    @if(!empty($user->profile_image))
                                        <img class="home-profile-image moin-image" src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $user->profile_image }}" />
                                    @else
                                        <img class="home-profile-image moin-image" src="{{ url('public/front1/images/noimage.png')}}"/>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="module-border-index">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="Modules">
                                        <div>
                                            <span>Modules suivis</span>
                                        </div>
                                        <div>
                                            <span class="review">{{$user->ModuleQuiz->count()}}
                                            </span>
                                            <span class="read">/{{$Entcategor->getAllPrivateFormationResource($user->entreprise_id) + $getpublicFormationResourcedata}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="Commentaires">
                                        <div>
                                            <span>Commentaires</span>
                                        </div>
                                        <div>
                                            0
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="denniel-border">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="dennied-bor">
                                        <div class="Commentaires">
                                            <span>Dernière connexion</span>
                                        </div>
                                        <div>
                                           {{-- {{$user->created_at->format('d-M-Y')}}--}}
                                            {!! (strftime('%e %B %Y' , strtotime ($user->created_at->format('d-M-Y'))) )  !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row">
                            <div class="col-md-12">
                                <ul class="icon">
                                    <li>
                                        <a href="javascript:void(0)" onclick="deletedata(this.id);" id ="{{$user->id}}" class="member-delete delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{url('user/edit/'.$user->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>--}}
                    </div>
                </div>
                    @endforeach

                {{--<div class="col-md-4">
                    <div class="username_mon_add">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="add-mem">
                                              <span>
                                                  Ajouter un membre
                                              </span>
                                    <div class="add-membre">
                                        <a href="{{url('user/create')}}">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>

        </div>
    </section>
    </div>
@endsection
@push('script')
<script>
    function deletedata (id) {

        var confirmstatus = confirm("Êtes-vous sûr de vouloir supprimer ?");
        var url = app.config.SITE_PATH+'/user/destroy';
        if(confirmstatus){
            $.ajax({

                data: {id: id},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    window.location = app.config.SITE_PATH+'/user';
                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });
        }
    }
</script>
@endpush
