@extends('layouts.front1.app')
@push('css')
<style>
    .panel.panel-default{border: none;}
    .class-pl-zero{padding-left: 0;}
</style>
@endpush
@section('content')
    <div class="">
        <div class="col-sm-12 col-md-10 " >
        <div class="row">

            <div class="col-md-6">
                <div class="panel-heading" id="myprofile">
                    <div class="title-heading">
                        <h1>Ma communauté</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">


                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('userAccountUpdate') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}" />
                            <input type="hidden" name="dbAction" value="Edit" />

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">{{ __('probtp.first_name') }}</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $editData->first_name }}" placeholder="{{ __('probtp.first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">{{ __('probtp.last_name') }}</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $editData->last_name }}" placeholder="{{ __('probtp.last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <input type="hidden" name="id" id="id" value="{{Auth::user()->id}}" />

                            @if($editData->entreprise_id > 0 && Auth::user()->user_type == 3)

                                <div class="form-group">
                                    <label for="entreprise_id" class="col-md-4 control-label">{{ __('probtp.select')." ".__('probtp.entreprise')}}</label>

                                    <div class="col-md-6">
                                        <label for="entreprise_id" class="control-label">
                                            @foreach($entrepriseData as $row)
                                                @if($editData->entreprise_id == $row->id)
                                                    {{ trim($row->name) }}
                                                @endif
                                            @endforeach
                                        </label>

                                    </div>
                                </div>
                            @endif

                            <div class="form-group {{ $errors->has('profile_image') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.profile_image') }}</label>

                                <div class="col-md-6">
                                    <input id="profile_image" type="file" class="form-control" name="profile_image" value="{{ $editData->profile_image or "" }}"   autofocus>
                                    @if ($errors->has('profile_image'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('profile_image') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-sm-12 col-xs-12">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4 col-sm-12">
                                    @if(pathinfo($editData->profile_image, PATHINFO_EXTENSION) === 'jpg' || pathinfo($editData->profile_image, PATHINFO_EXTENSION) === 'png' || pathinfo($editData->profile_image, PATHINFO_EXTENSION) === 'jpeg' )
                                        <div class="col-sm-12 image_class class-pl-zero" >
                                            <?php if(isset($editData->profile_image)){$display_name = substr($editData->profile_image, strpos($editData->profile_image, "_") + 1); }
                                            $display_name = str_replace('_', ' ', $display_name);
                                            ?>
                                            <div class="class-pl-zero">
                                                <a href="{{url('storage/app/public/files/module/'.$editData->profile_image)}}" class="html5lightbox" data-width="750" data-height="450" title="{{$editData->original_name or $display_name}}">
                                                    <img class="my-profile-img" src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $editData->profile_image }}"   />
                                                </a>
                                            </div>

                                            {{-- <div class="col-sm-10" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>--}}

                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-4"></div>

                            </div>


                            <input type="hidden" id="original_old_filename" name="original_old_filename" value="{{ old('profile_image', $editData->profile_image)}}" />

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.email') }}</label>
                                <div class="col-md-6">
                                    <label for="email" class="control-label">{{ $editData->email }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                    &nbsp; &nbsp;

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
@push('css')

@endpush
