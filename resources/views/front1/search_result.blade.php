@extends('layouts.front1.app')
@section('content')

        <div class="col-sm-10">
            <div class="row">

                <div class="col-md-6">
                    <div class="panel-heading mar-bottom" >
                        <div class="title-heading">
                            <h3>Afficher les résultats pour : <i>{{$search_keyword}}</i> </h3>
                            <h4>Nombre de réponses :{{count($search_data)}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        <div>
            <div class="col-md-12">
                @if(count($search_data)>0)
                    @foreach($search_data as $search_module)

                            <div class="search_block">
                                <div class="search_title">
                                  <a href="{{url('/details/'.$search_module->category_id.'/'.$search_module->category_id.'#category_'.$search_module->category_id)}}"><h3>{{$search_module->name}}</h3></a>
                                </div>
                                <div class="search_content">
                                    <div class="dis-block">
                                        <i > Résumé: </i>{{$search_module->description}}<br>
                                        <i > Module :</i> {{$search_module->title}}<br>
                                    </div>
                                </div>

                            </div>

                    @endforeach

                @else
                    <h1>No Record Found</h1>
                @endif


            </div>
        </div>
        </div>

@endsection
