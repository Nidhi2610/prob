@if(count($comments) > 0)
<div >
    <table class="table table-bordered table-stripped">
    <h3 class="page-header">Comments</h3>
        
           
        <div class="container clearfix" style="overflow-y: auto; max-height: 450px;">
  <div class="row">
    <div class="col-md-12 clearfix">
      
        <section class="comment-list">
          
         
          @foreach($comments as $comment)
          <!-- Fourth Comment -->
          <article class="row">
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thumbnail">
                <img class="img-responsive" src="http://www.keita-gaming.com/assets/profile/default-avatar-c5d8ec086224cb6fc4e395f4ba3018c2.jpg" />
                <figcaption class="text-center">{{$comment->User->name}}</figcaption>
              </figure>
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
              <div class="panel panel-default arrow left">
                <div class="panel-body">
                  <header class="text-left">
                    <div class="comment-user"><i class="fa fa-user fa-fw"></i> {{$comment->User->name}}</div>
                    <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o fa-fw"></i> {{date('M d, Y', strtotime($comment->created_at))}}</time>
                  </header>
                  <div class="comment-post">
                    <p>
                      {{$comment->comment}}
                    </p>
                  </div>
                 
                </div>
              </div>
            </div>
          </article>
         
          @endforeach
          
        </section>
    </div>
  </div>
</div>



        
    </table>
</div>
@endif 

