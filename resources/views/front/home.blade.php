@extends('layouts.front.app')

@section('content')
    <div class = "container">
        <section>
            <div class = "row" style = "background-color:#eee;">
                @foreach($data as $key => $category)
                     <div id = "first-tab_{{$category['id']}}"class = "col-md-3 col-xs-12 col-sm-6">
                        <div class = "applicationsDisplay_{{$category['id']}} top-tab-border-first_{{$category['id']}}">
                            <div class = "">
                                <div class = "bottom-first_{{$category['id']}}" style="background:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}}">
                                    <div id="applications-image-haut-hover" class="applications-image-haut" style="display: none;">
                                        <img src="{{ url('public/front/images/header-blanc-hover.png')}}"/>
                                    </div>
                                    <div id="icone" class="applications-picto_{{$category['id']}}" style="display: block;">
                                        <i class="fa {{ ($category['font_awesome'] == "") ? 'fa-creative-commons' : $category['font_awesome'] }} image-ico_{{$category['id']}}"  aria-hidden="true"></i>
                                    </div>
                                    <div id="iconeAuSurvol" class="applications-picto" style="display: none;">
                                        <i class="fa {{ ($category['font_awesome'] == "") ? 'fa-creative-commons' : $category['font_awesome']}} image-ico_{{$category['id']}}" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="applications-title_{{$category['id']}}"> {{$category['name']}} </div>
                           {{-- <div id="resume" class="applications-resume_{{$category['id']}} title1_{{$category['id']}}">
                                Prix ou conformité ?
                            </div>--}}
                            <div class="applications-description_{{$category['id']}}">
                               {{$category['description']}}
                            </div>

                            <div id="bouton" class="applications-bouton_{{$category['id']}} button-first_{{$category['id']}} ">
                                <a class="Blanc" href="category/detail/{{$category['id']}}">En savoir plus</a>
                            </div>

                        </div>
                    </div>
                    <style>
                                .button-first_{{$category['id']}}{
                                    background-color:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}}
                                }
                                .top-tab-border-first_{{$category['id']}}:hover{

                                    border-top:5px solid #fff !important;
                                }
                                .top-tab-border-first_{{$category['id']}}{

                                    border-top:5px solid {{ ($category['color'] == "") ? '#ea6212' : $category['color']}} !important;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover{
                                    background-color:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}}
                                }
                                .applicationsDisplay_{{$category['id']}}{

                                    background-color: #fff;
                                    float: left;
                                    height: 300px;
                                    margin-left: 7px;
                                    text-align: center;
                                    margin-top: 15px;
                                    margin-bottom: 15px;
                                }

                                .title1_{{$category['id']}}{
                                    color:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}}
                                }
                                .applications-resume_{{$category['id']}}{
                                    font-family: Arial;
                                    font-size: 14px;
                                    font-weight: bold;
                                    margin-left: 2px;
                                    margin-right: 2px;
                                    text-align: center;
                                }
                                .image-ico_{{$category['id']}}{
                                    font-size: 24px;
                                    color: #fff;
                                    margin-top: 5px;
                                }

                                .applications-description_{{$category['id']}}
                                {
                                    font-family: Arial;
                                    font-size: 14px;
                                    height: 28px;
                                    line-height: 20px;
                                    margin-top: 10px;
                                    text-align: center;
                                    min-height: 48px;
                                }
                                .applications-bouton_{{$category['id']}}
                                 {
                                    font-family: Arial;
                                    font-size: 14px;
                                    font-weight: bold;
                                    margin-bottom: 20px;
                                    margin-left: 10px;
                                    margin-right: 10px;
                                    margin-top: 43px;
                                    padding-bottom: 13px;
                                    padding-left: 0;
                                    padding-right: 0;
                                    padding-top: 13px;
                                    text-align: center;
                                    text-transform: uppercase;
                                    width: 175px;
                                }
                                .applications-title_{{$category['id']}} {
                                    font-family: Arial;
                                    font-size: 18px;
                                    font-weight: bold;
                                    margin-top: 60px;
                                    text-transform: uppercase;
                                    min-height: 75px;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover{
                                    background-color: {{ ($category['color'] == "") ? '#ea6212' : $category['color']}};
                                }

                                .applicationsDisplay_{{$category['id']}}:hover .applications-picto_{{$category['id']}} {
                                    color: {{ ($category['color'] == "") ? '#ea6212' : $category['color']}} !important;
                                }

                                .applicationsDisplay_{{$category['id']}}:hover  .image-ico_{{$category['id']}}{
                                    color:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}} !important;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .Blanc  {
                                    color: {{ ($category['color'] == "") ? '#ea6212' : $category['color']}};
                                }
                                .applicationsDisplay_{{$category['id']}}:hover  .title1_{{$category['id']}} {
                                    color:#fff !important;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .applications-title_{{$category['id']}}{
                                    color:#fff !important;
                                }

                                .top-tab-border-first_{{$category['id']}}:hover {
                                    border-top:5px solid #fff;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .bottom-first_{{$category['id']}} {
                                    background-color:#fff !important;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover.image-ico_{{$category['id']}} {
                                    color:{{ ($category['color'] == "") ? '#ea6212' : $category['color']}};
                                    font-size: 24px;
                                    margin-top: 5px;
                                }
                               #first-tab_{{$category['id']}} :hover .applicationsDisplay_{{$category['id']}} .applications-title{
                                    color:#fff;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .applications-description_{{$category['id']}} {
                                    color:#fff;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .applications-lien a {
                                    color:#fff;
                                }
                                .applications-picto_{{$category['id']}} {
                                    margin-bottom: auto;
                                    margin-left: auto;
                                    margin-right: auto;
                                    margin-top: -44px;
                                }
                                .bottom-first_{{$category['id']}}{
                                    border-bottom-left-radius: 90px;
                                    border-bottom-right-radius: 90px;
                                    height: 45px;
                                    width: 90px;
                                    left: 54px;
                                    position: relative;
                                    top: 44px;
                                }
                                .applicationsDisplay_{{$category['id']}}:hover .button-first_{{$category['id']}} {
                                    background-color: #fff;
                                }
                    </style>
               @endforeach
            </div>
        </section>
    </div>
@endsection
