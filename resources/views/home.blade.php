@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="title-heading">
                        <h1> {{ __('probtp.dashboard') }}</h1>
                    </div>
                </div>

                <div class="panel-body">
                    {{ __('probtp.login_message') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
