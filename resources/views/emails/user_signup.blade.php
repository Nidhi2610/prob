<!DOCTYPE html>
<html>
    <head>
        <title>Probtp : User Login Information</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    </head>
    <body style="margin: 0;padding: 0;width: 100%;display: table;font-weight: 100;height: 100%;">
        {!! $user_data['body'] !!}
    </body>
</html>