<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('public/front1/css/bootstrap.css') }}" rel="stylesheet">

    <link href="{{ asset('public/front1/css/style.css') }}" rel="stylesheet">

    <title>{{ __('probtp.site_title') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Styles -->
    <style>
        body, html {
            height: 100%;
            margin: 0;
        }


    </style>
</head>
<body>

<div class="bg">
    <div class="company-logo">
        <img src="../../public/front1/images/logo_probtp.jpg">
    </div>
    <div id="button_content" class="btn-flt">
        <h1>Formation des équipes RH en protection sociale</h1>
        {{--<p>Plateforme personnalisee pour mieux comprendre les regles et le fonctionnement de la protection sociale ainsi que les caracteristiques des produits PRO BTP</p>--}}
        <button type="button" id="button1" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" >DECOUVRIR LE SERVICE</button>
        <button type="button" id="button2" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal1">ME CONNECTER</button>
    </div>
</div>

<!-- Information Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Présentation du site</h4>
            </div>
            <div class="modal-body">
                <?php echo $settingData->content; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Login Modal-->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
            {!! csrf_field() !!}
            <input type="hidden" name="category_id" id="category_id" value="">
            <input type="hidden" name="category_classification" id="category_class" value="">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Se connecter</h4>
                </div>
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">{{__('probtp.email')}}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">{{__('probtp.password')}}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{__('probtp.remember_me')}}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <a class="btn btn-link" href="javascript:void(0)" id="forgot_password" data-toggle="modal" data-target="#myModal3">
                                {{__('probtp.forgot_password')}}
                            </a>

                        </div>
                    </div>



                </div>
                <div class="modal-footer">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-sign-in"></i>   {{__('probtp.login')}}
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{__('probtp.reset_password')}}</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{__('probtp.email')}}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i style="padding-right: 5px" class="fa fa-btn fa-envelope"></i>  {{__('probtp.send')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
            </div>
        </div>
    </div>
</div>



</body>
</html>

<script src="{{ asset('public/js/app.js') }}"></script>

<script src="{{ asset('public/front/js/config.js') }}"></script>

<script>
    $(document).ready(function(){
        <?php if ($errors->first('password') ||$errors->first('email') ): ?>
            $("#myModal1").modal('show');
        <?php endif ?>
         $('#forgot_password').on('click', function() {
            $("#myModal1").modal('hide');
        });

        <?php if(session('status')): ?>
           $("#myModal3").modal('show');
        <?php endif ?>
    });


</script>
