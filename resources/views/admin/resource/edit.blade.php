@extends('layouts.app')
@push('css')
<style>
    .font_picker_icon i{font-size: 30px;}
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.resource_edit') }}</h1>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('resourceUpdate') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}">
                            <input type="hidden" name="dbAction" value="Edit">

                            <div class="form-group{{ $errors->has('parent_resource') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.category_parent') }}</label>

                                <div class="col-md-6">
                                    <?php $tree =  \App\Helpers\LaraHelpers::buildTree($data); ?>

                                    <?php print("<select class='form-control' name='parent_resource' id='parent_resource'>\n");?>
                                    <option value="0">Les Resource</option>
                                    <?php  \App\Helpers\LaraHelpers::printTree($tree, $r = 0, $p = null,$editData->parent_id);
                                    print("</select>");
                                    ?>
                                    @if ($errors->has('parent_resource'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('parent_resource') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.resource_name') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="name" value="{{$editData->name}}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Public / Privé</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="resource_type_id" id="resource_type_id">
                                        <option value="">{{ __('probtp.select')}}</option>
                                        @foreach($viewData as $viewDatas)
                                            <option value="{{$viewDatas['resource_type_id']}}" @if($editData->resource_type_id == $viewDatas['resource_type_id']) {{"selected"}} @endif>{{ $viewDatas['resource_type'] }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('resource_type_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resource_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.description') }}</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control" id="description"  cols="70" rows="5" maxlength="140">{{$editData->description}}</textarea>
                                </div>
                                <div style="color:red;font-weight:bold" class="col-md-8">

                                </div>
                                <div style="color:red;font-weight:bold" class="col-md-4"><div id="textarea_feedback" class="pull-right" style="margin-right: 190px"></div></div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.color') }}</label>
                                <div class="col-md-6">
                                    <input id="color" type="text" class="form-control jscolor" name="color" value="{{$editData->color}}">
                                </div>
                                <div class="color_text col-md-1" style="width: 10px; height: 35px; border-radius: 5px;"></div>
                            </div>

                            <div class="form-group"{{ $errors->has('font_awesome') ? ' has-error' : '' }}>
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.font_awesome') }}</label>
                                <div class="col-md-6">
                                    <input data-placement="bottomRight" id="font_awesome" name="font_awesome" class="form-control icp icp-auto icp-glyphs" value="{{$editData->font_awesome}}" type="text" />
                                    @if ($errors->has('font_awesome'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('font_awesome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="font_picker_icon"><i class="fa {{$editData->font_awesome or ""}}"></i></div>
                            </div>



                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{ __('probtp.status')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">{{ __('probtp.select')." ".__('probtp.status') }}</option>
                                        <option value="0" @if($editData->status == "0") {{"selected"}} @endif>Active</option>
                                        <option value="1" @if($editData->status == "1") {{"selected"}} @endif>Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

<script src="{{ asset('public/plugins/colorbox/jquery.colorbox-min.js') }}"></script>
<link href="{{ asset('public/plugins/colorbox/data/colorbox.css') }}" rel="stylesheet">
<script src="{{ asset('public/plugins/colorpicker/colorpicker.js') }}"></script>
<link href="{{ asset('public/plugins/colorpicker/colorpicker.css') }}" rel="stylesheet">

<script src="{{ asset('public/plugins/colorpicker/jscolor.js') }}"></script>

<link href="{{ asset('public/plugins/fontpicker/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('public/plugins/fontpicker/fontawesome-iconpicker.min.js') }}"></script>
<script src="{{ asset('public/plugins/fontpicker/jquery-migrate-3.0.0.js') }}"></script>




<script>

    /*$('#color').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {

            $(el).val(hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        },
        onChange: function (hsb, hex, rgb) {
            $(".color_text").css("background-color","#"+hex);
            $('#color').val("#"+hex);
        }
    }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
    });*/


    $(function() {
        $("#color").css("background-color","{{$editData->color}}");
        // Live binding of buttons


            $('.icp-glyphs').iconpicker({
                title: 'Fa Fa Icon Selector',
                icons: $.merge([], $.iconpicker.defaultOptions.icons),
                fullClassFormatter: function(val){
                    if(val.match(/^fa-/)){
                        return 'fa '+val;
                    }else{
                        return 'glyphicon '+val;
                    }
                }
            });


            $(".iconpicker .iconpicker-item").on("click change", function () {
                $(".font_picker_icon").html($(this).html());
            });

        // Events sample:
        // This event is only triggered when the actual input value is changed
        // by user interaction

    });

    $(document).ready(function() {

        var text_max = 140;
        var remaining = $('#description').val().length;
         var now_remaining = text_max - remaining;

        $('#textarea_feedback').html(now_remaining);

        $('#description').keyup(function () {
            var text_length = $('#description').val().length;
            var text_remaining = text_max - text_length;

            $('#textarea_feedback').html(text_remaining);
        });
    });
</script>
@endpush
