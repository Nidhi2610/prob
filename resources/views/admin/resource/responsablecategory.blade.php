@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('probtp.module_list', ['module' => __('probtp.category')]) }}

                    </div>
                    <div class="panel-body">
                        <div class="col-md-1"> &nbsp; </div>
                        <div class="col-md-10">
                            <div id="categoryList"></div>
                        </div>
                        <div class="col-md-1"> &nbsp; </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.category.collaborateuruser',['userObj' => $userObj])
@push('script')
<script>
    var data = '<?php echo addslashes(json_encode($data))?>';
    data = JSON.parse(data);
</script>
<script src="{{ asset('public/sortable/responsable_sortable.js') }}"></script>
<link href="{{ asset('public/sortable/sortable.min.css') }}" rel="stylesheet">
@endpush


