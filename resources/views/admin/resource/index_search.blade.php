@extends('layouts.app')
@push('css')
<style>
    .circle:before {
        content: ' \25CF';
        font-size: 20px;
        color:#ea6212;
    }
</style>
@endpush
@section('content')

    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('resourceDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1> {{ __('probtp.resource_list') }}</h1>
                            <a href="{{route('resourceIndex')}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ __('probtp.back_category_list')}}
                            </a>
                            {{--<a type="button" href="{{route('formAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}
                            <a href="{{route('resourceAdd')}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add')}}
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width: 20px" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th>Les Resource</th>
                                <th style="width: 35px">{{ __('probtp.action') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $resource)
                                <tr>
                                    <td><input type="checkbox" name="member_id[]" value="{{$resource->id}}"  class="checkAllChild" /></td>

                                    <td>@if($resource->ParentCheck == 1)<i class="circle"></i>
                                        @elseif($resource->ParentCheck == 2)<i class="circle"></i>
                                            <i class="circle"></i>
                                        @else <i class="circle"></i>
                                            <i class="circle"></i>
                                            <i class="circle"></i>
                                        @endif{{$resource->name}}
                                        @if($resource->resource_type_id == 2)
                                            <i style = "padding-right: 10px;" title="Public"   class="fa fa-unlock-alt"></i>
                                        @else
                                            <i style = "padding-right: 10px;"  title="Privé"  class="fa fa-lock"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{--<a type="button" href="{{route('entrepriseEdit',['id'=>$entreprise->id])}}"><i title="Edit" class="fa fa-btn fa-pencil"></i></a>
                                        <a type="button" data-id ="{{$entreprise->id}}" href="javascript:void(0);" title="Delete" class="delete"><i class="fa fa-btn fa-trash"></i></a>--}}
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                            <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                <li> <a href="{{route('resourceEdit',['id'=>$resource->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i>{{ __('probtp.edit') }}</a></li>
                                                <li> <a href="javascript:void(0)"   data-id ="{{$resource->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                <li> <a href="javascript:void(0)" onclick="duplicate(this.id);" id="{{$resource->id}}"   data-id ="{{$resource->id}}"><i class="fa fa-btn fa-files-o" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.duplicate') }}</a></li>
                                               {{-- <li> <a type="button" href="detail_page/{{$resource->id}}"><i class="fa fa-eye" style="padding-right: 10px;"  aria-hidden="true"> </i>{{ __('probtp.preview') }}</a></li>
                                                <li><a href ="{{route('moduleAdd',['id'=>$resource->id])}}" ><i class="fa fa-plus-square" style="padding-right: 10px;"  aria-hidden="true"> </i>Ajouter un nouveau module</a></li>
                                                @if($resource->resource_type_id == 2)
                                                    @else
                                                <li><a  type="button" href="javascript:void(0);" onclick="getentreprise(this.id);" data-toggle="modal" id="{{$resource->id}}"><i id="probtp_form" style="padding-right: 10px;" class="fa fa-etsy"></i>Assigner une entreprise</a></li>
                                                    @endif
                                                <li><a  type="button" href="javascript:void(0);" onclick="getunassignedmodule(this.id);" data-toggle="modal" id="{{$resource->id}}"><i style="padding-right: 10px;" id="probtp_form" class="fa fa-plug"></i>Assigner un module</a></li>
                                                @if($resource->IsModuledata > 0)
                                                    <li><a  type="button" href="javascript:void(0);" onclick="getassignedmodule(this.id);" data-toggle="modal" id="{{$resource->id}}"><i style="padding-right: 10px;"  class="fa fa-list"></i>Les modules attribués</a></li>
                                                    @else
                                                   <li><a  type="button" href="javascript:void(0);"><i style = "color:grey;padding-right: 10px;" class="fa fa-list"></i>Les modules attribués</a></li>
                                                    @endif--}}
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.resource.resourcemodal')
@include('layouts.stack',['extra'=>''])
@include('admin.resource.resourceentreprise',['entObj' => $entObj])

@push('script')
<script src="{{ asset('public/sortable/sortable.js') }}"></script>
@endpush