
<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <form class="form-horizontal" role="form" method="POST" id="add_form" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="resource_id" id="resource_id" value="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="title-heading"><h1 style="color:#ea6212">{{__('probtp.modules_list')}}</h1></div>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body module_list" >


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
                    <button type="submit" class="btn btn-primary" id = "attached">{{__('probtp.attach_submit')}}</button>
                    <button type="submit" class="btn btn-primary" id = "detached">{{__('probtp.detached_submit')}}</button>

                </div>

            </div>
        </div>

    </form>
</div>
</div>
@push('script')
<script>
    $(window).on('load', function() {
        $('tbody').sortable();
    });

</script>
@endpush
