<div id="myModalCategoryEntreprise" class="modal fade">

    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="POST" action="{{route('entrepriseCategory')}}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" value="" name="category_id" id="categoryid" />
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ __('probtp.assign_enterprise')}}</h4>
                </div>


                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="table table-striped  table-bordered" cellspacing="0" id="data-table2">
                                        <thead>
                                        <tr>
                                            <th style="text-align:center" class="no-sort"><input type="checkbox" id="checkAll2"  class="checkAll2" /></th>
                                            <th>{{ __('probtp.select_enterprise') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($entObj as $ent)
                                            <tr>

                                                <td style="text-align:center"><input type="checkbox" name="entreprise_id[]" value="{{$ent->id}}" class="modalcheckbox enterprises" id = "entreprise_id_{{$ent->id}}" /></td>
                                                <td>{{$ent->name}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
                    <button type="submit" class="btn btn-primary" id = "submitForm">{{__('probtp.attach_enterprise')}}</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </form>
    </div>
</div>