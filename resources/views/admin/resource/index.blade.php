@extends('layouts.app')
@section('content')
    @inject('EntObj', 'App\Models\Entreprise')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('resourceDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.resource_list') }}</h1>
                        {{-- <a type="button" href="{{route('entrepriseAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}
                            <a href="{{route('getResourceIndexSearch')}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ __('probtp.back_category_list')}}
                            </a>
                        <a href="{{route('resourceAdd')}}" class="btn btn-white btn-default pull-right ">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add_category')}}
                        </a>
                        </div>
                    </div>

                    <div class="panel-body">
					<div class="col-md-1"> &nbsp; </div>
                       <div class="col-md-10">
							<div id="resourceList"></div>
						</div>
					<div class="col-md-1"> &nbsp; </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.resource.resourcemodal')
@include('layouts.stack',['extra'=>''])
@include('admin.resource.resourceentreprise',['entObj' => $entObj])

@push('script')
<script src="{{ asset('public/modalpopup/popup.js') }}"></script>
<script>
	var data = '<?php echo addslashes(json_encode($data))?>';
	data = JSON.parse(data);

</script>

<script src="{{ asset('public/sortable/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/sortable/jquery.mjs.nestedSortable.js') }}"></script>
<script src="{{ asset('public/sortable/sortableresource.js') }}"></script>
@endpush


