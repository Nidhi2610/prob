@extends('layouts.app')
@push('css')
<style>
    .font_picker_icon i{font-size: 30px;}
</style>
@endpush
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                       <h1>{{ __('probtp.resource_add_index') }}</h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('resourceStore') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">


                            <div class="form-group{{ $errors->has('parent_category') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.category_parent') }}</label>

                                <div class="col-md-6">
                                    <?php $tree =  \App\Helpers\LaraHelpers::buildTree($data); ?>

                                    <?php print("<select class='form-control' name='parent_id' id='parent_category'>\n");?>
                                    <option value="0">Les Resource</option>
                                   <?php  \App\Helpers\LaraHelpers::printTree($tree, $r = 0, $p = null,$ids = null);
                                    print("</select>");
                                    ?>

                                    @if ($errors->has('parent_category'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('parent_category') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.resource_name') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('resource_type_id') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Public / Privé</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="resource_type_id" id="resource_type_id">
                                        <option value="">{{ __('probtp.select')}}</option>
                                        @foreach($viewData as $viewDatas)
                                            <option value="{{$viewDatas['resource_type_id']}}"@if($viewDatas['resource_type_id'] == 2){{"selected"}}@endif>{{ $viewDatas['resource_type'] }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('resource_type_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('resource_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.description') }}</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control" id="description" value="{{ old('description') }}" cols="70" rows="5" maxlength="140"></textarea>
                                </div>
                                <div style="color:red;font-weight:bold" class="col-md-8">

                                </div>
                                <div style="color:red;font-weight:bold" class="col-md-4"><div id="textarea_feedback" class="pull-right" style="margin-right: 190px"></div></div>
                            </div>


                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.color') }}</label>
                                <div class="col-md-6">
                                    <input id="color" type="text" onchange="update(this.jscolor)" class="form-control jscolor" name="color" value="{{ old('color') }}">
                                </div>
                                <div class="color_text col-md-1" style="width: 10px; height: 35px;border-radius: 5px;"></div>

                            </div>


                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.font_awesome') }}</label>
                                <div class="col-md-6">
                                    <input id="target"  name="font_awesome" class="form-control icp icp-auto icp-glyphs" value="" type="text"/>
                                </div>
                                <div class="font_picker_icon"></div>
                            </div>
                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{__('probtp.status')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">{{ __('probtp.select')." ".__('probtp.status') }}</option>
                                        <option value="0">Active</option>
                                        <option value="1" {{"selected"}} >Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

<script src="{{ asset('public/plugins/colorbox/jquery.colorbox-min.js') }}"></script>
<link href="{{ asset('public/plugins/colorbox/data/colorbox.css') }}" rel="stylesheet">
<script src="{{ asset('public/plugins/colorpicker/colorpicker.js') }}"></script>
<link href="{{ asset('public/plugins/colorpicker/colorpicker.css') }}" rel="stylesheet">

<script src="{{ asset('public/plugins/colorpicker/jscolor.js') }}"></script>

<link href="{{ asset('public/plugins/fontpicker/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('public/plugins/fontpicker/fontawesome-iconpicker.min.js') }}"></script>
<script src="{{ asset('public/plugins/fontpicker/jquery-migrate-3.0.0.js') }}"></script>

<script>

    // Color Picker

    /*$('#color').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
            $(el).val(hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            $(this).ColorPickerSetColor(this.value);
        },
        onChange: function (hsb, hex, rgb) {
            $(".color_text").css("background-color","#"+hex);
            $('#color').val("#"+hex);
        },


    }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
    });*/

    function update(jscolor) {
        // 'jscolor' instance can be used as a string
            }

    /// For Fa -Fa icons

    $(function() {
        // Live binding of buttons
        $('.icp-glyphs').iconpicker({
            title: 'Fa Fa Icon Picker',
            icons: $.merge(['glyphicon-home', 'glyphicon-repeat', 'glyphicon-search',
                'glyphicon-arrow-left', 'glyphicon-arrow-right', 'glyphicon-star'], $.iconpicker.defaultOptions.icons),            fullClassFormatter: function(val){
                if(val.match(/^fa-/)){
                    return 'fa '+val;
                }else{
                    return 'glyphicon '+val;
                }
            }
        });
        $('.icp-glyphs').val("");
        $(".iconpicker .iconpicker-item").on("click change", function () {
           $(".font_picker_icon").html($(this).html());
        });
    });

    $(document).ready(function() {
        var text_max = 140;
        $('#textarea_feedback').html(text_max );

        $('#description').keyup(function () {
            var text_length = $('#description').val().length;
            var text_remaining = text_max - text_length;

            $('#textarea_feedback').html(text_remaining);
        });
    });

</script>
@endpush