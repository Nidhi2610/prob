@if(count($module_list))
    <table class="table table-striped module_list  table-bordered" cellspacing="0" id="data-table3">

        <thead>
        <tr>
            <th style="display: none"></th>
            <th style="text-align:center;width:20px;" class="no-sort"><input type="checkbox" id="checkAll2"  class="checkAll2" /></th>
            <th>{{ __('probtp.modules') }}</th>
            <th>{{ __('probtp.keyword') }}</th>
            {{-- <th>{{ __('probtp.quiz') }}</th>--}}
        </tr>
        </thead>
        <tbody class="module_category" style="cursor: move">
      <div style="display: none!important;"> {{$i=1}}</div>

        @foreach($module_list as $row)

            <tr>
                <td style="display: none !important;">{{$i}} <input type="hidden" class="module_category_ids" value="{{$row->id}}" /></td>
                <td style="text-align:center"><input type="checkbox" name="module_id[]" value="{{$row->module_id}}" class="modalcheckbox" id = "user_id" /></td>
                <td>{{$row->Module->title}}</td>
                <td>{{$row->Module->keyword}}</td>
                {{-- <td>{{$row->Module->keyword}}</td>--}}
            </tr>
            {{$i++}}
        @endforeach

        </tbody>
        <input type="hidden" id="savebutton" value="1">

    </table>

@endif
