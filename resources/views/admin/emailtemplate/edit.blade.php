@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.add_email_template') }}</h1>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('emailTemplateUpdate') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}">
                            <input type="hidden" name="dbAction" value="Edit">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.template_name') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="name" value="{{$editData->name}}" required autofocus>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email_content') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.content') }}</label>
                                <div class="col-md-6">
                                    <textarea id="email_content" name="email_content" value="{{ old('email_content', $editData->email_content)}}" class="form-control ckeditor" cols="50" rows="5">{{ old('email_content', $editData->email_content)}}</textarea>
                                    @if ($errors->has('email_content'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email_content') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div style="color:red;font-weight:bold" class="col-md-8 col-md-offset-4">
                                    {{ __('probtp.email_note') }}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{route('emailTemplateIndex')}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
{{--<script src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>--}}
<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    $(document).ready(function(){

        // For editor

        tinymce.init({     selector: '#email_content',    relative_urls : false,    remove_script_host : false,    convert_urls : true,
            plugins: [  'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code autoresize'],
            toolbar:    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect | fontselect |  fontsizeselect',
            language:   'fr_FR',
        });

    });
</script>
@endpush
