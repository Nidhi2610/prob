@extends('layouts.app')

@section('content')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('entrepriseDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.email_list') }}</h1>
                       <!-- <a type="button" href="{{route('emailTemplateAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>-->
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width: 20px" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th>{{ __('probtp.email_template') }}</th>
                                <th style="width: 60px"  class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $template)
                                <tr>
                                    <td><input type="checkbox" name="template_id[]" value="{{$template->id}}"  class="checkAllChild" /></td>
                                    <td>{{$template->name}}</td>
                                    <td class="text-center">

                                        &nbsp; <a type="button" href="{{route('emailTemplateEdit',['id'=>$template->id])}}"><i class="fa fa-btn fa-pencil"></i> {{__('probtp.edit')}}</a>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.stack',['extra'=>''])