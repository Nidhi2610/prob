@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('probtp.module_add', ['module' => __('probtp.email_template')]) }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="probtp_form" role="form" method="POST" action="{{ route('emailTemplateStore') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.template_name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.content') }}</label>

                                <div class="col-md-6">

                                    <textarea name="email_content" id="email_content" value="{{ old('email_content') }}" class="form-control ckeditor" cols="50" rows="5"></textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">                                    &nbsp; &nbsp;
                                    <button type="button" class="btn btn-primary" onclick="window.location='{{route('emailTemplateIndex')}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
{{--<script src="{{ asset('public/js/ckeditor/ckeditor.js') }}"></script>--}}
<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    $(document).ready(function(){

        // For editor

        tinymce.init({     selector: '#email_content',    relative_urls : false,    remove_script_host : false,    convert_urls : true,
            plugins: [  'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code autoresize'],
            toolbar:    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect | fontselect |  fontsizeselect',
            language:   'fr_FR',
        });

    });
</script>
@endpush
