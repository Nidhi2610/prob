@extends('layouts.app')
@push('css')
<link href="{{ asset('public/dynamic-form/css/form-builder.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/dynamic-form/css/form-render.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/dynamic-form/css/demo.css') }}" rel="stylesheet">
<style>
    .form-actions #frmb-0-view-data,.form-actions #frmb-0-save{display: none;}
    .access-wrap{display: none;}
    .form-wrap.form-builder #frmb-0-cb-wrap .icon-autocomplete,.form-wrap.form-builder #frmb-0-cb-wrap .icon-button-input
    ,.form-wrap.form-builder #frmb-0-cb-wrap .icon-checkbox,.form-wrap.form-builder #frmb-0-cb-wrap .icon-file-input,.form-wrap.form-builder #frmb-0-cb-wrap .icon-hidden-input,
    .form-wrap.form-builder #frmb-0-cb-wrap .icon-paragraph,.form-wrap.form-builder #frmb-0-cb-wrap .icon-number{display: none;}
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.form_edit') }}</h1>


                        &nbsp; &nbsp;
                        </div>

                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" id="form_submit" role="form" method="POST" action="{{ route('formUpdate') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->form_id}}" />
                            <input type="hidden" name="dbAction" value="Edit" />

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">{{ __('probtp.title') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ $editData->title }}" placeholder="{{ __('probtp.title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('identifier') ? ' has-error' : '' }}" style="display: none;">
                                <label for="identifier" class="col-md-4 control-label">{{ __('probtp.identifier') }}</label>

                                <div class="col-md-6">
                                    <input id="identifier" type="text" class="form-control" name="identifier" value="{{ $editData->identifier }}" placeholder="{{ __('probtp.identifier') }}" required autofocus>

                                    @if ($errors->has('identifier'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('identifier') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{__('probtp.status')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">{{ __('probtp.select')." ".__('probtp.status') }}</option>
                                        <option value="0" @if($editData->status == "0") {{"selected"}} @endif>Active</option>
                                        <option value="1" @if($editData->status == "1") {{"selected"}} @endif>Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <textarea name="content" id="content" style="display: none;">{{$editData->content}}</textarea>
                        </form>
                        <div class="form-group col-md-12">
                            <div id="build-wrap" class="build-wrap"></div>
                            <div class="render-wrap"></div>
                        </div>
                        @if ($errors->has('content'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                        @endif

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" style="margin-top: -7px;" class="btn btn-primary dynamic-form-submit pull-right">
                                    {{ __('probtp.update') }}
                                </button>
                                <button type="button" class="btn btn-danger pull-right" style="margin-right: 10px; margin-top: -7px;" onclick="window.location='{{route('formIndex')}}'">
                                    {{ __('probtp.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script src="{{ asset('public/dynamic-form/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/dynamic-form/js/form-builder.min.js') }}"></script>
<script src="{{ asset('public/dynamic-form/js/form-render.min.js') }}"></script>
<script>
   var formData1 = $("#content").text();
</script>
<script src="{{ asset('public/js/modules/dynamicform.js') }}"></script>
@endpush