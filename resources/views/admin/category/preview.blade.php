@extends('layouts.admin.app')
@push('css')
{{--<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> --}}
<style>

	/*body{*/
		/*font-family: 'Source Sans Pro', sans-serif;*/
	/*}*/
    label.error{color: #FF0000;}
    .childli{
                   width: 68px;
                   display: inline-block;
                   height: 18px;
                   background: #ececec;
                   margin-right: 0px;
                   border-bottom: 3px solid rgba(0,0,0,.25);
               }
               .childli:hover{background: #D8D8D8}
               .steps .fa.fa-circle.fa-stack-2x{
               	color: #B4B4B4;
               }
               .steps .fa.fa-circle.fa-stack-2x:hover{
               	color: #d34215;
               }
               .steps ul{
               		padding:0px;
               		margin: 0px;
               }
               .clearfix{
               	padding: 0px;
               }
               .stepline{

               }

               .input-group label{
               	margin-left: 10px;
               }

				.stepline {
				    margin-top: 18px;
				}
				.summary p{
					font-size: 1.5rem;
				    line-height: 1.5;
				    color: rgba(0,0,0,.85);
				    margin-bottom: 15px;}

				.containttestform .panel-default > .panel-heading {
					border-color: #00a3b4 ;
					background: #00a3b4;

					color: #fff;
					font-size: 18px;
				}

                .fa.fa-map-marker.fa-stack-1x.fa-inverse,.fa.fa-flag.fa-stack-1x.fa-inverse{
                    border-radius: 50%;
                    background:#b4b4b4;
                }
                .fa.fa-map-marker.fa-stack-1x.fa-inverse:hover{
                    background: #00a3b4;
                }

                .fa.fa-flag.fa-stack-1x.fa-inverse:hover{background: #00a3b4;}
                .category_name{

				}
				.containttestform .panel-heading{
					font-size: 16px;
					font-weight: bold;

				}
				.containttestform .form-horizontal .control-label{
					padding-top: 0px;
				}
				.containttestform .panel-default{
					border:none;
				}
				.containttestform .panel-body{
					background-color: #f8f8f8;
					border-color: transparent;
					padding-top: 2.5em;
				}
				.containttestform .form-control {
					border-radius:0px !important;

				}
				.addcomment .form-control {
					border-radius:0px !important;
				}
				.containttestform .form-control:focus{
					border-color: #00a3b4;
				    outline: 0;
				    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(234, 98, 18, 0.58);
				    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(234, 98, 18, 0.58);
				}
				.form-group{font-size: 16px;}
				.addcomment{margin-bottom: 3em}
				.addcomment .form-control:focus{
					border-color: #00a3b4;
				    outline: 0;
				    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(234, 98, 18, 0.58);
				    box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(234, 98, 18, 0.58);
				}
				.addcomment .btn-primary,.containttestform .btn-primary{ background-color:#00a3b4; color: #fff;  border-color:#00a3b4; }
				.containttestform .btn-primary:hover{background:rgba(234, 98, 18, 0.5)}
				.containttestform .input-group label{ font-weight: 600;font-size: 16px; }
				.addcomment .btn-primary:hover:{background:rgba(234, 98, 18, 0.5)}
				.right {float:right;	}
				.finalanswers label{margin-bottom: 20px;}
				.fa.fa-check-circle.fa-lg.fa-fw{color: #2cce2e;}
				.fa.fa-times-circle.fa-lg.fa-fw{color: red;}
				.quizmark span{margin-right: 10px;}
				.quizmark label{}
				.ansoption{margin-bottom: 25px; line-height: 2;}
				.youroint{text-align: right; font-size: 20px;}
				.youroints .pts {color: #00a3b4;font-weight:700; font-size:20px; text-align: left; }

 /*font Awesome http://fontawesome.io*/

/*Comment List styles*/
.comment-list .row {
  margin-bottom: 0px;
}
.comment-list .panel .panel-heading {
  padding: 4px 15px;
  position: absolute;
  border:none;
  /*Panel-heading border radius*/
  border-top-right-radius:0px;
  top: 1px;
}
.comment-list .panel .panel-heading.right {
  border-right-width: 0px;
  /*Panel-heading border radius*/
  border-top-left-radius:0px;
  right: 16px;
}
.comment-list .panel .panel-heading .panel-body {
  padding-top: 6px;
}
.comment-list figcaption {
  /*For wrapping text in thumbnail*/
  word-wrap: break-word;
}
.comment-date .fa,.comment-user .fa {color: #00a3b4}
/* Portrait tablets and medium desktops */
@media (min-width: 768px) {
  .comment-list .arrow:after, .comment-list .arrow:before {
    /*content: "";*/
    position: absolute;
    width: 0;
    height: 0;
    border-style: solid;
    border-color: transparent;
  }
  .comment-list .panel.arrow.left:after, .comment-list .panel.arrow.left:before {
    border-left: 0;
  }
  /*****Left Arrow*****/
  /*Outline effect style*/
  .comment-list .panel.arrow.left:before {
    left: 0px;
    top: 30px;
    /*Use boarder color of panel*/
    border-right-color: inherit;
    border-width: 16px;
  }
  /*Background color effect*/
  .comment-list .panel.arrow.left:after {
    left: 1px;
    top: 31px;
    /*Change for different outline color*/
    border-right-color: #F6F6F6F;
    border-width: 15px;
  }
  /*****Right Arrow*****/
  /*Outline effect style*/
  .comment-list .panel.arrow.right:before {
    right: -16px;
    top: 30px;
    /*Use boarder color of panel*/
    border-left-color: inherit;
    border-width: 16px;
  }
  /*Background color effect*/
  .comment-list .panel.arrow.right:after {
    right: -14px;
    top: 31px;
    /*Change for different outline color*/
    border-left-color: #FFFFFF;
    border-width: 15px;
  }
}
.comment-list .comment-post {
    margin-top: 6px;
}



</style>
@endpush
@section('content')
    <div class="col-sm-10">

        {{--<section class="category_modules">--}}
            {{--<div class="col-md-12">--}}

            {{--<li class="btn_ajtr">--}}
                {{--<a href="{{ url()->previous() }}"><i class="fa fa-arrow-left left_arrow_back"></i>Retour</a>--}}
            {{--</li>--}}
            {{--</div>--}}
            {{--@include('admin.category.module_detail',["moduleid_data"=>$moduleid_data,"category_id"=>$category_id])--}}
        {{--</section>--}}

            <section class="category_modules" id="myGroup">
                @include('admin.category.module_detail',["moduleid_data"=>$moduleid_data,"category_id"=>$category_id,"parent_category_data"=>$parent_category_data])
            </section>
            <div class="container">
                <div class="row">

                    <div class="comment">
                        <div class="col-md-12 all_comments"> {{ \Illuminate\Support\Facades\Log::Info($commentsData) }}
                            @include('front1.comments',["commentsData"=>$commentsData])
                            <div align="center">{{ $commentsData->links() }}</div>
                        </div>
                        <form class="form-horizontal" id="comment_form" role="form" action="javascript:void(0);" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" id="category_id" name="category_id" value="{{$category_id}}" />



                            <div class = "col-md-12">
                                <div class="addcomment">
                                    <h3 class="page-header">Ajouter un commentaire</h3>
                                    <div class="form-group" {{ $errors->has('comment') ? ' has-error' : '' }}>
                                        <div class="col-md-12">
                                            <textarea name="comment" id="comment" class="form-control required" cols="100" style="resize: none" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 ">
                                            <button class="btn btn-primary right" id="comment_button"  type="submit">
                                                Poster
                                            </button>

                                        </div>
                                    </div>
                                </div></div>
                        </form>

                    </div>

                </div>
            </div>




</div>
    <a href="{{url('/home')}}" id="back-to-home" title="Back to Home">Accueil</a>
    <a href="#" id="back-to-top" title="Back to top">Haut de page </a>





@endsection
@push('external_script')
<script src="{{url('public/js/jquery.validate.min.js')}}"></script>
@endpush
@push('script')
<script>

    if ($('#back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 500);
        });
    }

    $("#comment_form").validate();

    function hover(description) {
        document.getElementById('category_name').innerHTML = description;
    }
    function leave() {
        document.getElementById('category_name').innerHTML = "";
    }

    $('#comment_form').on('submit', function(event) {
        event.preventDefault();
        event.stopPropagation();
        if(!$("#comment_form").valid()){
            return false;
        }
        var form_data = new FormData(this);
        $.ajax({
            cache:false,
            url: '{{url('store_comment')}}',
            type: "POST",
            data: form_data,
            contentType: false,
            processData:false,
            success: function(response)
            {
                if(response.statusCode == '1'){
                    $(".all_comments").html(response.data);
                }
                $("#comment").val("");
            }
        });

    });
</script>
@endpush