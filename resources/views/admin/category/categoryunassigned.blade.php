@if(count($module_list))
    <table class="table table-striped module_list  table-bordered" cellspacing="0" id="data-table3">
        <thead>
        <tr>
            <th style="text-align:center;width:20px;" class="no-sort"><input type="checkbox" id="checkAll2"  class="checkAll2" /></th>
            <th>Les modules</th>
            <th>{{ __('probtp.keyword') }}</th>
            <th style="width:35px;">{{ __('probtp.action') }}</th>
        </tr>
        </thead>
        <tbody>
        {{--{{$i = 1}}--}}
        @foreach($module_list as $row)
            <tr>
                <td style="text-align:center;"><input type="checkbox" name="module_id[]" value="{{$row->id}}" class="modalcheckbox" id = "user_id" /></td>
                <td>{{$row->title}}</td>
                <td>{{$row->keyword}}</td>
                <td> <a type="button" href="javascript:void(0);" onclick="changelink(this.id);" class="duplicate_data" id="{{$row->id}}"><i
                                title="Copy Module" class="fa fa-btn fa-files-o"></i></a></td>
            </tr>

        @endforeach
        <input type="hidden" id="savebutton" value="2">
        </tbody>
    </table>
@endif

<script>
    // for Duplicate of module

    function changelink(id){

        type_class = $("#category_class").val();
        var confirmstatus = confirm("Êtes-vous sûr de vouloir dupliquer cette record ?");

        var path = app.config.SITE_PATH+'module/replicate/'+id+'/'+type;

        if(confirmstatus){
            window.location = path;
        }

        /*$.ajax({

            data: {id:id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {
                window.location = app.config.SITE_PATH+'category';
            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });*/
    }


</script>