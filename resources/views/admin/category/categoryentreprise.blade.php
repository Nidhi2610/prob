<div id="myModalCategoryEntreprise" class="modal fade">

    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="POST" action="{{route('entrepriseCategory')}}"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" value="" name="category_id" id="categoryid"/>
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ __('probtp.assign_enterprise')}}</h4>
                </div>


                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                                        <thead>
                                        <tr>
                                            <th style="text-align:center" class="no-sort"><input type="checkbox"
                                                                                                 id="checkAll2"
                                                                                                 class="checkAll2"/>
                                            </th>
                                            <th>{{ __('probtp.select_enterprise') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($entObj as $ent)
                                          <?php  //  dd($ent); ?>
                                            @if( $ent->parent_id == 0 )

                                                <tr>
                                                    <td style="text-align:center"><input type="checkbox"
                                                                                         name="entreprise_id[]"
                                                                                         value="{{$ent->id}}"
                                                                                         class="modalcheckbox enterprises"
                                                                                         id="entreprise_id_{{$ent->id}}"/>
                                                    </td>
                                                    <td> {{$ent->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <table class="table table-striped  table-bordered"
                                                               cellspacing="0" >
                                                            <tbody>

                                                            <?php  $ids = \App\Models\Entreprise::where('parent_id', '=', $ent->id)->get();  ?>
                                                            @foreach($ids as $id)
                                                                <tr>
                                                                    <td style="text-align:center"><input type="checkbox"
                                                                                                         name="child_entreprise_id[]"
                                                                                                         value="{{$id->id}}"
                                                                                                         class="childmodalcheckbox enterprises parent_entreprise_id_{{$ent->id}}" id="{{$ent->id}}"
                                                                        />
                                                                    </td>
                                                                    <td>
                                                                        {{$id->name}}
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>

                                                        </table>
                                                    </td>
                                                </tr>

                                            @endif
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
                    <button type="submit" class="btn btn-primary"
                            id="submitForm">{{__('probtp.attach_enterprise')}}</button>
                </div>

            </div>
            <input type="hidden" name="category_classification" id="category_type" value="">
            <!-- /.modal-content -->
        </form>
    </div>
</div>

@push('script')
<script src="{{ asset('public/sortable/sortable.js') }}"></script>
@endpush