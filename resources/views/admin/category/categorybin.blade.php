@extends('layouts.app')
@section('content')
<form class="form-horizontal" role="form" id="deleteForm" method="POST" action="{{ route('categoryPermenantDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"/>
        <input type="hidden" value="{{$type}}" name="category_classification" id="category_classification"/>
    </form>
    <form class="form-horizontal" role="form" id="restoreForm" method="POST" action="{{ route('categoryRestore') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="restore_id"/>
        <input type="hidden" value="{{$type}}" name="category_classification" id="category_classification"/>
    </form>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>
                            @if($type == 'F')
                                {{ __('probtp.trash_category_list') }}
                            @else
                                {{ __('probtp.trash_resource_list') }}
                            @endif
                        </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width:20px" class="no-sort"><input type="checkbox" id="checkAll" class="checkAll"/></th>
                                <th>
                                        @if($type == 'F')
                                            {{ __('probtp.category') }}
                                        @else
                                            {{ __('probtp.resources') }}
                                        @endif
                                </th>
                                <th style="width:35px" class="no-sort">{{ __('probtp.action') }}</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $category)
                                <tr>

                                    <td><input type="checkbox" name="member_id[]" value="{{$category->id}}"
                                               class="checkAllChild"/></td>
                                    <td>{{$category->name}}</td>
                                    <td class="text-center">
                                        @if(Auth::user()->user_type == 1 ||Auth::user()->user_type == 2 )
                                            <div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                                <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">

                                                    <li> <a href="javascript:void(0)" id="{{$category->id}}"   data-id ="{{$category->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                    <li> <a href="javascript:void(0)" id="{{$category->id}}"   data-id ="{{$category->id}}" class=" restore"><i class="fa fa-btn fa-undo" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.restore') }}</a></li>
                                                </ul>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@include('layouts.stack',['extra'=>''])
