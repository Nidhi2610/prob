@extends('layouts.app')
@push('css')
<style>
    .category-active-status{color: green; margin-left: 10px;}
    .category-inactive-status{color: red; margin-left: 10px;}

</style>
@endpush
@section('content')
    @inject('EntObj', 'App\Models\Entreprise')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('categoryDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
        <input type="hidden" name="category_classification" id="category_classification" value="{{$type}}" />

    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                @if($type == 'F')
                                    {{ __('probtp.category_list') }}
                                @else
                                    {{ __('probtp.resource_list') }}
                                @endif
                            </h1>
                        {{-- <a type="button" href="{{route('entrepriseAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}
                            <a href="{{url('category/get_index_search/'.$type)}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-alt" aria-hidden="true"></i> {{ __('probtp.back_category_list')}}
                            </a>
                            <a href="{{url('category/create/'.$type)}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add_category')}}
                            </a>
                        </div>
                    </div>
                    <input type="hidden" name="category_classification" id="category_classification" value="{{$type}}" />
                    <div class="panel-body">
					<div class="col-md-1"> &nbsp; </div>
                       <div class="col-md-10 category-lists">
							<div id="categoryList"></div>
						</div>
					<div class="col-md-1"> &nbsp; </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.category.categorymodal')
@include('layouts.stack',['extra'=>''])
@include('admin.category.categoryentreprise',['entObj' => $entObj])

@push('script')
<script src="{{ asset('public/modalpopup/popup.js') }}"></script>
<script>
	var data = '<?php echo addslashes(json_encode($data))?>';
	data = JSON.parse(data);

</script>

<script src="{{ asset('public/sortable/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/sortable/jquery.mjs.nestedSortable.js') }}"></script>
<script src="{{ asset('public/sortable/sortable.js') }}"></script>
@endpush


