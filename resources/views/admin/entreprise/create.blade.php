@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                       <h1>{{ __('probtp.enterprise_create') }}</h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('entrepriseStore') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">
                            <div class="form-group{{ $errors->has('parent_category') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.enterprise_parent') }}</label>

                                <div class="col-md-6">
                                    <?php  $tree =  \App\Models\Entreprise::where('parent_id',0)->get() ;  ?>

                                        <?php  print("<select class='form-control' style='font-weight: bold;'  name='parent_id' id='parent_id '>\n");?>

                                        <option value="0" style="font-weight: bold;">
                                            Les enterprises
                                           </option>
                                        @foreach($tree as $t)
                                            <option value="{{$t->id}}">{{$t->name}}</option>
                                        @endforeach
                                       <?php  print("</select>");      ?>

                                        @if ($errors->has('parent_category'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('parent_category') }}</strong>
                                        </span>
                                        @endif

                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.entreprise_name') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.logo') }}</label>

                                <div class="col-md-6">
                                    <input id="logo" type="file" class="form-control" name="logo" value="{{ old('logo') }}"   autofocus>
                                    @if ($errors->has('logo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{route('entrepriseIndex')}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
