@foreach($users as $user)
    <?php
        $enterprise_ids = explode(",",$user->entreprise_id);
    ?>
    <tr>
        <td style="text-align:center">
            <input type="checkbox" value="{{$user->id}}" @if(in_array($enterprise_id,$enterprise_ids)) {{"checked"}} @endif class="modalcheckbox enterprises" />
            <input type="hidden" class="user_enterprise_ids" name="users[{{$user->id}}]" value="{{$user->entreprise_id}}" />
        </td>
        <td>{{$user->name}}</td>
    </tr>
@endforeach