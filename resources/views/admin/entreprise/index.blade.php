@extends('layouts.app')
@push('css')
<style>
    .enterprise_users{margin-left: 5px;}
    .assigner{margin-left: 10px;}
    .entrepriseList,.CommentList,.FormationList,.ResourceList{
        display: none;
    }

</style>
@endpush
@section('content')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('entrepriseDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.enterprise_list') }}</h1>
                       {{-- <a type="button" href="{{route('entrepriseAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}
                        <a href="{{route('entrepriseAdd')}}" class="btn btn-white btn-default pull-right ">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add')}}
                        </a>
                        <a href="{{route('entrepriseIndex')}}" class="entrepriseList btn btn-white btn-default pull-right ">
                            <i class="fa fa-list-ul" aria-hidden="true"></i> {{ __('probtp.entrepriseList')}}
                        </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width: 20px" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th style="width: 140px">{{ __('probtp.entreprise_name') }}</th>
                                <th style="width: 140px">{{ __('probtp.administrator')}}</th>
                                <th style="width: 140px">{{ __('probtp.formateur')}}</th>
                                {{--<th>{{ __('probtp.responsable')}}</th>--}}
                                <th>Membres</th>
                                {{--<th>{{ __('probtp.collaborateur')}}</th>--}}
                                <th>{{ __('probtp.logo')}}</th>
                                <th style="width: 35px" class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(sizeof($data) == 1)
                                    <tr id="user_{{$data->id}}">
                                        <td><input type="checkbox" name="member_id[]" value="{{$data->id}}"  class="checkAllChild" /></td>
                                        <td>
                                            @if($data->parent_id)
                                                {{$data->name}}  <a href="#user_{{$data->parent_id}}"><i class="fa fa-level-up" style="padding-left: 10px;"></i></a>
                                            @else
                                                {{$data->name}}
                                            @endif
                                            <?php $data_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($data->id); ?>

                                            @if(sizeof($data_collection))
                                                <a href="#childlist_{{$data->id}}"  data-toggle="collapse" data-parent="#accordion"   aria-expanded="true">
                                                    <i class="fa fa-chevron-down" area-hidden="true" style="font-size: 18px;"></i>
                                                </a>
                                            @endif
                                            <div class="collapse" id="childlist_{{$data->id}}">
                                                <?php $data_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($data->id); ?>
                                                @foreach($data_collection as $child)
                                                    <div style="padding-right: 10px;font-weight: normal;color:#00a3b4">
                                                        <a href="#user_{{$child->id}}">
                                                            <i class="fa fa-btn fa-minus" style="padding-right: 8px;"></i>{{$child->name}}
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" rel="{{$data->id}}" role="2" class="enterprise_users"><i class="fa fa-btn fa-plus"></i><span class="assigner">Assigner</span></a>
                                            <?php  $users = \App\Helpers\LaraHelpers::getUserEnterpriseData($data->id,2);
                                            if($users){
                                                echo "<ul><li>".$users ."</li></ul>";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" rel="{{$data->id}}" role="5" class="enterprise_users"><i class="fa fa-btn fa-plus"></i><span class="assigner">Assigner</span></a>
                                            <?php $users =  \App\Helpers\LaraHelpers::getUserEnterpriseData($data->id,5);
                                            if($users){
                                                echo "<ul><li>".$users ."</li></ul>";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php $users_collection =  \App\Helpers\LaraHelpers::getCollobaroteurEnterpriseData($data->id,3);
                                            //                                                    dd($users_collection);
                                            foreach ($users_collection as $user_data){
                                            echo'<div style="border-top: solid 1px #dddddd"></div>';
                                            echo   $user_data['name'].'<br/>';
                                            if(!empty($user_data['child_array'])){

                                            foreach ($user_data['child_array'] as $child_data){ ?>
                                            <div style="color:#00a3b4"><i style="float: left;margin-left:7px;margin-top: 5px; margin-right: 5px;color:#00a3b4;" id="{{$child_data->id}}" class="fa fa-btn fa-minus delete_user"></i> {{$child_data->name}}</div>

                                            <?php }

                                            }

                                            }?>

                                            {{-- <a href="javascript:void(0);" rel="{{$entreprise->id}}" role="3" class="enterprise_users"><i class="fa fa-btn fa-plus"></i></a>--}}
                                        </td>

                                        @if($data->logo != "")
                                            <td><img src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ $data->logo }}" style="width:50px;height:50px;cursor:pointer;" class="bm_image" data-title="{{ $data->name }}"/></td>
                                        @else
                                            <td><img src="{{ URL::to('/') }}/storage/app/public/files/enterprise/noimage.png" alt="{{ $data->logo }}" style="width:50px;height:50px;cursor:pointer;" class="bm_image" /></td>
                                        @endif

                                        <td class="text-center">
                                            {{--<a type="button" href="{{route('entrepriseEdit',['id'=>$entreprise->id])}}"><i title="Edit" class="fa fa-btn fa-pencil"></i></a>
                                            <a type="button" data-id ="{{$entreprise->id}}" href="javascript:void(0);" title="Delete" class="delete"><i class="fa fa-btn fa-trash"></i></a>--}}
                                            <div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                                <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                    <li> <a href="{{route('entrepriseEdit',['id'=>$data->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i>{{ __('probtp.edit') }}</a></li>
                                                    <li> <a href="javascript:void(0)"   data-id ="{{$data->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @else
                                    @foreach($data as $entreprise)
                                        <tr id="user_{{$entreprise->id}}">
                                            <td><input type="checkbox" name="member_id[]" value="{{$entreprise->id}}"  class="checkAllChild" /></td>
                                            <td>
                                                @if($entreprise->parent_id)
                                                   {{$entreprise->name}}  <a href="#user_{{$entreprise->parent_id}}"><i class="fa fa-level-up" style="padding-left: 10px;"></i></a>
                                                @else
                                                    {{$entreprise->name}}
                                                @endif
                                                <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($entreprise->id); ?>

                                                @if(sizeof($enterprise_collection))
                                                    <a href="#childlist_{{$entreprise->id}}"  data-toggle="collapse" data-parent="#accordion"   aria-expanded="true">
                                                        <i class="fa fa-chevron-down" area-hidden="true" style="font-size: 18px;"></i>
                                                    </a>
                                                @endif
                                            <div class="collapse" id="childlist_{{$entreprise->id}}">
                                                <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($entreprise->id); ?>
                                                @foreach($enterprise_collection as $child)
                                                <div style="padding-right: 10px;font-weight: normal;color:#00a3b4">
                                                    <a href="#user_{{$child->id}}">
                                                        <i class="fa fa-btn fa-minus" style="padding-right: 8px;"></i>{{$child->name}}
                                                    </a>
                                                </div>
                                                @endforeach
                                            </div>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" rel="{{$entreprise->id}}" role="2" class="enterprise_users"><i class="fa fa-btn fa-plus"></i><span class="assigner">Assigner</span></a>
                                            <?php  $users = \App\Helpers\LaraHelpers::getUserEnterpriseData($entreprise->id,2);
                                                if($users){
                                                    echo "<ul><li>".$users ."</li></ul>";
                                                }
                                               ?>
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" rel="{{$entreprise->id}}" role="5" class="enterprise_users"><i class="fa fa-btn fa-plus"></i><span class="assigner">Assigner</span></a>
                                            <?php $users =  \App\Helpers\LaraHelpers::getUserEnterpriseData($entreprise->id,5);
                                                if($users){
                                                    echo "<ul><li>".$users ."</li></ul>";
                                                }
                                            ?>
                                            </td>
                                            <td>
                                                <?php $users_collection =  \App\Helpers\LaraHelpers::getCollobaroteurEnterpriseData($entreprise->id,3);
    //                                                    dd($users_collection);
                                                        foreach ($users_collection as $user_data){
                                                            echo'<div style="border-top: solid 1px #dddddd"></div>';
                                                            echo   $user_data['name'].'<br/>';
                                                            if(!empty($user_data['child_array'])){

                                                                foreach ($user_data['child_array'] as $child_data){ ?>
                                                                  <div style="color:#00a3b4"><i style="float: left;margin-left:7px;margin-top: 5px; margin-right: 5px;color:#00a3b4;" id="{{$child_data->id}}" class="fa fa-btn fa-minus delete_user"></i> {{$child_data->name}}</div>

                                                        <?php }

                                                          }

                                                          }?>

                                               {{-- <a href="javascript:void(0);" rel="{{$entreprise->id}}" role="3" class="enterprise_users"><i class="fa fa-btn fa-plus"></i></a>--}}
                                            </td>

                                            @if($entreprise->logo != "")
                                                <td><img src="{{ URL::to('/') }}/storage/app/public/files/enterprise/{{ $entreprise->logo }}" style="width:50px;height:50px;cursor:pointer;" class="bm_image" data-title="{{ $entreprise->name }}"/></td>
                                            @else
                                               <td><img src="{{ URL::to('/') }}/storage/app/public/files/enterprise/noimage.png" alt="{{ $entreprise->logo }}" style="width:50px;height:50px;cursor:pointer;" class="bm_image" /></td>
                                            @endif

                                            <td class="text-center">
                                                {{--<a type="button" href="{{route('entrepriseEdit',['id'=>$entreprise->id])}}"><i title="Edit" class="fa fa-btn fa-pencil"></i></a>
                                                <a type="button" data-id ="{{$entreprise->id}}" href="javascript:void(0);" title="Delete" class="delete"><i class="fa fa-btn fa-trash"></i></a>--}}
                                                <div class="dropdown">
                                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                                    <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                        <li> <a href="{{route('entrepriseEdit',['id'=>$entreprise->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i>{{ __('probtp.edit') }}</a></li>
                                                        <li> <a href="javascript:void(0)"   data-id ="{{$entreprise->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                @endif
                            </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Image preview</h4>
                </div>
                <div class="modal-body" style="height: auto;">
                    <img src="" id="imagepreview" style="width: 100%;" >
                </div>
            </div>
        </div>
    </div>
    @include('admin.entreprise.modal')
@endsection
@include('layouts.stack',['extra'=>''])
@push('script')
<script>
    $(document).ready(function () {
        $(".bm_image").on("click", function() {
            var ent_name = $(this).attr('data-title');
            $('.modal-title').html(ent_name);
            $('#imagepreview').attr('src', $(this).attr('src'));
            $('#imagemodal').modal('show');
        });

        $(document).on('click','.enterprise_users',function () {
            var enterprise_id = $(this).attr('rel');
            var user_type = $(this).attr('role');

            $.ajax({
                type: "POST",
                url: '{{url('entreprise/get_user_of_enterprise')}}',
                data: { enterprise_id : enterprise_id,user_type:user_type,status:status,_token: csrf_token}
            }).done(function(data){
                if(data.statusCode == 1){
                    $("#enterprise_id").val(enterprise_id);
                    $(".append_users").html(data.data);
                    $("#myEnterpriseUserModal").modal('show');
                    if(user_type == 5){
                        $(".modal-title").html('{{ __('probtp.assign_enterprise_formature')}}');
                    }else{
                        $(".modal-title").html('{{ __('probtp.assign_enterprise_admin')}}');

                    }
                }else{
                    alert("Users Not available");
                }

            });

        });

        $(document).on('click','.enterprises', function (event) {
            var enterprise_id = $("#enterprise_id").val();
            var user_enterprise_ids = $(this).closest("td").children(".user_enterprise_ids").val().split(",");
            if($(this).prop('checked') == true){

                user_enterprise_ids.push(enterprise_id);

                $(this).closest("td").children(".user_enterprise_ids").val(user_enterprise_ids);
            }else{
                var user_enterprise_ids = $(this).closest("td").children(".user_enterprise_ids").val().split(",");
                removeA(user_enterprise_ids, enterprise_id);
                $(this).closest("td").children(".user_enterprise_ids").val(user_enterprise_ids);
            }
        });

    });

    var windowHref = window.location.href.toString();
    if(windowHref.indexOf('/entreprise?id=') !== -1){
        jQuery('.entrepriseList').css('display','block');
    }else{ console.log('gerui');
        jQuery('.entrepriseList').css('display','none');
    }


    $(document).on('click','.delete_user',function () {
       var id = $(this).attr('id');

        $.ajax({

            data: {id:id},
            type: 'POST',
            url: url,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {

            },
            error: function (xhr, status, error) {
                // check status && error
                console.log(status);
            }
        });

    });

    function removeA(arr) {
        var what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        return arr;
    }

</script>
<script src="{{ asset('public/modalpopup/popup.js') }}"></script>
@endpush