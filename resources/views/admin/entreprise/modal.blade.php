<div id="myEnterpriseUserModal" class="modal fade">
    <div class="modal-dialog">
        <form class="form-horizontal" role="form" method="POST"
              action="{{url('entreprise/add_enterprise_to_user')}}"
              enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" value="" name="enterprise_id" id="enterprise_id"/>
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ __('probtp.assign_enterprise_admin')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <table class="table table-striped  table-bordered" cellspacing="0" id="data-table2">
                                        <thead>
                                        <tr>
                                            <th style="text-align:center;width:20px;" class="no-sort"><input type="checkbox"
                                                                                                 id="checkAll2"
                                                                                                 class="checkAll2"/>
                                            </th>
                                            <th>{{ __('probtp.select_all_user')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody class="append_users">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
                    <button type="submit" class="btn btn-primary" id="submitForm">{{__('probtp.attach_enterprise')}}</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </form>
    </div>
</div>