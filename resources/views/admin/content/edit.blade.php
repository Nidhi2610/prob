@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                       <h1>Présentation du site</h1>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('contentUpdate') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$data->id or ""}}">
                            <input type="hidden" name="dbAction" value="Edit">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.content') }}</label>

                                <div class="col-md-6">
                                        <textarea id="summary" name="content" value="{{ old('content', $data->content)}}" class="form-control ckeditor" cols="50" rows="5">{{ old('content', $data->content)}}</textarea>
                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                    &nbsp; &nbsp;

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    $(document).ready(function(){

        // For Editor

        tinymce.init({     selector: '#summary',    relative_urls : false,    remove_script_host : false,    convert_urls : true,
            plugins: [  'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code autoresize'],
            toolbar:    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect | fontselect |  fontsizeselect',
            language:   'fr_FR',
        });

    });
</script>
@endpush
@push('css')
<style>
    #html5-watermark{
        display:none!important;
    }
</style>
@endpush
