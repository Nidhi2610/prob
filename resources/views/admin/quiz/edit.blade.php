@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1> {{ __('probtp.quiz_edit') }}</h1>
                        </div>
                        </div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('quizUpdate') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}">
                            <input type="hidden" name="dbAction" value="Edit">

                            <div class="form-group{{ $errors->has('quiz_name') ? ' has-error' : '' }}">
                                <label for="quiz_name" class="col-md-4 control-label">{{ __('probtp.name')  }}</label>

                                <div class="col-md-6">
                                    <input id="quiz_name" type="text" class="form-control" name="quiz_name" value="{{ $editData->quiz_name }}" required autofocus>

                                    @if ($errors->has('quiz_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('quiz_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="add_questions">
                                @if(count($editData->QuizQueAns) > 0)
                                    @foreach($editData->QuizQueAns as $quiz_key => $quiz_value)
                                        <?php
                                            $class = "fa-minus";
                                            $class_action = "remove_que";
                                            if($quiz_key == "0"){
                                                $class = "fa-plus";
                                                $class_action = "add_que";
                                            }
                                            $answers = json_decode($quiz_value->answers);

                                            $true_ans = explode(",",$quiz_value->true_answers);

                                        ?>
                                        <div class="form-group col-md-12 que_ans_manage">
                                            <label for="quiz_name" class="col-md-3 control-label"></label>
                                            <table class="col-md-6 table table-bordered table-stripped" style="width: 60%;">
                                                <thead>
                                                <tr>
                                                    <th>{{__('probtp.questions')}}</th>
                                                    <th><input type="text" class="form-control required" name="quiz[{{$quiz_key}}][question]" value="{{$quiz_value->question}}" /></th>
                                                    <th> <a type="button" href="javascript:void(0);" class="{{$class_action}}"><i class="fa fa-btn {{$class}}"></i></a> </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{__('probtp.answers')}}</td>
                                                    <td><input type="text" name="quiz[{{$quiz_key}}][answers][A]" class="form-control required" value="{{$answers->A}}"  /></td>
                                                    <td><input type="checkbox" name="quiz[{{$quiz_key}}][true_ans][]" value="A" @foreach($true_ans as $val) @if($val == "A") {{"checked"}} @endif @endforeach /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><input type="text" class="form-control required" name="quiz[{{$quiz_key}}][answers][B]" value="{{$answers->B}}" /></td>
                                                    <td><input type="checkbox" name="quiz[{{$quiz_key}}][true_ans][]" value="B" @foreach($true_ans as $val) @if($val == "B") {{"checked"}} @endif @endforeach /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="quiz[{{$quiz_key}}][answers][C]" value="{{$answers->C}}" /></td>
                                                    <td><input type="checkbox" name="quiz[{{$quiz_key}}][true_ans][]" value="C" @foreach($true_ans as $val) @if($val == "C") {{"checked"}} @endif @endforeach /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><input type="text" class="form-control" name="quiz[{{$quiz_key}}][answers][D]" value="{{$answers->D}}" /></td>
                                                    <td><input type="checkbox" name="quiz[{{$quiz_key}}][true_ans][]" value="D" @foreach($true_ans as $val) @if($val == "D") {{"checked"}} @endif @endforeach /></td>
                                                </tr>
                                                <tr>
                                                    <td>{{__('probtp.description')}}</td>
                                                    <td><textarea class="form-control" name="quiz[{{$quiz_key}}][answers][desc]">{{$answers->desc or ""}}</textarea></td>
                                                    <td></td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    @endforeach
                                 @else
                                    <div class="form-group col-md-12 que_ans_manage">
                                        <label for="quiz_name" class="col-md-3 control-label"></label>
                                        <table class="col-md-6 table table-bordered table-stripped" style="width: 60%;">
                                            <thead>
                                            <tr>
                                                <th>{{__('probtp.questions')}}</th>
                                                <th><input type="text" class="form-control required" name="quiz[0][question]" /></th>
                                                <th> <a type="button" href="javascript:void(0);" class="add_que"><i class="fa fa-btn fa-plus"></i></a> </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{__('probtp.answers')}}</td>
                                                <td><input type="text" name="quiz[0][answers][A]" class="form-control required"  /></td>
                                                <td><input type="checkbox" name="quiz[0][true_ans][]" value="A" /></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><input type="text" class="form-control required" name="quiz[0][answers][B]" /></td>
                                                <td><input type="checkbox" name="quiz[0][true_ans][]" value="B" /></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><input type="text" class="form-control" name="quiz[0][answers][C]" /></td>
                                                <td><input type="checkbox" name="quiz[0][true_ans][]" value="C" /></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><input type="text" class="form-control" name="quiz[0][answers][D]" /></td>
                                                <td><input type="checkbox" name="quiz[0][true_ans][]" value="D" /></td>
                                            </tr>
                                            <tr>
                                                <td>{{__('probtp.description')}}</td>
                                                <td><textarea class="form-control" name="quiz[0][answers][desc]"></textarea></td>
                                                <td></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                 @endif


                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{__('probtp.status')  }}</label>

                                <div class="col-md-6">
                                    <select id="status" class="form-control" name="status" required autofocus>
                                        <option value="0" @if($editData->status == "0") {{"selected"}} @endif>{{__('probtp.enable')}}</option>
                                        <option value="1" @if($editData->status == "1") {{"selected"}} @endif>{{__('probtp.disable')}}</option>
                                    </select>

                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{route('quizIndex')}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script src="{{url('public/js/jquery.validate.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#probtp_form").validate();

        $(document).on('submit',"#probtp_form",function () {
            if(!$("#probtp_form").valid()){
                return false;
            }
        });
        $(document).on('click',".add_que", function () {
            var count = $(".que_ans_manage").length;
            var que = '{{__('probtp.questions')}}';
            var ans = '{{__('probtp.answers')}}';
            var desc = '{{__('probtp.description')}}';

            var html = '<div class="form-group col-md-12 que_ans_manage">'
                +'<label for="quiz_name" class="col-md-3 control-label"></label>'
                +'<table class="col-md-6 table table-bordered table-stripped" style="width: 60%;">'
                +'<thead>'
                +'<tr>'
                +'<th>'+que+'</th>'
                +'<th><input type="text" class="form-control required" name="quiz['+count+'][question]" /></th>'
                +'<th> <a type="button" href="javascript:void(0);" class="remove_que"><i class="fa fa-btn fa-minus"></i></a> </th></tr></thead>'
                +'<tbody>'
                +'<tr>'
                +'<td>'+ans+'</td>'
                +'<td><input type="text" name="quiz['+count+'][answers][A]" class="form-control required"  /></td>'
                +'<td><input type="checkbox" name="quiz['+count+'][true_ans][]" value="A" /></td></tr>'
                +'<tr>'
                +'<td></td>'
                +'<td><input type="text" class="form-control required" name="quiz['+count+'][answers][B]" /></td>'
                +'<td><input type="checkbox" name="quiz['+count+'][true_ans][]" value="B" /></td></tr>'
                +'<tr>'
                +'<td></td>'
                +'<td><input type="text" class="form-control" name="quiz['+count+'][answers][C]" /></td>'
                +'<td><input type="checkbox" name="quiz['+count+'][true_ans][]" value="C" /></td></tr>'
                +'<tr>'
                +'<td></td>'
                +'<td><input type="text" class="form-control" name="quiz['+count+'][answers][D]" /></td>'
                +'<td><input type="checkbox" name="quiz['+count+'][true_ans][]" value="D" /></td></tr>'
                +'<tr>'
                +'<td>'+desc+'</td>'
                +'<td><textarea class="form-control" name="quiz['+count+'][answers][desc]"></textarea></td>'
                +'<td></td></tr></tbody></table></div>';

            $(".add_questions").append(html);

        });

        $(document).on('click',".remove_que", function () {
            $(this).closest('.que_ans_manage').remove();
            var count = 0;
            $(".que_ans_manage").each(function () {

                $(this).closest('tr').children('th:eq(1)').children('input').attr('name','quiz['+count+'][question]');

                $(this).closest('table').children('tbody').children('tr:eq(0)').children('td:eq(2)').children('input').attr('name','quiz['+count+'][answers][A]');
                $(this).closest('table').children('tbody').children('tr:eq(0)').children('td:eq(3)').children('input').attr('name','quiz['+count+'][true_ans][]');

                $(this).closest('table').children('tbody').children('tr:eq(1)').children('td:eq(2)').children('input').attr('name','quiz['+count+'][answers][B]');
                $(this).closest('table').children('tbody').children('tr:eq(1)').children('td:eq(3)').children('input').attr('name','quiz['+count+'][true_ans][]');

                $(this).closest('table').children('tbody').children('tr:eq(2)').children('td:eq(2)').children('input').attr('name','quiz['+count+'][answers][C]');
                $(this).closest('table').children('tbody').children('tr:eq(2)').children('td:eq(3)').children('input').attr('name','quiz['+count+'][true_ans][]');

                $(this).closest('table').children('tbody').children('tr:eq(3)').children('td:eq(2)').children('input').attr('name','quiz['+count+'][answers][D]');
                $(this).closest('table').children('tbody').children('tr:eq(3)').children('td:eq(3)').children('input').attr('name','quiz['+count+'][true_ans][]');

                $(this).closest('table').children('tbody').children('tr:eq(4)').children('td:eq(2)').children('textarea').attr('name','quiz['+count+'][answers][desc]');
                count++;
            });
        });

    });
</script>
@endpush
