@extends('layouts.app')

@section('content')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('quizDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>
    <form class="form-horizontal" role="form" id="duplicateForm" method="POST" action="{{ route('quizReplicate') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="duplicate_id"/>
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.quiz_list') }}</h1>
                      {{-- <a type="button" href="{{route('quizAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}
                        <a href="{{route('quizAdd')}}" class="btn btn-white btn-default pull-right ">
                            <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add')}}
                        </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width: 20px;" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th>{{ __('probtp.quiz') }}</th>
                                <th style="width: 35px" class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $template)
                                <tr>
                                    <td><input type="checkbox" name="template_id[]" value="{{$template->id}}"  class="checkAllChild" /></td>
                                    <td>{{$template->quiz_name}}</td>
                                    <td class="text-center">

                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                            <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                <li> <a href="{{route('quizEdit',['id'=>$template->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i>{{ __('probtp.edit') }}</a></li>
                                                <li> <a href="javascript:void(0)" id="{{$template->id}}"   data-id ="{{$template->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                <li> <a href="javascript:void(0)" id="{{$template->id}}"   data-id ="{{$template->id}}" class="duplicate"><i class="fa fa-btn fa-files-o" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.duplicate') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.stack',['extra'=>''])