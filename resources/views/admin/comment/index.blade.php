@extends('layouts.app')

@section('content')

    <form class="form-horizontal" role="form" id="deleteForm" method="POST" action="{{ route('commentDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"/>
    </form>

    <form class="form-horizontal" role="form" id="getCategory" method="POST" action="{{ route('getCommentById') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="catgory_id" id="category_value"/>
    </form>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1> Gestion des commentaires</h1>
                            <a href="{{url('comment')}}" class="CommentList btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-ul" aria-hidden="true"></i> {{ __('probtp.CommentList')}}
                            </a>
                        </div>

                    </div>

                    <div class="panel-body">

                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                          <thead>

                            <tr>
                                <th>{{ __('probtp.comment') }}</th>
                                <th class="no-sort">{{ __('probtp.user') }}</th>

                                <th class="no-sort" style="width: 300px">
                                    <select class="form-control" name="catgory_id" id="catgory_id">
                                        <option value="0">{{ __('probtp.select')}} Catégorie</option>
                                        <optgroup label="Formation">

                                          @foreach($categoryFormation as $categoryForData)
                                                <option value="{{$categoryForData['id']}}"@if(isset($category_id) && $category_id == $categoryForData['id'] ){{"selected"}}@endif> {{ $categoryForData['name'] }}</option>

                                            @endforeach
                                        </optgroup>
                                        <optgroup label="Ressources">
                                            @foreach($categoryResource as $categoryResData)
                                                <option value="{{$categoryResData['id']}}"@if(isset($category_id) && $category_id == $categoryResData['id'] ){{"selected"}}@endif>{{ $categoryResData['name'] }}</option>
                                            @endforeach
                                        </optgroup>

                                    </select>
                                </th>
                                <th style="width: 35px" class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($data as $comment)
                                <tr>
                                    <td>{{$comment->comment}}</td>
                                    <td>{{$comment->User->name}}</td>
                                    <td>
                                        <?php $category_name = \App\Helpers\LaraHelpers::getCommentCategory($comment->category_id);
                                        echo $category_name->name;
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" id="{{$comment->id}}" data-id="{{$comment->id}}"
                                           class="member-delete delete"><i class="fa fa-trash fa-lg"
                                                                           aria-hidden="true"></i>{{ __('probtp.delete') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@include('layouts.stack',['extra'=>''])

@push('script')
<script>
    // For submitting the Category Form
    $(document).ready(function () {
        $("#catgory_id").on("change", function () {
            var id = $(this).val();
            if (id == 0) {
                window.location = "{{url('comment')}}";
                return false;
            }
            $('#category_value').val($(this).val());
            $('#getCategory').submit();

        });
    });
    $(window).on('load', function() {
        var windowHref = window.location.href.toString();

        if ((windowHref.indexOf('/comment?id=') !== -1) || (windowHref.indexOf('/comment?cat_id=') !== -1)) {
            jQuery('.CommentList').css('display', 'block');
        } else {
            console.log('gerui');
            jQuery('.CommentList').css('display', 'none');
        }
    });
</script>
@endpush