@extends('layouts.app')

@section('content')

    <form class="form-horizontal" role="form" id="deleteComment" method="POST"  action="{{ route('commentPermenantDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <form class="form-horizontal" role="form" id="restoreComment" method="POST" action="{{ route('commentRestore') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="restore_id"/>
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>Gestion des commentaires</h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th>{{ __('probtp.comment') }}</th>
                                <th class>{{ __('probtp.user') }}</th>
                                <th style="width:35px" class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $comment)
                                <tr>
                                    <td>{{$comment->comment}}</td>
                                    <td>{{($comment->User->name)}}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                            <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1">

                                                <li> <a href="javascript:void(0)" id="{{$comment->id}}"   data-id ="{{$comment->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                <li> <a href="javascript:void(0)" id="{{$comment->id}}"   data-id ="{{$comment->id}}" class=" restore"><i class="fa fa-btn fa-undo" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.restore') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.stack',['extra'=>''])
