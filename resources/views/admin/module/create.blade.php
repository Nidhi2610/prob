@extends('layouts.app')
@push('css')
<style>
    #s2id_category_id {
        height: auto!important;
    }
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.module_index_create') }}
                            @if($types == 'F')
                                (formation)
                            @else
                                (ressource)
                            @endif
                        </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="probtp_form" role="form" method="POST" action="{{ route('moduleStore') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">

							 <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.title') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Référence</label>

                                <div class="col-md-6">
                                    <input id="reference" type="text" class="form-control" name="reference" value="{{ old('reference') }}" autofocus>
                                    @if ($errors->has('reference'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('reference') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

							 <div class="form-group {{ $errors->has('keyword') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_keyword') }}</label>

                                <div class="col-md-6">
                                    <input id="keyword" type="text" class="form-control" name="keyword" value="{{ old('keyword') }}" autofocus>
								@if ($errors->has('keyword'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('keyword') }}</strong>
                                    </span>
                                    @endif
								</div>
								
                            </div>
							
							<div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_summary') }}</label>

                                <div class="col-md-6">
                                    
									<textarea name="summary" id="summary" value="{{ old('summary') }}" class="form-control ckeditor" cols="50" rows="5"></textarea>
								</div>
                            </div>

                            <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">
                                    @if($types == "F")
                                        Formations
                                        @elseif(($types == "R"))
                                        Ressources
                                        @endif
                                </label>

                                <div class="col-md-6">
                                    <?php $tree =  \App\Helpers\LaraHelpers::buildTree($category); ?>

                                    <select multiple class='form-control' style="height: auto !important;"  name='category_id[]' id='category_id'>

                                        <?php  \App\Helpers\LaraHelpers::printTree($tree, $r = 0, $p = null,$category_id);?>
                                    </select>

                                        @if ($errors->has('category_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                        @endif
                                </div>
                            </div>

							<div class="form-group {{ $errors->has('form_id') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.form') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control"  value="{{old('form_id')}}" name="form_id" id="form_id">
                                        <option value="">{{ __('probtp.select_form')  }}</option>
                                        @foreach($form as $forms)
										    <option value="{{$forms->form_id}}">{{ $forms->title }}</option>
                                        @endforeach
                                    </select>
                                   @if ($errors->has('form_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('form_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('quiz_id') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.quiz') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control"  value="{{old('quiz_id')}}" name="quiz_id" id="quiz_id">
                                        <option value="">{{ __('probtp.select_quiz')  }}</option>
                                        @foreach($quiz as $quizs)
                                            <option value="{{$quizs->id}}">{{ $quizs->quiz_name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('quiz_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('quiz_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{__('probtp.status')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">{{__('probtp.status') }}</option>
                                        <option value="0">Active</option>
                                        <option value="1" {{"selected"}} >Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
							
							<div class="form-group {{ $errors->has('file_name') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_file_name') }}</label>

                                <div class="col-md-6">
                                    <input id="file_name" type="file" class="form-control" name="file_name" value="{{ old('file_name') }}"   autofocus>
								@if ($errors->has('file_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('file_name') }}</strong>
                                    </span>
                                    @endif
								</div>
                            </div>
							<input type="hidden" value="{{$types}}" name="module_type" id="module_type">
							<div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{ url()->previous() }}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('public/multiselect/select2.min.js') }}"></script>
<link href="{{ asset('public/multiselect/select2.css') }}" rel="stylesheet">

<script>
$(document).ready(function(){
	$("#probtp_form").submit(function(){
		app.showLoader("#probtp_form");
	});

	// For editor

    tinymce.init({     selector: '#summary',    relative_urls : false,    remove_script_host : false,    convert_urls : true,
        plugins: [  'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code autoresize'],
        toolbar:    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect | fontselect |  fontsizeselect',
        language:   'fr_FR',
    });

});


$(window).on('load', function() {
    $("#category_id").select2();
});

</script>
@endpush
