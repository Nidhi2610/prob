@extends('layouts.app')
@push('css')
<style>
    #s2id_category_id {
        height: auto!important;
    }
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.module_index_edit') }}</h1>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" id="probtp_form" method="POST" enctype="multipart/form-data" action="{{ route('moduleUpdate') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$editData->id}}">
                            <input type="hidden" name="dbAction" value="Edit">


                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.title') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="title" value="{{$editData->title}}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Référence</label>

                            <div class="col-md-6">
                                <input id="reference" type="text" class="form-control" name="reference" value="{{$editData->reference}}" autofocus>
                                @if ($errors->has('reference'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reference') }}</strong>
                                    </span>
                                @endif
                            </div>
                    </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_keyword') }}</label>

                                <div class="col-md-6">
                                    <input id="keyword" type="text" class="form-control" name="keyword" value="{{$editData->keyword}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_summary') }}</label>

                                <div class="col-md-6">

                                    <textarea id="summary" name="summary" value="{{ old('summary', $editData->summary)}}" class="form-control ckeditor" cols="50" rows="5">{{ old('summary', $editData->summary)}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">

                                    @if($editData->module_type == "F")
                                        Formations
                                    @elseif($editData->module_type == "R")
                                        Ressources
                                    @endif
                                </label>

                                <div class="col-md-6">
                                    <?php $tree =  \App\Helpers\LaraHelpers::buildTree($categoryData); ?>
                                    <select multiple class='form-control' name='category_id[]' id='category_id'>
                                    <?php  \App\Helpers\LaraHelpers::printTree($tree, $r = 0, $p = null,$editData->category_id);?>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.form') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="form_id" id="form_id">
                                        <option value="0">{{ __('probtp.select_form')  }}</option>
                                        @foreach($form as $forms)
                                            <option value="{{$forms->form_id}}" @if($editData->form_id == $forms->form_id) selected @endif>{{ $forms->title }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.quiz') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="quiz_id" id="quiz_id">
                                        <option value="0">{{ __('probtp.select_quiz')  }}</option>
                                        @foreach($quiz as $quizs)
                                            <option value="{{$quizs->id}}" @if($editData->quiz_id == $quizs->id) selected @endif>{{ $quizs->quiz_name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="status" class="col-md-4 control-label">{{__('probtp.status')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status">
                                        <option value="">{{__('probtp.status') }}</option>
                                        <option value="0" @if($editData->status == "0") {{"selected"}} @endif>Active</option>
                                        <option value="1" @if($editData->status == "1") {{"selected"}} @endif>Inactive</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.modules_file_name') }}</label>

                                <div class="col-md-6">
                                    <input id="file_name" type="file" class="form-control" name="file_name" value="{{ old('file_name', $editData->file_name)}}" >
                                    <br/>
                                @if($editData->file_name != "")
                                            @if (pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'MP4' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'MOV' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'AVI' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'wmv' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'mp4')

                                            <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                    <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); } ?>
                                                    <div class="col-sm-1" style="padding-left: 0;">  <i class="fa fa-btn fa-video-camera fa-2x"></i></div>

                                                    <div class="col-sm-11" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;" rel="{{$editData->id}}"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$display_name}}</div>
                                                    {{--<div class="col-sm-4 deleteImage fa fa-window-close pull-right" style="cursor:pointer;" rel="{{$editData->id}}"></div>--}}

                                            </div>
                                            @elseif(pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'jpg' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'png' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'jpeg' )
                                                <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                    <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); }
                                                    $display_name = str_replace('_', ' ', $display_name);
                                                    ?>
                                                   <div class="col-sm-2" style="padding-left: 0;"><a href="{{url('storage/app/public/files/module/'.$editData->file_name)}}" class="html5lightbox" data-width="750" data-height="320" title="{{$editData->original_name or $display_name}}">
                                                           <img src="{{ URL::to('/') }}/storage/app/public/files/module/{{ $editData->file_name }}" style="width:50px;height:50px;cursor:pointer;" class="bm_image" />
                                                       </a>
                                                   </div>

                                                    <div class="col-sm-10" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>

                                                </div>
                                            @elseif(pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'pdf')

                                            <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); } ?>
                                                <div class="col-sm-1" style="padding-left: 0;">
                                                    <a href="{{url('storage/app/public/files/module/'.$editData->file_name)}}" class="html5lightbox" data-width="750" data-height="320" title="{{$editData->original_name or $display_name}}">
                                                    <i class="fa fa-btn fa-file-pdf-o fa-2x"></i>
                                                    </a>
                                                </div>

                                                <div class="col-sm-11" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;" rel="{{$editData->id}}"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>
                                                {{--<div class="col-sm-4 deleteImage fa fa-window-close pull-right" style="cursor:pointer;" rel="{{$editData->id}}"></div>--}}

                                            </div>

                                            @elseif(pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'doc'||pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'docx')
                                            <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); } ?>
                                                <div class="col-sm-1" style="padding-left: 0;">  <i class="fa fa-file-word-o fa-2x"></i></div>

                                                <div class="col-sm-11" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;" rel="{{$editData->id}}"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>
                                                {{--<div class="col-sm-4 deleteImage fa fa-window-close pull-right" style="cursor:pointer;" rel="{{$editData->id}}"></div>--}}

                                            </div>
                                            @elseif(pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'pptx' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'pptm' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'ppt'  )
                                            <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); } ?>
                                                <div class="col-sm-1" style="padding-left: 0;">  <i class="fa fa-file-powerpoint-o fa-2x"></i></div>

                                                <div class="col-sm-11" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;" rel="{{$editData->id}}"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>
                                            </div>                                            @elseif(pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'xlsx' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'xlsm' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'xls' || pathinfo($editData->file_name, PATHINFO_EXTENSION) === 'xml')
                                            <div class="col-sm-12 image_class" style="padding-left: 0;">
                                                <?php if(isset($editData->file_name)){$display_name = substr($editData->file_name, strpos($editData->file_name, "_") + 1); } ?>
                                                <div class="col-sm-1" style="padding-left: 0;">  <i class="fa fa-file-excel-o fa-2x"></i></div>

                                                <div class="col-sm-11" style="max-width: 480px;word-wrap: break-word;padding-left: 30px;" rel="{{$editData->id}}"> <i class=" fa fa-window-close fa-2x deleteImage" rel="{{$editData->id}}" style="position: absolute;cursor:pointer;color:red;left: -4px;">&nbsp;</i>{{$editData->original_name or $display_name}}</div>
                                            </div>
                                            @endif
                                      @endif
                                </div>
                            </div>
                            <input type="hidden" value="{{$editData->module_type}}" name="module_type" id="module_type">
                            <input type="hidden" name="delete_file" value="{{$editData->file_name}}" id="delete_file">
                            <input type="hidden" id="file_old" name="file_old" value="{{ old('file_name', $editData->file_name)}}">
                            <input type="hidden" id = "original_old_filename" name="original_old_filename" value="{{ old('original_name', $editData->original_name)}}">


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{url()->previous()}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    var category_data = JSON.stringify(<?php if(isset($moduleCategory)){echo $moduleCategory; }?>);

</script>
@push('script')
<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('public/multiselect/select2.min.js') }}"></script>
<link href="{{ asset('public/multiselect/select2.css') }}" rel="stylesheet">
<script src="{{ asset('public/multiselect/multiselect.js') }}"></script>
<script>
    $(document).ready(function(){
        $("#probtp_form").submit(function(){
            app.showLoader("#probtp_form");
        });

        // For Editor

        tinymce.init({     selector: '#summary',    relative_urls : false,    remove_script_host : false,    convert_urls : true,
            plugins: [  'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code autoresize'],
            toolbar:    'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | sizeselect | fontselect |  fontsizeselect',
            language:   'fr_FR',
        });

    });

    // For deleting the picture
    $(document).ready(function () {
        $(document).on('click','.deleteImage',function () {
            var result = confirm("Êtes-vous sûr de vouloir supprimer cette?");
            var id = $(this).attr('rel');
            var file_name = $('#delete_file').val();
            if (result) {
                $(this).closest('div.image_class').remove();
                $.ajax({
                    type: "POST",
                    url: app.config.SITE_PATH +'module/delete_file',
                    headers: {
                        'X-CSRF-TOKEN': window.Probtp.csrfToken
                    },
                    data: {id : id,file_name:file_name}

                }).done(function(data){
                    $('#original_old_filename').val("");
                    $('#file_old').val("");
                });
            }
        });
    });
</script>

@endpush
