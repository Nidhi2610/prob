@foreach($moduleid_data as $module)
    <div class="content">

        <div class="col-md-12 module_data">
            <div class="panel panel-default" id="subcategory_">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel-heading" id="module_title">
                            <h2>{{$module->Module->title}}</h2>
                        </div>
                    </div>
                    <div class="col-md-6 already_submitted_{{ $module->id }}">
                        <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                        @if(count($status) > 0)
                            <button type="submit"
                                    id="module_submit_button_{{$module->id}}"
                                    class="btn btn-primary  disabled submit_module">
                                Module déjà consulté
                            </button>
                        @endif
                    </div>
                </div>


                <div class="panel-body">

                    <div class="col-sm-12 col-header-nopadding">
                        <div class="attachement">

                            @if (pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'mp4' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MOV' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'AVI' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'wmv'|| pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'MP4')
                                <video class="video-js" controls preload="auto" width="80%"
                                       height=""
                                       data-setup="{}">
                                    <source src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                            type='video/mp4'>
                                </video>

                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'txt')
                                <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                    <i class="fa fa-btn fa-file-text fa-4x"></i>
                                </a>
                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpg' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) == 'png' || pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'jpeg' )
                                <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                    <img src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                         width="80%"/>
                                </a>
                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pdf')
                                {{-- <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                     <i class="fa fa-btn fa-file-pdf-o fa-2x"></i>
                                 </a>--}}
                                <iframe id="fred" class="fred"
                                        title="PDF in an i-Frame"
                                        src="{{url("storage/app/public/files/module/".$module->Module->file_name)}}"
                                        frameborder="1" scrolling="auto" height="600"
                                        width="80%"></iframe>
                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'doc'||pathinfo($module->file_name, PATHINFO_EXTENSION) === 'docx')
                                <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                    <i class="fa fa-file-word-o fa-4x "></i>
                                </a>
                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'pptx')
                                <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                    <i class="fa fa-file-powerpoint-o fa-4x "></i>
                                </a>
                            @elseif(pathinfo($module->Module->file_name, PATHINFO_EXTENSION) === 'xlsx')
                                <a href="{{url('category_module/'.$module->Module->file_name)}}">
                                    <i class="fa fa-file-excel-o fa-4x "></i>
                                </a>

                            @endif

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="summary">
                            {!! $module->Module->summary !!}
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6 submit_button_id_{{$module->id}}">
                            <div class="panel-default">
                                <!-------button is cretaed here.--->
                                <div class="row">
                                    <form id="module_submit_form{{$module->id}}" role="form"
                                          method="POST" action=""
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="hidden" name="module_id"
                                                   id="form_module_id"
                                                   value="{{$module->id}}"/>
                                            <input type="hidden" name="user_id"
                                                   id="form_user_id"
                                                   value="{{Auth::user()->id}}"/>
                                            <div class="col-md-12">
                                                <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                @if(count($status) > 0)
                                                    {{--<button type="submit"--}}
                                                    {{--id="module_submit_button_{{$module->id}}"--}}
                                                    {{--class="btn btn-primary  disabled submit_module">--}}
                                                    {{--Module déjà consulté--}}
                                                    {{--</button>--}}

                                                @else
                                                    <button type="submit"
                                                            id="module_submit_button_{{$module->id}}"
                                                            class="btn btn-primary module_submit submit_module title_btn disabled">
                                                        J'ai terminé ce module
                                                    </button>
                                                    {{--<p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p> --}}
                                                @endif

                                            </div>
                                            <br/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @push('script')
                            <script>
                                $(document).ready(function () {
                                    var module_id = '{{$module->id}}';
                                    $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                        event.preventDefault();
                                        event.stopPropagation();
                                        if (!$("#module_submit_button_" + module_id).valid()) {
                                            return false;
                                        }
                                        var form_data = {
                                            'module_id': module_id,
                                        };

                                        var path = app.config.SITE_PATH + 'add_module_count';

                                        $.ajax({
                                            data: form_data,
                                            type: 'POST',
                                            url: path,
                                            headers: {
                                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                                            },
                                            success: function (data) {
                                                if (data.status == 1) {
                                                    console.log(data);

                                                    $('#module_submit_button_' + module_id).attr('disabled', true);
                                                    $('#module_submit_button_' + module_id).text('Module déjà consulté');
//                                                                            $('submit_button_id_'+ module_id).appendTo('already_submitted_'+ module_id);
                                                } else {
                                                    //$('#module_submit_button_' + module_id).attr('disabled', true);

                                                    //$(".submit_successfully_"+ module_id).css('display','block');


                                                }
                                            },
                                            error: function (xhr, status, error) {
                                                // check status && error
                                                console.log(status);
                                            }
                                        });

                                    });

                                });
                            </script>
                            @endpush
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form_title">
                            <div class="form_content actualites_cat">
                                @if(!empty($module->form_data))
                                    <?php $form_content = json_decode($module->form_data->content);?>
                                    <div class="row">
                                        <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                        @if(count($status) > 0)
                                            <button type="submit"
                                                    id="module_submit_button_{{$module->id}}"
                                                    class="btn btn-primary disabled  module_submit test_form_uniq">
                                                Module déjà consulté
                                            </button>
                                        @endif
                                        <div class="col-md-12 containttestform">
                                            <div class="panel panel-default" id="subcategory_{{ $module->form_data->title}}">

                                                <div class="panel-heading"><i
                                                            class="fa fa-tasks fa-lg fa-fw"
                                                            aria-hidden="true"></i>{{ $module->form_data->title }}
                                                </div>

                                                <div class="panel-body">
                                                    @if(count($module['form_user_data']) > 0)
                                                        @foreach($module['form_user_data'] as $formData)

                                                            <div class="form-group">
                                                                <div class="finalanswers">
                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal">{{$formData->form_name}}</label>
                                                                    <div class="col-md-6">
                                                                        <label class="font-normal" >{{$formData->form_value}}</label><br>
                                                                    </div>
                                                                </div>
                                                                <br></div>
                                                        @endforeach

                                                    @else
                                                        <form class="form-horizontal"
                                                              id="dynamic_form_{{$module->id}}"
                                                              role="form"
                                                              method="POST"
                                                              action="javascript:void(0);"
                                                              enctype="multipart/form-data">
                                                            {{ csrf_field() }}
                                                            @foreach($form_content as $key => $row)
                                                                <div class="form-group">
                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal">{{$row->label}}</label>
                                                                    <div class="col-md-6">
                                                                        <input type="hidden"
                                                                               name="form_data[{{$key}}][name]"
                                                                               value="{{$row->label}}"/>
                                                                        @if($row->type == "text")
                                                                            <input type="text"
                                                                                   class="form-control required"
                                                                                   name="form_data[{{$key}}][value]"
                                                                                   value=""/>
                                                                        @elseif($row->type == "textarea")
                                                                            <textarea
                                                                                    name="form_data[{{$key}}][value]"
                                                                                    class="form-control required"></textarea>
                                                                        @elseif($row->type == "select")
                                                                            <select name="form_data[{{$key}}][value]"
                                                                                    class="form-control required">
                                                                                @foreach($row->values as $select)
                                                                                    <option value="{{$select->value}}">{{$select->label}}</option>
                                                                                @endforeach

                                                                            </select>
                                                                        @elseif($row->type == "checkbox-group")
                                                                            <div class="input-group">
                                                                                @foreach($row->values as $check_box)
                                                                                    <input type="checkbox"
                                                                                           class="required"
                                                                                           name="form_data[{{$key}}][value][]"
                                                                                           id="check_{{$check_box->value}}"
                                                                                           value="{{$check_box->value}}"/>
                                                                                    <label class="font-normal"
                                                                                           for="check_{{$check_box->value}}">{{$check_box->label}}</label>
                                                                                    <br/>
                                                                                @endforeach
                                                                            </div>
                                                                        @elseif($row->type == "radio-group")
                                                                            <div class="input-group">
                                                                                @foreach($row->values as $radio_box)
                                                                                    <input type="radio"
                                                                                           class="required"
                                                                                           name="form_data[{{$key}}][value]"
                                                                                           id="radio_{{$radio_box->value}}"
                                                                                           value="{{$radio_box->value}}"/>
                                                                                    <label class="font-normal"
                                                                                           for="radio_{{$radio_box->value}}">{{$radio_box->label}}</label>
                                                                                    <br/>
                                                                                @endforeach
                                                                            </div>
                                                                        @else
                                                                            <input type="text"
                                                                                   class="form-control required"
                                                                                   name="form_data[{{$key}}][value]"
                                                                                   value=""/>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            <input type="hidden" name="form_id"
                                                                   value="{{$module->form_data->form_id}}"/>
                                                            <input type="hidden"
                                                                   name="category_id"
                                                                   value=""/>
                                                            <input type="hidden" name=""
                                                                   value=""/>
                                                            <input type="hidden"
                                                                   name="module_category_id"
                                                                   value="{{$module->id}}"/>
                                                            <div class="form-group">
                                                                <div class="col-md-8 col-md-offset-4">
                                                                    <button type="submit"
                                                                            id="dynamic_button_{{$module->id}}"
                                                                            class="btn btn-primary module_submit disabled">
                                                                        Poster
                                                                    </button>
                                                                    &nbsp; &nbsp;
                                                                    <button type="reset"
                                                                            class="btn btn-primary module_submit disabled">
                                                                        {{ __('probtp.reset') }}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-------button is cretaed here.--->
                                            <div class="panel-default">
                                                <div class="row">
                                                    <form id="module_submit_form{{$module->id}}"
                                                          role="form"
                                                          method="POST" action=""
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <input type="hidden"
                                                                   name="module_id"
                                                                   id="form_module_id"
                                                                   value="{{$module->id}}"/>
                                                            <input type="hidden" name="user_id"
                                                                   id="form_user_id"
                                                                   value="{{Auth::user()->id}}"/>
                                                            <div class="col-md-12">
                                                                <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                                @if(count($status) > 0)
                                                                    <button type="submit"
                                                                            id="module_submit_button_{{$module->id}}"
                                                                            class="btn btn-primary disabled  module_submit test_form_uniq">
                                                                        Module déjà consulté
                                                                    </button>

                                                                @else
                                                                    <button type="submit"
                                                                            id="module_submit_button_{{$module->id}}"
                                                                            class="btn btn-primary module_submit title_btn disabled">
                                                                        J'ai terminé ce module
                                                                    </button>
                                                                    {{---  <p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p> --}}
                                                                @endif

                                                            </div>
                                                            <br/>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @push('script')
                                        <script>
                                            $(document).ready(function () {
                                                var module_id = '{{$module->id}}';
                                                $("#dynamic_form_" + module_id).validate({
                                                    errorPlacement: function (error, element) {
                                                        if ($(element).attr('type') == "checkbox" || $(element).attr('type') == "radio") {
                                                            $(element).closest("div").after(error);
                                                        } else {
                                                            $(element).after(error);
                                                        }
                                                    }
                                                });

                                                $(document).on('submit', "#dynamic_form_" + module_id, function (event) {

                                                    var module_id = '{{$module->id}}';
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    if (!$("#dynamic_form_" + module_id).valid()) {
                                                        return false;
                                                    }
                                                    $('#dynamic_button_' + module_id).attr('disabled', true);
                                                    var form_data = new FormData(this);
                                                    $.ajax({
                                                        cache: false,
                                                        url: '{{url('form_data_update')}}',
                                                        type: "POST",
                                                        data: form_data,
                                                        contentType: false,
                                                        processData: false,
                                                        success: function (response) {
                                                            if (response.statusCode == '1') {
                                                                $(".category_modules").html(response.data);
                                                                $('#dynamic_button_' + module_id).attr('enabled', true);

                                                            }
                                                        },
                                                        error: function (xhr) {
                                                            alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                                                        }
                                                    });
                                                });

                                                $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                    if (!$("#module_submit_button_" + module_id).valid()) {
                                                        return false;
                                                    }
                                                    var form_data = {
                                                        'module_id': module_id,
                                                    };

                                                    var path = app.config.SITE_PATH + 'add_module_count';

                                                    $.ajax({
                                                        data: form_data,
                                                        type: 'POST',
                                                        url: path,
                                                        headers: {
                                                            'X-CSRF-TOKEN': window.Probtp.csrfToken
                                                        },
                                                        success: function (data) {
                                                            if (data.status == 1) {
                                                                console.log(data);
                                                                $('#module_submit_button_' + module_id).attr('disabled', true);
                                                                $('#module_submit_button_' + module_id).text('Module déjà consulté');

                                                                //$('#module_submit_button_' + module_id).attr('disabled', true);


                                                            } else {
//                                                                                        $('#module_submit_button_' + module_id).attr('disabled', true);
//                                                                                        // $(".submit_successfully_"+ module_id).css('display','block');
//                                                                                        $('#module_submit_button_' + module_id).text('Module déjà consulté');

                                                            }
                                                        },
                                                        error: function (xhr, status, error) {
                                                            // check status && error
                                                            console.log(status);
                                                        }
                                                    });

                                                });

                                            });

                                        </script>
                                        @endpush

                                    </div>

                                @endif
                                @if(!empty($module->quiz_data->QuizQueAns))
                                    <div class="row">
                                        <div class="col-md-12 containttestform">
                                            <div class="panel panel-default" id="subcategory_{{$module->quiz_data->id}}">
                                                <div class="panel-heading"><i
                                                            class="fa fa-tasks fa-lg fa-fw"
                                                            aria-hidden="true"></i>{{ $module->quiz_data->quiz_name }}
                                                </div>
                                                <div class="panel-body">
                                                    @if(count($module['quiz_user_data']) > 0)
                                                        <?php $user_ponts = [];?>
                                                        @foreach($module['quiz_user_data'] as $quiz_key => $quizData)
                                                            <?php
                                                            $answers = json_decode($quizData->quiz_ans);
                                                            $true_ans = explode(",", $quizData->true_ans);
                                                            $user_ans = explode(",", $quizData->user_ans);
                                                            $user_ponts[] = $quizData->user_point;
                                                            $ans_class = "fa-times-circle";
                                                            if ($quizData->user_point) {
                                                                $ans_class = "fa-check-circle";
                                                            }

                                                            ?>
                                                            <div class="quizmark">
                                                                <div class="form-group">

                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal">Question {{$quiz_key + 1}}
                                                                        .</label>
                                                                    <div class="col-md-6">
                                                                        <label >{{$quizData->quiz_que}}</label><i
                                                                                class="fa {{$ans_class}} fa-lg fa-fw font-normal"
                                                                                aria-hidden="true"></i>
                                                                    </div>

                                                                </div>
                                                                <div class="form-group">
                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal"></label>
                                                                    <div class="col-md-6 ansoption">
                                                                        @if(!empty($answers->A))
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        class="required"
                                                                                        @foreach($user_ans as $row) @if($row == "A") {{"checked"}} @endif @endforeach value="A"/></span>{{$answers->A}}   @foreach($true_ans as $row) @if($row == "A")
                                                                                <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                   aria-hidden='true'></i> @endif @endforeach
                                                                            <br>
                                                                        @endif
                                                                        @if(!empty($answers->B))
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        class="required"
                                                                                        @foreach($user_ans as $row) @if($row == "B") {{"checked"}} @endif @endforeach value="B"/></span>{{$answers->B}} @foreach($true_ans as $row) @if($row == "B")
                                                                                <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                   aria-hidden='true'></i> @endif @endforeach
                                                                            <br>
                                                                        @endif
                                                                        @if(!empty($answers->C))
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        class="required"
                                                                                        @foreach($user_ans as $row) @if($row == "C") {{"checked"}} @endif @endforeach value="C"/></span>{{$answers->C}} @foreach($true_ans as $row) @if($row == "C")
                                                                                <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                   aria-hidden='true'></i> @endif @endforeach
                                                                            <br>
                                                                        @endif
                                                                        @if(!empty($answers->D))
                                                                            <span><input
                                                                                        type="checkbox"
                                                                                        class="required"
                                                                                        @foreach($user_ans as $row) @if($row == "D") {{"checked"}} @endif @endforeach value="D"/></span>{{$answers->D}} @foreach($true_ans as $row) @if($row == "D")
                                                                                <i class='fa fa-check-circle fa-lg fa-fw'
                                                                                   aria-hidden='true'></i> @endif @endforeach
                                                                            <br>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                @if(!empty($answers->desc))
                                                                    <div class="form-group">
                                                                        <label
                                                                                for="email"
                                                                                class="col-md-4 control-label font-normal">{{__('probtp.description')}}</label>
                                                                        <div class="col-md-6 ansoption">
                                                                            <p>{{$answers->desc}}</p>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        @endforeach



                                                    @else
                                                        <form class="form-horizontal"
                                                              id="dynamic_quiz_{{$module->id}}"
                                                              role="form"
                                                              method="POST"
                                                              action="javascript:void(0);"
                                                              enctype="multipart/form-data">
                                                            @foreach($module->quiz_data->QuizQueAns as $quiz_key => $quiz_value)
                                                                {{ csrf_field() }}
                                                                <?php $answers = json_decode($quiz_value->answers);?>
                                                                <div class="form-group">
                                                                    <input type="hidden"
                                                                           name="quiz_data[{{$quiz_key}}][quiz_que]"
                                                                           value="{{$quiz_value->question}}"/>
                                                                    <input type="hidden"
                                                                           name="quiz_data[{{$quiz_key}}][quiz_ans]"
                                                                           value="{{$quiz_value->answers}}"/>
                                                                    <input type="hidden"
                                                                           name="quiz_data[{{$quiz_key}}][true_ans]"
                                                                           value="{{$quiz_value->true_answers}}"/>
                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal">Question {{$quiz_key + 1}}
                                                                        .</label>
                                                                    <div class="col-md-6">
                                                                        <label class="font-normal">{{$quiz_value->question}}</label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label
                                                                            for="email"
                                                                            class="col-md-4 control-label font-normal"></label>
                                                                    <div class="col-md-6">
                                                                        <div class="input-group">
                                                                            @if(!empty($answers->A))

                                                                                <div class="col-md-2">
                                                                                    <input type="checkbox"
                                                                                           name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                           id="quiz_{{$module->id}}_{{$quiz_key}}_1"
                                                                                           class="required"
                                                                                           value="A"/>
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <label class="font-normal"
                                                                                           for="quiz_{{$module->id}}_{{$quiz_key}}_1">{{$answers->A}}</label>
                                                                                    <br>
                                                                                </div>
                                                                            @endif
                                                                            @if(!empty($answers->B))
                                                                                <div class="col-md-2">
                                                                                    <input type="checkbox"
                                                                                           name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                           id="quiz_{{$module->id}}_{{$quiz_key}}_2"
                                                                                           class="required"
                                                                                           value="B"/>
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <label class="font-normal"
                                                                                           for="quiz_{{$module->id}}_{{$quiz_key}}_2">{{$answers->B}}</label>
                                                                                    <br>
                                                                                </div>
                                                                            @endif
                                                                            @if(!empty($answers->C))
                                                                                <div class="col-md-2">
                                                                                    <input type="checkbox"
                                                                                           name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                           id="quiz_{{$module->id}}_{{$quiz_key}}_3"
                                                                                           class="required"
                                                                                           value="C"/>
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <label class="font-normal"
                                                                                           for="quiz_{{$module->id}}_{{$quiz_key}}_3">{{$answers->C}}</label>
                                                                                    <br>
                                                                                </div>
                                                                            @endif
                                                                            @if(!empty($answers->D))
                                                                                <div class="col-md-2">
                                                                                    <input type="checkbox"
                                                                                           name="quiz_data[{{$quiz_key}}][user_ans][]"
                                                                                           id="quiz_{{$module->id}}_{{$quiz_key}}_4"
                                                                                           class="required"
                                                                                           value="D"/>
                                                                                </div>
                                                                                <div class="col-md-10">
                                                                                    <label class="font-normal"
                                                                                           for="quiz_{{$module->id}}_{{$quiz_key}}_4">{{$answers->D}}</label>
                                                                                    <br>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            <input type="hidden" name="quiz_id"
                                                                   value="{{$module->quiz_data->id}}"/>
                                                            <input type="hidden"
                                                                   name="category_id"
                                                                   value=""/>
                                                            <input type="hidden" name=""
                                                                   value=""/>
                                                            <input type="hidden"
                                                                   name="module_category_id"
                                                                   value="{{$module->id}}"/>
                                                            <div class="form-group">
                                                                <div class="col-md-8 col-md-offset-4">
                                                                    <button type="submit"
                                                                            id="quiz_button_{{$module->id}}"
                                                                            class="btn btn-primary module_submit disabled">
                                                                        Poster
                                                                    </button>
                                                                    &nbsp; &nbsp;
                                                                    <button type="reset"
                                                                            id="resetbtn"
                                                                            class="btn btn-primary module_submit disabled">
                                                                        {{ __('probtp.reset') }}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    @endif
                                                </div>
                                            </div>
                                            <!-------button is cretaed here.--->

                                            <div class="panel-default submit_button_module">
                                                <div class="row">
                                                    <form id="module_submit_form{{$module->id}}"
                                                          role="form"
                                                          method="POST" action=""
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                            <input type="hidden"
                                                                   name="module_id"
                                                                   id="form_module_id"
                                                                   value="{{$module->id}}"/>
                                                            <input type="hidden" name="user_id"
                                                                   id="form_user_id"
                                                                   value="{{Auth::user()->id}}"/>

                                                            <div class="col-md-12">
                                                                <?php $status = \App\Models\UserModuleComplete::where('module_id', $module->id)->where('user_id', Auth::user()->id)->first();?>
                                                                @if(count($status) > 0)
                                                                    {{--<button type="submit"--}}
                                                                    {{--id="module_submit_button_{{$module->id}}"--}}
                                                                    {{--class="btn btn-primary  disabled module_submit test_form_uniq title_btn">--}}
                                                                    {{--Module déjà consulté--}}
                                                                    {{--</button>--}}

                                                                @else
                                                                    <button type="submit"
                                                                            id="module_submit_button_{{$module->id}}"
                                                                            class="btn btn-primary module_submit title_btn disabled">
                                                                        J'ai terminé ce module
                                                                    </button>
                                                                    {{--<p class="submit_successfully_{{$module->id}}" style="display:none "> Soumission terminée!!</p>--}}
                                                                @endif

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                            @push('script')
                                            <script>
                                                $(document).ready(function () {
                                                    var module_id = '{{$module->id}}';
                                                    $("#dynamic_quiz_" + module_id).validate({
                                                        errorPlacement: function (error, element) {
                                                            if ($(element).attr('type') == "radio") {
                                                                $(element).closest("div").after(error);
                                                            } else if ($(element).attr('type') == "checkbox") {
                                                                $(element).closest(".input-group").after(error);
                                                            } else {
                                                                $(element).after(error);
                                                            }
                                                        }
                                                    });

                                                    $(document).on('click', "#resetbtn", function () {
                                                        $("#dynamic_quiz_" + module_id).find("label.error").remove();
                                                    });

                                                    $(document).on('submit', "#dynamic_quiz_" + module_id, function (event) {

                                                        var module_id = '{{$module->id}}';
                                                        $('#quiz_button_' + module_id).attr('disabled', true);
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        if (!$("#dynamic_quiz_" + module_id).valid()) {
                                                            return false;
                                                        }
                                                        var form_data = new FormData(this);

                                                        $.ajax({
                                                            cache: false,
                                                            url: '{{url('quiz_data_update')}}',
                                                            type: "POST",
                                                            data: form_data,
                                                            contentType: false,
                                                            processData: false,
                                                            success: function (response) {
                                                                if (response.statusCode == '1') {
                                                                    $(".category_modules").html(response.data);
                                                                    $('#quiz_button_' + module_id).attr('enabled', false);
                                                                }
                                                            }
                                                        });
                                                    });
                                                    $('#module_submit_form{{$module->id}}').on('click', "#module_submit_button_" + module_id, function (event) {

                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                        if (!$("#module_submit_button_" + module_id).valid()) {
                                                            return false;
                                                        }
                                                        var form_data = {
                                                            'module_id': module_id,
                                                        };

                                                        var path = app.config.SITE_PATH + 'add_module_count';

                                                        $.ajax({
                                                            data: form_data,
                                                            type: 'POST',
                                                            url: path,
                                                            headers: {
                                                                'X-CSRF-TOKEN': window.Probtp.csrfToken
                                                            },
                                                            success: function (data) {
                                                                if (data.status == 1) {
                                                                    console.log(data);

                                                                    $('#module_submit_button_' + module_id).attr('disabled', true);
                                                                    $('#module_submit_button_' + module_id).text('Module déjà consulté');

                                                                } else {
//                                                                                            $('#module_submit_button_' + module_id).attr('disabled', true);
//                                                                                            // $(".submit_successfully_"+ module_id).css('display','block');
//                                                                                            $('#module_submit_button_' + module_id).text('Module déjà consulté');
                                                                }
                                                            },
                                                            error: function (xhr, status, error) {
                                                                // check status && error
                                                                console.log(status);
                                                            }
                                                        });

                                                    });

                                                });

                                            </script>
                                            @endpush
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <?php if(1 == 1) break; ?>
@endforeach

@push('css')
<style>
    #html5-watermark{
        display:none!important;
    }
    a:hover, a:focus {
        color: #00a3b4;
        text-decoration: none;
    }
    a {
        color: #00a3b4;
        text-decoration: none;
    }
</style>

@endpush