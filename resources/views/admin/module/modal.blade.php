<!-- Boottrap modal POPUP -->
  
  <div id="myModal" class="modal fade">
    <div class="modal-dialog">
	<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="modal-content">
			 
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">{{ __('probtp.assign_category_list')}}</h4>
					</div>

					<div class="modal-body">
					
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default">
									<div class="panel-body">
										<table class="table table-striped module_list  table-bordered" cellspacing="0" id="data-table2">

												<thead>
												<tr>
													<th>{{__('probtp.name')}}</th>
												</tr>
												</thead>
												<tbody class="push_catgory_list">

												</tbody>

										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">{{__('probtp.close')}}</button>
					</div>

			</div>
			<!-- /.modal-content -->
	</form>	
    </div>
  </div>