@extends('layouts.app')
@section('content')


    @inject('formObj', 'App\Models\Form')
     @inject('userObj', 'App\Models\User')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST" action="{{ route('moduleDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"/>
		<input type="hidden" value="{{Auth::user()->user_type}}" name="user_type" id="user_type"/>
        <input type="hidden" value="{{$type}}" name="module_type" id="module_type"/>
		
    </form>


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.module_index_list') }}
                        @if($type == 'F')
                                (formations)
                        @else
                                (ressources)
                        @endif
                        </h1>
                        @if(Auth::user()->user_type == 1 ||Auth::user()->user_type == 2 ||Auth::user()->user_type == 5 )
                       {{-- <a type="button" href="{{route('moduleAdd')}}" class="pull-right"><i
                                    class="fa fa-btn fa-plus"></i></a>--}}
                            <a href="{{url('module/create/0/'.$type)}}" class="btn btn-white btn-default pull-right " >
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add')}}
                            </a>
                        @endif
                            <a href="{{url('module/F')}}" class="FormationList btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-ul" aria-hidden="true"></i> {{ __('probtp.FormationList')}}
                            </a>
                            <a href="{{url('module/R')}}" class="ResourceList btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-ul" aria-hidden="true"></i> {{ __('probtp.ResourceList')}}
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width: 20px" class="no-sort"><input type="checkbox" id="checkAll" class="checkAll"/></th>
                                <th>Référence</th>
                                <th>{{ __('probtp.modules_title') }}</th>
                                <th>{{ __('probtp.modules_keyword') }}</th>
                                <th>{{ __('probtp.form') }}</th>
                                <th style="width: 35px" class="no-sort">{{ __('probtp.file') }}</th>
                                <th>{{ __('probtp.last_updated') }}</th>
                                <th>{{ __('probtp.status') }}</th>
                                <th style="width: 35px" class="no-sort">{{ __('probtp.action') }}</th>
                               
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $module)

                                <tr>

                                    <td><input type="checkbox" name="member_id[]" value="{{$module->id}}"
                                               class="checkAllChild"/></td>
                                    <td>{{$module->reference}}</td>
                                    <td>{{$module->title}}</td>
                                    <td>{{$module->keyword}}</td>
                                    <td>{{$formObj->getFormName($module->form_id)}}</td>
                                    <td style="text-align: center">

                                        <div class="dropdown">
                                            <?php $display_name = substr($module->file_name, strpos($module->file_name, "_") + 1); ?>
                                                @if($module->file_name)
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                            <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                <li>

                                                        <a href="{{ URL::to( '/storage/app/public/files/module/'. $module->file_name)  }}" target="_blank" download="{{$module->file_name}}" class="memver-edit">@if($module->file_name != "")<i class="fa fa-download" style="padding-right: 10px" aria-hidden="true"></i>{{ __('probtp.modules_file_download') }}@endif

                                                        @if (pathinfo($module->file_name, PATHINFO_EXTENSION) === 'MP4' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'MOV' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'AVI' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'wmv' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'mp4')
                                                            <i class="fa fa-btn fa-video-camera fa-1x"></i>&nbsp;&nbsp;

                                                            <a href="{{url('storage/app/public/files/module/'.$module->file_name)}}" class="html5lightbox" data-width="750" data-height="320" title="{{$module->original_name or $display_name}}"><i class="fa fa-eye" aria-hidden="true" style="padding-right: 10px"></i>{{ __('probtp.preview') }}</a>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'txt')
                                                            <i class="fa fa-btn fa-file-text fa-1x"></i>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'jpg' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'png' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'jpeg' )
                                                            <i class="fa fa-file-image-o fa-1x"></i> &nbsp;&nbsp;
                                                            <a href="{{url('storage/app/public/files/module/'.$module->file_name)}}" class="html5lightbox" data-width="750" data-height="320" title="{{$module->original_name or $display_name}}"><i class="fa fa-eye" aria-hidden="true" style="padding-right: 10px" ></i>{{ __('probtp.preview') }}</a>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'pdf')
                                                            <i class="fa fa-btn fa-file-pdf-o fa-1x"></i>&nbsp;&nbsp;
                                                            <a href="{{url('storage/app/public/files/module/'.$module->file_name)}}" class="html5lightbox" data-width="750" data-height="320" title="{{$module->original_name or $display_name}}"><i class="fa fa-eye" aria-hidden="true"style="padding-right: 10px"></i>{{ __('probtp.preview') }}</a>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'doc'||pathinfo($module->file_name, PATHINFO_EXTENSION) === 'docx')
                                                            <i class="fa fa-file-word-o fa-1x "></i>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'pptx' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'pptm' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'ppt'  )
                                                            <i class="fa fa-file-powerpoint-o fa-1x "></i>
                                                        @elseif(pathinfo($module->file_name, PATHINFO_EXTENSION) === 'xlsx' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'xlsm' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'xls' || pathinfo($module->file_name, PATHINFO_EXTENSION) === 'xml')
                                                            <i class="fa fa-file-excel-o fa-1x "></i>
                                                        @else
                                                            -
                                                        @endif
                                                    </a>

                                                </li>
                                            </ul>
                                                @endif
                                        </div>

                                    </td>
                                    <td>{{ $module->updated_at->format('d-M-Y')}}</td>
                                    <td>@if($module->status== 0){{"Active"}} @else{{"Inactive"}} @endif</td>
                                    <td class="text-center">
                                        @if(Auth::user()->user_type == 1 ||Auth::user()->user_type == 2||Auth::user()->user_type == 5 )
                                            {{--<a type="button" href="{{route('moduleEdit',['id'=>$module->id])}}"><i
                                                title="Edit" class="fa fa-btn fa-pencil"></i></a>
                                            <a type="button" data-id="{{$module->id}}" href="javascript:void(0);"
												 class="delete"><i title="Delete" class="fa fa-btn fa-trash"></i></a>&nbsp;
                                            <a type="button" href="javascript:void(0);" onclick="changelink(this.id);" title="Copy" class="duplicate_data" id="{{$module->id}}"><i
                                                        class="fa fa-btn fa-files-o"></i></a>
                                           <a  type="button" href="javascript:void(0);" onclick="getCategory(this.id);" id="{{$module->id}}" data-toggle="modal">
                                                <span title="List of Category" class="fa fa-list"></span></a>
                                            <a type="button" href="{{route('previewPage',['id'=>$module->id])}}"><i
                                                        title="Preview" class="fa fa-btn fa-eye"></i></a>--}}

                                            <div class="dropdown">
                                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                                <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                    <li> <a href="{{url('module/edit/'.$module->id.'/'.$type)}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i>{{ __('probtp.edit') }}</a></li>
                                                    <li> <a href="javascript:void(0)" id="{{$module->id}}"   data-id ="{{$module->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                    <li> <a href="javascript:void(0)" onclick="changelink(this.id);" class="duplicate_data" id="{{$module->id}}"><i class="fa fa-btn fa-files-o" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.duplicate') }}</a></li>
                                                    <li> <a href="javascript:void(0)" onclick="getCategory(this.id);" data-toggle="modal" class="duplicate_data" id="{{$module->id}}"><i class="fa fa-list" style="padding-right: 10px;"  aria-hidden="true"></i>Liste des catégories</a></li>
                                                    <li> <a href="{{route('previewPage',['id'=>$module->id])}}" id="{{$module->id}}"><i class="fa fa-btn fa-eye" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.preview') }}</a></li>
                                                </ul>
                                            </div>
                                        @endif
										      <!--<a type="button" href="{{route('moduleDetail',['id'=>$module->id])}}"><i
                                                class="fa fa-btn fa-eye"></i></a>-->
										
                                    </td>
                                    
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
	
    /*function getmoduleid(id) {
		
            $("#module_id").val(id);
			var usertype =  $("#user_type").val();
			
			if(usertype == 2 || usertype == 1){
				var url = "{{route('getResponsableModule')}}";
			}
			else{
				var url = "{{route('getCollaborateurModule')}}"
			}
			
            $.ajax({
				
                data: {id: id},
                type: 'POST',
                url: url,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function (data) {
                    
                    $('.modalcheckbox').prop('checked', false);
                    $.each(data, function (key, val) {
						$('.modalcheckbox[value="' + val.responsable_id + '"]').prop('checked', true);
                    });
                    $("#myModal").modal("show");
                },
                error: function (xhr, status, error) {
                    // check status && error
                    console.log(status);
                }
            });


        }*/
    // for Duplicate of module

    function changelink(id) {
        type = $("#module_type").val();
        var confirmstatus = confirm("Êtes-vous sûr de vouloir dupliquer ce module");

        var path = app.config.SITE_PATH+'module/replicate/'+id+'/'+type;

        if(confirmstatus){
            window.location = path;
        }
    }
	</script>
@include('admin.module.modal')
@endsection
@include('layouts.stack',['extra'=>''])
@push('script')
<script src="{{ asset('public/modalpopup/popup.js') }}"></script>
<script>

    function getCategory(id) {


        $.ajax({

            data: {id: id},
            type: 'POST',
            url: app.config.SITE_PATH+'module/get_module_categorylist',
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            success: function (data) {

                if(data.statusCode === 1){
                    $(".push_catgory_list").html(data.data);
                    $("#myModal").modal("show");
                }else{
                    alert("No category assigned")
                }


            },
            error: function (xhr, status, error) {
                // check status && error
                //console.log(status);
            }
        });


    }

// For removing watermark logo in html five

    var html5lightbox_options = {
        watermark: "http://html5box.com/images/html5boxlogo.png",
        watermarklink: ""
    };
    $(window).on('load', function() {
        var windowHref = window.location.href.toString();
        if (windowHref.indexOf('/module/F?id=') !== -1) {
            jQuery('.FormationList').css('display', 'block');
        } else {
            console.log('gerui');
            jQuery('.FormationList').css('display', 'none');
        }

        if (windowHref.indexOf('/module/R?id=') !== -1) {
            jQuery('.ResourceList').css('display', 'block');
        } else {
            console.log('gerui');
            jQuery('.ResourceList').css('display', 'none');
        }
    });
</script>
@endpush
@push('css')
<style>
    #html5-watermark{
        display:none!important;
    }
</style>

@endpush
