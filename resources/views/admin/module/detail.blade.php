@extends('layouts.app')
@section('content')
<!--{{$likesdata}}-->
	<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$data->title}}</div>

                    <div class="panel-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="title" class="col-md-4 control-label">{{ __('probtp.keyword') }}</label>
                                <div class="col-md-6">
                                   @foreach($keyword as $keys)
									<span class="badge badge-default">{{$keys}}</span>
									@endforeach
									
                                </div>
                            </div>
							
							<div class="form-group">
                                <label for="title" class="col-md-4 control-label">{{ __('probtp.summary') }}</label>
                                <div class="col-md-6">
                                    <label for="email" class="control-label">{{$data->summary}}</label>
                                </div>
                            </div>
							
							<div class="form-group">
                                <label for="title" class="col-md-4 control-label">{{ __('probtp.category') }}</label>
                                <div class="col-md-6">
                                    <label for="email" class="control-label">{{$category}}</label>
                                </div>
                            </div>
							
							<div class="form-group">
                                <label for="title" class="col-md-4 control-label">{{ __('probtp.file') }}</label>
                                <div class="col-md-6">
                                    <label for="email" class="control-label"><a href="{{route('moduleDownload',['filename'=>$data->file_name])}}">{{ __('probtp.modules_file_download') }}</a></label>
                                </div>
                            </div>
							
							<textarea name="content" id="content" style="display: none;">{{$formdata}}</textarea>
						  
                        </form>
						
						
						<div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <a href="{{route('moduleIndex')}}"><button class="btn btn-primary" value="">
                                        {{ __('probtp.back') }}
                                    </button></a>
                                    &nbsp; &nbsp;
                                </div>
                        </div>
						
                    </div>
                </div>
            </div>
			
			<div class="col-md-12">
			  <div class="panel panel-default">
				<div class = "panel-heading"> {{ __('probtp.comment') }} & {{ __('probtp.likes') }} </div>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
							<input type="hidden" name="module_id"  value="{{ $data->id }}" />
							<input type="hidden" name="user_id"  value="{{ Auth::user()->id }}" />
							<div class = "col-md-8">
							
							<div class="form-group">
								<div class="col-md-4">
									<textarea name="comments" class="form-control" cols="50" rows="5"></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-4">
									<button class="btn btn-primary">
										{{ __('probtp.comment') }}
									</button>
									&nbsp; &nbsp;
								</div>
							</div>
							</div>
						</form>
							<div class="col-md-4">
									<div class="form-group">
										<button onClick ="getlikes();" id = "likes" class="btn btn-default" aria-label="Left Align">
											<span id="likecolor" class="fa fa-thumbs-up" aria-hidden="true"></span>
										</button>
									</div>
							</div>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection
