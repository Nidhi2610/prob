@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>{{ __('probtp.user_add') }}</h1>
                       {{--<h1>{{ __('probtp.module_add', ['module' => __('probtp.user')]) }}</h1>--}}
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('userStore') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="dbAction" value="Create">

                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-4 control-label">{{ __('probtp.first_name') }}</label>

                                <div class="col-md-6">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="{{ __('probtp.first_name') }}" required autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">{{ __('probtp.last_name') }}</label>

                                <div class="col-md-6">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="{{ __('probtp.last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <input type="hidden" name="parent_id" id="parent_id" value="{{Auth::user()->id}}" />

                            <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                                <label for="user_type" class="col-md-4 control-label">{{ __('probtp.user')." ".__('probtp.type')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="user_type" id="user_type">
                                    <?php  $types = [];
                                               $types[2]="Administrator";
                                               $types[5]= "Formateur";
                                               $types[3]="Responsable RH";
                                               $types[4]="Collaborateur";
                                               ?>
                                        <option @if(old('user_type'))value="<?php echo old('user_type');?>" @else value="" @endif>@if(old('user_type')) <?php echo $types[old('user_type')];?> @else {{ __('probtp.select')}}@endif</option>
                                        @foreach($userTypes as $userType)
                                            <option value="{{$userType['user_type_id']}}" @if(Auth::user()->user_type == '3') {{"selected"}} @endif>{{ $userType['user_type'] }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 2)
                            <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}" id ="parent_div">
                                <label for="Parent User" class="col-md-4 control-label">{{ __('probtp.parent')." ".__('probtp.user')}}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="parent_id" id="parent_id">
                                            <option value="">{{ __('probtp.select')." ".__('probtp.parent')." ".__('probtp.user') }}</option>
                                        @foreach($responsableData as $Data)
                                            <option value="{{$Data['id']}}" @if(old('parent_id') == $Data['id']){{"selected"}} @endif>{{ $Data['name'] }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('parent_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('parent_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @endif


                            <div  id = "single_enterpriseid" class="form-group{{ $errors->has('entreprise_id') ? ' has-error' : '' }}" @if(Auth::user()->user_type == 3)style="display:none" @endif>
                                <label for="entreprise_id" class="col-md-4 control-label">{{ __('probtp.select')." ".__('probtp.entreprise')}}</label>
                                {{--//#3346--}}
                                <div class="col-md-6">
                                    <select class="form-control" required name="entreprise_id" id="singleid">

                                        <option value="">{{ __('probtp.select')." ".__('probtp.entreprise') }}</option>
                                        <?php $enterprise_id = Auth::user()->entreprise_id?>
                                        @foreach($entrepriseData as $row)
                                            @if($row->parent_id == 0 )
                                            <option style="font-weight:600" value="{{$row->id}}" @if($enterprise_id == $row->id) {{"selected"}} @endif >{{ $row->name }}</option>

                                                <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($row->id);?>
                                                @foreach($enterprise_collection as $child)
                                                       <option style="padding-left: 15px;color:#5e5e5e;" value="{{$child->id}}" @if($enterprise_id == $child->id) {{"selected"}} @endif > - {{$child->name}} </option>
                                                @endforeach
                                             @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('entreprise_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('entreprise_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div id="multiple_enterpriseid" class="form-group{{ $errors->has('entreprise_id') ? ' has-error' : '' }}" @if(Auth::user()->user_type == 3)style="display:none" @endif>
                                <label for="entreprise_id" class="col-md-4 control-label">{{ __('probtp.select')." ".__('probtp.entreprise')}}</label>

                                <div class="col-md-6">
                                    <select multiple class="form-control"  name="entreprise_id[]" id="entreprise_id">
                                        <option value="">{{ __('probtp.select')." ".__('probtp.entreprise') }}</option>
                                        <?php $ente_id = Auth::user()->entreprise_id;
                                        $enterprise_ids = explode(",",$ente_id);
                                        ?>
                                        @foreach($entrepriseData as $row)
                                            @if($row->parent_id == 0 )
                                                <option value="{{$row->id}}" @if($enterprise_id == $row->id) {{"selected"}} @endif >{{ $row->name }}</option>

                                            <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($row->id);?>
                                                @foreach($enterprise_collection as $child)

                                                    {{--<option style="padding-left: 15px; color:#3346;" value="{{$child->id}}" @if(old('entreprise_id'))  {{(in_array($child->id,old('entreprise_id'))) ? 'selected' : '' }} @else($enterprise_id == $child->id) {{"selected"}} @endif > - {{$child->name}} </option>--}}
                                                    {{--<option style="padding-left: 15px; color:#3346;" value="{{$child->id}}" @if(old('entreprise_id')) {{(in_array($child->id,old('entreprise_id'))) ? 'selected' : '' }} @endif  @if($enterprise_id == $child->id) {{"selected"}} @endif > - {{$child->name}} </option>--}}
                                                    {{--<option style="padding-left: 15px; color:#3346;" value="{{$child->id}}" @if(old('entreprise_id')) {{(in_array($child->id,old('entreprise_id'))) ? 'selected' : '' }} @elseif($enterprise_id == $child->id) {{"selected"}} @endif > - {{$child->name}} </option>--}}
                                                    {{--<option style="padding-left: 15px; color:#3346;" value="{{$child->id}}" @if($enterprise_id == $child->id) {{"selected"}} @endif > - {{$child->name}} </option>--}}
                                                    <option style="padding-left: 15px; color:#3346;" value="{{$child->id}}" @if(count(old('entreprise_id'))) {{(in_array($child->id,old('entreprise_id'))) ? 'selected' : '' }} @elseif(in_array($child->id,$enterprise_ids)) {{"selected"}} @endif > - {{$child->name}} </option>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('entreprise_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('entreprise_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">{{ __('probtp.email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('probtp.email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">{{ __('probtp.password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="{{ __('probtp.password') }}" required autofocus>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                                <label for="confirm_password" class="col-md-4 control-label">{{ __('probtp.confirm_password') }}</label>

                                <div class="col-md-6">
                                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}" placeholder="{{ __('probtp.confirm_password') }}" required autofocus>

                                    @if ($errors->has('confirm_password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-danger" onclick="window.location='{{route('userIndex')}}'">
                                        {{ __('probtp.cancel') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('probtp.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script src="{{ asset('public/multiselect/select2.min.js') }}"></script>
<link href="{{ asset('public/multiselect/select2.css') }}" rel="stylesheet">
<script>
    $('#user_type').on('change', function() {
        var user_value = $(this).val();
        if(user_value == 2 || user_value == 5){
            $('#parent_div').hide();
            $('#multiple_enterpriseid').show();
            $('#single_enterpriseid').hide();
            $('#singleid').prop('disabled','disabled');

        }else if(user_value == 3){
            $('#parent_div').hide();
            $('#single_enterpriseid').show();
            $('#singleid').prop('disabled',false);
            $('#multiple_enterpriseid').hide();
            $('#singleid').attr('required','required');
        }else if(user_value == 4){
            $('#parent_div').show();
            $('#multiple_enterpriseid').hide();
            $('#single_enterpriseid').hide();
            $('#singleid').prop('disabled','disabled');
        }
    });

 // For Multi select
    $(window).on('load', function() {
        $("#entreprise_id").select2();
        $('#parent_div').hide();
        $('#single_enterpriseid').hide();
        $('#singleid').prop('disabled','disabled');

        var user_type = $('#user_type').val(); console.log(user_type);
            if(user_type == 4){
                $('#parent_div').show();
                $('#multiple_enterpriseid').hide();
                $('#single_enterpriseid').hide();
                $('#singleid').prop('disabled','disabled');
        }
    });
</script>
@endpush