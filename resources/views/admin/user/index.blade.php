@extends('layouts.app')
@push('css')
<style>
    .UserList{display: none;}

</style>
@endpush
@section('content')
    @inject('userObj', 'App\Models\User')
    @inject('enterpriseObj', 'App\Models\Entreprise')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('userDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>{{ __('probtp.user_list') }}</h1>
                            {{--<a type="button" href="{{route('userAdd')}}" class="pull-right"><i class="fa fa-btn fa-plus"></i></a>--}}

                            <a href="{{route('userAdd')}}" class="btn btn-white btn-default pull-right ">
                                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('probtp.add')}}
                            </a>
                            <a href="{{route('userIndex')}}" class="UserList btn btn-white btn-default pull-right ">
                                <i class="fa fa-list-ul" aria-hidden="true"></i> {{ __('probtp.UserList')}}
                            </a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width:20px;" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th>{{ __('probtp.user_name') }}</th>
                                <th>{{ __('probtp.type') }}</th>
                                <th>{{ __('probtp.email') }}</th>
                                <th>Entreprises</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $userTypes = Config::get('constant.user_types');?>

                            @if(count($data) > 0)
                            @if(sizeof($data)==1)
                                @if($data->user_type != 4)

                                    <tr id="user_{{$data->id}}">
                                        <td><input type="checkbox" name="member_id[]" value="{{$data->id}}"  class="checkAllChild" /></td>

                                        <td style="width:480px; ";>

                                            {{$data->name}}

                                            <?php $users_collection =  \App\Helpers\LaraHelpers::getchildColloborateru($data->id); ?>

                                            @if(sizeof($users_collection))
                                                <a href="#childlist_{{$data->id}}"  data-toggle="collapse" data-parent="#accordion"   aria-expanded="true">
                                                    <i class="fa fa-chevron-down" area-hidden="true" style="font-size: 18px;"></i>
                                                </a>
                                            @endif

                                            <div  class="collapse" style="margin-left: 16px;" id="childlist_{{$data->id}}">
                                                <?php $users_collection =  \App\Helpers\LaraHelpers::getchildColloborateru($data->id); ?>
                                                @if(count($users_collection)>0)
                                                    @foreach($users_collection as $user_data)

                                                        @if($data->user_type != 4)
                                                            <table style="margin-left: 16px;">
                                                                <thead>
                                                                <tr style="border-bottom: 1px solid #ddd;border-right:1px solid #ddd ">
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"><i class="fa fa-btn fa-minus"></i></th>
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"> {{$user_data->name}}</th>
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"; >{{$user_data->email}}</th>
                                                                    <th><a href="{{route('userEdit',['id'=>$user_data['id']])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i></a>
                                                                        <a href="javascript:void(0)"   data-id ="{{$user_data->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a></th>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$userTypes[$data->user_type]}}</td>
                                        <td>{{$data->email}}
                                            <a href="{{route('userEdit',['id'=>$data->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-left:10px;padding-right: 10px;" aria-hidden="true"></i></a>
                                            <a href="javascript:void(0)"   data-id ="{{$data->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a>
                                        </td>
                                        <td>
                                            @if($data->user_type == 2 ||$data->user_type == 5 )
                                                <?php $entreprises = $enterpriseObj->getMultipleEntrepriseByID($data->entreprise_id);  ?>
                                                @if(count($entreprises) > 0)
                                                    @foreach($entreprises as $entrepris)
                                                        {{$entrepris->name}},
                                                        <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($entrepris->id);?>
                                                        @foreach($enterprise_collection as $child)
                                                            <div style="font-weight: normal;color:#00a3b4">    <i class="fa fa-minus" style="color:#00a3b4;padding-right: 5px;"></i>{{$child->name}}</div>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @else

                                                <?php $user_entreprise = $enterpriseObj->getEntrepriseByID($data->entreprise_id) ?>
                                                @if(count($user_entreprise) > 0)
                                                    {{$user_entreprise->name}}
                                                    <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($user_entreprise->id);?>
                                                    @foreach($enterprise_collection as $child)
                                                        <div style="font-weight: normal;color:#00a3b4"><i class="fa fa-minus" style="color:#00a3b4;padding-right: 5px;"></i>{{$child->name}}</div>

                                                    @endforeach
                                                @endif
                                            @endif
                                        </td>


                                    </tr>

                                @endif
                            @else
                            @foreach($data as $user)
                                @if($user->user_type != 4)

                                    <tr id="user_{{$user->id}}">
                                        <td><input type="checkbox" name="member_id[]" value="{{$user->id}}"  class="checkAllChild" /></td>

                                        <td style="width:480px; ";>

                                            {{$user->name}}

                                            <?php $users_collection =  \App\Helpers\LaraHelpers::getchildColloborateru($user->id); ?>

                                            @if(sizeof($users_collection))
                                                <a href="#childlist_{{$user->id}}"  data-toggle="collapse" data-parent="#accordion"   aria-expanded="true">
                                                    <i class="fa fa-chevron-down" area-hidden="true" style="font-size: 18px;"></i>
                                                </a>
                                            @endif

                                            <div  class="collapse" style="margin-left: 16px;" id="childlist_{{$user->id}}">
                                                <?php $users_collection =  \App\Helpers\LaraHelpers::getchildColloborateru($user->id); ?>
                                                @if(count($users_collection)>0)
                                                    @foreach($users_collection as $user_data)

                                                        @if($user->user_type != 4)
                                                            <table style="margin-left: 16px;">
                                                                <thead>
                                                                <tr style="border-bottom: 1px solid #ddd;border-right:1px solid #ddd ">
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"><i class="fa fa-btn fa-minus"></i></th>
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"> {{$user_data->name}}</th>
                                                                    <th style="padding-right: 10px;font-weight: normal;color:#00a3b4"; >{{$user_data->email}}</th>
                                                                    <th><a href="{{route('userEdit',['id'=>$user_data['id']])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-right: 10px;" aria-hidden="true"></i></a>
                                                                        <a href="javascript:void(0)"   data-id ="{{$user_data->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a></th>
                                                                </tr>
                                                                </thead>
                                                            </table>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{$userTypes[$user->user_type]}}</td>
                                        <td>{{$user->email}}
                                            <a href="{{route('userEdit',['id'=>$user->id])}}" class="memver-edit"><i class="fa fa-pencil fa-lg" style="padding-left:10px;padding-right: 10px;" aria-hidden="true"></i></a>
                                            <a href="javascript:void(0)"   data-id ="{{$user->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i></a>
                                        </td>
                                        <td>
                                            @if($user->user_type == 2 ||$user->user_type == 5 )
                                                <?php $entreprises = $enterpriseObj->getMultipleEntrepriseByID($user->entreprise_id);  ?>
                                                @if(count($entreprises) > 0)
                                                    @foreach($entreprises as $entrepris)
                                                        {{$entrepris->name}},
                                                        <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($entrepris->id);?>
                                                        @foreach($enterprise_collection as $child)
                                                            <div style="font-weight: normal;color:#00a3b4">    <i class="fa fa-minus" style="color:#00a3b4;padding-right: 5px;"></i>{{$child->name}}</div>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @else

                                                <?php $user_entreprise = $enterpriseObj->getEntrepriseByID($user->entreprise_id) ?>
                                                @if(count($user_entreprise) > 0)
                                                    {{$user_entreprise->name}}
                                                    <?php $enterprise_collection =  \App\Helpers\LaraHelpers::getchildEnterprise($user_entreprise->id);?>
                                                    @foreach($enterprise_collection as $child)
                                                        <div style="font-weight: normal;color:#00a3b4"><i class="fa fa-minus" style="color:#00a3b4;padding-right: 5px;"></i>{{$child->name}}</div>

                                                    @endforeach
                                                @endif
                                            @endif
                                        </td>


                                    </tr>

                                @endif
                            @endforeach
                            @endif
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.stack',['extra'=>''])
@push('script')
<script>
    $(window).on('load', function() {
        $('tbody.module_category').sortable();
            var windowHref = window.location.href.toString();
            if(windowHref.indexOf('/user?id=') !== -1){
                jQuery('.UserList').css('display','block');
            }else{ console.log('gerui');
                jQuery('.UserList').css('display','none');
            }
    });
</script>
<script src="{{ asset('public/sortable/jquery-ui.min.js') }}"></script>
@endpush