@extends('layouts.app')
<style>
    #pieChart {
        width: 90%;
        height: 360px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    @media screen and (max-width: 320px){
        .hscroll{
            overflow: auto;
        }
    }
    @media screen and (max-width: 360px){
        table.mostRecent>tbody>tr>td{
            width: 33.33%;
            word-wrap: break-word;
        }

    }
    @media screen and (max-width: 460px){
        #pieLegend > svg {
            position: relative ;
            right:50%;
        }
        #pieChart{
            height: 300px;
        }
        h3.title-chart{
            font-size: 16px;
        }
        #chartdiv{
            height: 500px;
        }

        .amChartsLegend > svg {
            position: relative ;
            right:50%;
            left:0;
        }

    }
    h3.title-chart {
        text-align: center;
    }

    @media screen and (max-width: 767px){

        .paddinglr{
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        .float-right-div{
            text-align: center !important;
        }
        .float-right-div .pull-right{
            text-align: center !important;
            float: none !important;
        }

    }


</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                {{ __('probtp.statistiques') }}
                            </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default" style="padding-bottom: 15px;">

                            <h3 class="title-chart"> {{ __('probtp.total_user') }}</h3>
                            <form class="form-inline" id="filterForm" role="form"
                                  method="POST" action="javascript:void(0);" align="center">
                                <span class="title-period"> {{ __('probtp.select Period') }}: </span> <div class="form-group mb-2">
                                    <label for="staticEmail2" class="sr-only">  {{ __('probtp.Start Date') }}:</label>
                                    <input type="text" id="startDate" name="startDate" class="form-control" placeholder="{{ __('probtp.Start Date') }}"/>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="inputPassword2" class="sr-only">{{ __('probtp.End Date') }}: </label>
                                    <input type="text" class="form-control"  name="endDate" id="endDate" placeholder="{{ __('probtp.End Date') }}"/>
                                </div>

                                <button type="submit" class="btn btn-primary mb-2">{{ __('probtp.Show Chart') }}</button>
                            </form>
                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div id="pieChart"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div id="pieLegend"></div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

@push('script')
<script type="text/javascript" src="{{ asset('public/js/statistiques.js') }}"></script>
<script type="text/javascript">


    window.onload = function(){
        var dataCollection = '<?=$user?>';
        var Sdate =  $('#startDate').datepicker({ dateFormat: 'dd-mm-yy' });
        var Edate =  $('#endDate').datepicker({ dateFormat: 'dd-mm-yy' });

        getChartData(dataCollection);

        $(document).on('submit','#filterForm',function(){
            var form_data = new FormData(this);
            $.ajax({
                cache: false,
                url: window.Probtp.baseUrl +'user/getpieChartDataByDate',
                type: "POST",
                data: form_data,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                processData: false,
                success: function (response) {
                    console.log(response);
                    getChartData(response);
                },
                error: function (xhr) {
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                }
            });
        });
    };


    /**
     * Function that generates data for Comment by user
     *  @param total_time
     *  @return totalTime object
     */
    function generatePiechartDiv(dataCollection){
        var totalUser = [];
        $.each(JSON.parse(dataCollection),function(k,v){
            totalUser.push({
                "user": k,
                "total" :v
            });
        });
        //  console.log(totalTime);
        return totalUser;
    }


    function getChartData (dataCollection){
        var chart = AmCharts.makeChart( "pieChart", {
            // "libs": { "autoLoad": false },
            "type": "pie",
            "theme": "light",
            "labelRadius": -36,
            "backgroundAlpha": 1,
            "labelText": "[[percents]]%",
            "fontFamily":" Raleway,sans-serif",
            "dataProvider":generatePiechartDiv(dataCollection),
            "legend": {
                "horizontalGap": 15,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": false,
                "markerSize": 15,
                "marginTop": 20,
                "useMarkerColorForLabels":false,
                "useMarkerColorForValues":false,
                "autoMargins":true
            },
            /*
             "legend":{
             "divId":"pieLegend",
             "position":"bottom",
             "marginRight":100,
             "autoMargins":false,
             "horizontalGap": 100,
             "valueWidth": 9,
             },*/
            "valueField": "total",
            "titleField": "user",
            "graphs": [
                {
                    "showBalloon": true,

                }
            ],
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:11px'><b>[[value]]</b> ([[percents]]%)</span>",
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "maxWidth":1000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            //      "angle": 30,
            "export": {
                "enabled": false
            },
            "responsive": {
                "enabled": true,
                "rule":
                    {
                        "maxWidth":400,
                        "maxHeight":400,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10,
                            }
                        }
                    }
            },
        } );
        addLabel(chart);
    }









</script>
@endpush