@extends('layouts.app')
<style>
    #pieChart {
        width: 90%;
        height: 360px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    #chartdiv {
        width   : 100%;
        height    : auto;
        font-size : 11px;
    }
    #doublechartdiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    #RecentLoginchartdiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    .float-right-div{
        float:right;
        padding: 10px;
        padding-right: 30px;
    }
    #MostcompletedModuleDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    #MostAccessedModuleDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    #commentsbyUserDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    #timeSpentonSystemDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    .float-right-div .dropdown-menu{
        padding-right: 20px;
    }
    .mostRecent{
        font-size   : 11px;
    }
    .mostRecent td{
        word-wrap: break-word;
    }
    @media screen and (max-width: 320px){
        .hscroll{
            overflow: auto;
        }
    }
    @media screen and (max-width: 360px){
        table.mostRecent>tbody>tr>td{
            width: 33.33%;
            word-wrap: break-word;
        }

    }
    @media screen and (max-width: 460px){
        #pieLegend > svg {
            position: relative ;
            right:50%;
        }
        #pieChart{
            height: 300px;
        }
        h3.title-chart{
            font-size: 16px;
        }
        #chartdiv{
            height: 500px;
        }

        .amChartsLegend > svg {
            position: relative ;
            right:50%;
            left:0;
        }

    }
    h3.title-chart {
        text-align: center;
    }

    @media screen and (max-width: 767px){

        .paddinglr{
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        .float-right-div{
            text-align: center !important;
        }
        .float-right-div .pull-right{
            text-align: center !important;
            float: none !important;
        }

    }


</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                {{ __('probtp.statistiques') }}
                            </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default" style="padding-bottom: 15px;">

                            <h3 class="title-chart"> {{ __('probtp.total_user') }}</h3>

                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div id="pieChart"></div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div id="pieLegend"></div>
                                    </div>


                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">

                            <h3 class="title-chart"> {{ __('probtp.assigned_entreprise') }}</h3>

                            <div class="panel-body paddinglr">
                                <div class="col-md-12 col-sm-12 col-xs-12 paddinglr">
                                    <div class="">
                                        <div id="chartdiv"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">

                            <h3 class="title-chart"> {{ __('probtp.CommentsbyUSer') }}</h3>
                            <div class="panel-body paddinglr">
                                <div class="col-md-12  col-sm-12 col-xs-12 paddinglr">
                                    <div id="doublechartdiv"></div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <h3 class="title-chart"> {{ __('probtp.totalTimeSpentOnSystem') }}</h3>
                            <div class="panel-body paddinglr">
                                <div class="col-md-12  col-sm-12 col-xs-12 paddinglr">
                                    <div id="timeSpentonSystemDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <h3 class="title-chart  ">{{ __('probtp.mostCompletedModuleDiv') }}</h3>
                            <div class="dropdown float-right-div col-xs-12  col-sm-12 ">
                                <button class="btn btn-primary dropdown-toggle pull-right" type="button" data-toggle="dropdown"><span class="userSelection">{{ __('probtp.All_Users') }}</span>
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu formaResource pull-right">
                                    <li><a data-value="F" href="javascript:void(0);">Formations</a></li>
                                    <li><a data-value="R" href="javascript:void(0);">Ressources </a></li>
                                    <li><a data-value="A" href="javascript:void(0);">{{ __('probtp.All_Users') }} </a></li>
                                </ul>
                            </div>
                            <div class="panel-body paddinglr">
                                <div class="col-md-12  col-sm-12 col-xs-12 paddinglr">
                                    <div id="MostcompletedModuleDiv"></div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <h3 class="title-chart "> {{ __('probtp.mostAccessedModuleDiv') }}</h3>
                            <div class="dropdown float-right-div col-sm-12 col-xs-12">
                                <button class="btn btn-primary dropdown-toggle pull-right " type="button" data-toggle="dropdown"><span class="userSelection">{{ __('probtp.All_Users') }}</span>
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu mostAccessedDiv pull-right">
                                    <li><a data-value="F" href="javascript:void(0);">Formations</a></li>
                                    <li><a data-value="R" href="javascript:void(0);">Ressources </a></li>
                                    <li><a data-value="A" href="javascript:void(0);">{{ __('probtp.All_Users') }}</a></li>
                                </ul>
                            </div>
                            <div class="panel-body paddinglr">
                                <div class="col-md-12  col-sm-12 col-xs-12 paddinglr">
                                    <div id="MostAccessedModuleDiv"></div>
                                </div>
                            </div>

                        </div>
                        <div class="panel panel-default">
                            <h3 class="title-chart"> {{ __('probtp.commentsonModulesDiv') }}</h3>
                            <div class="dropdown float-right-div col-sm-12 col-xs-12">
                                <button class="btn btn-primary dropdown-toggle pull-right" type="button" data-toggle="dropdown"><span class="userSelection">{{ __('probtp.All_Users') }}</span>
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu comments pull-right">
                                    <li><a data-value="F" href="javascript:void(0);">Formations</a></li>
                                    <li><a data-value="R" href="javascript:void(0);">Ressources </a></li>
                                    <li><a data-value="A" href="javascript:void(0);">{{ __('probtp.All_Users') }}</a></li>
                                </ul>
                            </div>
                            <div class="panel-body paddinglr">
                                <div class="col-md-12 col-sm-12 col-xs-12 paddinglr">
                                    <div id="commentsbyUserDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <h3 class="title-chart"> {{ __('probtp.mostRecentLoginuser') }}</h3>
                            <div class="panel-body">
                                <div class="hscroll table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xs">
                                    <table class="table table-striped  table-bordered mostRecent" cellspacing="0" id="data-table">
                                        <thead>
                                        <tr>

                                            <th>{{ __('probtp.user_name') }}</th>
                                            <th>{{ __('probtp.type') }}</th>
                                            <th>{{ __('probtp.last_login') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $userTypes = Config::get('constant.user_types');?>
                                        @foreach($mostRecent as $data)
                                            <tr id="">
                                                <td>{{$data->name}}</td>
                                                <td>{{ $userTypes[$data->user_type]}}</td>

                                                <?php setlocale (LC_TIME, "fr_FR");
                                                $d = utf8_encode(strftime ("%d %B %Y &agrave; %Hh%M", strtotime($data->last_login)));?>
                                                <td> {{ $d }} </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

@push('script')
<script src="{{asset('public/js/charts.js')}}" type="text/javascript"></script>
<script src="{{asset('public/js/statistiques.js')}}" type="text/javascript"></script>
<script type="text/javascript">

    /**
     * Function that generates data for recent Login Chart
     *
     * @return recentChartData object
     */
    function generateChartDataforRecentLogint(){
        var arrValue = '<?php echo $mostRecent;  ?>';
        var recentChartData = [];
        $.each(JSON.parse(arrValue),function(k,v){
            //var date = formatDate(v.last_login);
            var ndate = AmCharts.formatDate(new Date(v.last_login), "DD MMM, YYYY");
            recentChartData.push({
                "User": v.name,
                "Login" : ndate
            });
        });
        return recentChartData;
    }


    /**
     * Function that generates data for most accessed module
     * @param mostAccessed
     * @return mostAccessedData object
     */
    function generateMostAccessedModuleDiv(mostAccessed){
        var mostAccessedData = [];
        $.each(JSON.parse(mostAccessed),function(k,v){
            mostAccessedData.push({
                "Title": v.title,
                "Total" : v.counter
            });
        });
        return mostAccessedData;
    }

    /**
     * Function that generates data for most completed module
     * @param mostCompleted
     * @return mostCompletedData object
     */
    function generateMostcompletedModuleDiv(mostCompleted){
        var mostCompletedData = [];
        $.each(mostCompleted,function(k,v){
            var title=v.title;
            mostCompletedData.push({
                "Title": title ,
                "Total" : v.total
            });
        });
        return mostCompletedData;
    }

    /**
     * Function which is replacing quotes to shash
     * @param str
     * @return string object
     */
    function stripSlashes(str) {
        return str.replace(/\\'/g,'\'').replace(/\\"/g,'"').replace(/\\0/g,'\0').replace(/\\\\/g,'\\');
    }

    /**
     * Function that generates data for Comment by user
     *  @param comments
     *  @return commentsByuser object
     */
    function generatecommentsbyUserDiv(comments){

        var commentsByuser = [];
        // for (var i = comments.length - 1; i >= 0; i--) {
        //     console.log(comments[i]);

        //   }
     console.log(comments);
        $.each(JSON.parse(comments),function(k,v){
            commentsByuser.push({
                "Title":v.name,
                "Total":v.total,
            });
//        if(v.Formation){
//          commentsByuser.push({
//          "Title":v.Formation.name,
//          "Formation" : v.Formation.total,
//          "Responsable" : v.Responsable
//          });
//        }else{
//          commentsByuser.push({
//          "Title":v.Responsable.name,
//          "Responsable" : v.Responsable.total,
//          "Formation" : v.Formation
//          });
//        }
        });

        return commentsByuser;
    }


    jQuery(document).ready(function($) {

        $(document).on("click",".formaResource li a", function(){
            var userType = $(this).attr('data-value');

            var userName ;
            if(userType == "F"){
                userName="Formations";
            }else if(userType == "R"){
                userName="Ressources ";
            }else{
                userName="{{ __('probtp.All_Users') }}";
            }
            $(".formaResource").siblings('button').children('.userSelection').text(userName);
            var SITE_PATH = window.Probtp.baseUrl;
            $.ajax({
                url: SITE_PATH +'user/barChartforMostCompleted',
                type: "POST",
                data: {data : userType},
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function(response)
                {mostCompletedData=[];
                    var modules = response;
                    generateMostcompletedModuleDiv(response);
                    getMostcompletedModuleDiv(response);
                },
                error: function(xhr, status, error) {
                    console.log(error);
                    // check status && error
                }
            });
        });

        $(document).on("click",".mostAccessedDiv li a", function(){
            var userType = $(this).attr('data-value');
            var userName ;
            if(userType == "F"){
                userName="Formations";
            }else if(userType == "R"){
                userName="Ressources ";
            }else{
                userName="{{ __('probtp.All_Users') }}";
            }
            $(".mostAccessedDiv").siblings('button').children('.userSelection').text(userName);
            var SITE_PATH = window.Probtp.baseUrl;
            $.ajax({
                url: SITE_PATH +'user/barChartforMostAccessed',
                type: "POST",
                data: {data : userType},
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function(response)
                {mostCompletedData=[];
                    var modules = response;
                    generateMostAccessedModuleDiv(response);
                    getMostAccessedModuleDiv(response);
                },
                error: function(xhr, status, error) {
                    console.log(error);
                    // check status && error
                }
            });
        });

        $(document).on("click",".comments li a", function(){
            var userType = $(this).attr('data-value');

            var userName ;
            if(userType == "F"){
                userName="Formations";
            }else if(userType == "R"){
                userName="Ressources ";
            }else{
                userName="{{ __('probtp.All_Users') }}";
            }
            $(".comments").siblings('button').children('.userSelection').text(userName);
            var SITE_PATH = window.Probtp.baseUrl;
            $.ajax({
                url: SITE_PATH +'user/barChartforComments',
                type: "POST",
                data: {data : userType},
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function(response)
                {mostCompletedData=[];
                    var modules = response;
                    console.log(response);
                    generatecommentsbyUserDiv(response);
                    getUserCommentDataDiv(response);
                },
                error: function(xhr, status, error) {
                    console.log(error);
                    // check status && error
                }
            });
        });


        var chart = AmCharts.makeChart( "pieChart", {
            // "libs": { "autoLoad": false },
            "type": "pie",
            "theme": "light",
            "labelRadius": -36,
            "backgroundAlpha": 1,
            "labelText": "[[percents]]%",
            "fontFamily":" Raleway,sans-serif",
            autoMargins: false,
            marginTop: 0,
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 0,
            pullOutRadius: 0,
            "dataProvider": [
                <?php
                foreach ($user as $key => $val){
                    echo'{"user": "'.$key.'" , "total": '.$val.' },';
                    echo "\r\n";
                }
                ?>
            ],
            "legend": {
                "horizontalGap": 15,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": false,
                "markerSize": 15,
                "marginTop": 20,
                "useMarkerColorForLabels":false,
                "useMarkerColorForValues":false,
                "autoMargins":true
            },
            /*
             "legend":{
             "divId":"pieLegend",
             "position":"bottom",
             "marginRight":100,
             "autoMargins":false,
             "horizontalGap": 100,
             "valueWidth": 9,
             },*/
            "valueField": "total",
            "titleField": "user",
            "graphs": [
                {
                    "showBalloon": true,

                }
            ],
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:11px'><b>[[value]]</b> ([[percents]]%)</span>",
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "maxWidth":1000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            //      "angle": 30,
            "export": {
                "enabled": false
            },
            "responsive": {
                "enabled": true,
                "rule":
                    {
                        "maxWidth":400,
                        "maxHeight":400,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10,
                            }
                        }
                    }
            },
        } );
    });



    // define varibles for passing the data in chart.js

    var arrValue = '<?=$entreprises_user_obj?>';
    var total_time = '<?=$time?>';
    var commentsData = '<?=addslashes($commentsData)?>';
    var user = '{{ __("probtp.user") }}';
    var Login_time = '{{ __("probtp.total_login") }}';
    var totalTimeSpentByUser = '{{ __("probtp.totalTimeSpentByUser") }}';
    var entreprise = "{{ __('probtp.Utilisateurs') }}";
    var valueAxisTitle = "{{ __('probtp.valueAxisTitle') }}";
    var collaborateur = "{{ __('probtp.collaborateur') }}";
    var assigned_entreprise = "{{ __('probtp.assigned_entreprise') }}";
    var total = "{{ __('probtp.total') }}";
    var mostCompletedModule = " {{ __('probtp.mostCompletedModule') }} " ;
    window.onload = function(){
        /**
         * Create the Bar chart
         */

            // var user = "User";
            // showBarChart(arrValue);

        var mostAccessed ='<?=addslashes($mostAccessed)?>';
        var mostCompleted = <?=($mostCompleted)?>;
        getMostAccessedModuleDiv(mostAccessed);
        getMostcompletedModuleDiv(mostCompleted);
        var commentsData = '<?=addslashes($commentsData)?>';
        getUserCommentDataDiv(commentsData);
    };

    /**
     * Function  generates charts for "Most Accessed Modules"
     *  @param mostAccessed
     *
     */
    function getMostAccessedModuleDiv(mostAccessed){
        AmCharts.makeChart( "MostAccessedModuleDiv", {
            "type": "serial",
            "theme": "none",
            "categoryField": "Title",
            "rotate": true,
            "fontFamily":" Raleway,sans-serif",
//        "autoMarginOffset": 40,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "title":'{{ __('probtp.user') }} ',
                "labelFunction": function(label, item, axis) {
                    var chart = axis.chart;
                    if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                        return label.substr(0, 10) + '...';
                    }else{
                        return label;
                    }
                }
            },
            "trendLines": [],
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "graphs": [
                {
                    "balloonText": "[[Title]] ",
                    "fillAlphas": 1,
                    "id": "AmGraph-1",
                    "labelText": "[[Total]]",
                    "title": "{{ __('probtp.user') }}",
                    "type": "column",
                    "showBalloon": true,
                    "valueField": "Total",
                    "fixedColumnWidth": 5,
                    "labelFunction": function(item, label) {
                        return label == "0" ? "" : label;
                    }
                }
            ],
            "balloon": {
                "fixedPosition": true,
                "maxWidth": 10000
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
            },

            "guides": [],
            "valueAxes": [
                {   "axisAlpha": 0,
                    "axisTitleOffset": 7,
                    "id": "ValueAxis-1",
                    "integersOnly":true,
                    "title": "{{ __('probtp.mostAccessedModule') }} ",
                    "position":"top",
                }
            ],
            "allLabels": [],
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "maxWidth":1000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            "titles": [],
            "responsive": {
                "enabled": false,
                "addDefaultRules": true,
                "rules": [
                    {
                        "maxWidth": 400,
                        "maxHeight": 400,
                        "minWidth":200,
                        "overrides": {
                            "marginLeft":-30,
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10
                            },

                        }
                    },
                    {
                        "maxWidth": 1440,
                        "minHeight": 900,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                // "marginRight":150,
                                // "marginLeft":20,
                            }
                        }
                    }
                ]
            },
            "dataProvider": generateMostAccessedModuleDiv(mostAccessed),
        });
    }

    /**
     * Function  generates charts for "Most Completed Modules"
     *  @param mostCompleted
     *
     */
    function getMostcompletedModuleDiv(mostCompleted){
        var completed=    AmCharts.makeChart( "MostcompletedModuleDiv", {
            "type": "serial",
            "theme": "none",
            "rotate": true,
            "startDuration": 1,
            "fontFamily":" Raleway,sans-serif",
//        "autoMarginOffset": 40,
            "categoryField": "Title",
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "labelFunction": function(label, item, axis) {
                    var chart = axis.chart;
                    if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                        return label.substr(0, 10) + '...';
                    }else{
                        return label;
                    }
                },
                "title":'{{ __('probtp.user') }} ',
            },
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "gridAboveGraphs": true,
            "graphs": [ {
                "balloonText": "[[Title]]",
                "labelText": "[[Total]]",
                "labelFunction": function(item, label) {
                    return label == "0" ? "" : label;
                },
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "title": total,
                "valueField": "Total",
                "fixedColumnWidth": 5,
                "showBalloon": true,
            } ],
            "balloon": {
                "fixedPosition": true,
                "maxWidth": 10000
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
            },
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0,
                    "integersOnly":true,
                    "title": mostCompletedModule,
//                "minimum": 1
                }
            ],
            "allLabels": [],
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "maxWidth":1000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            "titles": [],
            "dataProvider": generateMostcompletedModuleDiv(mostCompleted),
            "export": {
                "enabled": false
            },
            "responsive": {
                "enabled": false,
                "addDefaultRules": true,
                "rules": [
                    {
                        "maxWidth": 400,
                        "maxHeight": 400,
                        "minWidth":200,
                        "overrides": {
                            "marginLeft":-30,
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10
                            },

                        }
                    },
                    {
                        "maxWidth": 1440,
                        "minHeight": 900,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                // "marginRight":150,
                                // "marginLeft":20,
                            }
                        }
                    }
                ]
            }

        } );
    }

    /**
     * Function  generates charts for "Comments by category"
     *  @param commentsData
     *
     */
    function getUserCommentDataDiv(commentsData){
        var charts = AmCharts.makeChart("commentsbyUserDiv", {
            "type": "serial",
            "theme": "none",
            "categoryField": "Title",
            "rotate": true,
            // "autoMarginOffset": 40,
            "orderByField": "Total",
            "fontFamily":" Raleway,sans-serif",
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "title":"{{ __('probtp.category') }}",
                "labelsEnabled":true,
                "labelFunction": function(label, item, axis) {
                    var chart = axis.chart;
                    if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                        return label.substr(0, 10) + '...';
                    }else{
                        return label;
                    }
                },
                "listeners": [{
                    "event": "clickItem",
                    "method": function(e) {
                        window.location.href = app.config.SITE_PATH+"comment";
                    }
                }]
            },
            //"sortColumns":true,
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "trendLines": [],
            "graphs": [
                {
                    "labelText": "[[Total]]",
                    "labelFunction": function(item, label) {
                        return label == "0" ? "" : label;
                    },
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "{{ __('probtp.category') }}",
                    "type": "column",
                    "valueField": "Total",
                    "showBalloon": true,
                    "fixedColumnWidth": 5,
                    "balloonText": "[[Title]] ",
                },
                {{--{--}}
                {{--"labelText": "[[Responsable]]",--}}
                {{--"labelFunction": function(item, label) {--}}
                {{--return label == "0" ? "" : label;--}}
                {{--},--}}
                {{--"fillAlphas": 0.8,--}}
                {{--"id": "AmGraph-2",--}}
                {{--"lineAlpha": 0.2,--}}
                {{--"title": "{{ __('probtp.ressources') }}",--}}
                {{--"type": "column",--}}
                {{--"valueField": "Responsable",--}}
                {{--"showBalloon": false,--}}
                {{--"fixedColumnWidth": 1,--}}
                {{--"integersOnly":true--}}
                {{--}--}}
            ],
            "chartCursor": {
                "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
            },
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0,
                    "title": "  {{ __('probtp.commentsonModules') }} ",
                    "integersOnly":true,
                    //        "minimum": 1
                }
            ],
            "allLabels": [],
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "fixedPosition": true,
                "maxWidth": 10000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            "titles": [],
            "dataProvider": generatecommentsbyUserDiv(commentsData),
            "export": {
                "enabled": false
            },
            "responsive": {
                "enabled": false,
                "addDefaultRules": true,
                "rules": [
                    {
                        "maxWidth": 400,
                        "maxHeight": 400,
                        "minWidth":200,
                        "overrides": {
                            "marginLeft":-30,
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10
                            }

                        }
                    },
                    {
                        "maxWidth": 1440,
                        "minHeight": 900,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                // "marginRight":150,
                                // "marginLeft":20,
                            }
                        }
                    }
                ]
            }

        });
    }

    /**
     * Function  generates charts for "Comments done By User"
     *
     */
    var doublechartDiv = AmCharts.makeChart( "doublechartdiv", {
        "type": "serial",
        "theme": "none",
        "categoryField": "User",
        "rotate": true,
        "fontFamily":" Raleway,sans-serif",
        "startDuration": 1,
        // "autoMarginOffset": 20,
        "orderByField": "Comment",
        "categoryAxis": {
            "gridPosition": "start",
            "position": "left",
            "labelFunction": function(label, item, axis) {
                var chart = axis.chart;  console.log(chart.realWidth);
                if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                    return label.substr(0, 10) + '...';
                }else{
                    return label;
                }
            },
            "title": "{{ __('probtp.user') }}"
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10,
            "marginTop": 20
        },
        "trendLines": [],
        "graphs": [ {
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "showBalloon": true,
            "valueField": "Comment",
            "title": "{{ __('probtp.comment') }}",
            "labelText": "[[Comment]]",
            "labelFunction": function(item, label) {
                return label == "0" ? "" : label;
            },
            "fixedColumnWidth": 5,
            "balloonText": "[[User]] ",
        } ],
        "guides": [],
        "balloon": {
            "fixedPosition": true,
            "maxWidth": 10000
        },
        "chartCursor": {
            "cursorAlpha": 0,
            "oneBalloonOnly": true,
            "categoryBalloonText": "&raquo;"
        },

        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "position": "top",
                "integersOnly":true,
                "axisAlpha": 0,
                "title": " {{ __('probtp.numberofCommentsbyUSer') }} "
            }
        ],
        "allLabels": [],
        "balloon": {
            "borderThickness": 1,
            "borderAlpha": 1,
            "fillAlpha": 1,
            "horizontalPadding": 4,
            "verticalPadding": 2,
            "shadowAlpha": 0,
            "maxWidth":1000,
            "textAlign":"middle",
            //  "cornerRadius":4,
            "pointerOrientation":"down"
        },
        "titles": [],
        "dataProvider": [
            <?php
            foreach ($getUserCommentData as $key => $val){
                echo'{"User": "'.$key.'" , "Comment": '.$val.' },';
                echo "\r\n";
            }
            ?>
        ],
        "export": {
            "enabled": false
        },
        "responsive": {
            "enabled": false,
            "addDefaultRules": true,
            "rules": [
                {
                    "maxWidth": 400,
                    "maxHeight": 400,
                    "minWidth":200,
                    "overrides": {
                        "marginLeft":-30,
                        "legend": {
                            "enabled": true,
                            "marginRight":100,
                            "marginLeft":10
                        },

                    }
                },
                {
                    "maxWidth": 1440,
                    "minHeight": 900,
                    "overrides": {
                        "legend": {
                            "enabled": true,
                            // "marginRight":150,
                            // "marginLeft":20,
                        }
                    }
                }
            ]
        }

    });

    /**
     * Function  generates charts for " Most Recent Loggedin user"
     *
     */
    var RecentLoginchartDiv = AmCharts.makeChart( "RecentLoginchartdiv", {
        "type": "serial",
        "theme": "none",
        "categoryField": "User",
        "rotate": true,
        "startDuration": 1,
        "fontFamily":" Raleway,sans-serif",
        "autoMarginOffset": 40,
        "categoryAxis": {
            "gridPosition": "start",
            "position": "left",
            "title": " {{ __('probtp.user') }} " ,
            "tickLength": 10,
        },
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
            "useGraphSettings": true,
            "markerSize": 10,
            "marginTop": 20
        },
        "trendLines": [],
        "graphs": [ {
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "showBalloon": true,
            "valueField": "Login",
            "title": "{{ __('probtp.login') }}",
            "labelText": "[[Login]]",
            "labelFunction": function(item, label) {
                return label == "0" ? "" : label;
            },
            "fixedColumnWidth": 5,
            "balloonText": "[[User]] ",
        } ],
        "guides": [],
        "balloon": {
            "fixedPosition": true,
            "maxWidth": 10000
        },
        "chartCursor": {
            "cursorAlpha": 0,
            "oneBalloonOnly": true,
            "categoryBalloonText": "&raquo;"
        },

        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "position": "top",
                "type": "date",
                "axisAlpha": 0,
                "title": " {{ __('probtp.numberofCommentsbyUSer') }} "
            }
        ],
        "allLabels": [],
        "balloon": {
            "borderThickness": 1,
            "borderAlpha": 1,
            "fillAlpha": 1,
            "horizontalPadding": 4,
            "verticalPadding": 2,
            "shadowAlpha": 0,
            "maxWidth":1000,
            "textAlign":"middle",
            //  "cornerRadius":4,
            "pointerOrientation":"down"
        },
        "titles": [],
        "responsive": {
            "enabled": true
        },
        "dataProvider":generateChartDataforRecentLogint(),
        "export": {
            "enabled": false
        }

    } );


</script>
@endpush