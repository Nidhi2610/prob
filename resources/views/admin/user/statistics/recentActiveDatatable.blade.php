<div class="panel panel-default" id="tableData">
    <?php $userTypes = Config::get('constant.user_types');?>

    <div class="panel-body">
    <div class="hscroll table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl table-responsive-xs">
        <table class="table table-striped  table-bordered mostRecent" cellspacing="0" id="data-table4">
            <thead>
            <tr>
                <th>{{ __('probtp.user_name') }}</th>
                <th>{{ __('probtp.type') }}</th>
                <th>{{ __('probtp.last_login') }}</th>
            </tr>
            </thead>
            <tbody>
            @if(sizeof($mostRecent))
            @foreach($mostRecent as $data)
                <tr id="">
                    <td>{{$data->name}}</td>
                    <td data-search="{{ $data->user_type }}">{{ $userTypes[$data->user_type]}}</td>

                    <?php setlocale (LC_TIME, "fr_FR");
                    $d = utf8_encode(strftime ("%d %B %Y &agrave; %Hh%M", strtotime($data->last_login)));?>
                    <td data-search="{{ date_format(date_create($data->last_login),'Y-m-d') }}"> {{ $d }} </td>
                    {{--<td> {{ $data->last_login }} </td>--}}
                </tr>
            @endforeach
            @else
                <tr><td colspan="3"><h3 align="center">No data Found!</h3></td></tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
</div>