@extends('layouts.app')
<style>
    #MostcompletedModuleDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }

    @media screen and (max-width: 320px){
        .hscroll{
            overflow: auto;
        }
    }
    @media screen and (max-width: 360px){
        table.mostRecent>tbody>tr>td{
            width: 33.33%;
            word-wrap: break-word;
        }

    }
    @media screen and (max-width: 460px){
        #pieLegend > svg {
            position: relative ;
            right:50%;
        }
        #pieChart{
            height: 300px;
        }
        h3.title-chart{
            font-size: 16px;
        }
        #chartdiv{
            height: 500px;
        }

        .amChartsLegend > svg {
            position: relative ;
            right:50%;
            left:0;
        }

    }
    h3.title-chart {
        text-align: center;
    }

    @media screen and (max-width: 767px){

        .paddinglr{
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        .float-right-div{
            text-align: center !important;
        }
        .float-right-div .pull-right{
            text-align: center !important;
            float: none !important;
        }

    }


</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                {{ __('probtp.statistiques') }}
                            </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <h3 class="title-chart  ">{{ __('probtp.mostCompletedModuleDiv') }}</h3>
                            <form class="form-inline" id="filterForm" role="form"
                                  method="POST" action="javascript:void(0);" align="center">
                                <span class="title-period"> {{ __('probtp.select Period') }}: </span><div class="form-group mb-2">
                                    {{--<label for="staticEmail2" class="sr-only">  {{ __('probtp.Start Date') }}:</label>--}}
                                    <input type="text" id="startDate" name="startDate" class="form-control" placeholder="{{ __('probtp.Start Date') }}"/>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    {{--<label for="inputPassword2" class="sr-only">{{ __('probtp.End Date') }}: </label>--}}
                                    <input type="text" class="form-control"  name="endDate" id="endDate" placeholder="{{ __('probtp.End Date') }}"/>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <select class="form-control" name="module_type">
                                        <option value="A">{{ __('probtp.All_Users') }}</option>
                                        <option value="F">Formations</option>
                                        <option value="R">Ressources</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">{{ __('probtp.Show Chart') }}</button>
                            </form>

                            {{--<div class="dropdown float-right-div col-xs-12  col-sm-12 ">--}}
                                {{--<button class="btn btn-primary dropdown-toggle pull-right" type="button" data-toggle="dropdown"><span class="userSelection">{{ __('probtp.All_Users') }}</span>--}}
                                    {{--<span class="caret"></span></button>--}}
                                {{--<ul class="dropdown-menu formaResource pull-right">--}}
                                    {{--<li><a data-value="F" href="javascript:void(0);">Formations</a></li>--}}
                                    {{--<li><a data-value="R" href="javascript:void(0);">Ressources </a></li>--}}
                                    {{--<li><a data-value="A" href="javascript:void(0);">{{ __('probtp.All_Users') }} </a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            <div class="panel-body paddinglr">
                                <div class="col-md-12  col-sm-12 col-xs-12 paddinglr">
                                    <div id="MostcompletedModuleDiv"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
<script type="text/javascript" src="{{ asset('public/js/statistiques.js') }}"></script>
@push('script')
<script type="text/javascript">
    var total = "{{ __('probtp.total') }}";
    var mostCompletedModule = " {{ __('probtp.mostCompletedModule') }} " ;
    window.onload = function(){

        var Sdate =  $('#startDate').datepicker({ dateFormat: 'dd-mm-yy' });
        var Edate =  $('#endDate').datepicker({ dateFormat: 'dd-mm-yy' });
        var mostCompleted = <?=($mostCompleted)?>;
        getMostcompletedModuleDiv(mostCompleted);

        $(document).on('submit','#filterForm',function(){
            var form_data = new FormData(this);
            $.ajax({
                cache: false,
                url: window.Probtp.baseUrl +'user/barChartforMostCompleted',
                type: "POST",
                data: form_data,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                processData: false,
                success: function (response) {
                    getMostcompletedModuleDiv(response);
                },
                error: function (xhr) {
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                }
            });
        });


    };

    jQuery(document).ready(function($) {
        $(document).on("click",".formaResource li a", function(){
            var userType = $(this).attr('data-value');

            var userName ;
            if(userType == "F"){
                userName="Formations";
            }else if(userType == "R"){
                userName="Ressources ";
            }else{
                userName="{{ __('probtp.All_Users') }}";
            }
            $(".formaResource").siblings('button').children('.userSelection').text(userName);
            var SITE_PATH = window.Probtp.baseUrl;
            $.ajax({
                url: SITE_PATH +'user/barChartforMostCompleted',
                type: "POST",
                data: {data : userType},
                headers: {
                    'X-CSRF-TOKEN': window.Probtp.csrfToken
                },
                success: function(response)
                {mostCompletedData=[];
                    var modules = response;
                    generateMostcompletedModuleDiv(response);
                    getMostcompletedModuleDiv(response);
                },
                error: function(xhr, status, error) {
                    console.log(error);
                    // check status && error
                }
            });
        });
    });

    /**
     * Function that generates data for most completed module
     * @param mostCompleted
     * @return mostCompletedData object
     */
    function generateMostcompletedModuleDiv(mostCompleted){
        var mostCompletedData = [];
        $.each(mostCompleted,function(k,v){
            var title=v.title;
            mostCompletedData.push({
                "Type": v.module_type ,
                "Id": v.id ,
                "Title": title ,
                "Total" : v.total
            });
        });
        return mostCompletedData;
    }

    /**
     * Function  generates charts for "Most Completed Modules"
     *  @param mostCompleted
     *
     */
    function getMostcompletedModuleDiv(mostCompleted){
        var completed=    AmCharts.makeChart( "MostcompletedModuleDiv", {
            "type": "serial",
            "theme": "none",
            "rotate": true,
            "startDuration": 1,
            "fontFamily":" Raleway,sans-serif",
//        "autoMarginOffset": 40,
            "categoryField": "Title",
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "title":'{{ __('probtp.user') }} ',
                "labelsEnabled":true,
                "labelFunction": function(label, item, axis) {
                    var chart = axis.chart;
                    if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                        return label.substr(0, 10) + '...';
                    }else{
                        return label;
                    }
                },
                "listeners": [{
                    "event": "clickItem",
                    "method": function(event) {
                        var itemId = event.serialDataItem.dataContext.Id; //i id of clicked label
                        var itemType =  event.serialDataItem.dataContext.Type; //i type of clicked label
                        window.location.href = app.config.SITE_PATH+"module/"+ itemType +"?id="+itemId;
                    }
                },{
                    "event": "rollOverItem",
                    "method": function(event) { console.log(event.target);
                        event.target.setAttr('fill','#ea6212 ');
                        event.target.setAttr("cursor", "pointer");
                    }
                },{
                    "event": "rollOutItem",
                    "method": function(event) { console.log(event.target);
                        event.target.setAttr('fill','#636b6f ');
                        event.target.setAttr("cursor", "default");
                    }
                }]
            },
            "legend":{
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "gridAboveGraphs": true,
            "graphs": [ {
                "balloonText": "[[Title]]",
                "labelText": "[[Total]]",
                "labelFunction": function(item, label) {
                    return label == "0" ? "" : label;
                },
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "title": total,
                "valueField": "Total",
                "fixedColumnWidth": 5,
                "showBalloon": true,
            } ],
            "balloon": {
                "fixedPosition": true,
                "maxWidth": 10000
            },
            "chartCursor": {
                "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
            },
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0,
                    "integersOnly":true,
                    "title": mostCompletedModule,
                    //                "minimum": 1
                }
            ],
            "allLabels": [],
            "balloon": {
                "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "maxWidth":1000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
            },
            "titles": [],
            "dataProvider": generateMostcompletedModuleDiv(mostCompleted),
            "export": {
                "enabled": false
            },
            "responsive": {
                "enabled": false,
                "addDefaultRules": true,
                "rules": [
                    {
                        "maxWidth": 400,
                        "maxHeight": 400,
                        "minWidth":200,
                        "overrides": {
                            "marginLeft":-30,
                            "legend": {
                                "enabled": true,
                                "marginRight":100,
                                "marginLeft":10
                            },

                        }
                    },
                    {
                        "maxWidth": 1440,
                        "minHeight": 900,
                        "overrides": {
                            "legend": {
                                "enabled": true,
                                // "marginRight":150,
                                // "marginLeft":20,
                            }
                        }
                    }
                ]
            }

        } );
        addLabel(completed);
    }




</script>
@endpush