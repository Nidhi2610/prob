@extends('layouts.app')
<style>
    #commentsbyUserDiv{
        width       : 100%;
        height      : auto;
        font-size   : 11px;
    }
    .float-right-div .dropdown-menu{
        padding-right: 20px;
    }

    @media screen and (max-width: 320px){
        .hscroll{
            overflow: auto;
        }
    }
    @media screen and (max-width: 360px){
        table.mostRecent>tbody>tr>td{
            width: 33.33%;
            word-wrap: break-word;
        }

    }
    @media screen and (max-width: 460px){
        #pieLegend > svg {
            position: relative ;
            right:50%;
        }
        #pieChart{
            height: 300px;
        }
        h3.title-chart{
            font-size: 16px;
        }
        #chartdiv{
            height: 500px;
        }

        .amChartsLegend > svg {
            position: relative ;
            right:50%;
            left:0;
        }

    }
    h3.title-chart {
        text-align: center;
    }

    @media screen and (max-width: 767px){

        .paddinglr{
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        .float-right-div{
            text-align: center !important;
        }
        .float-right-div .pull-right{
            text-align: center !important;
            float: none !important;
        }

    }


</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                {{ __('probtp.statistiques') }}
                            </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                    <div class="panel panel-default">
                    <h3 class="title-chart"> {{ __('probtp.commentsonModulesDiv') }}</h3>
                    <div class="">
                        <form class="form-inline" id="filterForm" role="form"
                              method="POST" action="javascript:void(0);" align="center">
                            <span class="title-period"> {{ __('probtp.select Period') }}: </span> <div class="form-group mb-2">
                                <label for="staticEmail2" class="sr-only">  {{ __('probtp.Start Date') }}:</label>
                                <input type="text" id="startDate" name="startDate" class="form-control" placeholder="{{ __('probtp.Start Date') }}"/>
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="inputPassword2" class="sr-only">{{ __('probtp.End Date') }}: </label>
                                <input type="text" class="form-control"  name="endDate" id="endDate" placeholder="{{ __('probtp.End Date') }}"/>
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <select class="form-control" name="module_type">
                                    <option value="A">{{ __('probtp.All_Users') }}</option>
                                    <option value="F">Formations</option>
                                    <option value="R">Ressources</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">{{ __('probtp.Show Chart') }}</button>
                        </form>

                    </div>
                    <div class="panel-body paddinglr">
                        <div class="col-md-12 col-sm-12 col-xs-12 paddinglr">
                            <div id="commentsbyUserDiv"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

@push('script')
<script type="text/javascript" src="{{ asset('public/js/statistiques.js') }}"></script>
<script type="text/javascript">
    var commentsData = '<?=addslashes($commentsData)?>';
    var totalTimeSpentByUser = '{{ __("probtp.totalTimeSpentByUser") }}';

    window.onload = function(){
        var Sdate =  $('#startDate').datepicker({ dateFormat: 'dd-mm-yy' });
        var Edate =  $('#endDate').datepicker({ dateFormat: 'dd-mm-yy' });
        var commentsData = '<?=addslashes($commentsData)?>';
        getUserCommentDataDiv(commentsData);
    };

    $(document).on('submit','#filterForm',function(){
        var form_data = new FormData(this);
        $.ajax({
            cache: false,
            url: window.Probtp.baseUrl +'user/barChartforCommentsByType',
            type: "POST",
            data: form_data,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': window.Probtp.csrfToken
            },
            processData: false,
            success: function (response) {

                //   console.log(response);
                getUserCommentDataDiv(response);

            },
            error: function (xhr) {
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
            }
        });
    });



    /**
     * Function that generates data for Comment by user
     *  @param comments
     *  @return commentsByuser object
     */
    function generatecommentsbyUserDiv(comments){
        var commentsByuser = [];
        $.each(JSON.parse(comments),function(k,v){
            commentsByuser.push({
                "Title":v.name,
                "Total":v.total,
                "cat_id":v.cat_id
            });
        });

        return commentsByuser;
    }


    /**
     * Function  generates charts for "Comments by category"
     *  @param commentsData
     *
     */
    function getUserCommentDataDiv(commentsData){
        var charts = AmCharts.makeChart("commentsbyUserDiv", {
            "type": "serial",
            "theme": "none",
            "categoryField": "Title",
            "rotate": true,
            // "autoMarginOffset": 40,
            "orderByField": "Total",
            "fontFamily":" Raleway,sans-serif",
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "title":"{{ __('probtp.category') }}",
                "labelsEnabled":true,
                "labelFunction": function(label, item, axis) {
                    var chart = axis.chart;
                    if ( (chart.realWidth <= 1050 ) && ( label.length > 10 ) ){
                        return label.substr(0, 10) + '...';
                    }else{
                        return label;
                    }
                },
                "listeners": [{
                    "event": "clickItem",
                    "method": function(event) {
                        var itemId = event.serialDataItem.dataContext.cat_id; //i id of clicked label

                        window.location.href = app.config.SITE_PATH+"comment?cat_id="+itemId;
                    }
                },{
                    "event": "rollOverItem",
                    "method": function(event) { console.log(event.target);
                        event.target.setAttr('fill','#ea6212 ');
                        event.target.setAttr("cursor", "pointer");
                    }
                },{
                    "event": "rollOutItem",
                    "method": function(event) { console.log(event.target);
                        event.target.setAttr('fill','#636b6f ');
                        event.target.setAttr("cursor", "default");
                    }
                }]
            },
            //"sortColumns":true,
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "marginTop": 20
            },
            "trendLines": [],
            "graphs": [
                {
                    "labelText": "[[Total]]",
                    "labelFunction": function(item, label) {
                        return label == "0" ? "" : label;
                    },
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "{{ __('probtp.category') }}",
                    "type": "column",
                    "valueField": "Total",
                    "showBalloon": true,
                    "fixedColumnWidth": 5,
                    "balloonText": "[[Title]] ",
                },
    ],
        "chartCursor": {
            "cursorAlpha": 0,
                "oneBalloonOnly": true,
                "categoryBalloonText": "&raquo;"
        },
        "guides": [],
            "valueAxes": [
            {
                "id": "ValueAxis-1",
                "position": "top",
                "axisAlpha": 0,
                "title": "  {{ __('probtp.commentsonModules') }} ",
                "integersOnly":true,
                //        "minimum": 1
            }
        ],
            "allLabels": [],
            "balloon": {
            "borderThickness": 1,
                "borderAlpha": 1,
                "fillAlpha": 1,
                "horizontalPadding": 4,
                "verticalPadding": 2,
                "shadowAlpha": 0,
                "fixedPosition": true,
                "maxWidth": 10000,
                "textAlign":"middle",
                //  "cornerRadius":4,
                "pointerOrientation":"down"
        },
        "titles": [],
            "dataProvider": generatecommentsbyUserDiv(commentsData),
            "export": {
            "enabled": false
        },
        "responsive": {
            "enabled": false,
                "addDefaultRules": true,
                "rules": [
                {
                    "maxWidth": 400,
                    "maxHeight": 400,
                    "minWidth":200,
                    "overrides": {
                        "marginLeft":-30,
                        "legend": {
                            "enabled": true,
                            "marginRight":100,
                            "marginLeft":10
                        }

                    }
                },
                {
                    "maxWidth": 1440,
                    "minHeight": 900,
                    "overrides": {
                        "legend": {
                            "enabled": true,
                            // "marginRight":150,
                            // "marginLeft":20,
                        }
                    }
                }
            ]
        }

    });
        addLabel(charts);
    }




</script>
@endpush