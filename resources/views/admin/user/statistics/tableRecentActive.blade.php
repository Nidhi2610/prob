@extends('layouts.app')
<style>
    h3.title-chart {
        text-align: center;
        font-size: 24px;
    }
    @media screen and (max-width: 360px){
        table.mostRecent>tbody>tr>td{
            width: 33.33%;
            word-wrap: break-word;
        }

    }
</style>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12  col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                            <h1>
                                {{ __('probtp.statistiques') }}
                            </h1>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php $userTypes = Config::get('constant.user_types');?>
                        <h3 class="title-chart"> {{ __('probtp.mostRecentLoginuser') }}</h3>
                        <form class="form-inline" id="filterForm"
                              method="POST"  align="center">
                            <span class="title-period"> {{ __('probtp.select Period') }}: </span> <div class="form-group mb-2">
                                <label for="startDate" class="sr-only control-label">  {{ __('probtp.Start Date') }}:</label>
                                <input type="text" id="startDate" name="startDate" class="form-control" placeholder="{{ __('probtp.Start Date') }}"/>
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="endDate" class="sr-only control-label">{{ __('probtp.End Date') }}: </label>
                                <input type="text" class="form-control"  name="endDate" id="endDate" placeholder="{{ __('probtp.End Date') }}"/>
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                                <select class="form-control " name="module_type" id="module_type">
                                    <option value="">{{ __('probtp.type') }}</option>
                                    <option value="1">{{ $userTypes[1] }}</option>
                                    <option value="2">{{ $userTypes[2] }}</option>
                                    <option value="3">{{ $userTypes[3] }}</option>
                                    <option value="4">{{ $userTypes[4] }}</option>
                                    <option value="5">{{ $userTypes[5] }}</option>
                                </select>
                            </div>
                            <button type="submit" id="submitDate" class="btn btn-primary mb-2">{{ __('probtp.Show Chart') }}</button>
                        </form>

                            @include('admin.user.statistics.recentActiveDatatable',["mostRecent"=>$mostRecent])

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
{{--@include('admin.user.statistics.tableRecentActive',["mostRecent"=>$mostRecent])--}}

@include('layouts.stack',['extra'=>''])

@push('script')
<script type="text/javascript">
  //  dataTableList();
// on response of datatable redraw/reload the datatable  insret the tr/td it will override the old values


  var Sdate =  $('#startDate').datepicker({ dateFormat: 'dd-mm-yy' });
  var Edate =  $('#endDate').datepicker({ dateFormat: 'dd-mm-yy' });
$(document).on('submit','#filterForm',function(e){
    var form_data = new FormData(this);

    event.preventDefault();
    event.stopPropagation();
    $.ajax({
        cache: false,
        url: window.Probtp.baseUrl +'user/getRecentlyLogin',
        type: "POST",
        data: form_data,
        contentType: false,
        headers: {
            'X-CSRF-TOKEN': window.Probtp.csrfToken
        },
        processData: false,
        success: function (response) {
            $('#tableData').html(response);
        },
        error: function (xhr) {
            alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
        }
    });
});

function dataTableList() {
    if($('#data-table4').length){
        dataTable4 =   $('#data-table4').DataTable(
            {
                "pageLength": 100,
                "order": [],
                "language": {
                    "paginate": {
                        "previous": "Précédente",
                        "next": "Suivante",
                        "emptyTable": " Désolé, aucun résultat n'a pas été trouvé !"
                    },
                    'lengthMenu' : '_MENU_ par page',
                    "info": "_START_ à _END_ sur _TOTAL_ réponses",
                    'search' : 'Rechercher : ',
                },
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false
                } ]}
        );

    }
}

//$('#submitDate').click(function () {
//    $('#module_type').val();
//    $('#startDate').val();
//    $('#endDate').val();
//
//
//});

$(document).ready(function() {
    $('#startDate , #endDate').change( function() {  } );
    // Add event listeners to the two range filtering inputs

} );
</script>
@endpush