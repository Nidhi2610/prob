@extends('layouts.app')

@section('content')
    @inject('userObj', 'App\Models\User')
    <form class="form-horizontal" role="form" id="deleteForm" method="POST"  action="{{ route('userPermenantDestroy') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="delete_id"  />
    </form>

    <form class="form-horizontal" role="form" id="restoreForm" method="POST" action="{{ route('userRestore') }}">
        {!! csrf_field() !!}
        <input type="hidden" value="" name="id" id="restore_id"/>
    </form>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="title-heading">
                        <h1>{{ __('probtp.trash_user_list')}}</h1>
                        </div>

                    </div>
                    <div class="panel-body">
                        <table class="table table-striped  table-bordered" cellspacing="0" id="data-table">
                            <thead>
                            <tr>
                                <th style="width:20px" class="no-sort"><input type="checkbox" id="checkAll"  class="checkAll" /></th>
                                <th>{{ __('probtp.user_name') }}</th>
                                <th>{{ __('probtp.user')." ".__('probtp.type') }}</th>
                                <th class="no-sort">{{ __('probtp.email') }}</th>
                                <th style="width:35px" class="no-sort">{{ __('probtp.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $userTypes = Config::get('constant.user_types'); ?>
                            @foreach($data as $user)
                                <tr>
                                    <td><input type="checkbox" name="member_id[]" value="{{$user->id}}"  class="checkAllChild" /></td>

                                    <td>{{$user->name}}</td>
                                    <td>{{$userTypes[$user->user_type]}}</td>
                                    <td>{{$user->email}}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span> </button>
                                            <ul class="dropdown-menu menu-lcons dropdown-menu-right" aria-labelledby="dropdownMenu1" style="min-width:auto;">
                                                <li> <a href="javascript:void(0)" id="{{$user->id}}"   data-id ="{{$user->id}}" class="member-delete delete"><i class="fa fa-trash fa-lg" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.delete') }}</a></li>
                                                <li> <a href="javascript:void(0)" id="{{$user->id}}"   data-id ="{{$user->id}}" class=" restore"><i class="fa fa-btn fa-undo" style="padding-right: 10px;"  aria-hidden="true"></i>{{ __('probtp.restore') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('layouts.stack',['extra'=>''])
