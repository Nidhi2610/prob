<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*if (env('APP_ENV') === 'local') {
    URL::forceSchema('https');
}*/
Auth::routes();
Route::get('/', 'Auth\LoginController@welcome')->name('welcome');
//For 404 error :Not Found




Route::get('/logout', 'Auth\LoginController@logout')->name('probtp_logout');

// For Front Page Route

/*Route::get('/index', 'IndexController@index')->name('Index');
Route::get('category/detail/{id}', 'IndexController@detail')->name('Detail');
Route::post('/store_comment', 'IndexController@storeComment')->name('commentStore');
Route::post('/form_data_update', 'IndexController@formDataUpdate')->name('formDataUpdate');
Route::post('/quiz_data_update', 'IndexController@quizDataUpdate')->name('quizDataUpdate');
Route::get('/category_module/{filename}','IndexController@download');*/
Route::get('/home', 'HomeController@index')->name('Home');
Route::get('/index', 'IndexController@index')->name('Index');
Route::get('/userlist', 'IndexController@userlist')->name('Userlist');
Route::post('/collaborateur_user', 'IndexController@getCollaborateurAssigendUser');
Route::post('/insert_collaborateur_assigned_category', 'IndexController@insertcollaborateurAssignedCategory')->name('collaborateurAssignedCategory');
Route::any('/details/{cat_id}/{id}', 'IndexController@detail')->name('detailFrontPage');
Route::get('/category_module/{filename}','IndexController@download');
Route::post('/store_comment', 'IndexController@storeComment')->name('commentStore');
Route::post('/form_data_update', 'IndexController@formDataUpdate')->name('formDataUpdate');
Route::post('/ ', 'IndexController@quizDataUpdate')->name('quizDataUpdate');
Route::post('/bookmark', 'IndexController@bookmark')->name('bookmark');
Route::post('/remove_bookmark', 'IndexController@removeBookmark')->name('removeBookmark');
Route::get('/search/{search_keyword}', 'IndexController@searchResult')->name('searchQuery');
Route::post('/add_module_count','IndexController@addUserModuleComplete')->name('addModuleCount');
Route::post('/like_comments','IndexController@CommentLikeToggle');
Route::post('/like_category', 'IndexController@likeCategory')->name('likeCategory');
Route::post('/count_accessby_user', 'IndexController@addCounterOnClick');
Route::get('/user_entreprise', 'IndexController@userEntreprise')->name('UserEntreprise');

// Category Route
Route::group(['prefix' => 'category'], function () {

    Route::get('/{type?}', 'Admin\CategoryController@index')->name('categoryIndex');
    Route::get('/create/{type?}', 'Admin\CategoryController@create')->name('categoryAdd');
    Route::post('/store', 'Admin\CategoryController@store')->name('categoryStore');
    Route::get('/edit/{id}/{type?}', 'Admin\CategoryController@edit')->name('categoryEdit');
    Route::post('/update', 'Admin\CategoryController@update')->name('categoryUpdate');
    Route::post('/destroy', 'Admin\CategoryController@destroy')->name('categoryDestroy');
	Route::post('/updatecategory', 'Admin\CategoryController@updatecategorytree')->name('updateCategory');
    Route::get('/replicate/{id}', 'Admin\CategoryController@replicate')->name('categoryReplicate');
    Route::post('/get_entreprise/{type?}', 'Admin\CategoryController@getEntreprise')->name('getEntrepriseCategory');
    Route::post('/insertentreprisecategory', 'Admin\CategoryController@entrepriseCategory')->name('entrepriseCategory');
    Route::get('/responsablecategory', 'Admin\CategoryController@getResponsableCategory')->name('responsableCategoryIndex');
    Route::post('/insert_collaborateur_category', 'Admin\CategoryController@insertcollaborateurcategory')->name('collaborateurCategory');
    Route::post('/get_user', 'Admin\CategoryController@getCollaborateurUser')->name('categoryCollaborateurUser');
    Route::get('/get_index_search/{type?}', 'Admin\CategoryController@getIndexSearch')->name('getIndexSearch');

// Soft Delete

    Route::get('/gettrashdata/{type?}','Admin\CategoryController@gettrashdata')->name('categoryTrashIndex');
    Route::post('/restorefrombin', 'Admin\CategoryController@restorefrombin')->name('categoryRestore');
    Route::post('/destroypermenant', 'Admin\CategoryController@destroypermenant')->name('categoryPermenantDestroy');
    Route::post('/updatemoduletree', 'Admin\CategoryController@updatemoduletree')->name('categoryUpdateTree');

// For Preview Detail page

        Route::get('/detail_page/{cat_id}/{id}', 'Admin\CategoryController@detail')->name('detailPage');
//        Route::get('/detail_page/{id}', 'Admin\CategoryController@detail')->name('detailPage');


    });

Route::get('/404','IndexController@page404')->name('404');

// Resource Route
Route::group(['prefix' => 'resource'], function () {

    Route::get('/', 'Admin\ResourceController@index')->name('resourceIndex');
    Route::get('/create', 'Admin\ResourceController@create')->name('resourceAdd');
    Route::post('/store', 'Admin\ResourceController@store')->name('resourceStore');
    Route::get('/edit/{id}', 'Admin\ResourceController@edit')->name('resourceEdit');
    Route::post('/update', 'Admin\ResourceController@update')->name('resourceUpdate');
    Route::post('/destroy', 'Admin\ResourceController@destroy')->name('resourceDestroy');
    Route::post('/updateresource', 'Admin\ResourceController@updateresourcetree')->name('updateResource');
    Route::get('/replicate/{id}', 'Admin\ResourceController@replicate')->name('resourceReplicate');
    Route::post('/get_entreprise', 'Admin\ResourceController@getEntreprise')->name('getEntrepriseResource');
    Route::post('/insertentrepriseresource', 'Admin\ResourceController@entrepriseResource')->name('entrepriseResource');
    Route::get('/responsableresource', 'Admin\ResourceController@getResponsableResource')->name('responsableResourceIndex');
    Route::post('/insert_collaborateur_resource', 'Admin\ResourceController@insertcollaborateurresource')->name('collaborateurResource');
    Route::post('/get_user', 'Admin\ResourceController@getCollaborateurUser')->name('resourceCollaborateurUser');
    Route::get('/get_index_search', 'Admin\ResourceController@getIndexSearch')->name('getResourceIndexSearch');

// Soft Delete

    Route::get('/gettrashdata','Admin\ResourceController@gettrashdata')->name('resourceTrashIndex');
    Route::post('/restorefrombin', 'Admin\ResourceController@restorefrombin')->name('resourceRestore');
    Route::post('/destroypermenant', 'Admin\ResourceController@destroypermenant')->name('resourcePermenantDestroy');
    Route::post('/updatemoduletree', 'Admin\ResourceController@updatemoduletree')->name('resourceUpdateTree');

// For Preview Detail page

    Route::get('/detail_page/{id}', 'Admin\CategoryController@detail')->name('detailPage');


});


// Entreprise Route
Route::group(['prefix' => 'entreprise'], function () {

    Route::get('/', 'Admin\EntrepriseController@index')->name('entrepriseIndex');
    Route::get('/create', 'Admin\EntrepriseController@create')->name('entrepriseAdd');
    Route::post('/store', 'Admin\EntrepriseController@store')->name('entrepriseStore');
    Route::get('/edit/{id}', 'Admin\EntrepriseController@edit')->name('entrepriseEdit');
    Route::post('/update', 'Admin\EntrepriseController@update')->name('entrepriseUpdate');
    Route::post('/destroy', 'Admin\EntrepriseController@destroy')->name('entrepriseDestroy');
    Route::post('/get_user_of_enterprise', 'Admin\EntrepriseController@getUserOfEnterprise')->name('getUserOfEnterprise');
    Route::post('/add_enterprise_to_user', 'Admin\EntrepriseController@addEnterpriseToUser')->name('addEnterpriseToUser');

    // Soft Delete

    Route::get('/gettrashdata','Admin\EntrepriseController@gettrashdata')->name('entrepriseTrashIndex');
    Route::post('/restorefrombin', 'Admin\EntrepriseController@restorefrombin')->name('entrepriseRestore');
    Route::post('/destroypermenant', 'Admin\EntrepriseController@destroypermenant')->name('entreprisePermenantDestroy');

});


// User Route
Route::group(['prefix' => 'user'], function () {

    Route::get('/', 'Admin\UserController@index')->name('userIndex');
    Route::get('/create', 'Admin\UserController@create')->name('userAdd');
    Route::post('/store', 'Admin\UserController@store')->name('userStore');
    Route::get('/edit/{id}', 'Admin\UserController@edit')->name('userEdit');
    Route::post('/update', 'Admin\UserController@update')->name('userUpdate');
    Route::post('/destroy', 'Admin\UserController@destroy')->name('userDestroy');
	Route::get('/myprofile', 'Admin\UserController@myprofile')->name('userProfile');
	Route::post('/updateuser', 'Admin\UserController@updateuser')->name('userAccountUpdate');
	Route::get('/changepassword', 'Admin\UserController@changepassword')->name('changePassword');
	Route::post('/updatepassword', 'Admin\UserController@updatepassword')->name('updatePassword');

    //for statistiques in admin
    Route::get('/statistiques', 'Admin\StatisticsController@getStatistics')->name('getStatistics');
    Route::get('/pieChart', 'Admin\StatisticsController@getPieChart')->name('getPieChart');
    Route::get('/getEntrepriseChart', 'Admin\StatisticsController@getEntrepriseData')->name('getEntreprise');
    Route::get('/gettimespentByuser', 'Admin\StatisticsController@mostTimeSpentonSystem')->name('gettimespentByuser');
    Route::get('/getCommnetsbyuser', 'Admin\StatisticsController@getUserCommentData')->name('getUserCommentData');
    Route::get('/getmostcompletedModule', 'Admin\StatisticsController@getMostcompletedModule')->name('getMostcompletedModule');
    Route::get('/getmostAccessedModules', 'Admin\StatisticsController@getMostAccessedModules')->name('getMostAccessedModules');
    Route::get('/getcommentsbyCategoryData', 'Admin\StatisticsController@getcommentsbyCategory')->name('getCommentsbyCategoryData');
    Route::post('/getRecentlyLogin', 'Admin\StatisticsController@getRecentlyActiveUserFilter')->name('getRecentlyActiveUserFilter');
    Route::get('/getRecentlyActiveUsers', 'Admin\StatisticsController@getRecentlyActiveUserData')->name('getRecentlyActiveUser');
    Route::post('/barChart', 'Admin\StatisticsController@getbarChartData')->name('getBarChart');
    Route::post('/barChartforMostCompleted', 'Admin\StatisticsController@getbarChartforMostCompletedData')->name('chartforMostCompleted');
    Route::post('/barChartforMostAccessed', 'Admin\StatisticsController@getbarChartforMostAccessedData')->name('chartforMostAccessed');
    Route::post('/barChartforCommentsByDate', 'Admin\StatisticsController@getbarChartforCommentsByDate')->name('chartforComments');
    Route::post('/barChartforCommentsByType', 'Admin\StatisticsController@getbarChartforCommentsByType')->name('chartforCommentsCate');
    Route::post('/getRecentlyActiveUserByDate', 'Admin\StatisticsController@getRecentlyActiveUserByDate')->name('userFilter');
    Route::post('/getEntreprisDataByDate', 'Admin\StatisticsController@getEntreprisDataByDate')->name('getEntreprisDataByDate');
    Route::post('/getCommnetsbyuserDataByDate', 'Admin\StatisticsController@getUserCommentDataByDate')->name('getUserCommentDataByDate');
    Route::post('/gettimespentByuserDataByDate', 'Admin\StatisticsController@mostTimeSpentonSystemByDate')->name('gettimespentByuserByDate');
    Route::post('/getpieChartDataByDate', 'Admin\StatisticsController@getPieChartByDate')->name('getPieChartByDate');

//test
        Route::get('/testsssion', 'Admin\UserController@testsession')->name('testSession');


    // Soft Delete

    Route::get('/gettrashdata','Admin\UserController@gettrashdata')->name('userTrashIndex');
    Route::post('/restorefrombin', 'Admin\UserController@restorefrombin')->name('userRestore');
    Route::post('/destroypermenant', 'Admin\UserController@destroypermenant')->name('userPermenantDestroy');

    // User COmmunity
    Route::get('/community','Admin\UserController@getCommnunity')->name('getCommunity');

    //  Responsable RH

    Route::group(['prefix'=>'responsable'] , function(){
        Route::get('/create/{id}', 'Admin\AddResponsableController@create')->name('user_Rh_Add');
        Route::post('/store', 'Admin\AddResponsableController@store')->name('userRHStore');
        Route::get('/edit/{id}', 'Admin\AddResponsableController@edit')->name('userRHEdit');
        Route::post('/update', 'Admin\AddResponsableController@update')->name('userRHUpdate');
        Route::post('/destroy','Admin\AddResponsableController@destroy')->name('userRHDelete');
    });

});

// Module
Route::group (['prefix' => 'module'], function (){

	Route::get('/{type?}','Admin\ModuleController@index')->name('moduleIndex');
	Route::get('/create/{id?}/{type?}', 'Admin\ModuleController@create')->name('moduleAdd');
    Route::post('/store', 'Admin\ModuleController@store')->name('moduleStore');
    Route::get('/edit/{id}/{type?}', 'Admin\ModuleController@edit')->name('moduleEdit');
    Route::post('/update', 'Admin\ModuleController@update')->name('moduleUpdate');
    Route::post('/destroy', 'Admin\ModuleController@destroy')->name('moduleDestroy');
	Route::get('/{filename}','Admin\ModuleController@download')->name('moduleDownload');
	Route::post('/insertresponsabel', 'Admin\ResponsableController@insertresponsabel')->name('responsableModule');
	Route::post('/insertcollaborateur', 'Admin\CollaborateurController@insertcollaborateur')->name('collaborateurModule');
	Route::get('/detail/{id}','Admin\ModuleController@detail')->name('moduleDetail');
	Route::post('/getresponsable', 'Admin\ResponsableController@getresponsable')->name('getResponsableModule');
	Route::post('/getcollaborateur', 'Admin\CollaborateurController@getcollaborateur')->name('getCollaborateurModule');
    Route::get('/replicate/{id}/{type?}', 'Admin\ModuleController@replicate')->name('moduleReplicate');
    Route::post('/replicateFromCategory/', 'Admin\ModuleController@replicateFromCategory')->name('moduleReplicateFromCategory');
    Route::post('/replicateFromResource/', 'Admin\ModuleController@replicateFromResource')->name('moduleReplicateFromResource');
    Route::post('/getunassignedmodule', 'Admin\ModuleController@getunassignedmodule')->name('moduleByResource');
    Route::post('/getassignedmodule', 'Admin\ModuleController@getassignedmodule')->name('assignedmoduleByResource');
    Route::post('/getunassignedmoduleresource', 'Admin\ModuleController@getunassignedmoduleresource')->name('moduleByResource');
    Route::post('/getassignedmoduleresource', 'Admin\ModuleController@getassignedmoduleresource')->name('assignedmoduleByResource');
    Route::post('/insertModuleCategory', 'Admin\ModuleController@insertModuleCategory')->name('moduleStoreByCategory');
    Route::post('/insertModuleResource', 'Admin\ModuleController@insertModuleResource')->name('moduleStoreByResource');
    Route::post('/deleteModuleCategory', 'Admin\ModuleController@deleteModuleCategory')->name('moduleDeleteByCategory');
    Route::post('/deleteModuleResource', 'Admin\ModuleController@deleteModuleResource')->name('moduleDeleteByResource');
    Route::post('/get_module_categorylist', 'Admin\ModuleController@getModuleCateogry')->name('moduleCategoryList');
    Route::post('/get_module_resourcelist', 'Admin\ModuleController@getModuleResource')->name('moduleResourceList');
    Route::post('/delete_file', 'Admin\ModuleController@deleteFile')->name('moduleDeleteFile');

    // For preview of the Moduel
    Route::get('/preview/{id}', 'Admin\ModuleController@preview')->name('previewPage');


});
// For Soft Delete
Route::get('/gettrashdata/{type?}','Admin\ModuleController@gettrashdata')->name('moduleTrashIndex');
Route::post('/restorefrombin', 'Admin\ModuleController@restorefrombin')->name('moduleRestore');
Route::post('/destroypermenant', 'Admin\ModuleController@destroypermenant')->name('modulePermenantDestroy');

// Form
Route::group (['prefix' => 'form'], function (){

    Route::get('/','Admin\FormController@index')->name('formIndex');
    Route::get('/create', 'Admin\FormController@create')->name('formAdd');
    Route::post('/store', 'Admin\FormController@store')->name('formStore');
    Route::get('/edit/{id}', 'Admin\FormController@edit')->name('formEdit');
    Route::post('/update', 'Admin\FormController@update')->name('formUpdate');
    Route::post('/destroy', 'Admin\FormController@destroy')->name('formDestroy');
    Route::get('/gettrashdata','Admin\FormController@gettrashdata')->name('formTrashIndex');
    Route::post('/restorefrombin', 'Admin\FormController@restorefrombin')->name('formRestore');
    Route::post('/destroypermenant', 'Admin\FormController@destroypermenant')->name('formPermenantDestroy');
    Route::post('/replicate', 'Admin\FormController@replicate')->name('formReplicate');


});

// Email Template

Route::group (['prefix' => 'email'], function (){

    Route::get('/','Admin\EmailTemplateController@index')->name('emailTemplateIndex');
    Route::get('/create', 'Admin\EmailTemplateController@create')->name('emailTemplateAdd');
    Route::post('/store', 'Admin\EmailTemplateController@store')->name('emailTemplateStore');
    Route::get('/edit/{id}', 'Admin\EmailTemplateController@edit')->name('emailTemplateEdit');
    Route::post('/update', 'Admin\EmailTemplateController@update')->name('emailTemplateUpdate');
    Route::post('/destroy', 'Admin\EmailTemplateController@destroy')->name('emailTemplateDestroy');

});

// Quiz
Route::group (['prefix' => 'quiz'], function (){

    Route::get('/','Admin\QuizController@index')->name('quizIndex');
    Route::get('/create', 'Admin\QuizController@create')->name('quizAdd');
    Route::post('/store', 'Admin\QuizController@store')->name('quizStore');
    Route::get('/edit/{id}', 'Admin\QuizController@edit')->name('quizEdit');
    Route::post('/update', 'Admin\QuizController@update')->name('quizUpdate');
    Route::post('/destroy', 'Admin\QuizController@destroy')->name('quizDestroy');
    Route::get('/gettrashdata','Admin\QuizController@gettrashdata')->name('quizTrashIndex');
    Route::post('/restorefrombin', 'Admin\QuizController@restorefrombin')->name('quizRestore');
    Route::post('/destroypermenant', 'Admin\QuizController@destroypermenant')->name('quizPermenantDestroy');
    Route::post('/replicate', 'Admin\QuizController@replicate')->name('quizReplicate');

});

//Update Private Content

Route::get('/get_content','Admin\ContentController@index')->name('getContent');
Route::post('/update', 'Admin\ContentController@update')->name('contentUpdate');

// Comment Managment

Route::group (['prefix' => 'comment'], function (){

    Route::get('/','Admin\CommentsController@index')->name('commentIndex');
    Route::post('/destroy', 'Admin\CommentsController@destroy')->name('commentDestroy');
    Route::get('/gettrashdata','Admin\CommentsController@gettrashdata')->name('commentTrashIndex');
    Route::post('/restorefrombin', 'Admin\CommentsController@restorefrombin')->name('commentRestore');
    Route::post('/destroypermenant', 'Admin\CommentsController@destroypermenant')->name('commentPermenantDestroy');
    Route::post('/getcommentsbyid', 'Admin\CommentsController@getCommentsByCategoryId')->name('getCommentById');
});

